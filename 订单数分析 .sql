
-- 1) 订单数

--- 目标

--- 按天的订单
use tmp_innofin;
create table if not exists order_count_daily (
	d string,
	order_count int
);

insert overwrite table tmp_innofin.order_count_daily
select to_date(creationdate) as d, count(1) from ods_innofin.fact_loan_order 
where creationdate >= '2017-01-01'
group by to_date(creationdate) order by d limit 400; 

drop table tmp_innofin.order_count_target;
use tmp_innofin;
create table if not exists order_count_target (
	d string,
	days int,
	trend_count int,
	target_count int
) ROW FORMAT DELIMITED
	FIELDS TERMINATED BY ','
    STORED AS TEXTFILE;

-- load data from file
-- art report
select d as `日期`,trend_count as `趋势线`,target_count as `目标(年底7万/日)`,actual_count as `实际订单量` from 
(select a.*,b.actual_count from tmp_innofin.order_count_target a
inner join (select to_date(creationdate) as d, count(1) as actual_count from ods_innofin.fact_loan_order 
		where creationdate >= #ONEMONTH#
		group by to_date(creationdate)) b 
on a.d = b.d 
where a.d < #TODAY# and a.d >=#ONEMONTH#
) c 
order by `日期` limit 30


-- 2) 已激活用户数 -- 天
--当天激活（update_time非精确，todo）
use tmp_innofin;
create table if not exists active_user_daily (
	d string,
	user_count int
);

-- 激活
insert overwrite table tmp_innofin.active_user_daily
select to_date(a.update_time),count(1) from ods_innofin.user_contract a
	where contract_status = 1 group by to_date(a.update_time);

-- 3) 各变量
use tmp_innofin;
create table if not exists user_order_count_daily (
	d string,
	days string,	
	order_count int,
	active_count_in_30 int comment '30天内累计激活数',
	active_count_31_60 int comment '31-60天内累计激活数', 
	active_count_61_90 int comment '61-90天内累计激活数',
	active_count_before_90 int comment '90天以上累计激活数'
);

insert overwrite table tmp_innofin.user_order_count_daily
select a.*,b.active_count_in_30,b.active_count_31_60,b.active_count_61_90,b.active_count_before_90
from tmp_innofin.order_count_daily a 
inner join (select d,
			SUM(user_count) OVER(PARTITION BY 1 ORDER BY d ROWS BETWEEN 30 PRECEDING AND CURRENT ROW) as active_count_in_30, 
			SUM(user_count) OVER(PARTITION BY 1 ORDER BY d ROWS BETWEEN 60 PRECEDING AND 31 PRECEDING) as active_count_31_60, 
			SUM(user_count) OVER(PARTITION BY 1 ORDER BY d ROWS BETWEEN 90 PRECEDING AND 61 PRECEDING) as active_count_61_90,
			SUM(user_count) OVER(PARTITION BY 1 ORDER BY d ROWS BETWEEN UNBOUNDED PRECEDING AND 91 PRECEDING) as active_count_before_90
			from tmp_innofin.active_user_daily) b on a.d = b.d
