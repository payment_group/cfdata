--target
use tmp_innofin;
create table if not exists nqh_order_amount_target (
	d string comment '日期',
	target_1 decimal(18,2) comment '每月相同',
	target_2 decimal(18,2) comment '月环比30%')
 ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED AS TEXTFILE
;
-- 实际
use tmp_innofin;
create table if not exists nqh_order_amount_actual(
	d string comment '日期',
	actual_amount decimal(18,2)
);

-- 每日
insert overwrite table tmp_innofin.nqh_order_amount_actual
select to_date(creationdate) as d, sum(totalamount) as actual_amount from ods_innofin.fact_loan_order group by to_date(creationdate);
-- 报告
select a.d as `日期`, a.target_1 as `目标（每月等量）`, a.target_2 as `目标（月环比30%）`, b.actual_amount as `实际订单金额`  
	from  tmp_innofin.nqh_order_amount_target a
	inner join tmp_innofin.nqh_order_amount_actual b
	on a.d = b.d 
	where a.d >='2017-05-01' order by `日期` limit 30
