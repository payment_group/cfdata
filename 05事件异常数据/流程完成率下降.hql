-- 20--
use tmp_innofin;
create table if not exists vqq_tmp_0428_PrePush(
	dt string,
	uid string
);
insert overwrite table vqq_tmp_0428_PrePush
select distinct d,uid from 
dw_mobdb.factmbpageview where d >= '2018-04-20' and query like '%mktype=PrePush%' 
	and pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431','10650001007') 
	
	
use tmp_innofin;
drop table if exists vqq_tmp_0428_PrePush_1;
create table if not exists vqq_tmp_0428_PrePush_1(
	dt string,
	uid string,
	push int,
	apply int,
	apply_button int,
	db_active int
);
insert overwrite table tmp_innofin.vqq_tmp_0428_PrePush_1
select a.d,a.uid,
	if(b.uid is null,0,1),
	1,
	if(c.uid is null,0,1),
	if(d.uid is null,0,1)
from (select * from tmp_innofin.apply_process_detail a where d >= '2018-04-20') a 
	left join tmp_innofin.vqq_tmp_0428_PrePush b on a.d = b.dt and lower(a.uid) = lower(b.uid)
	left join (select * from tmp_innofin.vqq_rpt_active_process_click_uv where dt >= '2018-04-20' and bid in ('101320','101321')) c on a.d = c.dt and lower(a.uid) = lower(c.uid)
	left join (select distinct to_date(request_time) as dt,user_id as uid from ods_innofin.user_activate_req where to_date(request_time) >= '2018-04-20' and req_source <>'PRE_CREDIT_TASK') d
		on a.d = d.dt and lower(a.uid) = lower(d.uid)
;

select dt,push,sum(apply),sum(apply_button),sum(apply_button*db_active) from tmp_innofin.vqq_tmp_0428_PrePush_1 group by dt,push


--- 
-- 4)������
use tmp_innofin;
create table if not exists vqq_temp_user_action_info_index (
	uid	string,
	action_desc string,
	dt string
);
insert overwrite table tmp_innofin.vqq_temp_user_action_info_index
select user_id as uid,action_desc,to_date(create_time) from
(select user_id,get_json_object(action_desc,'$.preCreditStatus') as action_desc,create_time,row_number() over (partition by user_id order by create_time desc) as no 
from ods_innofin.user_action_info 
where to_date(create_time) >= '2018-04-20' and action_type = 'INDEX') a
where a.no = 1;

	
use tmp_innofin;
drop table if exists vqq_tmp_0428_PrePush_2;
create table if not exists vqq_tmp_0428_PrePush_2(
	dt string,
	uid string,
	preCreditStatus string,
	apply int,
	apply_button int,
	db_active int
);
insert overwrite table tmp_innofin.vqq_tmp_0428_PrePush_2
select a.d,a.uid,
	nvl(b.action_desc,''),
	1,
	if(c.uid is null,0,1),
	if(d.uid is null,0,1)
from (select * from tmp_innofin.apply_process_detail a where d >= '2018-04-20') a 
	left join tmp_innofin.vqq_temp_user_action_info_index b on a.d = b.dt and lower(a.uid) = lower(b.uid)
	left join (select * from tmp_innofin.vqq_rpt_active_process_click_uv where dt >= '2018-04-20' and bid in ('101320','101321')) c on a.d = c.dt and lower(a.uid) = lower(c.uid)
	left join (select distinct to_date(request_time) as dt,user_id as uid from ods_innofin.user_activate_req where to_date(request_time) >= '2018-04-20' and req_source <>'PRE_CREDIT_TASK') d
		on a.d = d.dt and lower(a.uid) = lower(d.uid)
;

select dt,preCreditStatus,sum(apply),sum(apply_button),sum(apply_button*db_active) from tmp_innofin.vqq_tmp_0428_PrePush_2 group by dt,preCreditStatus