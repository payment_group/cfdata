use tmp_innofin;
create table vqq_tmp_boa_ubt(
	 uid string,
	 starttime string,
	 prepagecode string,
	 pagecode string,
	 vid string,
	 cityid string,
	 provincename string,
	 cityname string,
	 d string
);

insert overwrite table tmp_innofin.vqq_tmp_boa_ubt
select  uid,starttime , prepagecode , pagecode , vid , cityid , provincename , cityname , d  from 
dw_mobdb.factmbpageview where d >='2017-08-01' and upper(uid) in ('2880983962','M371611021','1185883288','2880983962','M475179211','M137548671',
'1111559178','3003852501','M371611021','M475179211','M137548671','3200622611','13626188881','_WB229720499','M318978045',
'2038058823','M95241101','_WB229720499','M318978045','M95241101');
-- 访问最多页面
select pagecode,pagename,count(1) as c from(
select a.pagecode,pagename from
 tmp_innofin.vqq_tmp_boa_ubt a
 inner join dim_mobdb.dimmbpagecode b on a.pagecode = b.pagecode)
 a group by pagecode,pagename order by c desc limit 300;
 -- 地域
 select distinct provincename,cityname from tmp_innofin.vqq_tmp_boa_ubt;
 