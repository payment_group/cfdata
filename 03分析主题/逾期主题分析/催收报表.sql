select
date(create_time),
count(1) as total_case,
sum(case when a.collection_status=3 then 1 else 0 end),
0,
sum(b.overdue_amt),
sum(case when b.overdue_amt - a.overdue_amt > 0 then b.overdue_amt - a.overdue_amt else 0 end )
from case_info a inner join (select case_no,sum(overdue_amt) as overdue_amt from case_loan_info group by case_no) b on a.case_no = b.case_no where a.company_code = 'CtripInner' group by date(create_time)