select uid
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 100000
) e;