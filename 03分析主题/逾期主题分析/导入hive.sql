-- user_overdue_status
use ods_innofin;
create table user_overdue_status(
	id	int  comment '唯一键', 
	user_id	string comment 'uid', 
	open_id	string comment 'openid', 
	is_overdue	int comment '0=逾期，1=非逾期', 
	overdue_Amt	decimal(19,4) comment '逾期金额', 
	push_time	string comment '推送时间', 
	create_time	string comment '创建时间', 
	update_time	string comment '更新时间' 
) comment '逾期状态表'
partitioned by (
	d string comment '数据时间'
);

################################################################注释代码
:<<EOF
名称 ：user_overdue_status
功能描述 ：从user_overdue_status导入到user_overdue_status
输入表 ：pay_cfb.user_overdue_status
输出表 ：ods_innofin.user_overdue_status
创建人：qq
创建时间：2017-6-27
运行类型 ： 
注意事项 ：
说明 ：
修改历史 ：
EOF
#################################################################注释代码

set -eu
sh datax_run.sh \
  -src mysqldal \
  -srcallinone pay_cfb_ETLZS_SH \
  -srcdb pay_cfb \
  -srctblnames user_overdue_status \
  -querys "select id,user_id,open_id,is_overdue,overdue_Amt,push_time,create_time,update_time from user_overdue_status" \
  -pks id \
  -tar hdfs \
  -tardb tmp_innofin \
  -tartblnames user_overdue_status \
  -loadtype 1

hive -e "
       use ods_innofin;
       insert overwrite table ods_innofin.user_overdue_status partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
       select * from tmp_innofin.user_overdue_status
       where is_overdue = 0;
"