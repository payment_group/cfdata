-- 导出到innofin;

use tmp_innofin;
drop table if exists vqq_tmp_user_profile_2;
create table if not exists vqq_tmp_user_profile_2(
	uid string,
	a_card double comment 'A卡得分',
	user_name	string	comment '用户姓名',
	gender	int	comment '性别',
	birthday	string	comment '出生日期',
	age int,
	native_city	string	comment '籍贯',
	train_power	double	comment '火车票消费能力',
	train_price_sensitive	double	comment '火车票价格敏感度',
	train_area_trend	string	comment '火车票地域倾向暂时做不了,需要从日志表里获取',
	train_price_per_km	double	comment '火车票每公里价格',
	train_cabin_trend	string	comment '火车票席位倾向[-1,硬座:p1、一等座:p2、二等座:p3、商务:p4、硬卧:p5、软卧:p6、高软:p7]',
	flight_power	double	comment '机票消费能力',
	flight_price_sensitive	double	comment '机票价格敏感度',
	flight_area_trend	string	comment '机票地域倾向[小型机场:中型机场:大型机场 = p1 : p2 : p3]',
	flight_price_per_km	double	comment '机票每公里价格',
	flight_cabin_trend	string	comment '机票仓位倾向[普通舱:头等舱:商务舱 = p1 : p2 ： p3]',
	flight_avg_distance	double	comment '机票单次平均距离',
	hotel_power	double	comment '酒店消费能力',
	hotel_price_sensitive	double	comment '酒店价格敏感度',
	hotel_area_trend	string	comment '酒店地域倾向[国外,国内一线,国内二线,国内滨海城市,国内热门旅游城市]',
	hotel_price_per_night	double	comment '酒店消费平均间夜价格',
	hotel_band_trend	string	comment '酒店品牌倾向[-1,经济:p1、舒适:p2、高档:p3、豪华:p4]',
	family_prob	double	comment '家庭账号概率',
	credit_max_amount	double	comment '信用卡最大金额',
	holiday_avg_count	int	comment '度假平均人数',
	work_city	string	comment '工作城市',
	app_freq	double	comment 'app粘度',
	login_freq	double	comment '登录账号频率,半衰期90天',
	total_sensitive	double	comment '价格敏感度',
	whole_power	double	comment '整体消费能力',
	consumer_stability	double	comment '消费稳定性',
	internation_prob	double	comment '国际用户概率',
	pay_type	string	comment '支付倾向,最常使用的前三种支付方式分别占比',
	vacation_day_desire	double	comment '节假日消费意愿',
	holiday_power	double	comment '度假消费能力',
	holiday_sensitive	double	comment '度假价格敏感度',
	holiday_city_tendency	string	comment '度假地域倾向[海外，滨海，人文城市，热门旅游城市]',
	plan_prob	double	comment '出行规划完备度',
	cancel_ratio	double	comment '取消订单占比',
	order_platform	string	comment '下单平台倾向',
	refund_ratio	double	comment '退款订单占比',
	login_period	string	comment '登录账号时间段偏好[早上,中午,下午,晚上,凌晨 = p1,p2,...]',
	rest_regular	double	comment '作息规律度',
	friend_prob	double	comment '好友影响度'
)ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;

use tmp_innofin;
create table if not exists vqq_tmp_a_apply_active (
	uid string,
	a_card double,
	apply int,
	success int
);
use tmp_innofin;

insert overwrite table vqq_tmp_a_apply_active
select a.uid,a.a_card,if(b.user_id is null,0,1),if(c.user_id is null,0,1) from 
vqq_tmp_user_profile_2 a 
left join (select distinct user_id from ods_innofin.user_activate_req where req_source not in ('PRE_CREDIT_TASK','CFB_TASK')) b on lower(a.uid) = lower(b.user_id)
left join (select distinct user_id from ods_innofin.user_contract where contract_status = 1) c on lower(a.uid) = lower(c.user_id)
;

select count(1),sum(apply),sum(apply*success) from tmp_innofin.vqq_tmp_a_apply_active;

		
select count(1) from tmp_innofin.vqq_tmp_a_apply_active where apply = 0 and success =1;
	