desc dw_mobdb.factmbpageview;
use tmp_innofin;
drop table if exists uv_count_1;
create table  if not exists uv_count_1(
	prepagecode string,
	pagecode string,
	uvcount int
);

insert overwrite table tmp_innofin.uv_count_1
select prepagecode,pagecode,count(distinct uid) as uvcount from dw_mobdb.factmbpageview where (d= '${zdt.addDay(-1).format("yyyy-MM-dd")}') group by prepagecode,pagecode;

-- 1000
use tmp_innofin;
create table if not exists uv_count_1000(
	pagecode string,
	uvcount int
);
insert overwrite table tmp_innofin.uv_count_1000
select pagecode,sum(uvcount)  as uvcount  
from tmp_innofin.uv_count_1 group by pagecode order by uvcount desc limit 1000;

--200
use tmp_innofin;
create table if not exists prepagecode_200(
	prepagecode string,
	uvcount int
);
insert overwrite table tmp_innofin.prepagecode_200
select prepagecode,sum(uvcount) as uvcount from (
select b.prepagecode,a.pagecode,b.uvcount from
tmp_innofin.uv_count_1000 a
inner join tmp_innofin.uv_count_1 b on a.pagecode = b.pagecode
) c group by prepagecode order by uvcount desc limit 200;


use tmp_innofin;
create table if not exists uv_count_result(
	prepagecode string,
	prepagename string,
	preuvcount int,
	pagecode string,
	pagename string,
	uvcount int
);
insert overwrite table tmp_innofin.uv_count_result
select a.prepagecode,d.pagename,a.uvcount as preuvcount,b.pagecode,e.pagename,b.uvcount from 
	tmp_innofin.prepagecode_200 a
	inner join tmp_innofin.uv_count_1 b on a.prepagecode = b.prepagecode
	inner join tmp_innofin.uv_count_1000 c on b.pagecode = c.pagecode
	inner join dim_mobdb.dimmbpagecode d on a.prepagecode = d.pagecode
	inner join dim_mobdb.dimmbpagecode e on c.pagecode = e.pagecode	
;
	
-- ����ȡǰ5
select * from(
select *, row_number() over (partition by prepagecode order by preuvcount desc ,uvcount desc ) as od from tmp_innofin.uv_count_result 
) a
where a.od <= 5;