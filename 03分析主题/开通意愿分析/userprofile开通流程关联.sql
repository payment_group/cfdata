-- job=116452 tmp_innofin.apply_user_pv
---------------------------------------------------------------------------------------------------------
-- 计算pv
use tmp_innofin;
create table if not exists apply_user_pv_count (
  uid string,
  pass int,
  trn int,
  flt int,
  htl int
);
insert overwrite table tmp_innofin.apply_user_pv_count
select a.uid, a.pass, nvl(b.trn,0), nvl(d.flt,0),nvl(c.htl,0)
	from (select uid, if(setpass+cardguide*card_number+active > 0 ,1,0) as pass from tmp_innofin.wqm_process_basic01 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}') a 
	left join (select uid,count(1) as trn from tmp_innofin.apply_user_pv where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and pagename like '%火车%' group by uid) b on a.uid = b.uid
	left join (select uid,count(1) as htl from tmp_innofin.apply_user_pv where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and pagename like '%酒店%' group by uid) c on a.uid = c.uid
	left join (select uid,count(1) as flt from tmp_innofin.apply_user_pv where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and pagename like '%机票%' group by uid) d on a.uid = d.uid
;


-- UserOS（处理成iso, andrion,null三类）、gender、country, province, city,  isorderoverseas
use tmp_innofin;
create table if not exists apply_user_profile (
  uid string,
  os string,
  gender string,
  country string,
  province string,
  city string,
  isorderoverseas string
);

insert overwrite table tmp_innofin.apply_user_profile
select a.uid,
   case when useros like 'Android%' then 'Android' when useros like 'i%' then 'IOS' else null end,
   case when gender in ('男','女') then gender else null end,
   country,province,city,isorderoverseas
	from (select uid from tmp_innofin.wqm_process_basic01 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}') a 
	left join tmp_innofin.user_profile_base b on lower(a.uid) = lower(b.uid);
	
select a.uid,trn,flt,htl,os,gender,country,province,city,isorderoverseas,b.pass from tmp_innofin.apply_user_profile a
inner join tmp_innofin.apply_user_pv_count b 
on a.uid = b.uid where a.uid is not null and a.uid <> '';