desc dw_mobdb.factmbpageview;
use tmp_innofin;
drop table if exists prefix_model_uv_detail_7;
create table  if not exists prefix_model_uv_detail_7(
	uid string,
	pagecode string,
	starttime string
);

insert overwrite table tmp_innofin.prefix_model_uv_detail_7
select a.uid,c.pagecode,c.starttime from tmp_innofin.cfbdb_vqq_user_prefix_model a 
left join (select user_id from ods_innofin.user_contract) b on lower(a.uid) = lower(b.user_id)
inner join (select uid,pagecode,starttime from dw_mobdb.factmbpageview where (d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}')) c on lower(a.uid) = lower(c.uid)
where b.user_id is null; 

	
-- ȡǰ1000
select a.pagecode,b.pagename,a.uv from
(select pagecode,count(distinct uid) as uv from tmp_innofin.prefix_model_uv_detail_7 group by pagecode order by desc uv limit 1000) a 
inner join dim_mobdb.dimmbpagecode b on a.pagecode = b.pagecode;
