use tmp_innofin;
drop table if exists apply_user_pv;
create table apply_user_pv(
  uid string comment 'uid',
  pagecode string comment 'pagecode',
  starttime string comment 'starttime',
  pagename string comment 'pagename',
  category string comment 'category',
  categoryname string comment 'categoryname')
comment 'apply_user_pv'
PARTITIONED BY(d STRING COMMENT 'date');

insert overwrite table tmp_innofin.apply_user_pv partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	a.uid,
	b.pagecode,
	b.starttime,
	c.pagename,
	c.category,
	c.categoryname 
  from tmp_innofin.wqm_process_basic01 a
  inner join (select uid,pagecode,starttime from dw_mobdb.factmbpageview
  	where d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' and d < '${zdt.addDay(-1).format("yyyy-MM-dd")}') b
  on lower(trim(a.uid)) = lower(trim(b.uid))
  inner join dim_mobdb.dimmbpagecode c on b.pagecode = c.pagecode
  where a.uid <> '' and  a.apply = 1 and a.d = '${zdt.addDay(-1).format("yyyy-MM-dd")}';
---------------------------------------------------------------------------------------------------------
-- 
-- ���ݵ���
use tmp_innofin;
create table if not exists apply_user_pv_training (
  uid string,
  pass int,
  category string,
  pvcount int
)PARTITIONED BY(d STRING COMMENT 'date');

insert overwrite table tmp_innofin.apply_user_pv_training partition (d='2017-07-12')
select a.uid, a.pass, b.category, pvcount 
	from (select uid, if(setpass+cardguide*card_number+active > 0 ,1,0) as pass from tmp_innofin.wqm_process_basic01 where d='2017-07-12') a 
	left join (select uid,category, count(1) as pvcount from tmp_innofin.apply_user_pv where d='2017-07-12' group by uid,category) b
	on a.uid = b.uid;
	
