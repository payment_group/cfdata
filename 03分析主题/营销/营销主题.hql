use tmp_innofin;
create table if not exists nqh_mkt_rpt_event(
	scene_name string,
	scene_code string,
	mktype string,
	min_date string,
	max_date string
);
-- 
insert overwrite table nqh_mkt_rpt_event values
('母亲节拉新','Pyou180511c1','Pyou180511c1','2018-05-09','2018-05-11'),
('开通流失挽回-1首页流失','Pyou180509c1','push0508','2018-05-08','2018-05-31'),
('开通流失挽回-2绑卡流失','Pyou180510c1','push0508','2018-05-08','2018-05-31'),
('开通流失挽回-3信息核实流失','Pyou180508c1','push0508','2018-05-08','2018-05-31'),

('开通流失挽回-1首页流失','Dfig509a','Dfig509a','2018-05-08','2018-05-31'),
('开通流失挽回-2绑卡流失','Dfig509b','Dfig509b','2018-05-08','2018-05-31'),
('开通流失挽回-3信息核实流失','Dfig509c','Dfig509c','2018-05-08','2018-05-31'),
('预授信通过短信','Pfig180510c1','Pfig180510c1','2018-05-07','2018-05-31'),
('母亲节拉新','Pfig180509c1','Pfig180509c1','2018-05-09','2018-05-11'),

('机票弹窗营销活动','Dfig509d','AppPush_FlightPop','2018-05-14','2018-06-30'),

('目标营销库全量拉新','Mfif180507f1','msg0507','2018-05-07','2018-05-31'),
('目标营销库全量拉新','Mfif180518f1','msg0518','2018-05-07','2018-05-31'),
('目标营销库全量拉新','Mfif180525f1','msg0525','2018-05-07','2018-05-31'),
('目标营销库全量拉新','Mfif180531f1','msg0531','2018-05-07','2018-05-31'),

('儿童节-门票营销活动','Dfig525a','children_Dfig525a','2018-05-07','2018-06-02'),
('儿童节-门票营销活动','Dfig525b','children_Dfig525b','2018-05-07','2018-06-02'),
('儿童节-门票营销活动','Dfig525c','children_Dfig525c','2018-05-07','2018-06-02'),

('儿童节-门票营销活动-banner','','children_Cbanner','2018-05-07','2018-06-02'),
('儿童节-门票营销活动-banner','','children_Tbanner','2018-05-07','2018-06-02')
;

use tmp_innofin;
create table if not exists nqh_mkt_user_action_info(
	dt string,
	uid string,
	mktype string,
	activate int,
	activate_success int
);
insert overwrite table tmp_innofin.nqh_mkt_user_action_info
select 
	distinct to_date(create_time) as dt,
	b.user_id,
	b.mktype,
	if(c.osid is null, 0,1),
	if(c.action_desc = 'CREDIT_SUCCESS',1,0)
 from 
 	(select user_id,osid,get_json_object(action_desc,'$.mktype') as mktype,create_time from ods_innofin.user_action_info where create_time >= '2018-05-01' and action_type = 'INDEX') b
	left join (select osid, get_json_object(action_desc,'$.creditStatus') as action_desc from ods_innofin.user_action_info where create_time >= '2018-05-01' and action_type = 'ACTIVATE') c on b.osid = c.osid;

-- 6)总表
use tmp_innofin;
create table if not exists nqh_mkt_rpt_event_details (
	scene_name string,
	mktype string,

	dt string,
	uid string,

	enter_ctrip int,
	enter_realname int,
	enter_wallet int,
	enter_more_func int,
	enter_others_channel int,
	
	page_apply	int,	
	page_cardguide	int,	
	page_card_number	int,	
	page_card	int,	
	page_setpass	int,	
	page_active	int,
	page_finish	int,	
	page_success	int,
	
	button_apply int,
	button_contract int,
	button_active int,

	db_active int,
	db_success int,	

	log_creditProcess int,
	log_existPwd int,
	log_hasMobile int,
	log_preCreditStatus int,
	log_realName int,
	
	activate int,
	activate_success int
);
insert overwrite table tmp_innofin.nqh_mkt_rpt_event_details
select c.scene_name,c.mktype,
	a.dt ,
	a.uid ,

	enter_ctrip ,
	enter_realname ,
	enter_wallet ,
	enter_more_func ,
	enter_others_channel ,
	
	page_apply	,	
	page_cardguide	,	
	page_card_number	,	
	page_card	,	
	page_setpass	,	
	page_active	,
	page_finish	,	
	page_success	,
	
	button_apply ,
	button_contract ,
	button_active ,

	db_active ,
	db_success ,	

	log_creditProcess ,
	log_existPwd ,
	log_hasMobile ,
	log_preCreditStatus,
	log_realName,
	
	if(b.user_id is null,0,1),
	if(d.user_id is null,0,1)
from
(select * from tmp_innofin.vqq_rpt_active_process_detail where dt > '2018-05-01') a 
inner join (select * from ods_innofin.vqq_rpt_active_process_pages_mktype where dt > '2018-05-01') e on lower(a.uid) = lower(e.uid) and a.dt= e.dt
left join (select distinct user_id, to_date(request_time) as dt from ods_innofin.user_activate_req) b on lower(a.uid) = lower(b.user_id) and a.dt= b.dt
left join (select distinct user_id from ods_innofin.user_contract where contract_status =1 ) d on lower(a.uid) = lower(d.user_id)
inner join tmp_innofin.nqh_mkt_rpt_event c on e.mktype = c.mktype;

-- datasource 
use tmp_innofin;
drop table vqq_rpt_mkt_process_all;
create table if not exists vqq_rpt_mkt_process_all
(
	scene_name string comment '营销活动',
	dt string comment '日期',
	
	page_apply    int comment  '开通首页UV',
	button_apply    int comment  '立即开通btn',

	button_contract    int comment  '协议页下一步btn',

	page_cardguide   int  comment  '支付绑卡引导页UV',

	page_card    int comment  '支付绑卡页UV',

	page_active    int comment  '核实信息页UV',
	button_active    int comment  '核实信息页提交btn',

	need_setpass   int comment  '需要设置支付密码',

	page_finish   int  comment  '开通完成页UV',
	
	db_active int comment '完成流程(req)',
	db_success   int  comment  '授信成功(activate)',

	activate int comment '完成流程(action)',
	activate_success   int  comment  '授信成功(action)'


)partitioned by (
	flow_name string
);

insert overwrite table tmp_innofin.vqq_rpt_mkt_process_all partition (flow_name = '已实名需绑卡')
select scene_name,dt,
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_cardguide*greatest(page_card_number,page_card)),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish),
	sum(db_active),
	sum(db_success),
	sum(activate),
	sum(activate_success)
from tmp_innofin.nqh_mkt_rpt_event_details where (log_creditProcess = 0 and log_realName = 1) 
group by scene_name,dt;

insert overwrite table tmp_innofin.vqq_rpt_mkt_process_all partition (flow_name = '未实名')
select scene_name,dt,
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_cardguide*greatest(page_card_number,page_card)),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish),
	sum(db_active),
	sum(db_success),
	sum(activate),
	sum(activate_success)
from tmp_innofin.nqh_mkt_rpt_event_details where (log_creditProcess = 0 and log_realName = 0) 
group by scene_name,dt;

insert overwrite table tmp_innofin.vqq_rpt_mkt_process_all partition (flow_name = '已实名无需绑卡')
select scene_name,dt,
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish),
	sum(db_active),
	sum(db_success),
	sum(activate),
	sum(activate_success)
from tmp_innofin.nqh_mkt_rpt_event_details where (log_creditProcess = 1)  
group by scene_name,dt;


insert overwrite table tmp_innofin.vqq_rpt_mkt_process_all partition (flow_name = '已实名一键开通')
select scene_name,dt,
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish),
	sum(db_active),
	sum(db_success),
	sum(activate),
	sum(activate_success)
from tmp_innofin.nqh_mkt_rpt_event_details where (log_creditProcess = 2) 
group by scene_name,dt;




