-- 1) 获取页面访问数据
use tmp_innofin;
create table if not exists active_uv_stage (
	uid	string,	
	apply	int,	
	pass	int,	
	card	int,	
	active	int,	
	finish	int,	
	success	int	
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_stage add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_stage partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select pv.* from 
		(select
				uid,
				max(case when(
					(pagecode in ('10320607100','10320607163')) -- apply
					or (pagecode in ('10320660460','10320660459'))	-- preauthbiz 预授信业务线
					) then 1 else 0 end) as apply, 
				max(case when(
					(pagecode in ('271074','272092') and prepagecode in ('10320607100','10320607163'))  -- 设置密码 set pass 
					or (pagecode in ('10320661614','10320661613') and prepagecode in ('10320607100','10320607163'))--输入密码页 inputpass
					) then 1 else 0 end) as pass, 
                max(case when(
                	-- (pagecode in ('10320643008','10320643007') and query like '%consume%')--  as cardguide, --绑卡引导页
--                	 (pagecode in ('10320643027','10320643028')) ----填写银行卡号 cardnumber
                	(pagecode in ('10320642997','10320642998')) -- card
                	)then 1 else 0 end) as card, -- card 绑定银行卡
				max(case when(
					(pagecode in ('10320657063','10320657064')) -- as active,				--授信激活页
					or (pagecode in ('10320660457','10320660458')) -- preauth,			--预授信页-常规
					or (pagecode in ('10320637265','10320637264')) -- as bindauth,				--绑定授权页
				    )then 1 else 0 end) as active,	
				max(case when( (pagecode in ('10320607112','10320607172','10320607113','10320607173','10320607111','10320607171','10320654471','10320654472')) --	--常规线完成流程
				 	or (pagecode in ('10320660987','10320660988','10320660991','10320660992','10320660989','10320660990','10320660993','10320660994')) -- as finishbiz,		--预授信-业务线-完成流程
				 	)then 1 else 0 end) as finish,		--常规线完成流程
				max(case when( (pagecode in ('10320607113','10320607173'))  -- as success,			--常规线进入激活成功界面
         		 or (pagecode in ('10320660989','10320660990','10320660996','10320660995')) --  as successbiz				
         		  )then 1 else 0 end) as success
		from dw_mobdb.factmbpageview 
			where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
			group by uid)  pv
	    where pv.apply = 1;
-- 合计
use tmp_innofin;
create table if not exists active_uv_stage_sum (
	apply	int,	
	pass	int,	
	card	int,	
	active	int,	
	finish	int,	
	success	int	
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_stage_sum add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_stage_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select sum(apply) as apply,
	sum(pass) as pass,
	sum(card) as card,
	sum(active) as active,
	sum(finish) as finish,
	sum(success) as success
from tmp_innofin.active_uv_stage where (d='${zdt.addDay(-1).format("yyyy-MM-dd")}'); 

-- 2） -- 入口   
use tmp_innofin;
create table if not exists active_uv_entry (
	uid	string,	
	
	ctrip_view	int,
	wallet_view	int,
	realname_view int,
	biz_view	int,
	others_view int,
	
	ctrip_entry	int,
	wallet_entry	int,
	realname_entry int,
	biz_entry	int,
	others_entry int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_entry add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_entry partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid as uid,
	   max(case when pagecode = 'myctrip_home' then 1 else 0 end) as ctrip_view,
       max(case when pagecode in ('272040','271022') then 1 else 0 end) as wallet_view,
       max(case when pagecode in ('600001514','600001511') then 1 else 0 end) as realname_view,
       max(case when pagecode in ('flight_lowprice_home_rn') then 1 else 0 end) as biz_view,
       max(case when (pagecode in ('10320607100','10320607163') 
       		and prepagecode not in ('myctrip_home','272040','271022','600001514','600001511','flight_lowprice_home_rn')) then 1 else 0 end ) as others_view,
	   
	   max(case when pagecode in ('10320607100','10320607163') and prepagecode = 'myctrip_home' then 1 else 0 end) as ctrip_entry,
       max(case when pagecode in ('10320607100','10320607163') and prepagecode in ('272040','271022') then 1 else 0 end) as wallet_entry,
       max(case when pagecode in ('10320607100','10320607163') and prepagecode in ('600001514','600001511') then 1 else 0 end) as realname_entry,
       max(case when pagecode in ('10320607100','10320607163') and prepagecode in ('flight_lowprice_home_rn') then 1 else 0 end) as biz_entry,
       max(case when (pagecode in ('10320607100','10320607163') 
       		and prepagecode not in ('myctrip_home','272040','271022','600001514','600001511','flight_lowprice_home_rn')) then 1 else 0 end ) as others_entry
from dw_mobdb.factmbpageview a
   	left join 
    (select user_id from ods_innofin.user_contract_daily where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and contract_status = 1) b
    on lower(trim(a.uid)) = lower(trim(b.user_id))
   where b.user_id is null and d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and pagecode in ('10320607100','10320607163','myctrip_home','272040','271022','600001514','600001511','flight_lowprice_home_rn')
   group by uid;
   
use tmp_innofin;
create table if not exists active_uv_entry_sum (
	ctrip_view	int,
	wallet_view	int,
	realname_view int,
	biz_view	int,
	others_view int,
	
	ctrip_entry	int,
	wallet_entry	int,
	realname_entry int,
	biz_entry	int,
	others_entry int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_entry_sum add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_entry_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	sum(ctrip_view),
	sum(wallet_view),
	sum(realname_view),
	sum(biz_view),
	sum(others_view),

	sum(ctrip_entry),
	sum(wallet_entry),
	sum(realname_entry),
	sum(biz_entry),
	sum(others_entry)
from tmp_innofin.active_uv_entry where (d='${zdt.addDay(-1).format("yyyy-MM-dd")}'); 


-- 3) 按类别计算

use tmp_innofin;
create table if not exists active_lost_sum (
	user_type string,
	total_entry int,
	apply	int,	
	pass	int,	
	card	int,	
	active	int,	
	finish	int,	
	success	int	
) partitioned by (
	d string
); 
alter table tmp_innofin.active_lost_sum add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
-- 总量
insert overwrite table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	'all',
	sum(ctrip_view + wallet_view + biz_view + others_view),
	sum(apply),
	sum(pass),
	sum(card),
	sum(active),
	sum(finish),
	sum(success)
from tmp_innofin.active_uv_entry_sum a 
inner join tmp_innofin.active_uv_stage_sum b on 
a.d = b.d and 
a.d='${zdt.addDay(-1).format("yyyy-MM-dd")}' 
and b.d ='${zdt.addDay(-1).format("yyyy-MM-dd")}';

-- 实名
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	'realname',
	sum(ctrip_view + wallet_view + biz_view + others_view),
	sum(apply),
	sum(pass),
	sum(card),
	sum(active),
	sum(finish),
	sum(success)
from tmp_innofin.active_uv_entry_sum a 
() b
inner join tmp_innofin.active_uv_stage_sum b on 
a.d = b.d and 
a.d='${zdt.addDay(-1).format("yyyy-MM-dd")}' 
and b.d ='${zdt.addDay(-1).format("yyyy-MM-dd")}';

-- 绑卡
-- 实名绑卡
-- 前置模型
-- 前置模型实名
-- 前置模型绑卡
-- 前置模型实名绑卡

--- 4) Alluvial Diagram -- 