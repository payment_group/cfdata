-- 客单价 忠诚指标

--- 按月的订单
use tmp_innofin;
create table if not exists user_order_count (
	uid	string,	
	year_month string,
	order_count int
);
insert overwrite table tmp_innofin.user_order_count
select uid ,substring(creationdate,1,7) ,count(1) from ods_innofin.fact_loan_order group by uid,substring(creationdate,1,7); 

use tmp_innofin;
create table if not exists user_loyalty (
	uid	string,	
	user_loyalty decimal(9,2)
);
-- 计算忠诚
insert overwrite table tmp_innofin.user_loyalty
select user_id , if(uid is null,0,user_loyalty) 
from ods_innofin.user_contract a  
left join (select uid, sum(order_count/(order_count+1)) as user_loyalty from tmp_innofin.user_order_count group by uid) b 
on a.user_id = b.uid where a.contract_status = 1;

-- 忠诚指标分布
select user_loyalty,count(1) as user_count from tmp_innofin.user_loyalty group by user_loyalty;
-- 用户订单量 分布
select order_count, count(1) as user_count from(
select uid,sum(order_count) as order_count  from tmp_innofin.user_order_count group by uid) a
-- 订单量时间分布

-- 2）客户价值

-- 2.0 数据准备
-- BU订单
use tmp_innofin;
drop table if exists tmp_innofin.user_value_order;
CREATE TABLE tmp_innofin.user_value_order (
  orderid string COMMENT '业务订单号',
  order_type string,
  order_time string comment '订单时间',
  order_value_rate decimal(9,2)
);
-- 度假
insert overwrite table tmp_innofin.user_value_order
 select distinct orderid,'vacation',bookingdate,0.1
 from ods_innofin.etl_oi_vacation_order;
----机票
insert into table tmp_innofin.user_value_order
select orderid,'flight',bookingdate,0.05
  from ods_innofin.etl_oi_flight_order;
--火车票
insert into table tmp_innofin.user_value_order
select orderid,'train',bookingdate,0.1
  from ods_innofin.etl_oi_trains_order;
--酒店
insert into table tmp_innofin.user_value_order
select orderid,'hotel',bookingdate,0.1
  from ods_innofin.etl_oi_hotel_order;

-- 实际支付
use tmp_innofin;
drop table if exists tmp_innofin.user_value_payment;
CREATE TABLE tmp_innofin.user_value_payment (
  orderid string COMMENT '业务订单号',
  gatheringtype string COMMENT '支付标记',
  totalamount float COMMENT '金额'
);
-- 拉有效数据
insert overwrite table tmp_innofin.user_value_payment
select orderid,gatheringtype,totalamount-COALESCE(payamt_coupon,0)-COALESCE(payamt_wallet,0) as totalamount from dw_paypubdb.FactPayGatheringMain_Loan
where applys_cnt>0;
-- 计算实际支付
use tmp_innofin;
CREATE TABLE if not exists tmp_innofin.user_value_actualpayment (
  orderid string COMMENT '业务订单号',
  damount float COMMENT '支付',
  ramount float COMMENT '退款',
  actualamount float COMMENT '金额'
);

insert overwrite table tmp_innofin.user_value_actualpayment
select orderid,
	sum(case when gatheringtype='D' then totalamount else 0 end), 
	sum(case when gatheringtype='R' then totalamount else 0 end), 
	sum(case when gatheringtype='D' then totalamount else -totalamount end)
from tmp_innofin.user_value_payment group by orderid; 

-- 明细数据
use tmp_innofin;
CREATE TABLE if not exists tmp_innofin.user_value_order_actualpayment (
  uid string,
  orderid string COMMENT '业务订单号',
  order_type string,
  order_value_rate decimal(9,2),
  actualamount float COMMENT '金额'
);

insert overwrite table tmp_innofin.user_value_order_actualpayment
select b.uid,b.orderid,d.order_type,d.order_value_rate,c.actualamount
	from 
	ods_innofin.user_contract a 
	inner join ods_innofin.fact_loan_order b on lower(trim(a.user_id)) = lower(trim(b.uid))
	inner join tmp_innofin.user_value_actualpayment c on b.orderid = c.orderid  
	inner join tmp_innofin.user_value_order d on b.orderid = d.orderid;
	

-- 2.1 按订单金额计算价值
use tmp_innofin;
create table if not exists user_value_1(
	uid string,
	user_value_bu decimal(18,2),
	user_value_amount decimal(18,2)
);

-----------
insert overwrite table tmp_innofin.user_value_1
select uid, sum(if(order_type = 'train',if(actualamount = 0,0,1),actualamount * order_value_rate)),sum(actualamount*0.003)
	from tmp_innofin.user_value_order_actualpayment 
	group by uid;

-- 2.2 按订单金额和分期金额（分期）
use tmp_innofin;
create table if not exists user_value_2(
	uid string,
	install_amount decimal(18,2)
);

insert overwrite table tmp_innofin.user_value_2
select b.uid,sum(amt_dr) * 0.0036 
	from ods_innofin.fact_loan_order_ist b
	where b.installmentnum > 0
	group by b.uid;
	

-- 2.3）合并
use ods_innofin;
create table user_value(
	uid string comment 'uid',
	user_value_bu decimal(18,2) comment '按机票5%，酒店度假10%，火车票￥1计算客户价值',
	user_value_amount decimal(18,2) comment '实际支付0.3%',
	install_amount decimal(18,2) comment '分期金额0.36%'
) comment '用户价值';

insert overwrite table ods_innofin.user_value
select a.uid, a.user_value_bu, a.user_value_amount,if(b.uid is null,0,b.install_amount) 
	from tmp_innofin.user_value_1 a
	left join tmp_innofin.user_value_2 b
	on lower(trim(a.uid)) = lower(trim(b.uid));
	
insert into table ods_innofin.user_value
select a.user_id,0,0,0 from 
	(select user_id from ods_innofin.user_contract where contract_status = 1) a 
	left join ods_innofin.user_value b on lower(trim(a.user_id)) = lower(trim(b.uid)) 
	where b.uid is null;



--分布

select '零' as `区间`, count(1) as `用户数` from ods_innofin.user_value where user_value_bu = 0
union all
select '一千以下' as `区间`, count(1)as `用户数` from ods_innofin.user_value where user_value_bu > 0 and user_value_bu <=1000
union all
select '一千~三千' as `区间`, count(1) as `用户数` from ods_innofin.user_value where user_value_bu > 1000 and user_value_bu <=3000
union all
select '三千~五千' as `区间`, count(1)as `用户数` from ods_innofin.user_value where user_value_bu > 3000 and user_value_bu <=5000
union all
select '五千以上' as `区间`, count(1)as `用户数` from ods_innofin.user_value where user_value_bu > 5000
--
select '10以下(非0)' as `区间`, count(1) as `用户数` from ods_innofin.user_value where user_value_amount+install_amount > 0 and user_value_amount+install_amount <=10
union all
select '10~30' as `区间`, count(1) as `用户数` from ods_innofin.user_value where user_value_amount+install_amount > 10 and user_value_amount+install_amount <=30
union all
select '30~50' as `区间`, count(1)as `用户数` from ods_innofin.user_value where user_value_amount+install_amount > 30 and user_value_amount+install_amount <=50
union all
select '50~100' as `区间`, count(1)as `用户数` from ods_innofin.user_value where user_value_amount+install_amount > 50 and user_value_amount+install_amount <=100
union all
select '100+' as `区间`, count(1)as `用户数` from ods_innofin.user_value where user_value_amount+install_amount > 100
order by `用户数` desc limit 10


---平均用户价值（包含0的）
use ods_innofin;
create table user_value_by_activetime(
	uid string comment 'uid',
	active_time string comment '激活时间',
	user_value_bu decimal(9,2) comment '按机票5%，酒店度假10%，火车票￥1计算', 
	user_value_amount decimal(9,2) comment '客户价值,实际支付0.5%+分期金额0.3%' 
) comment '按激活时间计算客户价值' 
partitioned by (
	d comment '计算日期'
);
alter table ods_innofin.user_value_by_activetime add if not exists partition(d ='${zdt.format("yyyy-MM-dd")}');

insert overwrite table ods_innofin.user_value_by_activetime partition(d ='${zdt.format("yyyy-MM-dd")}')
select a.uid,b.update_time,a.user_value_bu,a.user_value_amount + install_amount 
	from ods_innofin.user_value a 
	inner join ods_innofin.user_contract b
	on lower(trim(a.uid)) = lower(trim(b.user_id));

-- 报表
-- 图
select substring(active_time,1,7) as `激活时间（年月）`, count(1) as `用户数`,cast(avg(user_value_amount) as double) as `平均价值(实际支付0.5%+分期金额0.3%)`
from ods_innofin.user_value_by_activetime where d= from_unixtime(unix_timestamp(),'yyyy-MM-dd') 
group by substring(active_time,1,7) 
-- 数据
select substring(active_time,1,7) as `激活时间（年月）`, count(1) as `用户数`,cast(avg(user_value_amount) as double) as `平均价值(实际支付0.5%+分期金额0.3%)`
from ods_innofin.user_value_by_activetime where d= from_unixtime(unix_timestamp(),'yyyy-MM-dd') 
group by substring(active_time,1,7)
union all 
select '全部' as `激活时间（年月）`, count(1) as `用户数`,cast(avg(user_value_amount) as double) as `平均价值(实际支付0.5%+分期金额0.3%)`
from ods_innofin.user_value_by_activetime where d= from_unixtime(unix_timestamp(),'yyyy-MM-dd') 
-- 3）流失客户
-- 王启明
