--1.1 总用户量
use tmp_innofin;
create table if not exists nqh_active_user_config(
	method_flag string comment '1=每月相同，2=月环比30'
	month int,
	month_days,
	month_newuser int
);

insert into table tmp_innofin.nqh_active_user_config 
	select '1',5,31,208601;
insert into table tmp_innofin.nqh_active_user_config 
	select '1',6,30,208601;
insert into table tmp_innofin.nqh_active_user_config 
	select '1',7,31,208601;
insert into table tmp_innofin.nqh_active_user_config 
	select '1',8,31,208601;
insert into table tmp_innofin.nqh_active_user_config 
	select '1',9,30,208601;
insert into table tmp_innofin.nqh_active_user_config 
	select '1',10,31,208601;
insert into table tmp_innofin.nqh_active_user_config 
	select '1',11,30,208601;
insert into table tmp_innofin.nqh_active_user_config 
	select '1',12,31,208601;

insert into table tmp_innofin.nqh_active_user_config 
	select '2',5,31,69949;
insert into table tmp_innofin.nqh_active_user_config 
	select '2',6,31,90933;
insert into table tmp_innofin.nqh_active_user_config 
	select '2',7,31,118213;
insert into table tmp_innofin.nqh_active_user_config 
	select '2',8,31,153677;
insert into table tmp_innofin.nqh_active_user_config 
	select '2',9,31,199780;
insert into table tmp_innofin.nqh_active_user_config 
	select '2',10,31,259714;
insert into table tmp_innofin.nqh_active_user_config 
	select '2',11,31,337628;
insert into table tmp_innofin.nqh_active_user_config 
	select '2',12,31,438917;

use tmp_innofin;
create table if not exists nqh_active_user_target(
	d string comment '日期',
	target_1 int,
	target_2 int
);

create table if not exists nqh_active_user_target (
	d string comment '日期',
	target_1 int comment '每月相同',
	target_2 int comment '月环比30%')
 ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED AS TEXTFILE
;
-- 5月
use tmp_innofin;
create table if not exists nqh_active_user_total(
	active_date string comment '日期',
	target_1 int comment '每月相同',
	target_2 int comment '月环比30%',
	active_user int,
	active_user_total int,
	active_rate_total decimal(9,2),
	active_rate_target1 decimal(9,2),
	active_rate_target2 decimal(9,2)
) partitioned by (
	d string 
);

-- 补数据
--alter table tmp_innofin.nqh_active_user_total add if not exists partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
--insert overwrite table tmp_innofin.nqh_active_user_total partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
--select a.d,target_1,target_2,b.actived_user ,b.actived_user*100/2000000 from 
--	tmp_innofin.nqh_active_user_target a 
--	inner join (select '${zdt.addDay(-1).format("yyyy-MM-dd")}' as d, count(1) as actived_user from ods_innofin.user_contract 
--		where substring(update_time,1,10) <= '${zdt.addDay(-1).format("yyyy-MM-dd")}' and contract_status = 1  ) b 
--	on a.d = b.d;
-- 每日
alter table tmp_innofin.nqh_active_user_total add if not exists partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.nqh_active_user_total partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.d,
	target_1,
	target_2, 
	b.active_user_total- c.active_user_total,
	b.active_user_total,
	b.active_user_total*100/2000000,
	b.active_user_total*100/target_1,
	b.active_user_total*100/target_2 
from tmp_innofin.nqh_active_user_target a 
	inner join (select '${zdt.addDay(-1).format("yyyy-MM-dd")}' as d, count(1) as active_user_total from ods_innofin.user_contract_daily where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and contract_status = 1) b 
	inner join (select '${zdt.addDay(-1).format("yyyy-MM-dd")}' as d, count(1) as active_user_total from ods_innofin.user_contract_daily where d = '${zdt.addDay(-2).format("yyyy-MM-dd")}' and contract_status = 1) c 
	on a.d = b.d and a.d = c.d;
--- 0822增加每天走完流程的数量	
use tmp_innofin;
create table if not exists vqq_nqh_apply_user_count(
	apply_user int,
	apply_user_total int
) partition by (
	d string 
);
insert overwrite table tmp_innofin.vqq_nqh_apply_user_count partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select b.apply_user_total- c.apply_user_total,
	b.apply_user_total 
from (select '${zdt.addDay(-1).format("yyyy-MM-dd")}' as d, count(1) as apply_user_total from ods_innofin.user_contract_daily where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}') b 
	inner join (select '${zdt.addDay(-1).format("yyyy-MM-dd")}' as d, count(1) as apply_user_total from ods_innofin.user_contract_daily where d = '${zdt.addDay(-2).format("yyyy-MM-dd")}') c 
	on b.d = c.d;

-- 报告
--1-17 修改申请人数，去除后台批量激活人数
select a.active_date as `日期`, a.target_1 as `目标（每月等量）`, a.target_2 as `目标（月环比30%）`, a.active_user_total as `实际累计激活用户`, a.active_user as `当日激活用户`,
	c.apply_user as `当日申请用户`,cast(a.active_rate_total as double) as `用户总量完成比%`, cast(a.active_rate_target1 as double) as `目标（每月等量）完成比%`  
	from  tmp_innofin.nqh_active_user_total a
	inner join (select * from tmp_innofin.vqq_nqh_apply_user_count where d >=#ONEMONTH# ) b on a.d = b.d
	inner join (select to_date(request_time) as req_date,count(1) as apply_user from  ods_innofin.user_activate_req  where req_source not in('CFB_TASK','FLIGHT','PRE_CREDIT_TASK')) c on a.d = c.req_date
	where a.d >=#ONEMONTH# order by `日期` limit 30

--1.2 整体分期率
use tmp_innofin;
create table if not exists nqh_installment_rate(
	installment_amount int,
	total_amount int,
	installment_rate decimal(9,2),
	d string 
);

insert overwrite table tmp_innofin.nqh_installment_rate
select sum(if(installmentnum = 0,0,totalamount)) as installment_amount, 
	sum(totalamount) as total_amount,
	100*sum(if(installmentnum = 0,0,totalamount))/sum(totalamount) as installment_rate,
	substring(creationdate,1,10) as d
	from ods_innofin.fact_loan_order_ist 
	where creationdate > '${zdt.addDay(-30).format("yyyy-MM-dd")}'
	group by substring(creationdate,1,10)
	order by d desc limit 30;
	--3.1用户主动开启关闭拿去花情况
--3.2用户主动关闭拿去花累计

-- 订单维度
use tmp_innofin;
create table report_use_order_daily(
	d string,
  nqh_user_orders int,
  nqh_support_orders int,
  nqh_pay_orders int,
  pay_support_rate decimal(9,2),
  pay_user_rate_rate decimal(9,2)
);
-- 补数据
insert into table report_use_order_daily
select orderdate,cnt_ord,ord_support,cnt_nqh,100*cnt_nqh/ord_support,100*cnt_nqh/cnt_ord 
from tmp_innofin.report_use_order_sta 
where buname='总计' --and orderdate='${zdt.addDay(-1).format("yyyy-MM-dd")}'
-- 跑批
insert into table report_use_order_daily
select orderdate,cnt_ord,ord_support,cnt_nqh,100*cnt_nqh/ord_support,100*cnt_nqh/cnt_ord 
from tmp_innofin.report_use_order_sta 
where buname='总计' and orderdate='${zdt.addDay(-1).format("yyyy-MM-dd")}';
--- 用户维度

use tmp_innofin;
create table report_use_uid_daily(
  d string,
  nqh_user_orders int,
  nqh_support_orders int,
  nqh_pay_orders int,
  pay_support_rate decimal(9,2),
  pay_user_rate_rate decimal(9,2)
);

insert into table report_use_uid_sta1
select '${zdt.addDay(-1).format("yyyy-MM-dd")}' as orderdate,sum(cnt_enable) as cnt_enable,sum(cnt_liv) as cnt_liv,sum(cnt_pay) as cnt_pay,sum(uid_count) as uid_count,sum(cnt_loan) as cnt_loan,concat(cast(cast(100.0*sum(cnt_loan)/sum(uid_count) as decimal(10,1)) as string),'%') as pct_loan_pay
from tmp_innofin.report_use_uid_sta