use tmp_innofin;
create table if not exists vqq_active_uv_entry_all_30 (
	uid	string,
	realname int,
	bindcard int,
	prefix_model int
);

create table if not exists vqq_active_process_uv_30 (
	uid	string,	
	apply	int,	
	cardguide	int,	
	card	int,	
	active	int,
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int
); 

create table if not exists vqq_active_process_nocard_uv_30 (
	uid	string,	
	apply	int,	
	active	int,	
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int
); 
