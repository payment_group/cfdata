-- 需上传
--	tmp_innofin.vqq_active_uv_entry_all_30 入口数据
--  tmp_innofin.vqq_active_process_uv_30 流程全部数据
--  tmp_innofin.vqq_active_process_nocard_uv_30 不绑卡流程数据
-- 公有云处理
--  前置 cfbdb.vqq_user_prefix
--  模型 cfbdb.vqq_user_model
-- 实名
use cfbdb;
create table if not exists vqq_realname(
	uid string
);
insert overwrite table cfbdb.vqq_realname
select uid from cfb_partnerdb.payment_realname_person_snap where auth_status = 1;
-- 绑卡
use cfbdb;
create table if not exists vqq_bindcard(
	uid string
);
insert overwrite table cfbdb.vqq_bindcard
select user_id from cfbdb.ctrip_cards_library where card_num >= 1;

--用公有云标记重新划分群体
-- 1) 入口数据
use cfbdb;
create table if not exists vqq_active_uv_entry_all_30_c (
	uid	string,
	realname int,
	bindcard int,
	prefix_model int
);

insert overwrite table cfbdb.vqq_active_uv_entry_all_30_c
select a.uid, 
	if(x.uid is null,0,1),
	if(y.uid is null,0,1),
	case when z1.uid is null or z2.uid is null then 0 else 1 end 
from 
	cfbdb.vqq_active_uv_entry_all_30 a
    left join cfbdb.vqq_realname x on lower(a.uid) = lower(x.uid)
   	left join cfbdb.vqq_bindcard y on lower(a.uid) = lower(y.uid)
   	left join cfbdb.vqq_user_prefix z1 on lower(a.uid) = lower(z1.uid)
   	left join cfbdb.vqq_user_model z2 on lower(a.uid) = lower(z2.uid)
; 
-- 2) 各页面数据

use cfbdb;
create table if not exists vqq_active_process_uv_30_c (
	uid	string,	
	apply	int,	
	cardguide	int,	
	card	int,	
	active	int,
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int

); 

insert overwrite table cfbdb.vqq_active_process_uv_30_c
select a.uid,apply,cardguide, card,active,finish,success,
	if(x.uid is null,0,1),
	if(y.uid is null,0,1),
	case when z1.uid is null or z2.uid is null then 0 else 1 end 
from
	cfbdb.vqq_active_process_uv_30 a
    left join cfbdb.vqq_realname x on lower(a.uid) = lower(x.uid)
   	left join cfbdb.vqq_bindcard y on lower(a.uid) = lower(y.uid)
   	left join cfbdb.vqq_user_prefix z1 on lower(a.uid) = lower(z1.uid)
   	left join cfbdb.vqq_user_model z2 on lower(a.uid) = lower(z2.uid)
;
--- 不绑卡流程
use cfbdb;
create table if not exists vqq_active_process_nocard_uv_30_c (
	uid	string,	
	apply	int,	
	active	int,	
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int
); 

insert overwrite table cfbdb.vqq_active_process_nocard_uv_30_c
select a.uid,apply,active,finish,success,
	if(x.uid is null,0,1),
	if(y.uid is null,0,1),
	case when z1.uid is null or z2.uid is null then 0 else 1 end 
from cfbdb.vqq_active_process_nocard_uv_30  a
    left join cfbdb.vqq_realname x on lower(a.uid) = lower(x.uid)
   	left join cfbdb.vqq_bindcard y on lower(a.uid) = lower(y.uid)
   	left join cfbdb.vqq_user_prefix z1 on lower(a.uid) = lower(z1.uid)
   	left join cfbdb.vqq_user_model z2 on lower(a.uid) = lower(z2.uid)
;
-- 3) 按类别计算
use cfbdb;
create table if not exists vqq_active_process_loss_sum_30 (
	user_type string comment '用户分类',
	type_flag string comment '备注',
	
	total_entry int comment '入口',
	apply	int comment '申请',	
	cardguide	int comment '密码',	
	card	int comment '绑卡',	
	active	int comment '激活',	
	active_nocard int comment '不绑卡',
	finish	int comment '完成',	
	success	int comment '成功'	
); 
-- 总量 
insert overwrite table cfbdb.vqq_active_process_loss_sum_30 
select '全部','1-***',a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '全部' 
		as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c) a  
	inner join 
	(select '全部' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c) b on a.user_type = b.user_type
	inner join 
	(select '全部' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c) c on a.user_type = c.user_type
;

-- 实名
insert into table cfbdb.vqq_active_process_loss_sum_30
select '实名','2-1**', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '实名' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c where realname = 1) a  
	inner join (select '实名' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c  where realname = 1) b on a.user_type = b.user_type
	inner join 
	(select '实名' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where realname = 1) c on a.user_type = c.user_type
;

-- 绑卡
insert into table cfbdb.vqq_active_process_loss_sum_30
select '绑卡','3-*1*', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '绑卡' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c where bindcard = 1) a  
	inner join (select '绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c  where bindcard = 1) b on a.user_type = b.user_type
	inner join 
	(select '绑卡' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where bindcard = 1) c on a.user_type = c.user_type
;
-- 实名绑卡
insert into table cfbdb.vqq_active_process_loss_sum_30
select '实名绑卡','4-11*', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '实名绑卡' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c  where realname = 1 and bindcard = 1) a  
	inner join (select '实名绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c  where realname = 1 and bindcard = 1) b on a.user_type = b.user_type
	inner join 
	(select '实名绑卡' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where realname = 1 and bindcard = 1) c on a.user_type = c.user_type
;

-- 前置模型
insert into table cfbdb.vqq_active_process_loss_sum_30
select '前置模型','5-**1', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c  where prefix_model = 1) a  
	inner join (select '前置模型' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c where prefix_model = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where prefix_model = 1) c on a.user_type = c.user_type
;

-- 前置模型实名
insert into table cfbdb.vqq_active_process_loss_sum_30
select '前置模型实名','6-1*1', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型实名' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c  where realname = 1 and prefix_model = 1) a  
	inner join (select '前置模型实名' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c where realname = 1 and prefix_model = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型实名' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where realname = 1 and prefix_model = 1) c on a.user_type = c.user_type
;
	
-- 前置模型绑卡
insert into table cfbdb.vqq_active_process_loss_sum_30
select '前置模型绑卡','7-*11', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型绑卡' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c where bindcard = 1 and prefix_model = 1) a  
	inner join (select '前置模型绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c where bindcard = 1 and prefix_model = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型绑卡' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where bindcard = 1 and prefix_model = 1) c on a.user_type = c.user_type
;

-- 前置模型实名绑卡
insert into table cfbdb.vqq_active_process_loss_sum_30
select '前置模型实名绑卡','8-111',a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型实名绑卡' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c where bindcard = 1 and prefix_model = 1 and realname = 1 ) a  
	inner join (select '前置模型实名绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c  where bindcard = 1 and prefix_model = 1 and realname = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型实名绑卡' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where bindcard = 1 and prefix_model = 1 and realname = 1) c on a.user_type = c.user_type
;

-- 以上皆非
insert into table cfbdb.vqq_active_process_loss_sum_30
select '非实名非绑卡非前置模型','9-000', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '非实名非绑卡非前置模型' as user_type, count(1) as entry_all from cfbdb.vqq_active_uv_entry_all_30_c  where bindcard = 0 and prefix_model = 0 and realname = 0 ) a  
	inner join (select '非实名非绑卡非前置模型' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from cfbdb.vqq_active_process_uv_30_c where bindcard = 0 and prefix_model = 0 and realname = 0) b on a.user_type = b.user_type
	inner join 
	(select '非实名非绑卡非前置模型' as user_type,
		sum(active) as active_nocard from cfbdb.vqq_active_process_nocard_uv_30_c where bindcard = 0 and prefix_model = 0 and realname = 0) c on a.user_type = c.user_type
;

