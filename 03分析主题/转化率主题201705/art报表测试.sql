use tmp_innofin;
create table tmp_art (
	d as string,
	v1 as decimal(18,2),
	v2 as decimal(18,2),
	v3 as decimal(9,2),
	v4 as int
);

insert into tmp_art select '2017-05-01',1.0,1.0,1.01,1;
insert into tmp_art select '2017-05-02',1.1,1.1,2.01,9;
insert into tmp_art select '2017-05-03',2.0,1.3,1.3431,10;
insert into tmp_art select '2017-05-04',1.7,1.5,4.01,5;
insert into tmp_art select '2017-05-05',3.1,1.9,4.01,2;
