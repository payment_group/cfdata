-- 1) 获取页面访问数据
use tmp_innofin;
create table if not exists active_uv_stage_30 (
	uid	string,	
	apply	int,	
	pass	int,	
	card	int,	
	active	int,	
	finish	int,	
	success	int	
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_stage_30 add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_stage_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid, max(apply),max(pass),max(card),max(active),max(finish),max(success)
	from tmp_innofin.active_uv_stage  
	where d >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' and d <= '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	group by uid;
	    
-- 阶段用户分类
use tmp_innofin;
create table if not exists active_uv_stage_type_30 (
	uid	string,	
	apply	int,	
	pass	int,	
	card	int,	
	active	int,	
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_stage_type_30 add if not exists partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_stage_type_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,max(apply),max(pass),max(card),max(active),max(finish), max(success),
	max(realname),max(bindcard),max(prefix_model)
	from tmp_innofin.active_uv_stage_type
	where d >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' and d <= '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	group by uid;

-- 2） -- 入口   
use tmp_innofin;
create table if not exists active_uv_entry_all_30 (
	uid	string,	
	entry_all int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_entry_all_30 add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_entry_all_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,max(entry_all) from 
	tmp_innofin.active_uv_entry_all
	where d >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' and d <= '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	group by uid;

-- 入口分类
use tmp_innofin;
create table if not exists active_uv_entry_all_type_30 (
	uid	string,	
	entry_all int,
	realname int,
	bindcard int,
	prefix_model int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_entry_all_type_30 add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_entry_all_type_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	uid,
	max(entry_all),
	max(realname),
	max(bindcard),
	max(prefix_model)
from tmp_innofin.active_uv_entry_all_type a
	where d >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' and d <= '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	group by uid;

-- 3) 按类别计算
use tmp_innofin;
create table if not exists active_lost_sum_30 (
	user_type string comment '用户分类',
	type_flag string comment '备注',
	
	total_entry int comment '入口',
	apply	int comment '申请',	
	pass	int comment '密码',	
	card	int comment '绑卡',	
	active	int comment '激活',	
	finish	int comment '完成',	
	success	int comment '成功'	
) partitioned by (
	d string comment '年-月'
); 
alter table tmp_innofin.active_lost_sum_30 add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');

-- 总量 
insert overwrite table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '全部','1-***',a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '全部' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' ) a  
	inner join (select '全部' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}') b on 
a.user_type = b.user_type;

-- 实名
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '实名','2-1**', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '实名' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1) a  
	inner join (select '实名' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1) b on 
a.user_type = b.user_type;

-- 绑卡
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '绑卡','3-*1*', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1) a  
	inner join (select '绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1) b on 
a.user_type = b.user_type;
-- 实名绑卡
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '实名绑卡','4-11*', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '实名绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and bindcard = 1) a  
	inner join (select '实名绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and bindcard = 1) b on 
a.user_type = b.user_type;

-- 前置模型
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型','5-**1', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and prefix_model = 1) a  
	inner join (select '前置模型' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and prefix_model = 1) b on 
a.user_type = b.user_type;

-- 前置模型实名
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型实名','6-1*1', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型实名' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and prefix_model = 1) a  
	inner join (select '前置模型实名' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and prefix_model = 1) b on 
a.user_type = b.user_type;

-- 前置模型绑卡
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型绑卡','7-*11', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1) a  
	inner join (select '前置模型绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1) b on 
a.user_type = b.user_type;

-- 前置模型实名绑卡
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型实名绑卡','8-111',a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型实名绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1 and realname = 1 ) a  
	inner join (select '前置模型实名绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1 and realname = 1) b on 
a.user_type = b.user_type;

-- 以上皆非
insert into table tmp_innofin.active_lost_sum_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '非实名非绑卡非前置模型','9-000', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '非实名非绑卡非前置模型' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type_30 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 0 and prefix_model = 0 and realname = 0 ) a  
	inner join (select '非实名非绑卡非前置模型' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type_30  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 0 and prefix_model = 0 and realname = 0) b on 
a.user_type = b.user_type;

--- 4) ftp -- 

crtdate8='${zdt.format("yyyyMMdd")}'
echo 'crtdate8='$crtdate8
hive -e "select *,substring(type_flag,1,1) as order_flag from tmp_innofin.active_lost_sum where d = '${zdt.addDay(-1).format('yyyy-MM-dd')}' order by type_flag limit 10;">/home/innofin/active_lost_data_1_$crtdate8.txt
curl --ftp-ssl -k -T /home/innofin/active_lost_data_1_$crtdate8.txt ftp://data_ctrip_cf:qX3yHHc0CFoovsn9ynjT@dataexchange.ctripcorp.com:53233/xfjr_sbu/ctrip/active_lost/

-- 5) 按类别计算流失率
use tmp_innofin;
create table if not exists active_lost_rate_30 (
	user_type string comment '用户分类',
	type_flag string comment '备注',
	
	entry_apply decimal(9,2) comment '入口->申请',
	apply_pass	decimal(9,2) comment '申请->密码',	
	pass_card	decimal(9,2) comment '密码->绑卡',	
	card_active	decimal(9,2) comment '绑卡->激活',	
	active_finish	decimal(9,2) comment '激活->完成',	
	finish_success	decimal(9,2) comment '完成->成功'	
) partitioned by (
	d string comment '日期'
);

alter table tmp_innofin.active_lost_rate_30 add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_lost_rate_30 partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.user_type,a.type_flag,
	100*(1-apply/total_entry),
	100*(1-pass/apply),
	100*(1-card/pass),
	100*(1-active/card),
	100*(1-finish/active),
	100*(1-success/finish)
	from tmp_innofin.active_lost_sum_30 a where a.d ='${zdt.addDay(-1).format("yyyy-MM-dd")}'; 



 