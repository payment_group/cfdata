-- 1) 获取页面访问数据
use tmp_innofin;
create table if not exists active_uv_stage (
	uid	string,	
	apply	int,	
	pass	int,	
	card	int,	
	active	int,	
	finish	int,	
	success	int	
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_stage add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_stage partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select pv.* from 
		(select
				uid,
				max(case when(
					(pagecode in ('10320607100','10320607163')) -- apply
					or (pagecode in ('10320660460','10320660459'))	-- preauthbiz 预授信业务线
					) then 1 else 0 end) as apply, 
				max(case when(
					(pagecode in ('271074','272092') and prepagecode in ('10320607100','10320607163'))  -- 设置密码 set pass 
					or (pagecode in ('10320661614','10320661613') and prepagecode in ('10320607100','10320607163'))--输入密码页 inputpass
					or (
					    (pagecode in ('10320643008','10320643007') and query like '%consume%')--  绑卡
                	or (pagecode in ('10320643027','10320643028')) ----填写银行卡号 cardnumber
                	or (pagecode in ('10320642997','10320642998')) -- card
						)
					) then 1 else 0 end) as pass, 
                max(case when(
                	(pagecode in ('10320643008','10320643007') and query like '%consume%')--  as cardguide, --绑卡引导页
                	or (pagecode in ('10320643027','10320643028')) ----填写银行卡号 cardnumber
                	or (pagecode in ('10320642997','10320642998')) -- card
                	) then 1 else 0 end) as card, -- card 绑定银行卡
				max(case when(
					(pagecode in ('10320657063','10320657064')) -- as active,				--授信激活页
					or (pagecode in ('10320660457','10320660458')) -- preauth,			--预授信页-常规
					or (pagecode in ('10320637265','10320637264')) -- as bindauth,				--绑定授权页
				    )then 1 else 0 end) as active,	
				max(case when( (pagecode in ('10320607112','10320607172','10320607113','10320607173','10320607111','10320607171','10320654471','10320654472')) --	--常规线完成流程
				 	or (pagecode in ('10320660987','10320660988','10320660991','10320660992','10320660989','10320660990','10320660993','10320660994')) -- as finishbiz,		--预授信-业务线-完成流程
				 	)then 1 else 0 end) as finish,		--常规线完成流程
				max(case when( (pagecode in ('10320607113','10320607173'))  -- as success,			--常规线进入激活成功界面
         		 or (pagecode in ('10320660989','10320660990','10320660996','10320660995')) --  as successbiz				
         		  )then 1 else 0 end) as success
		from dw_mobdb.factmbpageview 
			where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
			group by uid)  pv
	    where pv.apply = 1;
	    
-- 阶段用户分类
use tmp_innofin;
create table if not exists active_uv_stage_type (
	uid	string,	
	apply	int,	
	pass	int,	
	card	int,	
	active	int,	
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_stage_type add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_stage_type partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	a.uid,a.apply,a.pass,a.card,a.active,a.finish,a.success,
	(case when b.uid is null then 0 else 1 end) as realname,
	(case when c.uid is null then 0 else 1 end) as bindcard,
	(case when d.uid is null then 0 else 1 end) as prefix_model
from tmp_innofin.active_uv_stage a
   	left join tmp_innofin.cfbdb_user_relname b on a.uid = b.uid
   	left join tmp_innofin.cfbdb_user_card c on a.uid = c.uid
   	left join tmp_innofin.cfbdb_user_prefix_model d on a.uid = d.uid
   where a.d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
;

-- 2） -- 入口   
use tmp_innofin;
create table if not exists active_uv_entry_all (
	uid	string,	
	entry_all int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_entry_all add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_entry_all partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.uid,1 from 
	(select uid as uid from dw_mobdb.factmbpageview where pagecode in('myctrip_home','272040','271022','600001514','600001511','flight_lowprice_home_rn','10320607100','10320607163')
	and d = '${zdt.addDay(-1).format("yyyy-MM-dd")}') a
   	left join 
    (select user_id from ods_innofin.user_contract_daily where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and contract_status = 1 and update_time < '${zdt.addDay(-1).format("yyyy-MM-dd")}') b
    on lower(trim(a.uid)) = lower(trim(b.user_id))
   where b.user_id is null  
   group by uid;

-- 入口分类
use tmp_innofin;
create table if not exists active_uv_entry_all_type (
	uid	string,	
	entry_all int,
	realname int,
	bindcard int,
	prefix_model int
) partitioned by (
	d string
); 

alter table tmp_innofin.active_uv_entry_all_type add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_uv_entry_all_type partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	a.uid as uid,
	entry_all,
	(case when b.uid is null then 0 else 1 end) as realname,
	(case when c.uid is null then 0 else 1 end) as bindcard,
	(case when d.uid is null then 0 else 1 end) as prefix_model
from tmp_innofin.active_uv_entry_all a
   	left join tmp_innofin.cfbdb_user_relname b on a.uid = b.uid
   	left join tmp_innofin.cfbdb_user_card c on a.uid = c.uid
   	left join tmp_innofin.cfbdb_user_prefix_model d on a.uid = d.uid
   where a.d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
;

-- 3) 按类别计算
use tmp_innofin;
create table if not exists active_lost_sum (
	user_type string comment '用户分类',
	type_flag string comment '备注',
	
	total_entry int comment '入口',
	apply	int comment '申请',	
	pass	int comment '密码',	
	card	int comment '绑卡',	
	active	int comment '激活',	
	finish	int comment '完成',	
	success	int comment '成功'	
) partitioned by (
	d string comment '日期'
); 
alter table tmp_innofin.active_lost_sum add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
-- 总量 
insert overwrite table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '全部','1-***',a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '全部' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' ) a  
	inner join (select '全部' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}') b on 
a.user_type = b.user_type;

-- 实名
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '实名','2-1**', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '实名' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1) a  
	inner join (select '实名' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1) b on 
a.user_type = b.user_type;

-- 绑卡
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '绑卡','3-*1*', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1) a  
	inner join (select '绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1) b on 
a.user_type = b.user_type;
-- 实名绑卡
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '实名绑卡','4-11*', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '实名绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and bindcard = 1) a  
	inner join (select '实名绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and bindcard = 1) b on 
a.user_type = b.user_type;

-- 前置模型
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型','5-**1', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and prefix_model = 1) a  
	inner join (select '前置模型' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and prefix_model = 1) b on 
a.user_type = b.user_type;

-- 前置模型实名
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型实名','6-1*1', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型实名' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and prefix_model = 1) a  
	inner join (select '前置模型实名' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1 and prefix_model = 1) b on 
a.user_type = b.user_type;

-- 前置模型绑卡
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型绑卡','7-*11', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1) a  
	inner join (select '前置模型绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1) b on 
a.user_type = b.user_type;

-- 前置模型实名绑卡
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '前置模型实名绑卡','8-111',a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '前置模型实名绑卡' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1 and realname = 1 ) a  
	inner join (select '前置模型实名绑卡' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 1 and prefix_model = 1 and realname = 1) b on 
a.user_type = b.user_type;

-- 以上皆非
insert into table tmp_innofin.active_lost_sum partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select '非实名非绑卡非前置模型','9-000', a.entry_all,b.apply, b.pass, b.card, b.active, b.finish, b.success from 
	(select '非实名非绑卡非前置模型' as user_type, sum(entry_all) as entry_all from tmp_innofin.active_uv_entry_all_type where d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 0 and prefix_model = 0 and realname = 0 ) a  
	inner join (select '非实名非绑卡非前置模型' as user_type,
		sum(apply) as apply,
		sum(pass) as pass, 
		sum(card) as card ,
		sum(active) as active,
		sum(finish) as finish,
		sum(success) as success
	from tmp_innofin.active_uv_stage_type  where d ='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bindcard = 0 and prefix_model = 0 and realname = 0) b on 
a.user_type = b.user_type;

--- 4) ftp -- 

crtdate8='${zdt.format("yyyyMMdd")}'
echo 'crtdate8='$crtdate8
hive -e "select *,substring(type_flag,1,1) as order_flag from tmp_innofin.active_lost_sum where d = '${zdt.addDay(-1).format('yyyy-MM-dd')}' order by type_flag limit 10;">/home/innofin/active_lost_data_1_$crtdate8.txt
curl --ftp-ssl -k -T /home/innofin/active_lost_data_1_$crtdate8.txt ftp://data_ctrip_cf:qX3yHHc0CFoovsn9ynjT@dataexchange.ctripcorp.com:53233/xfjr_sbu/ctrip/active_lost/

-- 5) 按类别计算流失率
use tmp_innofin;
create table if not exists active_lost_rate (
	user_type string comment '用户分类',
	type_flag string comment '备注',
	
	entry_apply decimal(9,2) comment '入口->申请',
	apply_pass	decimal(9,2) comment '申请->密码',	
	pass_card	decimal(9,2) comment '密码->绑卡',	
	card_active	decimal(9,2) comment '绑卡->激活',	
	active_finish	decimal(9,2) comment '激活->完成',	
	finish_success	decimal(9,2) comment '完成->成功'	
) partitioned by (
	d string comment '日期'
);

alter table tmp_innofin.active_lost_rate add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_lost_rate partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.user_type,a.type_flag,
	100*(1-apply/total_entry),
	100*(1-pass/apply),
	100*(1-card/pass),
	100*(1-active/card),
	100*(1-finish/active),
	100*(1-success/finish)
	from tmp_innofin.active_lost_sum a where a.d ='${zdt.addDay(-1).format("yyyy-MM-dd")}'; 



 