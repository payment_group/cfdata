-- 1) 获取页面访问数据
-- wqm
use tmp_innofin;
create table if not exists active_page_daily(
uid string,
ctrip int comment '我携入口',
wallet int comment '钱包入口',
realname int comment '实名认证入口',
others int comment '其他入口',
biz int comment '业务线入口',
apply int comment '开通页',
setpass int comment '设置密码',
inputpass int comment '输入密码',
guide int comment '绑卡引导',
card int comment '银行卡号',
active int comment '授信激活',
preauth int comment '预授信常规',
preauthbiz int comment '预授信业务线',
bindauth int comment '绑定授权',
finish int comment '流程完成-常规',
finishbiz int comment '流程完成-业务线',
success int comment '激活成功',
successbiz int comment '激活成功-业务线',
nqh int comment '拿去花首页'
)partitioned by(
	d string
);
alter table tmp_innofin.active_page_daily add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_page_daily partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	uid,
	ctrip,
	wallet,
	realname ,
	if(apply = 1 and ctrip + wallet + realname = 0 ,1,0) as others,
	biz ,
	apply ,
	setpass ,
	inputpass ,
	guide ,
	card ,
	active ,
	preauth ,
	preauthbiz ,
	bindauth ,
	finish ,
	finishbiz ,
	if(success0+success1>0,1,0) as success,
	successbiz ,
	success1 as nqh 
 from tmp_innofin.wqm_procee_basic01 where d='${zdt.addDay(-1).format("yyyy-MM-dd")}';


-- 2)获取流转明细
use tmp_innofin;
create table if not exists active_lost(
	uid string comment 'uid',
	ctrip_apply int comment '我携入口=>开通页',
	wallet_apply int comment '钱包入口=>开通页',
	realname_apply int comment '实名认证入口=>开通页',
	others_apply int comment '其他入口=>开通页',
	biz_preauthbiz int comment '业务线入口=>预授信业务线',
	apply_setpass int comment '开通页=>设置密码',
	apply_inputpass int comment '开通页=>输入密码',
	setpass_guide int comment '设置密码=>绑卡引导',
	inputpass_guide int comment '输入密码=>绑卡引导',
	inputpass_active int comment '输入密码=>授信激活',
	inputpass_preauth int comment '输入密码=>预授信常规',
	inputpass_finishbiz int comment '输入密码=>流程完成-业务线',
	inputpass_bindauth int comment '输入密码=>绑定授权',
	cardguide_card int comment '绑卡引导=>银行卡号',
	card_active int comment '银行卡号=>授信激活',
	active_bindauth int comment '授信激活=>绑定授权',
	active_finish int comment '授信激活=>流程完成-常规',
	preauth_bindauth int comment '预授信常规=>绑定授权',
	preauth_finish int comment '预授信常规=>流程完成-常规',
	preauthbiz_inputpass int comment '预授信业务线=>输入密码',
	bindauth_finish int comment '绑定授权=>流程完成-常规',
	bindauth_finishbiz int comment '绑定授权=>流程完成-业务线',
	finish_success int comment '流程完成-常规=>激活成功',
	finishbiz_successbiz int comment '流程完成-业务线=>激活成功-业务线',	
	apply_lost int comment '开通页=>流失',
	setpass_lost int comment '设置密码=>流失',
	inputpass_lost int comment '输入密码=>流失',
	guide_lost int comment '绑卡引导=>流失',
	card_lost int comment '银行卡号=>流失',
	active_lost int comment '授信激活=>流失',
	preauth_lost int comment '预授信常规=>流失',
	preauthbiz_lost int comment '预授信业务线=>流失',
	bindauth_lost int comment '绑定授权=>流失',
	finish_lost int comment '流程完成-常规=>流失',
	finishbiz_lost int comment '流程完成-业务线=>流失'
) partitioned by
(
	d string comment '日期'
);

use tmp_innofin;
alter table tmp_innofin.active_lost add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_lost partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
    uid,	-- uid string comment 'uid',
	ctrip,-- ctrip_apply int, comment '我携入口=>开通页',
	wallet,-- wallet_apply int, comment '钱包入口=>开通页',
	realname,-- realname_apply int, comment '实名认证入口=>开通页',
	if(apply = 1 and ctrip + wallet +realname=0,1,0),-- others_apply int, comment '其他入口=>开通页',
	biz,-- biz_preauthbiz int, comment '业务线入口=>预授信业务线',
	if(apply = 1 and setpass = 1,1,0),--'开通页=>设置密码',
	if(apply = 1 and inputpass = 1,1,0),--'开通页=>输入密码',
	if(setpass = 1 and cardguide = 1,1,0),--'设置密码=>绑卡引导',
	if(inputpass = 1 and cardguide = 1,1,0),--'输入密码=>绑卡引导',
	if(inputpass = 1 and active = 1 and cardguide = 0 and card = 0,1,0),--'输入密码=>授信激活',
	if(inputpass = 1 and preauth = 1,1,0),--'输入密码=>预授信常规',
	if(inputpass = 1 and finishbiz = 1 and bindauth = 0,1,0),--'输入密码=>流程完成-业务线',
	if(inputpass = 1 and bindauth = 1 and preauth = 0 and active = 0,1,0),--'输入密码=>绑定授权',
	if(cardguide = 1 and card = 1,1,0),--'绑卡引导=>银行卡号',
	if(card = 1 and active = 1,1,0),--'银行卡号=>授信激活',
	if(active = 1 and bindauth = 1,1,0),--'授信激活=>绑定授权',
	if(active = 1 and finish = 1 and bindauth = 0,1,0),--'授信激活=>流程完成-常规',
	if(preauth = 1 and bindauth = 1,1,0),--'预授信常规=>绑定授权',
	if(preauth = 1 and finish = 1,1,0),--'预授信常规=>流程完成-常规',
	if(preauthbiz = 1 and inputpass = 1,1,0),--'预授信业务线=>输入密码',
	if(bindauth = 1 and finish = 1,1,0),--'绑定授权=>流程完成-常规',
	if(bindauth = 1 and finishbiz = 1,1,0),--'绑定授权=>流程完成-业务线',
	if((finish = 1 and success0 = 0) or (active = 1 and success1 = 0) ,1,0),--'流程完成-常规=>激活成功',
	if(finishbiz = 1 and successbiz = 1,1,0),--'流程完成-业务线=>激活成功-业务线',--
	
	if(apply = 1 and setpass = 0 and inputpass = 0,1,0),--'开通页=>流失',
	if(setpass = 1 and cardguide = 0,1,0),--'设置密码=>流失',
	if(inputpass = 1 and cardguide + active + preauth + bindauth + finishbiz = 0,1,0),--'输入密码=>流失',
	if(cardguide = 1 and card + card_number = 0,1,0),--'绑卡引导=>流失',
	if(card = 1 and active = 0,1,0),--'银行卡号=>流失',
	if(active = 1 and finish + bindauth = 0,1,0),--'授信激活=>流失',
	if(preauth = 1 and finish + bindauth = 0,1,0),--'预授信常规=>流失',
	if(preauthbiz = 1 and inputpass = 0,1,0),--'预授信业务线=>流失',
	if(bindauth = 1 and finish + finishbiz = 0,1,0),--'绑定授权=>流失'
	if(success0 + success1 = 0,1,0),--'流程完成-常规=>流失',
	if(successbiz = 0 and finishbiz = 1,1,0)--'流程完成-业务线=>流失'

 from tmp_innofin.wqm_process_basic01 where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and length(uid)>0;

--3)合计-----------
use tmp_innofin;
drop table active_lost_count;
create table if not exists active_lost_count(
	ctrip_apply int comment '我携入口=>开通页',
	wallet_apply int comment '钱包入口=>开通页',
	realname_apply int comment '实名认证入口=>开通页',
	others_apply int comment '其他入口=>开通页',
	biz_preauthbiz int comment '业务线入口=>预授信业务线',
	apply_setpass int comment '开通页=>设置密码',
	apply_inputpass int comment '开通页=>输入密码',
	setpass_guide int comment '设置密码=>绑卡引导',
	inputpass_guide int comment '输入密码=>绑卡引导',
	inputpass_active int comment '输入密码=>授信激活',
	inputpass_preauth int comment '输入密码=>预授信常规',
	inputpass_finishbiz int comment '输入密码=>流程完成-业务线',
	inputpass_bindauth int comment '输入密码=>绑定授权',
	cardguide_card int comment '绑卡引导=>银行卡号',
	card_active int comment '银行卡号=>授信激活',
	active_bindauth int comment '授信激活=>绑定授权',
	active_finish int comment '授信激活=>流程完成-常规',
	preauth_bindauth int comment '预授信常规=>绑定授权',
	preauth_finish int comment '预授信常规=>流程完成-常规',
	preauthbiz_inputpass int comment '预授信业务线=>输入密码',
	bindauth_finish int comment '绑定授权=>流程完成-常规',
	bindauth_finishbiz int comment '绑定授权=>流程完成-业务线',
	finish_success int comment '流程完成-常规=>激活成功',
	finishbiz_successbiz int comment '流程完成-业务线=>激活成功-业务线',	
	apply_lost int comment '开通页=>流失',
	setpass_lost int comment '设置密码=>流失',
	inputpass_lost int comment '输入密码=>流失',
	guide_lost int comment '绑卡引导=>流失',
	card_lost int comment '银行卡号=>流失',
	active_lost int comment '授信激活=>流失',
	preauth_lost int comment '预授信常规=>流失',
	preauthbiz_lost int comment '预授信业务线=>流失',
	bindauth_lost int comment '绑定授权=>流失',
	finish_lost int comment '流程完成-常规=>流失',
	finishbiz_lost int comment '流程完成-业务线=>流失'
) partitioned by
(
	d string comment '日期'
);

use tmp_innofin;
alter table tmp_innofin.active_lost_count add if not exists partition (d='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.active_lost_count partition(d='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
	sum(ctrip_apply), --  comment '我携入口=>开通页',
	sum(wallet_apply), --  comment '钱包入口=>开通页',
	sum(realname_apply), --  comment '实名认证入口=>开通页',
	sum(others_apply), --  comment '其他入口=>开通页',
	sum(biz_preauthbiz), --  comment '业务线入口=>预授信业务线',
	sum(apply_setpass), --  comment '开通页=>设置密码',
	sum(apply_inputpass), --  comment '开通页=>输入密码',
	sum(setpass_guide), --  comment '设置密码=>绑卡引导',
	sum(inputpass_guide), --  comment '输入密码=>绑卡引导',
	sum(inputpass_active), --  comment '输入密码=>授信激活',
	sum(inputpass_preauth), --  comment '输入密码=>预授信常规',
	sum(inputpass_finishbiz), --  comment '输入密码=>流程完成-业务线',
	sum(inputpass_bindauth), --  comment '输入密码=>绑定授权',
	sum(cardguide_card), --  comment '绑卡引导=>银行卡号',
	sum(card_active), --  comment '银行卡号=>授信激活',
	sum(active_bindauth), --  comment '授信激活=>绑定授权',
	sum(active_finish), --  comment '授信激活=>流程完成-常规',
	sum(preauth_bindauth), --  comment '预授信常规=>绑定授权',
	sum(preauth_finish), --  comment '预授信常规=>流程完成-常规',
	sum(preauthbiz_inputpass), --  comment '预授信业务线=>输入密码',
	sum(bindauth_finish), --  comment '绑定授权=>流程完成-常规',
	sum(bindauth_finishbiz), --  comment '绑定授权=>流程完成-业务线',
	sum(finish_success), --  comment '流程完成-常规=>激活成功',
	sum(finishbiz_successbiz), --  comment '流程完成-业务线=>激活成功-业务线',	
	sum(apply_lost), --  comment '开通页=>流失',
	sum(setpass_lost), --  comment '设置密码=>流失',
	sum(inputpass_lost), --  comment '输入密码=>流失',
	sum(guide_lost), --  comment '绑卡引导=>流失',
	sum(card_lost), --  comment '银行卡号=>流失',
	sum(active_lost), --  comment '授信激活=>流失',
	sum(preauth_lost), --  comment '预授信常规=>流失',
	sum(preauthbiz_lost), --  comment '预授信业务线=>流失',
	sum(bindauth_lost), --  comment '绑定授权=>流失',
	sum(finish_lost), --  comment '流程完成-常规=>流失',
	sum(finishbiz_lost)--  comment '流程完成-业务线=>流失'
from  tmp_innofin.active_lost where d='${zdt.addDay(-1).format("yyyy-MM-dd")}';
--- 4) Alluvial Diagram -- 