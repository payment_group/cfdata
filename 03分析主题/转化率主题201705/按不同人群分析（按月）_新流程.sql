-- 1) 入口数据
use tmp_innofin;
create table if not exists vqq_active_uv_entry_all_30 (
	uid	string,
	realname int,
	bindcard int,
	prefix_model int
);

insert overwrite table tmp_innofin.vqq_active_uv_entry_all_30
select a.uid, 
	if(x.uid is null,0,1),
	if(y.uid is null,0,1),
	if(z.uid is null,0,1)
from 
	(select distinct uid from dw_mobdb.factmbpageview where pagecode in('myctrip_home','272040','271022','600001514','600001511','flight_lowprice_home_rn','10320607100','10320607163')
	and d between '${zdt.addDay(-30).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}') a
	left join 
    (select user_id from ods_innofin.user_contract_daily where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and contract_status = 1 and update_time < '${zdt.addDay(-1).format("yyyy-MM-dd")}') b
    on lower(a.uid) = lower(b.user_id)
    left join tmp_innofin.cfbdb_user_relname x on lower(a.uid) = lower(x.uid)
   	left join tmp_innofin.cfbdb_user_card y on lower(a.uid) = lower(y.uid)
   	left join tmp_innofin.cfbdb_user_prefix_model z on lower(a.uid) = lower(z.uid)
where b.user_id is null;
 
-- 2) 各页面数据

use tmp_innofin;
create table if not exists vqq_active_process_uv_30 (
	uid	string,	
	apply	int,	
	cardguide	int,	
	card	int,	
	active	int,
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int

); 

insert overwrite table tmp_innofin.vqq_active_process_uv_30
select a.uid,apply,cardguide, card,active,finish,success,
	if(x.uid is null,0,1),
	if(y.uid is null,0,1),
	if(z.uid is null,0,1)
from
(select uid, max(apply) as apply,max(cardguide) as cardguide,max(cardguide*card) as card,max(active) as active,max(finish) as finish,max(success) as success
	from tmp_innofin.apply_process_detail  
	where d between '${zdt.addDay(-30).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	group by uid) a
    left join tmp_innofin.cfbdb_user_relname x on lower(a.uid) = lower(x.uid)
   	left join tmp_innofin.cfbdb_user_card y on lower(a.uid) = lower(y.uid)
   	left join tmp_innofin.cfbdb_user_prefix_model z on lower(a.uid) = lower(z.uid)
;
--- 不绑卡流程
use tmp_innofin;
create table if not exists vqq_active_process_nocard_uv_30 (
	uid	string,	
	apply	int,	
	active	int,	
	finish	int,	
	success	int,
	
	realname int,
	bindcard int,
	prefix_model int
); 

insert overwrite table tmp_innofin.vqq_active_process_nocard_uv_30
select a.uid,apply,active,finish,success,
	if(x.uid is null,0,1),
	if(y.uid is null,0,1),
	if(z.uid is null,0,1)
from (select uid, max(apply) as apply ,max(active) as active,max(finish) as finish,max(success)as success
	from tmp_innofin.apply_process_detail_nocard  
	where d between '${zdt.addDay(-30).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	group by uid)a
    left join tmp_innofin.cfbdb_user_relname x on lower(a.uid) = lower(x.uid)
   	left join tmp_innofin.cfbdb_user_card y on lower(a.uid) = lower(y.uid)
   	left join tmp_innofin.cfbdb_user_prefix_model z on lower(a.uid) = lower(z.uid)
;
-- 3) 按类别计算
use tmp_innofin;
create table if not exists vqq_active_process_loss_sum_30 (
	user_type string comment '用户分类',
	type_flag string comment '备注',
	
	total_entry int comment '入口',
	apply	int comment '申请',	
	cardguide	int comment '密码',	
	card	int comment '绑卡',	
	active	int comment '激活',	
	active_nocard int comment '不绑卡',
	finish	int comment '完成',	
	success	int comment '成功'	
); 
-- 总量 
insert overwrite table tmp_innofin.vqq_active_process_loss_sum_30 
select '全部','1-***',a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '全部' 
		as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30) a  
	inner join 
	(select '全部' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30) b on a.user_type = b.user_type
	inner join 
	(select '全部' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30) c on a.user_type = c.user_type
;

-- 实名
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '实名','2-1**', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '实名' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30 where realname = 1) a  
	inner join (select '实名' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30  where realname = 1) b on a.user_type = b.user_type
	inner join 
	(select '实名' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where realname = 1) c on a.user_type = c.user_type
;

-- 绑卡
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '绑卡','3-*1*', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '绑卡' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30 where bindcard = 1) a  
	inner join (select '绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30  where bindcard = 1) b on a.user_type = b.user_type
	inner join 
	(select '绑卡' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where bindcard = 1) c on a.user_type = c.user_type
;
-- 实名绑卡
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '实名绑卡','4-11*', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '实名绑卡' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30  where realname = 1 and bindcard = 1) a  
	inner join (select '实名绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30  where realname = 1 and bindcard = 1) b on a.user_type = b.user_type
	inner join 
	(select '实名绑卡' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where realname = 1 and bindcard = 1) c on a.user_type = c.user_type
;

-- 前置模型
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '前置模型','5-**1', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30  where prefix_model = 1) a  
	inner join (select '前置模型' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30 where prefix_model = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where prefix_model = 1) c on a.user_type = c.user_type
;

-- 前置模型实名
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '前置模型实名','6-1*1', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型实名' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30  where realname = 1 and prefix_model = 1) a  
	inner join (select '前置模型实名' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30 where realname = 1 and prefix_model = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型实名' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where realname = 1 and prefix_model = 1) c on a.user_type = c.user_type
;
	
-- 前置模型绑卡
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '前置模型绑卡','7-*11', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型绑卡' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30 where bindcard = 1 and prefix_model = 1) a  
	inner join (select '前置模型绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30 where bindcard = 1 and prefix_model = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型绑卡' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where bindcard = 1 and prefix_model = 1) c on a.user_type = c.user_type
;

-- 前置模型实名绑卡
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '前置模型实名绑卡','8-111',a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '前置模型实名绑卡' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30 where bindcard = 1 and prefix_model = 1 and realname = 1 ) a  
	inner join (select '前置模型实名绑卡' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30  where bindcard = 1 and prefix_model = 1 and realname = 1) b on a.user_type = b.user_type
	inner join 
	(select '前置模型实名绑卡' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where bindcard = 1 and prefix_model = 1 and realname = 1) c on a.user_type = c.user_type
;

-- 以上皆非
insert into table tmp_innofin.vqq_active_process_loss_sum_30
select '非实名非绑卡非前置模型','9-000', a.entry_all,b.apply, b.cardguide, b.card, b.active, c.active_nocard, b.finish, b.success from 
	(select '非实名非绑卡非前置模型' as user_type, count(1) as entry_all from tmp_innofin.vqq_active_uv_entry_all_30  where bindcard = 0 and prefix_model = 0 and realname = 0 ) a  
	inner join (select '非实名非绑卡非前置模型' as user_type,
		sum(apply) as apply, sum(cardguide) as cardguide,sum(card) as card ,sum(active) as active,sum(finish) as finish,sum(success) as success
	from tmp_innofin.vqq_active_process_uv_30 where bindcard = 0 and prefix_model = 0 and realname = 0) b on a.user_type = b.user_type
	inner join 
	(select '非实名非绑卡非前置模型' as user_type,
		sum(active) as active_nocard from tmp_innofin.vqq_active_process_nocard_uv_30 where bindcard = 0 and prefix_model = 0 and realname = 0) c on a.user_type = c.user_type
;

