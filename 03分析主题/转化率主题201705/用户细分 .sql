-- 新用户、活跃用户、忠诚用户、不活跃用户、流失用户、回流用户等。流失用户是长期不活跃，忠诚用户是长期活跃，回流用户是曾经不活跃或流失，后来又再次打开产品的活跃用户
-- 定义：
-- 新用户：当期激活  new
-- 活跃用户：当期有交易 active
-- 忠诚用户：最近三期有交易 loyal
-- 不活跃用户：当期无交易 inactive
-- 流失用户：最近三期无交易，lost 
-- 回流用户：上期是流失，本期活跃 return
-- 无（未激活）： na

--- 1按月的订单
use tmp_innofin;
create table if not exists user_order_count_ym (
	uid	string,	
	year_month string,
	order_count int
);

insert overwrite table tmp_innofin.user_order_count_ym
	select lower(trim(uid)),substring(creationdate,1,7) ,count(1) from ods_innofin.fact_loan_order group by uid,substring(creationdate,1,7); 

-- 2最早订单时间（用于修正update_time)
use tmp_innofin;
create table if not exists user_first_order_ym (
	uid	string,	
	year_month string
);

insert overwrite table tmp_innofin.user_first_order_ym
	select lower(trim(uid)), substring(min(creationdate),1,7) from ods_innofin.fact_loan_order group by uid;
	
-- 3激活时间
use tmp_innofin;
create table if not exists user_active_ym (
	uid	string,	
	year_month string
);

-- 已有数据不覆盖 -- 修正历史update_time
insert into table tmp_innofin.user_active_ym
select lower(trim(a.user_id)), case when c.uid is not null and c.year_month < substring(a.update_time,1,7) then c.year_month else substring(a.update_time,1,7) end from 
	(select user_id, update_time from ods_innofin.user_contract a where contract_status = 1) a
	left join tmp_innofin.user_active_ym b on lower(trim(a.user_id)) = lower(trim(b.uid))
	left join tmp_innofin.user_first_order_ym c on lower(trim(a.user_id)) = lower(trim(c.uid))
	where b.uid is null;
	
-- 4计算tag

select min(year_month) from tmp_innofin.user_active_ym; --'2016-06'

use tmp_innofin;
create table if not exists user_tag_ym (
	uid	string	
) partitioned by (
	year_month string,
	tag string
);
-- 2016-06 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-06',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2016-06';
-- 2016-07
--- new
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-07',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2016-07';
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-07',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-07'  ) b on a.uid = b.uid where a.year_month < '2016-07' and b.uid is null;
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-07',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-07'  ) b on a.uid = b.uid where a.year_month < '2016-07';
	
-- 2016-08
--- new
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-08',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2016-08';
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-08',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a 
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-08'  ) b on a.uid = b.uid where a.year_month < '2016-08' and b.uid is null;
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-08',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-08'  ) b on a.uid = b.uid where a.year_month < '2016-08';

-- 2016-09
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-09',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2016-09';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-09',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-07' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-08' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-09' ) d on a.uid = d.uid
	where  a.year_month < '2016-09';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-09',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-09'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-09' and tag = 'loyal') c  on a.uid = c.uid
	where a.year_month < '2016-09' and c.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-09',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-07' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-08' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-09' ) d on a.uid = d.uid
	where  a.year_month < '2016-09' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-09',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-09'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-09' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2016-09' and b.uid is null and c.uid is null;
-- =========================
-- 2016-10
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-10',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2016-10';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-10',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-08' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-09' ) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-10' ) d on a.uid = d.uid
	where  a.year_month < '2016-10' and d.uid is null;
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-10',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-10'  ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-09' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2016-10';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-10',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-10'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-10' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-10' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2016-10' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-10',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-08' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-09' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-10' ) d on a.uid = d.uid
	where  a.year_month < '2016-10' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-10',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-10'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-10' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2016-10' and b.uid is null and c.uid is null;

-- 2016-11
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-11',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2016-11';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-11',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-09' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-10' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-11' ) d on a.uid = d.uid
	where  a.year_month < '2016-11';
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-11',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-11'  ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-10' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2016-11';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-11',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-11'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-11' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-11' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2016-11' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-11',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-09' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-10' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-11' ) d on a.uid = d.uid
	where  a.year_month < '2016-11' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-11',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-11'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-11' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2016-11' and b.uid is null and c.uid is null;
-- ===================
-- 2016-12
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-12',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2016-12';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-12',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-10' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-11' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-12' ) d on a.uid = d.uid
	where  a.year_month < '2016-12';
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-12',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-12'  ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-11' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2016-12';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-12',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-12'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-12' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-12' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2016-12' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-12',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-10' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-11' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-12' ) d on a.uid = d.uid
	where  a.year_month < '2016-12' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2016-12',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-12'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-12' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2016-12' and b.uid is null and c.uid is null;
-- ===================
-- 2017-01
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-01',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2017-01';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-01',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-11' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-12' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-01' ) d on a.uid = d.uid
	where  a.year_month < '2017-01';
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-01',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-01'  ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-12' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-01';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-01',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-01'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-01' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-01' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2017-01' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-01',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-11' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-12' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-01' ) d on a.uid = d.uid
	where  a.year_month < '2017-01' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-01',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-01'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-01' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-01' and b.uid is null and c.uid is null;
-- ==========================
-- 2017-02
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-02',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2017-02';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-02',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2016-12' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-01' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-02' ) d on a.uid = d.uid
	where  a.year_month < '2017-02';
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-02',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-02'  ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-01' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-02';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-02',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-02'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-02' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-02' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2017-02' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-02',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2016-12' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-01' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-02' ) d on a.uid = d.uid
	where  a.year_month < '2017-02' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-02',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-02'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-02' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-02' and b.uid is null and c.uid is null;
-- ==========================
-- 2017-03
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-03',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2017-03';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-03',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-01' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-02' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-03' ) d on a.uid = d.uid
	where  a.year_month < '2017-03';
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-03',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-03'  ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-02' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-03';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-03',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-03'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-03' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-03' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2017-03' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-03',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-01' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-02' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-03' ) d on a.uid = d.uid
	where  a.year_month < '2017-03' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-03',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-03'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-03' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-03' and b.uid is null and c.uid is null;
-- ==========================
-- 2017-04
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-04',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2017-04';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-04',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-02' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-03' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-04' ) d on a.uid = d.uid
	where  a.year_month < '2017-04';
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-04',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-04') b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-03' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-04';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-04',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-04'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-04' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-04' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2017-04' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-04',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-02' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-03' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-04' ) d on a.uid = d.uid
	where  a.year_month < '2017-04' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-04',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-04'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-04' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-04' and b.uid is null and c.uid is null;
-- ==========================
-- 2017-05
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-05',tag='new') select uid from tmp_innofin.user_active_ym where year_month = '2017-05';
--- loyal
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-05',tag='loyal') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-03' ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-04' ) c on a.uid = c.uid
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-05' ) d on a.uid = d.uid
	where  a.year_month < '2017-05';
-- return 
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-05',tag='return') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-05'  ) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-04' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-05';
--- active
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-05',tag='active') 
select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-05'  ) b on a.uid = b.uid 
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-05' and tag = 'loyal') c  on a.uid = c.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-05' and tag = 'return') d  on a.uid = d.uid
	where a.year_month < '2017-05' and c.uid is null and d.uid is null;
--- lost
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-05',tag='lost') 
	select a.uid from tmp_innofin.user_active_ym a
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-03' and tag in ('inactive','lost')) b on a.uid = b.uid
	inner join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-04' and tag in ('inactive','lost')) c on a.uid = c.uid
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-05' ) d on a.uid = d.uid
	where  a.year_month < '2017-05' and d.uid is null;
--- inactive
insert overwrite table tmp_innofin.user_tag_ym partition(year_month = '2017-05',tag='inactive') 
select a.uid from tmp_innofin.user_active_ym a
	left join (select uid from tmp_innofin.user_order_count_ym where year_month = '2017-05'  ) b on a.uid = b.uid
	left join (select uid from tmp_innofin.user_tag_ym where year_month = '2017-05' and tag = 'lost') c  on a.uid = c.uid
	where a.year_month < '2017-05' and b.uid is null and c.uid is null;
	


-- 5求合计
use tmp_innofin;
create table if not exists user_tag_data (
	uid string,
	tag_201606 string,
	tag_201607 string,
	tag_201608 string,
	tag_201609 string,
	tag_201610 string,
	tag_201611 string,
	tag_201612 string,
	tag_201701 string,
	tag_201702 string,
	tag_201703 string,
	tag_201704 string,
	tag_201705 string
);

insert overwrite table tmp_innofin.user_tag_data
	select l.uid,a.tag, b.tag, c.tag, d.tag,e.tag,f.tag,g.tag,h.tag,i.tag,j.tag,k.tag,l.tag 
		from (select * from tmp_innofin.user_tag_ym where year_month = '2017-05') l
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2016-06') a on a.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2016-07') b on b.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2016-08') c on c.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2016-09') d on d.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2016-10') e on e.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2016-11') f on f.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2016-12') g on g.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2017-01') h on h.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2017-02') i on i.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2017-03') j on j.uid = l.uid
		left join (select * from tmp_innofin.user_tag_ym where year_month = '2017-04') k on k.uid = l.uid
;

--- 


select tag_201606 ,tag_201607 ,tag_201608 ,tag_201609 ,tag_201610 ,tag_201611 ,tag_201612 ,tag_201701 ,tag_201702 ,tag_201703 ,tag_201704 ,tag_201705, count(1) as user_count
 from tmp_innofin.user_tag_data group by tag_201606 ,tag_201607 ,tag_201608 ,tag_201609 ,tag_201610 ,tag_201611 ,tag_201612 ,tag_201701 ,tag_201702 ,tag_201703 ,tag_201704 ,tag_201705;

