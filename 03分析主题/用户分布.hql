-- 用户分布
-- 预授信成功未激活且当前可用
select count(distinct user_id) from 
fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap
where org_channel='CTRIP' and product_no='IOUS'
and req_status = 1 and activate_status = 0 and credit_type <> 1
and end_eff_time >= current_date;

-- 预授信成功已激活展期失败
select count(distinct a.user_id) from 
(select user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap 
where org_channel='CTRIP' and product_no='IOUS'
and req_status = 1 and activate_status = 2 and credit_type <> 1) a 
inner join 
(select user_id from dw_subject_finance.a_xj_busi_pay_ious_tbl_user_extend_term_req_snap
where status <> 2
) b on a.user_id = b.user_id

-- 主动申请激活成功(77.9w)
select count(distinct user_id) from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap 
where org_channel='CTRIP' and product_no='IOUS' and activate_status = 2;

-- 主动申请激活已用信展期失败
select count(distinct a.user_id) from 
(select * from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap 
where org_channel='CTRIP' and product_no='IOUS' and activate_status = 2) a 
inner join ods_innofin.user_contract b on a.user_id = b.open_id
inner join (select distinct uid from ods_innofin.fact_loan_order) c on lower(b.user_id) = lower(c.uid)
inner join (select user_id from dw_subject_finance.a_xj_busi_pay_ious_tbl_user_extend_term_req_snap
where status <> 2) d on a.user_id = d.user_id

-- 主动申请激活未用信展期失败
select count(distinct a.user_id) from 
(select * from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap 
where org_channel='CTRIP' and product_no='IOUS' and activate_status = 2) a 
inner join ods_innofin.user_contract b on a.user_id = b.open_id
left join (select distinct uid from ods_innofin.fact_loan_order) c on lower(b.user_id) = lower(c.uid)
inner join (select user_id from dw_subject_finance.a_xj_busi_pay_ious_tbl_user_extend_term_req_snap
where status <> 2) d on a.user_id = d.user_id
where c.uid is null;

-- 