select a.uid,b.tpp_code,ious_begin_time,ious_end_time 
from (select distinct uid,open_id from ods_innofin.nqh_user_active_date) a 
inner join (select distinct user_id,tpp_code,ious_begin_time,ious_end_time from fin_basic_data.a_xj_busi_pay_ious_tbl_ious_user_info_snap	where use_status = 1 and not (current_date between ious_begin_time and ious_end_time)) b
on lower(a.open_id) = lower(b.user_id);

-- 最近一个月机会下单
select a.uid, if(b.uid is null 0,1)
from
(select distinct uid from ods_innfoin.nqh_user_active_date where 
active_date >= add_months(current_date,-1)) a 

left join (select distinct uid from ods_innofin.fact_loan_order) b on lower(a.uid) = lower(b.uid)
