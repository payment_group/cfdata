-- 取数
-- BU限制
select orderid,mct.merchantname,amount
from (select* from dw_paypubdb.FactLoanPayRequest 
	where dt >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' 
	and burestrict&64<>64 and burestrict<>0
	and (activenqh=1 or preactivenqh=1)) a  
inner join dim_paydb.dimmerchant mct on a.bustype = mct.merchantid;
-- 余额不足
select orderid,mct.merchantname,amount
from (select* from dw_paypubdb.FactLoanPayRequest 
	where dt >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' 
	and (activenqh=1 or preactivenqh=1) and coalesce(usebalance,0)>'0' and cast(usebalance as decimal(10,2)) < cast(amount as decimal(10,2))) a
inner join dim_paydb.dimmerchant mct on a.bustype = mct.merchantid;

select count(1) from dw_paypubdb.FactLoanPayRequest 
	where dt = '2018-06-05' 
	and (amount>usebalance) 
	and activenqh=1 and status&1=1 and supportnqh&128=128
	
	
SELECT  dt
        ,COUNT(DISTINCT uid)  uidcnt
        ,COUNT(*)             ordcnt
        ,SUM(IF( preactivenqh=0 AND activenqh=0 ,1,0)) none_active
        ,SUM(IF( preactivenqh=1                 ,1,0)) pre_active
        ,SUM(IF( activenqh=1                    ,1,0)) active
        ,SUM(IF( (activenqh=1 or preactivenqh=1) and burestrict&64<>64 and burestrict<>0,1,0)) burestrict_loanpay
        ,SUM(IF( (activenqh=1 or preactivenqh=1) and length(buwhitelist) >=7 AND lower(buwhitelist) NOT LIKE '%loanpay%' ,1,0)) whitelist_loanpay
        ,SUM(IF( (activenqh=1 or preactivenqh=1) and lower(bublacklist) LIKE '%loanpay%' ,1,0)) blacklist_loanpay
        ,SUM(IF( (activenqh=1 or preactivenqh=1) and coalesce(usebalance,0)>'0' and cast(usebalance as decimal(10,2)) < cast(amount as decimal(10,2)),1,0)) balance_notenough
        ,SUM(IF( status&16=16                   ,1,0)) pre_available
        ,SUM(IF( status&16=16 AND lower(paymentwayidg)='loanpay' AND applypays=1,1,0)) pre_active_use
        ,SUM(IF( status&1=1                     ,1,0)) available
        ,SUM(IF( status&1=1   AND lower(paymentwayidg)='loanpay' AND applypays=1,1,0)) active_use
        ,SUM(IF( lower(brandid) LIKE '%loanpay%',1,0)) default_loanpay
        ,SUM(IF( lower(brandid) LIKE '%loanpay%' AND lower(paymentwayidg)='loanpay' AND applypays=1,1,0)) default_loanpay_use
        ,SUM(IF( status&16=16 AND recommend=1   ,1,0)) pre_active_comm
        ,SUM(IF( status&16=16 AND recommend=1 AND  click_preactive=1  ,1,0)) pre_active_comm_click
        ,SUM(IF( status&16=16 AND recommend=1 AND lower(paymentwayidg)='loanpay' AND applypays=1,1,0)) pre_active_comm_use
        ,SUM(IF( status&1=1   AND recommend=1   ,1,0)) active_comm
        ,SUM(IF( status&1=1   AND recommend=1 AND  click_active=1  ,1,0)) active_comm_click
        ,SUM(IF( status&1=1   AND recommend=1 AND lower(paymentwayidg)='loanpay' AND applypays=1,1,0)) active_comm_use
FROM    dw_paydb.FactLoanPayRequest
where  dt >='2018-05-20'
GROUP BY dt
