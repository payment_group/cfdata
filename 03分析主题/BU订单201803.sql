use tmp_innofin;
create table if not exists vqq_tmp_nqh_orders(
	uid string,
	o_id string,
	o_date string,
	bu_id string,
	bu_name string,
	amount double
);

insert overwrite table tmp_innofin.vqq_tmp_nqh_orders
select uid,orderid,creationdate,
	a.merchantid,b.merchantname,
	amount from ods_innofin.fact_loan_order a  
	left join dim_paydb.dimmerchant b
    on a.merchantid = b.merchantid
    where to_date(creationdate) between '2018-03-01' and '2018-03-31'
;

select (case when bu_name in ('国内机票','国际机票') then '机票'
		  when bu_name = ('火车票') then '火车票'
		  when bu_name in ('国内酒店','海外酒店') then '酒店'
		  when bu_name= ('团队游') then '度假'
		  when bu_name in ('团购糯米','酒店团购') then '团购'
		  else '其他' end) as bu,
       count(1) as o_count,
       count(distinct uid) as u_count,
       cast(sum(amount) as decimal(18,2)) as amount
  from tmp_innofin.vqq_tmp_nqh_orders
 group by (case when bu_name in ('国内机票','国际机票') then '机票'
		  when bu_name = ('火车票') then '火车票'
		  when bu_name in ('国内酒店','海外酒店') then '酒店'
		  when bu_name= ('团队游') then '度假'
		  when bu_name in ('团购糯米','酒店团购') then '团购'
		  else '其他' end);

-- bu 订单 2017,11,12
insert overwrite table tmp_innofin.vqq_tmp_nqh_orders
select uid,orderid,to_date(creationdate),
	a.merchantid,(case when merchantname in ('国内机票','国际机票') then '机票'
		  when merchantname = ('火车票') then '火车票'
		  when merchantname in ('国内酒店','海外酒店') then '酒店'
		  when merchantname= ('团队游') then '度假'
		  when merchantname in ('团购糯米','酒店团购') then '团购'
		  else '其他' end) as merchantname,
	amount from ods_innofin.fact_loan_order a  
	left join dim_paydb.dimmerchant b
    on a.merchantid = b.merchantid
    where to_date(creationdate) between '2017-11-01' and '2017-12-31'
;

-- 总数
select o_date,
       count(1) as o_count,
       cast(sum(amount) as decimal(18,2)) as amount
  from tmp_innofin.vqq_tmp_nqh_orders
 group by o_date
-- BU数
select o_date,
       count(1) as o_count,
       cast(sum(amount) as decimal(18,2)) as amount
  from tmp_innofin.vqq_tmp_nqh_orders where bu_name = '机票'
 group by o_date
---- 用户首次下单
use tmp_innofin;
create table if not exists vqq_rpt_nqh_first_order(
	o_date string,
	o_id string,
	uid string,
	merchant_id string,
	merchant_name string,
	bu_name string,
	amount decimal(18,2)
);

insert overwrite table tmp_innofin.vqq_rpt_nqh_first_order
select a.o_date,a.o_id,a.uid,a.merchantid,b.merchantname,
(case when merchantname in ('国内机票','国际机票') then '机票'
		  when merchantname = ('火车票') then '火车票'
		  when merchantname in ('国内酒店','海外酒店') then '酒店'
		  when merchantname= ('团队游') then '度假'
		  when merchantname in ('团购糯米','酒店团购') then '团购'
		  else '其他' end) as bu_name,
		  amount
 from (
	select to_date(creationdate) as o_date, orderid as o_id,
		uid,merchantid,amount,row_number() over (partition by uid order by creationdate) as r from ods_innofin.fact_loan_order
) a 
left join dim_paydb.dimmerchant b on a.merchantid = b.merchantid
where a.r = 1

-- 总数
select o_date,
       count(1) as o_count,
       cast(sum(amount) as decimal(18,2)) as amount
  from tmp_innofin.vqq_rpt_nqh_first_order where o_date between '2017-11-01' and '2017-12-31'
 group by o_date
 
 -- 酒店
select o_date,
       count(1) as o_count,
       cast(sum(amount) as decimal(18,2)) as amount
  from tmp_innofin.vqq_rpt_nqh_first_order where o_date between '2017-11-01' and '2017-12-31' and bu_name = '酒店'
 group by o_date
