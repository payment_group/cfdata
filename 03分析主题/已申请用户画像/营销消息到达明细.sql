-- 保留7天
use dw_innofin;
create table if not exists markting_message_batch_detail(
	plancode string comment '批次',
	uid string comment 'uid',
	sent int comment '是否成功flag',
	sendchannel string  comment '通道',
	d string comment '发送日期'
) comment '营销发送批次结果明细'
partitioned by (
    dt string comment '批次明细计算日期'
);
insert overwrite table dw_innofin.markting_message_batch_detail partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select scenecode, uid, if(ctisendstatus = 'F',0,1), sendchannel,d from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel in ('EDM','MIM','SMS') and 
lower(a.scenecode) in (select lower(plancode) from tmp_innofin.tmp_gpush)  and uid is not null;
