-- 营销明细 保留7天
use dw_innofin;
create table if not exists markting_message_batch_detail(
	plancode string comment '批次',
	uid string comment 'uid',
	sent int comment '是否成功flag',
	sendchannel string  comment '通道',
	d string comment '发送日期'
) comment '营销发送批次结果明细'
partitioned by (
    dt string comment '批次明细计算日期'
);
insert overwrite table dw_innofin.markting_message_batch_detail partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select scenecode, uid, if(ctisendstatus = 'F',0,1), sendchannel,d from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel in ('EDM','MIM','SMS') and 
lower(a.scenecode) in (select lower(plancode) from tmp_innofin.tmp_gpush)  and uid is not null;

-- 激活日期 保留7天
use dw_innofin;
create table nqh_user_apply_active_date(
	uid string comment 'uid',
	contract_status int comment '状态',
	apply_active_date string comment '申请/激活日期',
	user_name string  comment '姓名',
	contract_amount decimal(9,2)  comment '额度',
) comment '拿去花用户申请/激活日期'
partitioned by (
    dt string comment '数据日期（计算日-1）'
);
insert overwrite table dw_innofin.nqh_user_apply_active_date partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select user_id,contract_status,if(update_date<d,update_date,d),user_name,contract_amount
from(   
    select user_id,contract_status,to_date(update_time) as update_date,d,user_name,contract_amount,row_number() over (partition by user_id order by d) as no
    from ods_innofin.user_contract_daily 
    where  d < '${zdt.format("yyyy-MM-dd")}')
    ) a
where a.no =1;

--- 申请、激活前是否收到营销消息 保留7天
use dw_innofin;
create table nqh_user_markting_flag(
	uid string comment 'uid',
	active_flag int comment '开通状态',
	apply_active_date string comment '申请/激活日期',
	markting_flag int comment '营销状态'
) comment '拿去花用户申请/激活日期前收到营销消息标记'
partitioned by (
    dt string comment '数据日期（计算日-1）'
);
insert overwrite table dw_innofin.nqh_user_markting_flag partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct a.uid,if(contract_status = 1,1,0),apply_active_date,if(b.d is null,0,1)
from (select * from dw_innofin.nqh_user_apply_active_date where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}') a
left join (select * from dw_innofin.markting_message_batch_detail where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and sent = 1) b on lower(a.uid) = lower(b.uid)
where b.d is null or a.apply_active_date > b.d;
--- 激活日期历史
use dw_innofin;
create table vqq_nqh_user_active_date_snap(
	uid string comment 'uid',
	active_date string comment '激活日期'
)comment '激活日期';

insert overwrite table dw_innofin.vqq_nqh_user_active_date_snap
select user_id,to_date(update_time) from ods_innofin.user_contract where contract_status = 1 ;
--- 激活日期每日
insert into table dw_innofin.vqq_nqh_user_active_date_snap
select a.user_id,to_date(a.update_time) from ods_innofin.user_contract a 
left join dw_innofin.vqq_nqh_user_active_date_snap b on a.user_id = b.uid 
where a.contract_status = 1 and b.uid is null;
