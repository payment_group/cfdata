-- 针对开通首页“点击开通”但“未走完流程”流程的用户作分析，
-- 看其通过“前置模型”及“实名绑卡”的百分比情况。
-- 
-- 后期考虑在激活流程优化后，专门针对此类用户做推送；
-- 也可针对此类用户做抽样调研，采集开通流程优化建议。

use tmp_innofin;
create table if not exists vqq_process_not_finish_user(
	uid string comment 'uid'
) comment '进入中途离开通流程用户uid'
partitioned by (
    dt string comment '数据日期'
);
insert overwrite table tmp_innofin.vqq_process_not_finish_user partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid from tmp_innofin.wqm_process_basic01 a
left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id) 
where a.d='${zdt.addDay(-1).format("yyyy-MM-dd")}' and setpass+cardguide*card_number+active = 1
and b.user_id is null;

use tmp_innofin;
create table if not exists vqq_process_not_finish_user_flag(
	uid string comment 'uid',
	prefix_model_flag int,
	realname_card_flag int
) comment '通过“前置模型”及“实名绑卡”情况'
partitioned by (
    dt string comment '数据日期'
);
insert overwrite table tmp_innofin.vqq_process_not_finish_user_flag partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.uid,if(b.uid is null,0,1),if(c.uid is null,0,1) from tmp_innofin.vqq_process_not_finish_user a 
left join tmp_innofin.cfbdb_vqq_user_prefix_model b on lower(a.uid) = lower(b.uid)
left join tmp_innofin.cfbdb_user_relname_card c on lower(a.uid) = lower(c.uid)
where a.dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}';

use tmp_innofin;
create table if not exists vqq_process_not_finish_user_count(
	user_count int,
	prefix_model_count int,
	realname_card_count int,
	prefix_model_realname_card_count int
) comment '通过“前置模型”及“实名绑卡”统计情况'
partitioned by (
    dt string comment '数据日期'
);

insert overwrite table tmp_innofin.vqq_process_not_finish_user_count partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select 
count(1),
sum(prefix_model_flag),
sum(realname_card_flag),
sum(prefix_model_flag*realname_card_flag)
from tmp_innofin.vqq_process_not_finish_user_flag where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}';

-- ART
select dt as `日期`,
user_count as `人数`,
prefix_model_count as `前置模型`,
realname_card_count as `实名绑卡`,
prefix_model_realname_card_count as `前置模型实名绑卡`,
cast(cast(100*prefix_model_count/user_count as decimal(9,2)) as double) as `前置模型%`,
cast(cast(100*realname_card_count/user_count as decimal(9,2)) as double) as `实名绑卡%`,
cast(cast(100*prefix_model_realname_card_count/user_count as decimal(9,2)) as double) as `前置模型实名绑卡%`
from tmp_innofin.vqq_process_not_finish_user_count
where dt between #FROM# and #TO# order by `日期` limit 1000

