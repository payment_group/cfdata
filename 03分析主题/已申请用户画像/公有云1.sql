-- 加密uid方便联表
use cfbdb;
create table if not exists vqq_apply_user_markting_flag(
	uid	string,
	active_flag	int,
	apply_active_date	string,
	markting_flag	int,
	original_uid string
);
insert overwrite table cfbdb.vqq_apply_user_markting_flag
select nvl(b.uid,a.uid) as uid,active_flag,apply_active_date,markting_flag,a.uid as original_uid 
from (select * from cfbdb.nqh_user_markting_flag where dt ='${zdt.addDay(-1).format("yyyy-MM-dd")}') a 
left join cfbdb.sec_desensitization_uid_tel b on lower(a.uid) = lower(b.original_uid);

use cfbdb;
drop table cfbdb.vqq_apply_user_features;
create table if not exists vqq_apply_user_features(
	uid string,
	value string
) partitioned by (
	key string comment 'feature name'
);

-- 性别
insert overwrite table cfbdb.vqq_apply_user_features partition(key='gender')
select a.uid,b.gender from cfbdb.vqq_apply_user_markting_flag a 
left join cfb_partnerdb.payment_realname_person_snap b on lower(a.uid) = lower(b.uid) ;
-- 出生日期
-- insert overwrite table cfbdb.vqq_apply_user_features partition(key='birthday')
-- select a.uid,b.birthday from cfbdb.vqq_apply_user_markting_flag a 
-- left join cfb_partnerdb.payment_realname_person_snap b on lower(a.uid) = lower(b.uid);
-- 注册携程日期
insert overwrite table cfbdb.vqq_apply_user_features partition(key='signupdate')
select a.uid,to_date(b.signupdate) from cfbdb.vqq_apply_user_markting_flag a 
left join cfbdb.ctrip_cloud_uid_signup_info_snap b on lower(a.uid) = lower(b.uid);
-- 注册年-季度
insert overwrite table cfbdb.vqq_apply_user_features partition(key='signupgroup')
select uid, concat(year(value),'年', case when month(value) in (1,2,3) then 1 when month(value) in (4,5,6) then 2 when month(value) in (7,8,9) then 3 else 4 end,'季度')  
from cfbdb.vqq_apply_user_features where key='signupdate'; 
-- 年龄
insert overwrite table cfbdb.vqq_apply_user_features partition(key='age')
select a.uid,floor(datediff(to_date(from_unixtime(unix_timestamp())), to_date(birthday)) / 365.25) from cfbdb.vqq_apply_user_markting_flag a 
left join cfb_partnerdb.payment_realname_person_snap b on lower(a.uid) = lower(b.uid);
-- 年龄段
insert overwrite table cfbdb.vqq_apply_user_features partition(key='agegroup')
select uid,concat(floor(value)*5,'~',floor(value)*5+5)  
from cfbdb.vqq_apply_user_features where key='age'; 
-- 申请/激活拿去花年月
insert overwrite table cfbdb.vqq_apply_user_features partition(key='activedategroup')
select uid,concat(year(apply_active_date),'年',month(apply_active_date),'月') 
from cfbdb.vqq_apply_user_markting_flag;

-- 注册拿去花日期 已有

-- 统计
---- 性别 0 女 1男
select key,value,count(1) from cfbdb.vqq_apply_user_features where key in ('gender','activedategroup','agegroup','signupgroup') group by key,value;


