use tmp_innofin;
drop table tmp_innofin.vqq_apply_user_features;
create table if not exists vqq_apply_user_features(
	uid string,
	value string
) partitioned by (
	key string comment 'feature name'
);

-- 星级偏好
insert overwrite table tmp_innofin.vqq_apply_user_features partition(key='HTL.starprefer')
select a.uid,b.value from dw_innofin.nqh_user_markting_flag a 
left join (select uid,value from dw_mobdb.factuserprofiletotnew  where bu = 'HTL' and profile='starprefer' ) b on lower(a.uid) = lower(b.uid)
where dt ='${zdt.addDay(-1).format("yyyy-MM-dd")}' ;
-- 分组
insert overwrite table tmp_innofin.vqq_apply_user_features partition(key='HTL.starprefer.group')
select a.uid, 
case when a.value is null then '未知'
when a.value <= 20 then '0~20'
when a.value > 20 and value <= 40 then '20~40'
when a.value > 40 and value <= 60 then '40~60'
when a.value > 60 and value <= 80 then '60~80'
else '80~100' end
from tmp_innofin.vqq_apply_user_features a where key='HTL.starprefer';

-- 统计
select key,value,count(1) from tmp_innofin.vqq_apply_user_features where key in ('HTL.starprefer.group') group by key,value;


