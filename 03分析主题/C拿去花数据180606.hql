-- 全站用户总量
	select count(1) from fin_basic_data.c_corp_oc_cfbdb_uid_signup_info_snap;
-- APP日活跃用户数
	select count(distinct uid) from dw_mob.factmbpageview where d = '${zdt.format("yyyy-MM-dd")}';
-- 已实名用户数
	select count(distinct source_uid) from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap where auth_status = 1;

-- 强实名用户数(实名绑卡)
	select count(distinct a.source_uid) from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap a 
	inner join dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap b on lower(a.source_uid) = lower(b.uid)
	where auth_status = 1;
-- 强实名已激活用户数
	select count(distinct a.source_uid) from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap a 
	inner join dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap b on lower(a.source_uid) = lower(b.source_uid)
	inner join ods_innofin.nqh_user_active_date c on lower(a.source_uid) = lower(c.uid)
	where auth_status = 1;
-- 预授信成功用户数
	select count(distinct a.user_id) from 
	(select user_id from dw_subject_finance.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1) a
	left join fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap b on a.user_id = b.user_id 
	where b.user_id is null;
-- 预授信成功当前可用
	select count(distinct a.user_id) from 
	(select user_id from dw_subject_finance.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1) a
	left join fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap b on a.user_id = b.user_id 
	where b.user_id is null	and end_eff_time >= '${zdt.format("yyyy-MM-dd")}';
-- 预授信成功当前可用近6个月无主站交易
-- 预授信成功无六位密码
-- C预授信成功无四要素用户数
	select count(distinct a.user_id) from 
	(select user_id from dw_subject_finance.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1) a
	left join fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap b on a.user_id = b.user_id 
	inner join ods_innofin.user_contract d on a.user_id = d.open_id
	left join (
		select distinct a.source_uid from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap a 
		inner join dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap b on lower(a.source_uid) = lower(b.uid) 	
		where auth_status = 1
	) e on lower(d.user_id) = lower(e.source_uid)
	where b.user_id is null and e.source_uid is null;
-- 预授信过期用户数

-- 已激活交叉用户
	select count(distinct user_id) from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' and main_channel_code <> 'CTRIP';
-- 已激活非交叉用户
	select count(distinct a.user_id) from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap a where channel_code='CTRIP' and product_no='IOUS' and main_channel_code = 'CTRIP';

-- 已激活非交叉用户当前可用
	select count(distinct a.user_id) from (
		select user_id from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' and main_channel_code = 'CTRIP'
	) a 
	inner join (
		select user_id,max(use_status) as use_status from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' group by user_id 
	) b on a.user_id = b.user_id 
	where b.use_status = 1;
-- 已激活当前不可用用户数
	select count(distinct a.user_id) from (
		select user_id from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' and main_channel_code = 'CTRIP'
	)a 
	inner join (
		select user_id,max(use_status) as use_status from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap	where channel_code='CTRIP' and product_no='IOUS' group by user_id 
	) b on a.user_id = b.user_id 
	where b.use_status = 0;

-- 已激活当前展期失败用户数
-- 已激活逾期用户数
	select count(distinct a.user_id) from (
		select user_id from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap
	where channel_code='CTRIP' and product_no='IOUS' 
	and main_channel_code = 'CTRIP'
	)a 
	inner join (
	select user_id from dw_subject_finance.a_xj_busi_pay_repay_tbl_schedule_detail_snap
	where org_channel='CTRIP' and product_no='IOUS' and  due_date < '${zdt.format("yyyy-MM-dd")}'; and status in (0,1)) b on a.user_id = b.user_id
-- 已激活近6个月无主站交易用户
-- 