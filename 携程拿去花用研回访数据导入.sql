use tmp_innofin;
create table if not exists vqq_nqh_yy_1215(
	uid string,
	phone_decrypt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;


-- load data

-- 携程-拿去花用研专用的数据包批次号为：Sfif171219d1，数据量为20W，请于今日完成魔方数据库的上传，谢谢！

use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif171219d1',a.uid ,'','SMS','','FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.vqq_nqh_yy_1215 a;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif171219d1','SMS','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Sfif171219d1','2017-12-19','2017-12-29');