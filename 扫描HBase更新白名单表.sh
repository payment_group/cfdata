
# 2017-01-05下午饶新乐写了一次，当时表里有7.4万，生效了4.9万
# 当天的申请量就由800左右暴涨到2500+，之后三天缓慢回落，2017-01-08是1500+
# 饶新乐的作业是每周一和周四运行，10:00开始，11:00之前结束

download[hdfs:///zeus/hdfs-upload-dir/ETL_credit.jar-20161214-234214.jar ETL_credit.jar]
hadoop jar 'ETL_credit.jar' 'from_qunar_credit.txt'
hive -e "load data local inpath '/home/innofin/from_qunar_credit.txt' overwrite into table tmp_innofin.from_hbase_c100001;"

crtdate8='${zdt.format("yyyyMMdd")}'
crtdate10='${zdt.format("yyyy-MM-dd")}'

hive -e "

use ods_innofin;

download[hdfs:///zeus/hdfs-upload-dir/uid_md5.jar-20170109-155921.jar uid_md5.jar]
add jar uid_md5.jar;
create temporary function uid_md5 as 'pkg_sff.uid_md5';

drop table ods_innofin.nqh_uid_all_hbase_bak_$crtdate8;
create table ods_innofin.nqh_uid_all_hbase_bak_$crtdate8 as select * from ods_innofin.nqh_uid_all_hbase;

insert overwrite table ods_innofin.nqh_uid_all_hbase
select trim(lower(m.uid)) as uid,
       (case when wl.grp is not null then wl.grp
             when nwl.uid is not null then 'nwl'
             else 'other'
        end
       ) as grp,
       (case when wl.imp_date is not null then wl.imp_date
             when prv.uid is not null then prv.imp_date
             when nwl.uid is not null then concat('$crtdate10',' 10:00:00')
             else '2016-11-01 00:00:00'
        end
       ) as imp_date
  from tmp_innofin.from_hbase_c100001 hb
 inner join dw_mobdb.members m
    on hb.my_row = uid_md5(trim(lower(m.uid)))
  left join ods_innofin.nqh_uid_all wl
    on trim(lower(m.uid)) = wl.uid
  left join tmp_innofin.nqh_uid_nwl_apl nwl
    on trim(lower(m.uid)) = nwl.uid
  left join ods_innofin.nqh_uid_all_hbase_bak_$crtdate8 prv
    on trim(lower(m.uid)) = prv.uid;

"