-- 未走完主动开通流程用户数、有额度从未使用、预授信成功用户 统计
use tmp_innofin;
drop table vqq_tmp_not_finish_active_1;
create table vqq_tmp_not_finish_active_1 as 
select distinct uid from dw_mobdb.factmbpageview where d >= '2017-02-01' and pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431');

use tmp_innofin;
select count(1) from vqq_tmp_not_finish_active_1 a 
left join (select distinct user_id from ods_innofin.user_activate_req) b on lower(a.uid) = lower(b.user_id)
where b.user_id is null;
-- 4673304

select count(1) from 
ods_innofin.nqh_user_active_date a 
left join (select distinct uid from ods_innofin.fact_loan_order) b on  lower(a.uid) = lower(b.uid)
where b.uid is null;
-- 370348

select count(distinct user_id) from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel = 'CTRIP' and req_status = 1 and activate_status <> 2
-- 7278172
