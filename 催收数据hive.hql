use ods_innofin;
create table stakeholder_info(

	id	bigint,
	user_id	string,
	open_id	string,
	org_channel	string,
	user_name	string,
	mobile	string,
	identity_type	string,
	identity_code	string,
	intimacy_score	int,
	intimacy_relation	string,
	intimacy_occur_time	string,
	stakeholder_address	string,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);

select id,user_id,open_id,org_channel,user_name,mobile,identity_type,identity_code,intimacy_score,intimacy_relation,intimacy_occur_time,stakeholder_address,create_time,update_time,DataChange_LastTime from stakeholder_info with(nolock);

use ods_innofin;
create table tbl_overdue_loan(
	id	bigint,
	user_id	string,
	open_id	string,
	org_channel	string,
	order_no	string,
	order_time	string,
	tpp_code	string,
	tpp_name	string,
	product_no	string,
	product_name	string,
	loan_amt	decimal(15,2),
	loan_term	int,
	loan_provide_no	string,
	expire_amt	decimal(15,2),
	due_date	string,
	expire_count	int,
	repay_amt	decimal(15,2),
	setl_repay_amt	decimal(15,2),
	chargeback_amt	decimal(15,2),
	loan_status	int,
	user_group	tinyint,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);


select id,user_id,open_id,org_channel,order_no,order_time,tpp_code,tpp_name,product_no,product_name,loan_amt,loan_term,loan_provide_no,expire_amt,due_date,expire_count,repay_amt,setl_repay_amt,chargeback_amt,loan_status,user_group,create_time,update_time,DataChange_LastTime from tbl_overdue_loan;

use ods_innofin;
create table tbl_repayment_plan(
	id	bigint,
	user_id	string,
	loan_provide_no	string,
	period	int,
	capital_amt	decimal(15,2),
	actual_capital_amt	decimal(15,2),
	interest_amt	decimal(15,2),
	actual_interest_amt	decimal(15,2),
	overdue_interest_amt	decimal(15,2),
	actual_overdue_interest_amt	decimal(15,2),
	repayment_date	string,
	actual_repayment_date	string,
	status	int,
	process_status	int,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);

select id,user_id,loan_provide_no,period,capital_amt,actual_capital_amt,interest_amt,actual_interest_amt,overdue_interest_amt,actual_overdue_interest_amt,repayment_date,actual_repayment_date,status,process_status,create_time,update_time,DataChange_LastTime from tbl_repayment_plan;

use ods_innofin;
create table user_info(

	id	bigint,
	user_id	string,
	main_open_id	string,
	open_id	string,
	product_no	string,
	org_channel	string,
	main_org_channel	string,
	mobile	string,
	identity_type	string,
	identity_code	string,
	user_name	string,
	user_sex	string,
	user_birthday	string,
	user_group	tinyint,
	address	string,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);

select id,user_id,main_open_id,open_id,product_no,org_channel,main_org_channel,mobile,identity_type,identity_code,user_name,user_sex,user_birthday,user_group,address,create_time,update_time,DataChange_LastTime from user_info;

use ods_innofin;
drop table tbl_case_details;
create table tbl_case_details(
	id	bigint	comment '主键Id',
	case_no	string	comment '案件号',
	loan_provide_no	string	comment '借据号',
	period	int	comment '还款计划第x期',
	user_id	string	comment '用户ID',
	org_channel	string	comment '渠道编码: QUNAR, CTRIP',
	tpp_code	string	comment '贷款通道',
	fund_code string,
	product_no	string	comment '产品编码',
	capital_amt	decimal(15,2)	comment '应还本金',
	actual_capital_amt	decimal(15,2)	comment '已还本金',
	interest_amt	decimal(15,2)	comment '应还利息',
	actual_interest_amt	decimal(15,2)	comment '已还利息',
	overdue_interest_amt	decimal(15,2)	comment '应还罚息',
	actual_overdue_interest_amt	decimal(15,2)	comment '已还利息',
	repayment_date	string	comment '还款日期',
	actual_repayment_date	string	comment '实际还款日期',
	status	int	comment '状态，1逾期，2 已还清',
	create_time	string	comment '创建时间',
	update_time	string	comment '更新时间',
	datachange_lasttime	string	comment '最后更新时间'
);

