use cfbdb;
-- 支付成功订单交易
create table if not exists vqq_successful_payment(
	uid string,
	orderid string,
	billno string,
	amount double,
	createtime string
);
insert overwrite table cfbdb.vqq_successful_payment
select uid,orderid,billno,amount,createtime from cfbdb.finance_payment_sec_his_snap where billtype in ('A','D','G') and status=1;
-- 退款成功订单交易
use cfbdb;
create table if not exists vqq_refund_payment(
	uid string,
	orderid string,
	billno string,
	amount double,
	createtime string
);
insert overwrite table cfbdb.vqq_refund_payment
select  uid,orderid,billno,amount,createtime from cfbdb.finance_payment_sec_his_snap where billtype in ('R') and status=1;

-- 首次成功消费-- 3个月以上
use cfbdb;
create table if not exists vqq_first_successful_order(
	uid string,
	createtime string
);

insert overwrite table cfbdb.vqq_first_successful_order
select a.uid,min(a.createtime) as createtime 
from  cfbdb.vqq_successful_payment a 
	left join cfbdb.vqq_refund_payment b on a.orderid = b.orderid
	where b.orderid is null
group by a.uid;

-- 12个月总消费 大于0
use cfbdb;
create table if not exists vqq_total_amount_12m(
	uid string,
	totalamount double
);

insert overwrite table cfbdb.vqq_total_amount_12m
select a.uid, total- case when refund is null then 0 else refund end 
from 
(select uid, sum(amount) as total from cfbdb.vqq_successful_payment where createtime >= add_months(from_unixtime(unix_timestamp()),-12) group by uid) a
left join (select uid,sum(amount) as refund  from cfbdb.vqq_refund_payment where createtime >= add_months(from_unixtime(unix_timestamp()),-12)group by uid) b on a.uid = b.uid
; 

-- 退款次数占比 不超过40%
use cfbdb;
create table if not exists vqq_refund_rate(
	uid string,
	refundrate double
);

insert overwrite table cfbdb.vqq_refund_rate
select a.uid, case when refund is null then 0.0 else refund/total end 
from 
(select uid, count(distinct orderid) as total from cfbdb.vqq_successful_payment group by uid) a
left join (select uid,count(distinct orderid) as refund  from cfbdb.vqq_refund_payment group by uid) b on a.uid = b.uid
; 
-- 实名+年龄 -- 非3522
use cfbdb;
create table if not exists vqq_gender_age(
	uid string,
	gender int,
	age int
);
insert overwrite table cfbdb.vqq_gender_age
select uid,gender, floor(datediff(from_unixtime(unix_timestamp()), to_date(birthday)) / 365.25)  
from cfb_partnerdb.payment_realname_person_snap where region_code <> '3522';

-- join 

use cfbdb;
create table if not exists vqq_user_prefix(
	uid string,
	gender int,
	age int,
	createtime string,
	amount12m double,
	refundrate double
);

insert overwrite table cfbdb.vqq_user_prefix
select a.uid,gender, age,createtime,totalamount as amount12m,refundrate from
 cfbdb.vqq_gender_age a 
 inner join cfbdb.vqq_first_successful_order b on a.uid = b.uid
 inner join cfbdb.vqq_total_amount_12m c on a.uid = c.uid
 left join cfbdb.vqq_refund_rate d on a.uid = d.uid
 where a.age between 20 and 60 and createtime <= add_months(from_unixtime(unix_timestamp()),-3)
 and totalamount > 0 and (refundrate <= 0.4 or refundrate is null);
 