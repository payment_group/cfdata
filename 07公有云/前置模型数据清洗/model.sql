use cfbdb;
-- 基本信息
create table if not exists vqq_model_basic_info(
	uid string,
	age int,
	age_score int,
	gender int,
	gender_score int
);
insert overwrite table cfbdb.vqq_model_basic_info
select uid,age, 
	case when age >= 20 and age <= 25 then 37
		when age >25 and age <=30 then 44
		when age >30 and age <=35 then 56
		when age >35 and age <=40 then 64
		when age >40 and age <=60 then 76
		else 0 end,
		gender,
		case when gender=1 then 40 when gender=0 then 76 else 0 end
from  cfbdb.vqq_gender_age;
		
-- 下单未支付
use cfbdb;
create table if not exists vqq_nopayment_order_count(
	uid string,
	nopayment_count int
);
insert overwrite table cfbdb.vqq_nopayment_order_count
select uid,count(distinct a.orderid)  
from cfbdb.ctrip_cloud_uid_allorder_info_snap a
left join cfbdb.vqq_successful_payment b on a.orderid = b.orderid
where a.orderstatus <> '10' and b.orderid is null
group by a.uid;

use cfbdb;
create table if not exists vqq_all_order_count(
	uid string,
	all_order int
);
insert overwrite table cfbdb.vqq_all_order_count
select uid,count(distinct a.orderid)  
from cfbdb.ctrip_cloud_uid_allorder_info_snap a
group by a.uid; 

use cfbdb;
create table if not exists vqq_nopeyment_rate(
	uid string,
	nopeyment_rate double
);
insert overwrite table cfbdb.vqq_nopeyment_rate
select b.uid, case when a.uid is null then 0 else a.nopayment_count/b.all_order end
from cfbdb.vqq_nopayment_order_count a
right join cfbdb.vqq_all_order_count b on a.uid = b.uid;

use cfbdb;
create table if not exists vqq_nopeyment_rate_score(
	uid string,
	nopeyment_rate double,
	rate_score int
);
insert overwrite table cfbdb.vqq_nopeyment_rate_score
select uid,nopeyment_rate,
 case when nopeyment_rate >=0 and nopeyment_rate <=0.15 then 99
	 when nopeyment_rate >0.15 and nopeyment_rate <=0.25 then 81
	 when nopeyment_rate >0.25 and nopeyment_rate <=0.35 then 62
	 when nopeyment_rate >0.30 and nopeyment_rate <=0.55 then 49
	 when nopeyment_rate >0.35 and nopeyment_rate <=0.40 then 41
	 when nopeyment_rate >0.40 then 11
	 else 0 end
from cfbdb.vqq_nopeyment_rate;

-- 消费稳定性
use cfbdb;
create table if not exists vqq_stability_score(
	uid string,
	stability double,
	stability_score int
);
insert overwrite table cfbdb.vqq_stability_score
select uid,consumer_stability,
 case when consumer_stability >=0 and consumer_stability <=0.5 then 21
	 when consumer_stability >0.5 and consumer_stability <=0.65 then 36
	 when consumer_stability >0.65 and consumer_stability <=0.7 then 44
	 when consumer_stability >0.7 and consumer_stability <=0.85 then 55
	 when consumer_stability >0.85 and consumer_stability <=0.90 then 69
	 when consumer_stability >0.90 then 88
	 else 0 end
from cfbdb.ctrip_user_portrait_consumer_stability;

-- 信用卡最大消费
use cfbdb;
create table if not exists vqq_creditpay_max(
	uid string,
	amount double
);
insert overwrite table cfbdb.vqq_creditpay_max 
select uid,max(amount) from cfbdb.finance_payment_sec_his_snap
where billtype in ('A','D','G') and status=1 and payway='信用卡'
group by uid;

use cfbdb;
create table if not exists vqq_creditpay_max_score(
	uid string,
	amount double,
	amount_score int
);
insert overwrite table cfbdb.vqq_creditpay_max_score 
select uid,amount,
case when amount = 0 then 31
	when amount > 0 and amount <= 1250 then 45
	when amount > 1250 and amount <= 2650 then 55
	when amount > 2650 and amount <= 5100 then 65
	when amount > 5100 and amount <= 9500 then 69
	when amount > 9500 then 83
	else 0 end
from cfbdb.vqq_creditpay_max;

--- 求总分
use cfbdb;
create table if not exists vqq_user_model_score(
	uid string,
	total_score int
);
insert overwrite table cfbdb.vqq_user_model_score
select a.uid, 350 + a.age_score + a.gender_score 
	+ if(rate_score is null,0,rate_score)
	+ if(stability_score is null,0,stability_score)
	+ if(amount_score is null,0,amount_score)
from cfbdb.vqq_model_basic_info a 
left join cfbdb.vqq_nopeyment_rate_score b on a.uid = b.uid
left join cfbdb.vqq_stability_score c on a.uid = c.uid
left join cfbdb.vqq_creditpay_max_score d on a.uid = d.uid;

use cfbdb;
create table if not exists vqq_user_model(
	uid string,
	total_score int,
	risk_score double
);
insert overwrite table cfbdb.vqq_user_model
select a.uid, total_score,prob
from cfbdb.vqq_user_model_score a
 inner join	cfbdb.user_risk_all_ctirp b on lower(trim(a.uid)) = lower(trim(b.user_id))
--where (a.total_score <=540 and prob<0.3) or (a.total_score >540 and prob<0.6);
--where (a.total_score <=605 and prob<0.3) or (a.total_score >605 and prob<0.6);
where (a.total_score <=590 and prob<0.3) or (a.total_score >590 and prob<0.6);

