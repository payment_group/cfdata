use tmp_innofin;
drop table tmp_innofin.cfbdb_vqq_user_prefix_model_pageview;
create table if not exists cfbdb_vqq_user_prefix_model_pageview (
	 uid string,
	 starttime string,
	 prepagecode string,
	 pagecode string,
	 vid string,
	 cityid string,
	 provincename string,
	 cityname string,
	 d string
) partitioned by (month  string);

insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model_pageview partition(month = '2017-06') 
select a.uid,starttime , prepagecode , pagecode , vid , cityid , provincename , cityname , d  from tmp_innofin.cfbdb_vqq_user_prefix_model a 
inner join dw_mobdb.factmbpageview b on lower(a.uid) = lower(b.uid)
inner join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid)
where b.d between '2017-06-01' and '2017-06-30';

insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model_pageview partition(month = '2017-07') 
select a.uid,starttime , prepagecode , pagecode , vid , cityid , provincename , cityname , d  from tmp_innofin.cfbdb_vqq_user_prefix_model a 
inner join dw_mobdb.factmbpageview b on lower(a.uid) = lower(b.uid) 
inner join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid)
where b.d between '2017-07-01' and '2017-07-31';

insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model_pageview partition(month = '2017-08') 
select a.uid,starttime , prepagecode , pagecode , vid , cityid , provincename , cityname , d  from tmp_innofin.cfbdb_vqq_user_prefix_model a 
inner join dw_mobdb.factmbpageview b on lower(a.uid) = lower(b.uid) 
inner join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid)
where b.d between '2017-08-01' and '2017-08-31';


use tmp_innofin;
drop table tmp_innofin.cfbdb_vqq_user_prefix_model_uv_bymonth
create table if not exists cfbdb_vqq_user_prefix_model_uv_bymonth (
	 uid string,
	 pagecode string,
	 pagename string
) partitioned by (month  string);

insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model_uv_bymonth partition(month = '2017-06') 
select distinct a.uid,a.pagecode,b.pagename from tmp_innofin.cfbdb_vqq_user_prefix_model_pageview a
inner join dim_mobdb.dimmbpagecode b on a.pagecode = b.pagecode
where month = '2017-06';

insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model_uv_bymonth partition(month = '2017-07') 
select distinct a.uid,a.pagecode,b.pagename from tmp_innofin.cfbdb_vqq_user_prefix_model_pageview a
inner join dim_mobdb.dimmbpagecode b on a.pagecode = b.pagecode
where month = '2017-07';

insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model_uv_bymonth partition(month = '2017-08') 
select distinct a.uid,a.pagecode,b.pagename from tmp_innofin.cfbdb_vqq_user_prefix_model_pageview a
inner join dim_mobdb.dimmbpagecode b on a.pagecode = b.pagecode
where month = '2017-08';
-- ��Ծ
select month,pagecode,count(distinct uid,pagecode) as uv from tmp_innofin.cfbdb_vqq_user_prefix_model_uv_bymonth group by month,pagecode order by uv limit 100;
select * from (
select month,pagecode,count(distinct uid,pagecode) as uv , row_num() over (partition by month order by count(distinct uid,pagecode) desc)) as rank
 from tmp_innofin.cfbdb_vqq_user_prefix_model_uv_bymonth group by month,pagecode) a where a.rank <=10 ;

-- BU
select month,bu,count(distinct uid) from (
select month,uid, case when pagename like '%�Ƶ�%' then '�Ƶ�' when pagename like '%��Ʊ%' then '��Ʊ' when pagename like '%��%' then '��Ʊ' else '����' end as bu
  from tmp_innofin.cfbdb_vqq_user_prefix_model_uv_bymonth) a group by month,bu;
