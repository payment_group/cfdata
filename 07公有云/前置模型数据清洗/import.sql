use tmp_innofin;
create table if not exists cfbdb_vqq_user_model (uid string) STORED AS TEXTFILE;
create table if not exists cfbdb_vqq_user_prefix (uid string) STORED AS TEXTFILE;
use tmp_innofin;
create table if not exists cfbdb_vqq_user_prefix_model (uid string);
insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model
select a.uid from tmp_innofin.cfbdb_vqq_user_model a 
inner join tmp_innofin.cfbdb_vqq_user_prefix b on a.uid = b.uid;