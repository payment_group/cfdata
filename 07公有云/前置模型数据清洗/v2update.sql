-- backup old data
use tmp_innofin;
create table if not exists cfbdb_vqq_user_model_v1 (uid string);
create table if not exists cfbdb_vqq_user_prefix_v1 (uid string);
create table if not exists cfbdb_vqq_user_prefix_model_v1 (uid string);

insert overwrite table tmp_innofin.cfbdb_vqq_user_model_v1 select a.uid from tmp_innofin.cfbdb_vqq_user_model a;
insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_v1 select a.uid from tmp_innofin.cfbdb_vqq_user_prefix a;
insert overwrite table tmp_innofin.cfbdb_vqq_user_prefix_model_v1 select a.uid from tmp_innofin.cfbdb_vqq_user_prefix_model a;

-- upload cfbdb_vqq_user_model_v1,cfbdb_vqq_user_prefix_v1 to cloud
-- get diff
use cfbdb;
create table vqq_user_prefix_uid_insert as 
select a.uid from cfbdb.vqq_user_prefix_uid a 
left join cfbdb.cfbdb_vqq_user_prefix_v1 b on a.uid = b.uid where b.uid is null;

create table vqq_user_prefix_uid_delete as 
select b.uid from cfbdb.vqq_user_prefix_uid a 
right join cfbdb.cfbdb_vqq_user_prefix_v1 b on a.uid = b.uid where a.uid is null;

create table vqq_user_model_uid_insert as 
select a.uid from cfbdb.vqq_user_model_uid a 
left join cfbdb.cfbdb_vqq_user_model_v1 b on a.uid = b.uid where b.uid is null;

create table vqq_user_model_uid_delete as 
select b.uid from cfbdb.vqq_user_model_uid a 
right join cfbdb.cfbdb_vqq_user_model_v1 b on a.uid = b.uid where a.uid is null;