use tmp_innofin;
create table if not exists vqq_nqh_blocked_order(
	id bigint,
	user_id string,
	order_no string,
	block_reason string,
	order_time string
)partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_nqh_blocked_order partition(dt='${zdt.format("yyyy-MM-dd")}')
select id,user_id,order_no,block_reason,order_time
from fin_basic_data.c_xj_op_cfom_tbl_blocked_order where d='${zdt.format("yyyy-MM-dd")}';

-- 公有云
-- 选取第一条原因
use cfbdb;
create table vqq_nqh_blocked_order_reason
(	
	id bigint,
	user_id string,
	order_no string,
	block_reason string,
	order_time string
);

use cfbdb;
insert overwrite table vqq_nqh_blocked_order_reason
select id,user_id,order_no, block_reason,order_time from (
	select id,user_id,order_no,regexp_replace(regexp_replace(block_reason,'null,',''),',.+','') as block_reason,order_time, ROW_NUMBER() over 
	(partition by order_no order by id) as rn
    from cfbdb.vqq_nqh_blocked_order where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}') a
    where a.rn = 1;

use cfbdb;
drop table vqq_nqh_blocked_order_reason_list;
create table vqq_nqh_blocked_order_reason_list(
	 reason string,
	category string

);
insert overwrite table vqq_nqh_blocked_order_reason_list values
('模型拦截类','消金风控'),
('用户维度频次限额类','消金风控'),
('代理商维度频次限额类','消金风控'),
('用户黑名单','消金风控'),
('风险类策略','消金风控'),
('代理商黑名单','消金风控'),
('合作方资金限额类策略','消金风控'),
('业务线不支持','消金风控'),
('其他业务类策略','消金风控'),
('其他风控拦截类','消金风控'),
('资金方通道黑名单','消金风控'),
('系统异常','消金风控'),
('存在逾期账单','分期试算'),
('支付开关已关闭','分期试算'),
('当前无可用贷款通道','分期试算'),
('合同逾期','分期试算'),
('合同不可用','分期试算'),
('用户可用余额不足（系统漏洞）','分期试算'),
('风控拒绝（分期试算）','分期试算');

use cfbdb;
drop table vqq_orders_201711;
create table vqq_orders_201711 as 
select distinct uid,orderid,dt from cfbdb.ctrip_cloud_uid_allorder_info where dt >= '2017-11-01' and orderstatus = 10;

use cfbdb;
drop table vqq_tmp_1;
create table vqq_tmp_1 as 
select dt,category,reason,count(1) as c from 
vqq_orders_201711 a
inner join vqq_nqh_blocked_order_reason b on a.orderid = b.order_no 
left join vqq_nqh_blocked_order_reason_list c on b.block_reason = c.reason
group by dt,category,reason;