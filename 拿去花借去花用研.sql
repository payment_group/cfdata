use tmp_innofin;
create table vqq_nqh_yy_1108(
	uid string,
	phone_decrypt string
) 
FIELDS TERMINATED BY '\t
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;


use tmp_innofin;
create table vqq_jqh_yy_1108(
	uid string,
	gender int,
	phone_decrypt string,
	email string

) 
FIELDS TERMINATED BY '\t
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;

-- load data

-- @强哥，忠源数据已给到，请按照以往群分消息发送数据包格式，同步至魔方数据库，数据包号如下：
-- 拿去花用研：Sfif171114e1
-- 借趣花用研：Sfif171114e2

use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif171114e1',a.uid ,'','SMS','','FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.vqq_nqh_yy_1108 a;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif171114e1','SMS','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;


use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif171114e2',a.uid ,'','SMS','','FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.vqq_jqh_yy_1108 a;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif171114e2','SMS','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;


--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Sfif171114e1','2017-11-14','2017-11-24'),
('Sfif171114e2','2017-11-14','2017-11-24');