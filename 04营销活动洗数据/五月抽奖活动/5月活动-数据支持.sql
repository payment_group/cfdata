-- 五月活动推广需手工取数支持；
-- 1、每周一，需更新【已申请用户】的列表（用于数据魔方取数剔除用）；

-- 2、两个短信批次：Sfif170504a1 及 Sfif170506a1；需手工取数；
-- 推送量：分别为20W；
-- 取数要求：*已通过预授权但未申请开通拿去花*的用户；
-- 取数完成时间：4月28日完成；

use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail add if not exists partition (d='2017-04-28');
insert overwrite table tmp_innofin.factmarketingsenddetail partition(d='2017-04-28')
select 'Sfif170504a1',a.user_id,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
where a.contract_status = '4' and  a.req_src = 'PRE_CREDIT_TASK'
;

alter table tmp_innofin.factmarketingconfiguredetail add if not exists  partition (d='2017-04-28');
insert overwrite table tmp_innofin.factmarketingconfiguredetail partition(d='2017-04-28')
select 'Sfif170504a1','SMS','FIN',
0,1,1,0,1,1,0,0,
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
;


