-- 导入有资格的订单数据
use ods_innofin;
CREATE TABLE ods_innofin.activity_thursday_rebate (
  id bigint ,
  user_id string  COMMENT '携程用户编号',
  open_id string  COMMENT '去哪儿与携程公用的用户编号',
  preauth_apply_no string  COMMENT '预授权申请流水号',
  preauth_confirm_no string  COMMENT '预授权确认流水号',
  preauth_confirm_amt float  COMMENT '预授权金额',
  confirm_date date   COMMENT '预授权确认时间',
  rebate_amt float  COMMENT '返券金额',
  biz_order_no string COMMENT '业务订单号',
  order_amt float  COMMENT '订单实际金额',
  status int COMMENT '状态',
  remark string  COMMENT '备注',
  create_time string   COMMENT '创建时间',
  update_time string   COMMENT '修改时间');
 ----------------------------------------------------------------------------------------
 --导数
################################################################注释代码
:<<EOF
名称 ：activity_thursday_rebate
功能描述 ：从activity_thursday_rebate导入到activity_thursday_rebate
输入表 ：pay_cfb.activity_thursday_rebate
输出表 ：ods_innofin.activity_thursday_rebate
创建人：vqq钱强
创建时间：2017-4-5
运行类型 ： 注意事项 ：
说明 ：
修改历史 ：修改人    / 修改时间  / 主要改动说明
      1. vqq钱强 / 2017-4-5 / 创建流程
EOF
#################################################################注释代码

set -eu
sh datax_run.sh \
  -src mysqldal \
  -srcallinone pay_cfb_ETLZS_SH \
  -srcdb pay_cfb \
  -srctblnames activity_thursday_rebate \
  -querys "select id,user_id,open_id,preauth_apply_no,preauth_confirm_no,preauth_confirm_amt,confirm_date,rebate_amt,biz_order_no,order_amt,status,remark,create_time,update_time from activity_thursday_rebate" \
  -pks id \
  -tar hdfs \
  -tardb ods_innofin \
  -tartblnames activity_thursday_rebate \
  -loadtype 1
----------------------------------------------------------------------------------------
-- 出行时间
use tmp_innofin;
drop table if exists tmp_innofin.finishtime_vacation;
CREATE TABLE tmp_innofin.finishtime_vacation (
  order_no string COMMENT '业务订单号',
  dt string comment '订单日期',
  finish_time string COMMENT '出行日期'
);

insert overwrite table tmp_innofin.finishtime_vacation
select distinct orderid,dt,
       from_unixtime(cast(regexp_extract(content,'ReturnDate":"/Date\\(([0-9]{10})000',1) as bigint),'yyyy-MM-dd HH:mm:ss')
 from bbz_oi_orderdb.bbz_oi_order_vacation
 where dt in ('2017-04-06','2017-04-13','2017-04-20','2017-04-27')
   and dt <  '${zdt.format("yyyy-MM-dd")}'
   and regexp_extract(content,'OrderType":"([^"]+)",',1) = '团队游'
   and regexp_extract(content,'OrderStatus":"([^"]+)",',1) <> '0';

----机票
use tmp_innofin;
drop table if exists tmp_innofin.finishtime_flight;
CREATE TABLE tmp_innofin.finishtime_flight (
  order_no string COMMENT '业务订单号',
  dt string comment '订单日期',
  finish_time string COMMENT '出行日期'
);

insert overwrite table tmp_innofin.finishtime_flight
select orderid,dt,
       from_unixtime(cast(regexp_extract(content,'ArrivalTime":"/Date\\(([0-9]{10})000',1) as bigint),'yyyy-MM-dd HH:mm:ss')
  from bbz_oi_orderdb.bbz_oi_order_flight
 where dt in ('2017-04-06','2017-04-13','2017-04-20','2017-04-27')
   and dt <  '${zdt.format("yyyy-MM-dd")}';
--火车票
use tmp_innofin;
drop table if exists tmp_innofin.finishtime_train;
CREATE TABLE tmp_innofin.finishtime_train (
  order_no string COMMENT '业务订单号',
  dt string comment '订单日期',
  finish_time string COMMENT '出行日期'
);
insert overwrite table tmp_innofin.finishtime_train
select orderid,dt,
       from_unixtime(cast(regexp_extract(content,'ArrivalDateTime":"/Date\\(([0-9]{10})000',1) as bigint),'yyyy-MM-dd HH:mm:ss') 
  from bbz_oi_orderdb.bbz_oi_order_trains
 where dt in ('2017-04-06','2017-04-13','2017-04-20','2017-04-27')
   and dt <  '${zdt.format("yyyy-MM-dd")}';
--酒店
use tmp_innofin;
drop table if exists tmp_innofin.finishtime_hotel;
CREATE TABLE tmp_innofin.finishtime_hotel (
  order_no string COMMENT '业务订单号',
  dt string comment '订单日期',
  finish_time string COMMENT '出行日期'
);
insert overwrite table tmp_innofin.finishtime_hotel
select orderid,dt,
       from_unixtime(cast(regexp_extract(content,'DepartureTime":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd HH:mm:ss')
  from bbz_oi_orderdb.bbz_oi_order_hotel
 where dt in ('2017-04-06','2017-04-13','2017-04-20','2017-04-27')
   and dt <  '${zdt.format("yyyy-MM-dd")}';

-- 合并到总表
use tmp_innofin;
drop table if exists tmp_innofin.activity_thursday_finishtime;
CREATE TABLE tmp_innofin.activity_thursday_finishtime (
  order_no string COMMENT '业务订单号',
  order_type string COMMENT '业务订单类型',
  finish_time string COMMENT '出行日期'
);

insert into table tmp_innofin.activity_thursday_finishtime
	select order_no,'团队游', max(finish_time) from tmp_innofin.finishtime_vacation group by order_no;
insert into table tmp_innofin.activity_thursday_finishtime
	select order_no,'机票', max(finish_time) from tmp_innofin.finishtime_flight group by order_no;
insert into table tmp_innofin.activity_thursday_finishtime
	select order_no,'火车票', max(finish_time) from tmp_innofin.finishtime_train group by order_no;
insert into table tmp_innofin.activity_thursday_finishtime
	select order_no,'酒店', max(finish_time) from tmp_innofin.finishtime_hotel group by order_no;
	
-- 实际支付
use tmp_innofin;
drop table if exists tmp_innofin.activity_thursday_payment;
CREATE TABLE tmp_innofin.activity_thursday_payment (
  orderid string COMMENT '业务订单号',
  gatheringtype string COMMENT '支付标记',
  totalamount float COMMENT '金额'
);
-- 拉有效数据
insert overwrite table tmp_innofin.activity_thursday_payment
select orderid,gatheringtype,totalamount-COALESCE(payamt_coupon,0)-COALESCE(payamt_wallet,0) as totalamount from dw_paypubdb.FactPayGatheringMain_Loan
where applys_cnt>0;
-- 计算实际支付
use tmp_innofin;
CREATE TABLE if not exists tmp_innofin.activity_thursday_actualpayment (
  orderid string COMMENT '业务订单号',
  damount float COMMENT '支付',
  ramount float COMMENT '退款',
  actualamount float COMMENT '金额'
);
insert overwrite table tmp_innofin.activity_thursday_actualpayment
select orderid,
	sum(case when gatheringtype='D' then totalamount else 0 end), 
	sum(case when gatheringtype='R' then totalamount else 0 end), 
	sum(case when gatheringtype='D' then totalamount else -totalamount end)
from tmp_innofin.activity_thursday_payment group by orderid; 

-- 关联生成 最终表
use tmp_innofin;
-- drop table tmp_innofin.activity_thursday_rebate_ok;
CREATE TABLE if not exists tmp_innofin.activity_thursday_rebate_ok (
  userid string comment '用户id',
  orderid string COMMENT '业务订单号',
  actualamount float COMMENT '实际支付金额',
  create_time string comment '订单时间',
  order_type string COMMENT '业务订单类型',
  finish_time string COMMENT '出行日期'
)
 partitioned by(
	d 	string 	comment	'计算日期'
);

alter table tmp_innofin.activity_thursday_rebate_ok add if not exists partition(d='${zdt.format("yyyy-MM-dd")}');
insert overwrite table tmp_innofin.activity_thursday_rebate_ok partition(d='${zdt.format("yyyy-MM-dd")}')
select a.user_id,a.biz_order_no,c.actualamount,a.create_time,b.order_type,b.finish_time from 
	ods_innofin.activity_thursday_rebate a
	inner join tmp_innofin.activity_thursday_finishtime b on a.biz_order_no = b.order_no
	inner join tmp_innofin.activity_thursday_actualpayment c on a.biz_order_no = c.orderid
	left join (select user_id,create_time from ods_innofin.user_overdue_repay_plan where substring(create_time,1,10) = '${zdt.addDay(-1).format("yyyy-MM-dd")}') d on a.user_id = d.user_id
where a.status='0' and b.finish_time < '${zdt.addDay(-2).format("yyyy-MM-dd")}'
and c.actualamount >=100 and d.create_time is null;
-- test data
insert into tmp_innofin.activity_thursday_rebate_ok 
select uid,orderid,totalamount from ods_innofin.fact_loan_order where 
totalamount >100 limit 100;
