use cfbdb;
create table vqq_cruise_uids(
	uid string,
	merchant string
);

insert overwrite table cfbdb.vqq_cruise_uids
select distinct uid,merchant from cfbdb.finance_payment_sec_his
where merchant in('天海邮轮','度假-游轮') and dt > '${zdt.addDay(-32).format("yyyy-MM-dd")}' and billtype in('A','BING','D','G');

-- 统计
select a.merchant, count(a.uid),sum(if(b.uid is not null,1,0)), sum(if(b.uid is null and c.uid is not null,1,0)) from cfbdb.vqq_cruise_uids a
left join (select * from cfbdb.nqh_user_markting_flag where dt = '2017-08-06' and active_flag = 1) b on lower(a.uid) = lower(b.uid)
left join (select a.uid from cfbdb.vqq_user_prefix a inner join cfbdb.vqq_user_model b on a.uid = b.uid) c on lower(a.uid) = lower(c.uid)
group by a.merchant;