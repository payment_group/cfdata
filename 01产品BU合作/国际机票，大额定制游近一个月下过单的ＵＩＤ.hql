use tmp_innofin;
create table if not exists vqq_tmp_rm3420(
	uid string,
	overseas_flight int,
	customized int
);

insert overwrite table tmp_innofin.vqq_tmp_rm3420
select nvl(a.uid,b.uid),
	if(a.uid is null,0,1),
	if(b.uid is null,0,1)
from
(select distinct get_json_object(content, '$.Uid') as uid from bbz_oi_orderdb.bbz_oi_order_flight where dt >= add_months(current_date,-1) and get_json_object(content, '$.BizCategory') = 'FlightInternate') a
full outer join
(select distinct get_json_object(content, '$.Uid') as uid from bbz_oi_orderdb.bbz_oi_order_customized where dt >= add_months(current_date,-1)) b on lower(a.uid) = lower(b.uid);
 