-- 拿去花订单
use tmp_innofin;
create table if not exists vqq_user_value_bu(
	uid string comment 'uid',
	bu_value decimal(9,2) comment 'BU价值'
) comment '用户价值分BU' partitioned by (bu string comment 'bu');

insert overwrite table tmp_innofin.vqq_user_value_bu partition (bu = '机票') select uid, sum(totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036)) 
from ods_innofin.fact_loan_order_ist  where ordertypename = '机票' group by uid;
insert overwrite table tmp_innofin.vqq_user_value_bu partition (bu = '酒店') select uid, sum(totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036)) 
from ods_innofin.fact_loan_order_ist  where ordertypename = '酒店' group by uid;
insert overwrite table tmp_innofin.vqq_user_value_bu partition (bu = '火车票') select uid, sum(totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036)) 
from ods_innofin.fact_loan_order_ist  where ordertypename = '火车票' group by uid;
insert overwrite table tmp_innofin.vqq_user_value_bu partition (bu = '度假') select uid, sum(totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036)) 
from ods_innofin.fact_loan_order_ist  where ordertypename = '度假' group by uid;
insert overwrite table tmp_innofin.vqq_user_value_bu partition (bu = '其他') select uid, sum(totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036)) 
from ods_innofin.fact_loan_order_ist  where ordertypename not in('度假','机票','酒店','火车票') group by uid;

use tmp_innofin;
create table if not exists vqq_user_value_bu_annualized(
	uid string comment 'uid',
	active_date string comment '激活日',
	bu_value decimal(9,2) comment 'BU价值'
) comment '用户年化价值分BU' partitioned by (bu string comment 'bu');

insert overwrite table tmp_innofin.vqq_user_value_bu_annualized partition (bu='机票')
select a.uid,a.active_date, if(b.uid is null,0,365.25*bu_value/datediff(current_date,active_date)) 
from dw_innofin.vqq_nqh_user_active_date_snap a inner join tmp_innofin.vqq_user_value_bu b on lower(a.uid) = lower(b.uid) and bu='机票';
insert overwrite table tmp_innofin.vqq_user_value_bu_annualized partition (bu='酒店')
select a.uid,a.active_date, if(b.uid is null,0,365.25*bu_value/datediff(current_date,active_date)) 
from dw_innofin.vqq_nqh_user_active_date_snap a inner join tmp_innofin.vqq_user_value_bu b on lower(a.uid) = lower(b.uid) and bu='酒店';
insert overwrite table tmp_innofin.vqq_user_value_bu_annualized partition (bu='火车票')
select a.uid,a.active_date, if(b.uid is null,0,365.25*bu_value/datediff(current_date,active_date)) 
from dw_innofin.vqq_nqh_user_active_date_snap a inner join tmp_innofin.vqq_user_value_bu b on lower(a.uid) = lower(b.uid) and bu='火车票';
insert overwrite table tmp_innofin.vqq_user_value_bu_annualized partition (bu='度假')
select a.uid,a.active_date, if(b.uid is null,0,365.25*bu_value/datediff(current_date,active_date)) 
from dw_innofin.vqq_nqh_user_active_date_snap a inner join tmp_innofin.vqq_user_value_bu b on lower(a.uid) = lower(b.uid) and bu='度假';
insert overwrite table tmp_innofin.vqq_user_value_bu_annualized partition (bu='其他')
select a.uid,a.active_date, if(b.uid is null,0,365.25*bu_value/datediff(current_date,active_date)) 
from dw_innofin.vqq_nqh_user_active_date_snap a inner join tmp_innofin.vqq_user_value_bu b on lower(a.uid) = lower(b.uid) and bu='其他';


-- ART
-- 按活跃用户算，BU及总数
select '机票' as bu,count(1) as `活跃用户数`, cast(cast(avg(bu_value) as decimal(9,2)) as double) as `平均年化价值` from tmp_innofin.vqq_user_value_bu_annualized 
where bu = '机票' and bu_value >0 and active_date < add_months(current_date,-1)
union all
select '酒店' as bu,count(1) as `活跃用户数`, cast(cast(avg(bu_value) as decimal(9,2)) as double) as `平均年化价值` from tmp_innofin.vqq_user_value_bu_annualized 
where bu = '酒店' and bu_value >0 and active_date < add_months(current_date,-1)
union all
select '火车票' as bu,count(1) as `活跃用户数`, cast(cast(avg(bu_value) as decimal(9,2)) as double) as `平均年化价值` from tmp_innofin.vqq_user_value_bu_annualized 
where bu = '火车票' and bu_value >0 and active_date < add_months(current_date,-1)
union all
select '度假' as bu,count(1) as `活跃用户数`, cast(cast(avg(bu_value) as decimal(9,2)) as double) as `平均年化价值` from tmp_innofin.vqq_user_value_bu_annualized 
where bu = '度假' and bu_value >0 and active_date < add_months(current_date,-1)
union all
select '合计' as bu , count(distinct uid) as `活跃用户数` , cast(cast(sum(bu_value)/count(distinct uid) as decimal(9,2)) as double)  as `平均年化价值` 
from tmp_innofin.vqq_user_value_bu_annualized where active_date < add_months(current_date,-1) and bu not in ('其他')
-- BU活跃用户的年化价值分布
-- ftp 导出后python作图
select bu,uid,active_date,bu_value from tmp_innifin.vqq_user_value_bu_annualized where active_date < add_months(current_date,-1) and bu not in ('其他')




