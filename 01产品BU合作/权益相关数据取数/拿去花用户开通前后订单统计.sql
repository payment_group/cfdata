-- 拿去花用户订单统计
-- 1）用户
use cfbdb;
create table if not exists vqq_1712_users(
	uid string,
	register_date string,
	apply_active_date string,
	active_flag int,
	d1 string,
	d2 string,
	d3 string,
	d4 string
);
insert overwrite table cfbdb.vqq_1712_users 
select a.uid,signupdate,apply_active_date,active_flag,
add_months(apply_active_date,-12) as d1,add_months(current_date,-12) as d2,
apply_active_date as d3,current_date as d4
from 
cfbdb.nqh_user_markting_flag a 
inner join cfbdb.ctrip_cloud_uid_signup_info_snap b on a.dt = date_add(current_date,-1) and lower(a.uid) = lower(b.uid)
where apply_active_date < add_months(current_date,-3) and signupdate <= add_months(apply_active_date,-12)
and add_months(current_date,-12) <= apply_active_date;

-- 2）
use cfbdb;
create table if not exists vqq_1712_users_orders(
	uid string,
	active_flag int,
	order_id string,
	order_date string,
	order_amount decimal(9,2)
)partitioned by (before_after_flag int);

insert overwrite table cfbdb.vqq_1712_users_orders partition (before_after_flag = 0)
select a.uid,a.active_flag,orderid,to_date(orderdate),amount
from cfbdb.vqq_1712_users a 
inner join cfbdb.ctrip_cloud_uid_allorder_info_snap b on lower(a.uid) = lower(b.uid)
where b.orderstatustype = '支付' and to_date(orderdate) between d1 and d2;

insert overwrite table cfbdb.vqq_1712_users_orders partition (before_after_flag = 1)
select a.uid,a.active_flag,orderid,to_date(orderdate),amount
from cfbdb.vqq_1712_users a 
inner join cfbdb.ctrip_cloud_uid_allorder_info_snap b on lower(a.uid) = lower(b.uid)
where b.orderstatustype = '支付' and to_date(orderdate) between d3 and d4;

select active_flag,count(1) from cfbdb.vqq_1712_users group by active_flag;
select active_flag,before_after_flag,count(1),sum(order_amount) from cfbdb.vqq_1712_users_orders where before_after_flag in (0,1) group by active_flag,before_after_flag;


-- 3）
