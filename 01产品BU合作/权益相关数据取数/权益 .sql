--敏感类客户					活动敏感类潜在拿去花用户							携程APP端：1年内活跃用户+活动敏感用户+通过前置模型用户+未申请过拿去花用户（且）；分别按照火车票/机票/酒店/度假4个BU统计
--敏感类客户					积分敏感类潜在拿去花用户							携程APP端：1年内活跃用户+积分敏感用户+通过前置模型用户+未申请过拿去花用户（且）；分别按照火车票/机票/酒店/度假4个BU统计
-- 活动敏感、积分敏感定义不明确，暂停

--潜在可开通用户（分BU）		机票潜在拿去花用户									携程机票1年内/半年内活跃用户+未申请过拿去花+通过前置模型用户（且）；分国内/国际
--潜在可开通用户（分BU）		酒店潜在拿去花用户									携程酒店1年内/半年内活跃用户+未申请过拿去花+通过前置模型用户（且）；分国内/国际
--潜在可开通用户（分BU）		火车票潜在拿去花用户								携程火车票1年内/半年内活跃用户+未申请过拿去花+通过前置模型用户（且）
--潜在可开通用户（分BU）		度假潜在拿去花用户									携程度假1年内/半年内活跃用户+未申请过拿去花+通过前置模型用户（且）；分国内/国际
-- 公有云洗出 -- 做成一张表
-- 0) 目标表
use cfbdb;
drop table if exists vqq_nqh_potential_user_bu;
create table vqq_nqh_potential_user_bu(
	uid string,
	active_in_6m int
) partitioned by (bu string,domestic int);
-- 0) 未申请过拿去花+通过前置模型用户
use cfbdb;
drop table if exists vqq_nqh_potential_user;
create table vqq_nqh_potential_user as 
select a.uid
from cfbdb.vqq_user_prefix_uid a
inner join cfbdb.vqq_user_model_uid b on lower(a.uid) = lower(b.uid)
left join (select * from cfbdb.nqh_user_markting_flag where dt=date_add(current_date,-1)) c on lower(a.uid) = lower(c.uid)
where c.uid is null;

-- 1）一年内机票订单
use cfbdb;
drop table if exists vqq_user_flight_order_1y;
create table vqq_user_flight_order_1y as 
select a.uid,
 orderid,
 case when b.countryname == '中国' and c.countryname = '中国' then 1 else 0 end as domestic,
 a.createtime
from cfbdb.ctrip_cloud_uid_fltorder_detail_snap  a 
inner join cfbdb.ctrip_cloud_dim_airport b on a.dport = b.airport
inner join cfbdb.ctrip_cloud_dim_airport c on a.dport = c.airport
inner join cfbdb.vqq_nqh_potential_user d on lower(a.uid) = lower(d.uid)
where a.createtime >= add_months(current_date,-12) 
and orderstatustype in ('已支付');
-- 最后一条
use cfbdb;
drop table if exists vqq_user_last_flight_order_1y;
create table vqq_user_last_flight_order_1y as 
select uid,domestic,createtime from
(select uid,
 domestic,
 createtime,
row_number() over (partition by uid,domestic order by createtime desc) as rank
from cfbdb.vqq_user_flight_order_1y
) a where rank = 1;

-- 插入整合表
insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='flight',domestic=1)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_flight_order_1y where domestic=1;

insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='flight',domestic=0)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_flight_order_1y where domestic=0;

-- 机票统计
select domestic,count(1) as y1, sum(active_in_6m) as m6 from cfbdb.vqq_nqh_potential_user_bu where bu='flight' group by domestic;


-- 2）一年内酒店订单
use cfbdb;
drop table if exists vqq_user_hotel_order_1y;
create table vqq_user_hotel_order_1y as 
select a.uid,
 orderid,
 case when b.countryname == '中国' then 1 else 0 end as domestic,
 a.createtime
from cfbdb.ctrip_cloud_uid_htlorder_detail_snap  a 
inner join cfbdb.ctrip_cloud_dim_hotel b on a.hotel = b.hotel
inner join cfbdb.vqq_nqh_potential_user d on lower(a.uid) = lower(d.uid)
where a.createtime >= add_months(current_date,-12) 
and orderstatustype in ('已支付');
-- 最后一条
use cfbdb;
drop table if exists vqq_user_last_hotel_order_1y;
create table vqq_user_last_hotel_order_1y as 
select uid,domestic,createtime from
(select uid,
 domestic,
 createtime,
row_number() over (partition by uid,domestic order by createtime desc) as rank
from cfbdb.vqq_user_hotel_order_1y
) a where rank = 1;

insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='hotel',domestic=1)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_hotel_order_1y where domestic=1;

insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='hotel',domestic=0)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_hotel_order_1y where domestic=0;

-- 酒店统计
select domestic,count(1) as y1, sum(active_in_6m) as m6 from cfbdb.vqq_nqh_potential_user_bu where bu='hotel' group by domestic;

-- 3）火车票订单
use cfbdb;
drop table if exists vqq_user_train_order_1y;
create table vqq_user_train_order_1y as 
select a.uid,
 orderid,
 1 as domestic,
 a.createtime
from cfbdb.ctrip_cloud_uid_trnorder_detail_snap  a 
inner join cfbdb.vqq_nqh_potential_user d on lower(a.uid) = lower(d.uid)
where a.createtime >= add_months(current_date,-12) 
and orderstatustype in ('已支付');
-- 最后一条
use cfbdb;
drop table if exists vqq_user_last_train_order_1y;
create table vqq_user_last_train_order_1y as 
select uid,domestic,createtime from
(select uid,
 domestic,
 createtime,
row_number() over (partition by uid,domestic order by createtime desc) as rank
from cfbdb.vqq_user_train_order_1y
) a where rank = 1;

insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='train',domestic=1)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_train_order_1y where domestic=1;

insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='train',domestic=0)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_train_order_1y where domestic=0;

-- 火车票统计
select domestic,count(1) as y1, sum(active_in_6m) as m6 from cfbdb.vqq_nqh_potential_user_bu where bu='train' group by domestic;


-- 4）一年内度假订单
use cfbdb;
drop table if exists vqq_user_package_order_1y;
create table vqq_user_package_order_1y as 
select a.uid,
 orderid,
 case when b.countryname == '中国' and c.countryname = '中国' then 1 else 0 end as domestic,
 a.createtime
from cfbdb.ctrip_cloud_uid_pkgorder_detail_snap  a 
inner join cfbdb.ctrip_cloud_dim_city b on a.startcityid = b.cityid
inner join cfbdb.ctrip_cloud_dim_city c on a.destcityid = c.cityid
inner join cfbdb.vqq_nqh_potential_user d on lower(a.uid) = lower(d.uid)
where a.createtime >= add_months(current_date,-12) 
and orderstatustype in ('支付');
-- 最后一条
use cfbdb;
drop table if exists vqq_user_last_package_order_1y;
create table vqq_user_last_package_order_1y as 
select uid,domestic,createtime from
(select uid,
 domestic,
 createtime,
row_number() over (partition by uid,domestic order by createtime desc) as rank
from cfbdb.vqq_user_package_order_1y
) a where rank = 1;

-- 插入整合表
insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='package',domestic=1)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_package_order_1y where domestic=1;

insert overwrite table cfbdb.vqq_nqh_potential_user_bu partition (bu='package',domestic=0)
select uid,if(createtime >= add_months(current_date,-6),1,0) from cfbdb.vqq_user_last_package_order_1y where domestic=0;

-- 度假统计
select domestic,count(1) as y1, sum(active_in_6m) as m6 from cfbdb.vqq_nqh_potential_user_bu where bu='package' group by domestic;

--------------------------------------------------------------------------------------------

--已开通						单个用户预估半年度收益								拿去花全量用户中开通时间半年以上的用户最近半年的收益平均值
-- 半年度拿去花订单
use tmp_innofin;
drop table if exists tmp_innofin.vqq_user_value_order_6m;
CREATE TABLE tmp_innofin.vqq_user_value_order_6m (
	uid	string,	
	orderid	bigint,	
	amt_dr	double,	
	installmentnum	int,	
	totalamount	float,	
	creationdate string	
);
insert overwrite table tmp_innofin.vqq_user_value_order_6m 
select b.uid,orderid,nvl(amt_dr,0)),nvl(installmentnum,0),nvl(totalamount,0),creationdate from
ods_innofin.fact_loan_order_ist a 
right join dw_innofin.vqq_nqh_user_active_date_snap b on lower(a.uid) = lower(b.uid)
where b.active_date is null or b.active_date <= add_months(current_date,-6);


use tmp_innofin;
drop table vqq_user_value_6m;
create table if not exists vqq_user_value_6m(
	uid string,
	order_value_amount decimal(18,2),
	install_amount decimal(18,2),
	user_value_amount decimal(18,2)
);
insert overwrite table tmp_innofin.vqq_user_value_6m
select uid, sum(totalamount*0.003), sum(if(installmentnum=0,0,amt_dr) * 0.0036), sum(totalamount*0.003) + sum(if(installmentnum=0,0,amt_dr) * 0.0036)  
	from tmp_innofin.vqq_user_value_order_6m 
	group by uid;

--均值
select avg(user_value_amount) from tmp_innofin.vqq_user_value_6m;
--分布
--
select '0' as `区间`, count(1) as `用户数` from tmp_innofin.vqq_user_value_6m where user_value_amount =0
union all
select '0-10' as `区间`, count(1) as `用户数` from tmp_innofin.vqq_user_value_6m where user_value_amount > 0 and user_value_amount <=10
union all
select '10~30' as `区间`, count(1) as `用户数` from tmp_innofin.vqq_user_value_6m where user_value_amount > 10 and user_value_amount <=30
union all
select '30~50' as `区间`, count(1)as `用户数` from tmp_innofin.vqq_user_value_6m where user_value_amount > 30 and user_value_amount <=50
union all
select '50~100' as `区间`, count(1)as `用户数` from tmp_innofin.vqq_user_value_6m where user_value_amount > 50 and user_value_amount <=100
union all
select '100+' as `区间`, count(1)as `用户数` from tmp_innofin.vqq_user_value_6m where user_value_amount > 100
order by `区间` limit 10



--各BU平均用户价值（年化）	拿去花机票活跃用户-用户价值							1、均值；2、分布情况（同周报）
--各BU平均用户价值（年化）	拿去花酒店活跃用户-用户价值							1、均值；3、分布情况（同周报）
--各BU平均用户价值（年化）	拿去花火车票活跃用户-用户价值						1、均值；4、分布情况（同周报）
--各BU平均用户价值（年化）	拿去花度假活跃用户-用户价值							1、均值；5、分布情况（同周报）
-- 拿去花订单
use dw_innofin;
CREATE TABLE vqq_user_value_order (
	uid	string comment 'uid',	
	active_date string comment '激活日期',
	orderid	bigint comment '订单号',	
	amt_dr	double comment '分期金额',	
	installmentnum	int comment '期数',	
	totalamount	float comment '订单金额',	
	creationdate string comment '创建日',
	merchant string comment 'BU' 	
) comment '拿去花订单';

insert overwrite table dw_innofin.vqq_user_value_order 
select b.uid,active_date, orderid,nvl(amt_dr,0),nvl(installmentnum,0),nvl(totalamount,0),creationdate,
case when ordertypename in('度假','机票','酒店','火车票') then ordertypename else '其他' end
 from
ods_innofin.fact_loan_order_ist a 
inner join dw_innofin.vqq_nqh_user_active_date_snap b on lower(a.uid) = lower(b.uid);

use dw_innofin;
create table vqq_user_value_bu(
	uid string comment 'uid',
	active_date string comment '激活日期',
	value_flight decimal(9,2) comment '机票价值',
	value_hotel decimal(9,2) comment '酒店价值',
	value_train decimal(9,2) comment '火车票价值',
	value_vacation decimal(9,2) comment '度假价值',
	value_others decimal(9,2) comment '其他价值'
) comment '用户价值分BU';
insert overwrite table dw_innofin.vqq_user_value_bu
select uid,active_date,
	sum(if(merchant = '机票', totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036),0)),
	sum(if(merchant = '酒店', totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036),0)),
	sum(if(merchant = '火车票', totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036),0)),
	sum(if(merchant = '度假', totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036),0)),
	sum(if(merchant = '其他', totalamount*0.003 + if(installmentnum=0,0,amt_dr * 0.0036),0))
from dw_innofin.vqq_user_value_order group by uid,active_date;

use dw_innofin;
create table vqq_user_value_annualized(
	uid string comment 'uid',
	active_date string comment '激活日',
	value_flight decimal(9,2) comment '机票价值',
	value_hotel decimal(9,2) comment '酒店价值',
	value_train decimal(9,2) comment '火车票价值',
	value_vacation decimal(9,2) comment '度假价值',
	value_others decimal(9,2) comment '其他价值'
) comment '用户年化价值分BU';
insert overwrite table dw_innofin.vqq_user_value_annualized
select uid,active_date,
	365.25*value_flight/datediff(current_date,active_date),
	365.25*value_hotel/datediff(current_date,active_date),
	365.25*value_train/datediff(current_date,active_date),
	365.25*value_vacation/datediff(current_date,active_date),
	365.25*value_others/datediff(current_date,active_date)
from dw_innofin.vqq_user_value_bu;

-- BU活跃用户的年化价值
select 'flight' as bu, count(1) as users, avg(value_flight) as buvalue,avg(value_flight+value_hotel+value_train+value_vacation+value_others) as totalvalue from dw_innofin.vqq_user_value_annualized where value_flight > 0 and active_date < add_months(current_date,-1)
union all
select 'hotel' as bu, count(1) as users, avg(value_hotel) as buvalue,avg(value_flight+value_hotel+value_train+value_vacation+value_others) as totalvalue from dw_innofin.vqq_user_value_annualized where value_hotel > 0 and active_date < add_months(current_date,-1)
union all
select 'train' as bu, count(1) as users, avg(value_train) as buvalue,avg(value_flight+value_hotel+value_train+value_vacation+value_others) as totalvalue from dw_innofin.vqq_user_value_annualized where value_train > 0 and active_date < add_months(current_date,-1)
union all
select 'vacation' as bu, count(1) as users, avg(value_vacation) as buvalue,avg(value_flight+value_hotel+value_train+value_vacation+value_others) as totalvalue from dw_innofin.vqq_user_value_annualized where value_vacation > 0 and active_date < add_months(current_date,-1);

