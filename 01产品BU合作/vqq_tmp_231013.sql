select d,count(1) from
	(		select distinct d,a.uid from dw_mobdb.factmbpageview a inner join tmp_innofin.cfbdb_vqq_user_prefix_model b on lower(a.uid) = lower(b.uid)
		where d >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' and pagecode = '231013'
		) c group by d


use tmp_innofin;
drop table vqq_tmp_231013;
create table vqq_tmp_231013 as 
select distinct uid from dw_mobdb.factmbpageview where d >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' and pagecode = '231013';

use tmp_innofin;
create table vqq_tmp_231013_2 as 
select a.uid,if(b.uid is null,0,1) as f1,if(c.user_id is null,1,0) as f2 from tmp_innofin.vqq_tmp_231013 a
left join tmp_innofin.cfbdb_vqq_user_prefix_model b on lower(a.uid) = lower(b.uid)
left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) and c.contract_status = 1

select count(a.uid), sum(f1*f2) from tmp_innofin.vqq_tmp_231013_2