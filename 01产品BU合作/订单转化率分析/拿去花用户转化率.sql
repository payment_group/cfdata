-- 拿去花用户激活时间
use tmp_innofin;
create table if not exists vqq_nqh_user_active_date
(
	uid string,
	active_date string,
	active_ym string
);
insert overwrite table tmp_innofin.vqq_nqh_user_active_date
select uid, apply_active_date,substring(apply_active_date,1,7) as active_ym 
from dw_innofin.nqh_user_apply_active_date where dt= date_add(current_date,-1);

-- 拿去花用户所有订单，月统计数量
use tmp_innofin;
create table if not exists vqq_nqh_user_order_count_monthly
(
	uid string,
	ym string,
	bu string,
	order_count int,
	order_amount decimal(9,2)
);

insert overwrite table tmp_innofin.vqq_nqh_user_order_count_monthly
select uid, substring(orderdate,1,7) as ym , type as bu,
count(1) as order_count,sum(amount) as order_amount 
from temp_travelmoneydb.tmp_qmwang_nqhorders  
	group by uid, substring(orderdate,1,7), type;
	

-- uv数据 ubt 
use tmp_innofin;
create table if not exists vqq_nqh_user_query_page_uv_monthly
(
	uid string,
	ym string,
	uv_date_count int
)partitioned by (bu string);


insert overwrite table  tmp_innofin.vqq_nqh_user_query_page_uv_monthly partition(bu='flight')
select b.uid,substring(starttime,1,7),count(distinct to_date(starttime))
from tmp_innofin.vqq_nqh_user_active_date a
inner join dw_mobdb.factmbpageview b on lower(a.uid) = lower(b.uid) and b.d between '2016-01-01' and current_date 
inner join (select pagecode from dim_mobdb.dimmbpagecode where categoryname like '%机票%' and (pagename like '%国内机票查询%' or pagename like '%国际机票查询%')) c on b.pagecode = c.pagecode
group by b.uid,substring(starttime,1,7);

insert overwrite table  tmp_innofin.vqq_nqh_user_query_page_uv_monthly partition(bu='hotel')
select b.uid,substring(starttime,1,7),count(distinct to_date(starttime))
from tmp_innofin.vqq_nqh_user_active_date a
inner join dw_mobdb.factmbpageview b on lower(a.uid) = lower(b.uid) and b.d between '2016-01-01' and current_date 
inner join (select pagecode from dim_mobdb.dimmbpagecode where categoryname like '%酒店%' and (pagename like '%国内酒店查询页%' or pagename like '%海外酒店查询页%')) c on b.pagecode = c.pagecode
group by b.uid,substring(starttime,1,7);

insert overwrite table  tmp_innofin.vqq_nqh_user_query_page_uv_monthly partition(bu='train')
select b.uid,substring(starttime,1,7),count(distinct to_date(starttime))
from tmp_innofin.vqq_nqh_user_active_date a
inner join dw_mobdb.factmbpageview b on lower(a.uid) = lower(b.uid) and b.d between '2016-01-01' and current_date 
inner join (select pagecode from dim_mobdb.dimmbpagecode where categoryname like '%火车票%' and (pagename like '%查询%')) c on b.pagecode = c.pagecode
group by b.uid,substring(starttime,1,7);

-- 按月计算
use tmp_innofin;
create table if not exists vqq_nqh_user_order_count_uv_monthly
(
	uid string,
	active_ym string,
	ym string,
	order_count int,
	uv_date_count int
)partitioned by (bu string);

insert overwrite table  tmp_innofin.vqq_nqh_user_order_count_uv_monthly partition(bu='flight')
select a.uid,a.active_ym,b.ym,
nvl(order_count,0),
nvl(uv_date_count,0) from 
tmp_innofin.vqq_nqh_user_active_date a 
left join (select * from tmp_innofin.vqq_nqh_user_order_count_monthly where ym >='2016-01') b on lower(a.uid) = lower(b.uid) and b.bu='flt'
left join (select * from tmp_innofin.vqq_nqh_user_query_page_uv_monthly where ym >='2016-01') c on lower(a.uid) = lower(c.uid) and b.ym = c.ym and c.bu='flight';

insert overwrite table  tmp_innofin.vqq_nqh_user_order_count_uv_monthly partition(bu='hotel')
select a.uid,a.active_ym,b.ym,
nvl(order_count,0),
nvl(uv_date_count,0) from 
tmp_innofin.vqq_nqh_user_active_date a 
left join (select * from tmp_innofin.vqq_nqh_user_order_count_monthly where ym >='2016-01') b on lower(a.uid) = lower(b.uid) and b.bu='htl'
left join (select * from tmp_innofin.vqq_nqh_user_query_page_uv_monthly where ym >='2016-01') c on lower(a.uid) = lower(c.uid) and b.ym = c.ym and c.bu='hotel';


insert overwrite table  tmp_innofin.vqq_nqh_user_order_count_uv_monthly partition(bu='train')
select a.uid,a.active_ym,b.ym,
nvl(order_count,0),
nvl(uv_date_count,0) from 
tmp_innofin.vqq_nqh_user_active_date a 
left join (select * from tmp_innofin.vqq_nqh_user_order_count_monthly where ym >='2016-01') b on lower(a.uid) = lower(b.uid) and b.bu='trn'
left join (select * from tmp_innofin.vqq_nqh_user_query_page_uv_monthly where ym >='2016-01') c on lower(a.uid) = lower(c.uid) and b.ym = c.ym and c.bu='train';

-- 转化率
use tmp_innofin;
create table if not exists vqq_nqh_user_order_count_uv_rate
(
	uid string,
	active_ym string,
	rate_before double,
	rate_after double
)partitioned by (bu string);

insert overwrite table tmp_innofin.vqq_nqh_user_order_count_uv_rate partition(bu='flight')
select uid,active_ym, sum(if(ym <= active_ym,order_count,0))/sum(if(ym <= active_ym,uv_date_count,0)),
sum(if(ym >= active_ym,order_count,0))/sum(if(ym >= active_ym,uv_date_count,0)) from tmp_innofin.vqq_nqh_user_order_count_uv_monthly 
where bu='flight' group by uid,active_ym;

insert overwrite table tmp_innofin.vqq_nqh_user_order_count_uv_rate partition(bu='hotel')
select uid,active_ym, sum(if(ym <= active_ym,order_count,0))/sum(if(ym <= active_ym,uv_date_count,0)),
sum(if(ym >= active_ym,order_count,0))/sum(if(ym >= active_ym,uv_date_count,0)) from tmp_innofin.vqq_nqh_user_order_count_uv_monthly 
where bu='hotel' group by uid,active_ym;

insert overwrite table tmp_innofin.vqq_nqh_user_order_count_uv_rate partition(bu='train')
select uid,active_ym, sum(if(ym <= active_ym,order_count,0))/sum(if(ym <= active_ym,uv_date_count,0)),
sum(if(ym >= active_ym,order_count,0))/sum(if(ym >= active_ym,uv_date_count,0)) from tmp_innofin.vqq_nqh_user_order_count_uv_monthly 
where bu='train' group by uid,active_ym;

-- 统计
select bu,avg(rate_before),avg(rate_after) from tmp_innofin.vqq_nqh_user_order_count_uv_rate where bu in('flight','hotel','train') and active_ym <= '2017-06' group by bu;