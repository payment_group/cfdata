
use tmp_innofin;
create table vqq_uv_train_inquire as 
select distinct d,uid from dw_mobdb.factmbpageview where d between '2017-10-15' and '2017-10-31' 
and pagecode in('train_inquire');

-- 1) total
--- 导入公有云
use cfbdb;
drop table if exists vqq_active_uv;
create table vqq_active_uv as 
select d,uid from cfbdb.vqq_temp_cr_uv where dt = '2017-11-10';

-- 全部活跃用户uv
use cfbdb;
drop table if exists vqq_cr_uv;
create table vqq_cr_uv as 
select d,count(1) as uv from cfbdb.vqq_active_uv group by d;

-- 全部活跃用户订单
use cfbdb;
drop table if exists vqq_cr_order;
create table vqq_cr_order as 
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_trnorder_detail_snap a
inner join cfbdb.vqq_active_uv b on lower(a.uid) = lower(b.uid) where orderstatus = 10 and to_date(createtime) between '2017-10-15' and '2017-10-31' group by to_date(createtime);


-- 2) 已开通
use cfbdb;
drop table if exists vqq_cr_nqh;
create table vqq_cr_nqh as 
select distinct a.uid from cfbdb.vqq_active_uv a 
inner join (select distinct uid from cfbdb.vqq_user_contract where contract_status = 1) b on lower(a.uid) = lower(b.uid); 
-- 已开通uv
use cfbdb;
drop table if exists vqq_cr_nqh_uv;
create table vqq_cr_nqh_uv as 
select d,count(1) as uv from cfbdb.vqq_active_uv a
inner join cfbdb.vqq_cr_nqh b on a.uid = b.uid group by d;

-- 已开通 订单
use cfbdb;
drop table if exists vqq_cr_nqh_order;
create table vqq_cr_nqh_order as 
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_trnorder_detail_snap a
inner join cfbdb.vqq_cr_nqh b on lower(a.uid) = lower(b.uid) where orderstatus = 10 and  to_date(createtime) between '2017-10-15' and '2017-10-31' group by to_date(createtime) ;

-- 3) 可开通
use cfbdb;
drop table if exists vqq_cr_not_nqh;
create table vqq_cr_not_nqh as 
select distinct a.uid from cfbdb.vqq_active_uv a 
inner join cfbdb.vqq_user_model m on lower(a.uid) = lower(m.uid)
inner join cfbdb.vqq_user_prefix p on lower(a.uid) = lower(p.uid)
left join (select distinct uid from cfbdb.vqq_user_contract where contract_status = 1) b on lower(a.uid) = lower(b.uid) 
where b.uid is null;

-- 可开通 uv
use cfbdb;
drop table if exists vqq_cr_not_nqh_uv;
create table vqq_cr_not_nqh_uv as 
select d,count(1) as uv from cfbdb.vqq_active_uv a
inner join cfbdb.vqq_cr_not_nqh b on a.uid = b.uid group by d;


-- 可开通 订单
use cfbdb;
drop table if exists vqq_cr_not_nqh_order;
create table vqq_cr_not_nqh_order as 
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_trnorder_detail_snap a
inner join cfbdb.vqq_cr_not_nqh b on lower(a.uid) = lower(b.uid) where orderstatus = 10 and to_date(createtime) between '2017-10-15' and '2017-10-31' group by to_date(createtime) ;

-- 
use cfbdb;
select a.d,a.uv,b.uv,c.uv,dd.orders,e.orders,f.orders
from 
vqq_cr_uv a 
inner join vqq_cr_nqh_uv b on a.d = b.d 
inner join vqq_cr_not_nqh_uv c on a.d = c.d 
inner join vqq_cr_order dd on a.d = dd.d 
inner join vqq_cr_nqh_order e on a.d = e.d 
inner join vqq_cr_not_nqh_order f on a.d = f.d
; 

