-- 需求：
-- 1、 度假产品详情页日活用户中拿去花准入用户占比（近一周每日）
-- a) 度假产品详情页的pageid如下：
-- 团队游：401019、401020、401021、401023
-- 自由行：411017、415078
use tmp_innofin;
create table if not exists vqq_temp_uv_1(
	dt string,
	uid string
)partitioned by (
	product string
);
insert overwrite table vqq_temp_uv_1 partition(product = '团队游')
select distinct d,uid from dw_mobdb.factmbpageview where d>= date_add('2018-06-04',-7) and pagecode in ('401019','401020','401021','401023');

insert overwrite table vqq_temp_uv_1 partition(product = '自由行')
select distinct d,uid from dw_mobdb.factmbpageview where d>= date_add('2018-06-04',-7) and pagecode in ('411017','415078');

-- b) 拿去花准入用户口径
-- 宽口径：A卡评分<0.4分，未激活
-- 中口径：A卡评分在[0.2,0.35]之间，未激活
-- 窄口径：已授信未激活
use tmp_innofin;
create table if not exists vqq_temp_user_1(
	uid string
)partitioned by (
	u_group string
);
insert overwrite table vqq_temp_user_1 partition(u_group = '宽口径')
select distinct user_id from 
dw_usercreditscoredb.loan_before_v2_user_risk_final_result_dt a
left join (select distinct uid from ods_innofin.nqh_user_active_date) b on lower(a.user_id) = lower(b.uid) 
where a.prob < 0.4 and b.uid is null;

insert overwrite table vqq_temp_user_1 partition(u_group = '中口径')
select distinct user_id from 
dw_usercreditscoredb.loan_before_v2_user_risk_final_result_dt a
left join (select distinct uid from ods_innofin.nqh_user_active_date) b on lower(a.user_id) = lower(b.uid) 
where a.prob between 0.2 and 0.35 and b.uid is null;

insert overwrite table vqq_temp_user_1 partition(u_group = '窄口径')
select distinct b.user_id from 
(select distinct user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1 and activate_status = 0) a
inner join ods_innofin.user_contract b on a.user_id = b.open_id;

use tmp_innofin;
select dt,product,u_group,count(1)
from vqq_temp_uv_1 a 
inner join vqq_temp_user_1 b on lower(a.uid) = lower(b.uid)
group by dt,product,u_group;
-- 2、 近一个月每日分机票、酒店；整体订单量、频道首页UV、已授信未激活用户订单量、频道首页已授信未激活用户UV、已激活用户订单量、频道首页已激活用户UV
use tmp_innofin;
create table if not exists vqq_temp_orders_2(
	uid string,
	oid string,
	dt string
)partitioned by (
	bu string
);
insert overwrite table tmp_innofin.vqq_temp_orders_2 partition(bu = '机票')
select distinct get_json_object(content,'$.Uid'),orderid,from_unixtime(cast(regexp_extract(content,'BookingDate":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd') 
from bbz_oi_orderdb.bbz_oi_order_flight where dt >= date_add('2018-06-04',-30) 
and from_unixtime(cast(regexp_extract(content,'BookingDate":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd') >= date_add('2018-06-04',-30) 
and regexp_extract(content,'OrderDescription":"([^"]+)",',1) = '已出票';

insert overwrite table tmp_innofin.vqq_temp_orders_2 partition(bu = '酒店')
select distinct get_json_object(content,'$.Uid'),orderid,from_unixtime(cast(regexp_extract(content,'BookingDate":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd')
from bbz_oi_orderdb.bbz_oi_order_hotel where dt >= date_add('2018-06-04',-30)
and from_unixtime(cast(regexp_extract(content,'BookingDate":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd') >= date_add('2018-06-04',-30) 
and regexp_extract(content,'OrderDescription":"([^"]+)",',1) = '已成交';

-- 频道首页pageid ：酒店hotel_inland_inquire；机票10320642867、flight_int_inquire
use tmp_innofin;
create table if not exists vqq_temp_uv_2(
	dt string,
	uid string
)partitioned by (
	product string
);
insert overwrite table vqq_temp_uv_2 partition(product = '机票')
select distinct d,uid from dw_mobdb.factmbpageview where d>= date_add('2018-06-04',-30) and pagecode in ('10320642867','flight_int_inquire');

insert overwrite table vqq_temp_uv_2 partition(product = '酒店')
select distinct d,uid from dw_mobdb.factmbpageview where d>= date_add('2018-06-04',-30) and pagecode in ('hotel_inland_inquire');

-- 订单量
use tmp_innofin;
select a.dt,a.bu,count(distinct a.oid) as total,sum(if(b.uid is null,0,1)) as credit,sum(if(c.uid is null,0,1)) as active from
vqq_temp_orders_2 a 
left join (select uid from vqq_temp_user_1 where u_group = '窄口径') b on lower(a.uid) = lower(b.uid)
left join (select distinct uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid) 
group by a.dt,a.bu;
-- uv
use tmp_innofin;
select a.dt,a.product,count(1) as total,sum(if(b.uid is null,0,1)) as credit,sum(if(c.uid is null,0,1)) as active from
vqq_temp_uv_2 a 
left join (select uid from vqq_temp_user_1 where u_group = '窄口径') b on lower(a.uid) = lower(b.uid)
left join (select distinct uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid) 
group by a.dt,a.product;

-- 3、 度假活跃客户（下单活跃）中拿去花准入用户占比
-- a) 度假活跃客户uid 名单见附件。
use tmp_innofin;
create table if not exists vqq_temp_active_users_3(
	uid string
);
-- insert overwrite table vqq_temp_active_users_3 values

-- b) 拿去花准入用户口径同1中所述
-- 宽口径：A卡评分<0.4分，未激活
-- 中口径：A卡评分在[0.2,0.35]之间，未激活
-- 窄口径：已授信未激活
use tmp_innofin;
select u_group,count(1)
from vqq_temp_active_users_3 a 
inner join vqq_temp_user_1 b on lower(a.uid) = lower(b.uid)
group by u_group;
-- 4、 度假活跃用户中，拿去花已授信用户未激活用户、已激活用户、已使用用户的额度分布情况
-- a) 度假活跃用户取1中所述页面的活跃用户
-- Pageid in （401019、401020、401021、401023、411017、415078）
use tmp_innofin;
create table vqq_temp_uv_4(
	dt string,
	uid string
);
insert overwrite table tmp_innofin.vqq_temp_uv_4
select distinct d,uid from dw_mobdb.factmbpageview where d>= date_add('2018-06-04',-7) and pagecode in (401019,401020,401021,401023,411017,415078);

use tmp_innofin;
create table vqq_temp_4(
	dt string,
	uid string,
	credit int,
	active int,
	loan int,
	amount float
);
use tmp_innofin;
insert overwrite table vqq_temp_4
select a.dt,a.uid,if(b.uid is null,0,1),if(c.uid is null,0,1),if(d.uid is null,0,1),credit.activate_amt from vqq_temp_uv_4 a
inner join (select * from ods_innofin.user_contract_daily where d = '2018-06-03') u on lower(a.uid) = lower(u.user_id)
inner join (select user_id,max(activate_amt) as activate_amt from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap group by user_id) credit on u.open_id = credit.user_id
left join (select distinct uid from tmp_innofin.vqq_temp_user_1 where u_group = '窄口径') b on lower(a.uid) = lower(b.uid)
left join (select distinct uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
left join (select distinct uid from ods_innofin.fact_loan_order) d on lower(a.uid) = lower(d.uid)
;

select dt,amount,count(1) from tmp_innofin.vqq_temp_4 where credit = 1 group by dt,amount;



