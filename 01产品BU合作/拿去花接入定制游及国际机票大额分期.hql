use tmp_innofin;
create table if not exists vqq_tmp_customized(
	uid string,
	nqh int,
	credit_amt decimal(9,2),
	avg_order_amt decimal(9,2)
);

use tmp_innofin;
insert overwrite table vqq_tmp_customized
select a.uid,
	case when b.uid is null then 0 else 1 end,
	b.activate_amt,
	c.avg_order_amt
from
(select distinct get_json_object(content, '$.Uid') as uid from bbz_oi_orderdb.bbz_oi_order_customized 
	where dt >= add_months(current_date,-12)) a
left join ods_innofin.nqh_user_active_date b on lower(a.uid) = lower(b.uid)
left join (select get_json_object(content, '$.Uid') as uid, avg(get_json_object(content,'$.Amount')) as avg_order_amt from bbz_oi_orderdb.bbz_oi_order_customized 
	where dt >= add_months(current_date,-12) and get_json_object(content,'$.OrderDescription') = '已成交' and get_json_object(content,'$.Amount') > 0 group by get_json_object(content, '$.Uid')) c on lower(a.uid) = lower(c.uid)
;

use tmp_innofin;
create table if not exists vqq_tmp_flight(
	uid string,
	nqh int,
	credit_amt decimal(9,2),
	avg_order_amt decimal(9,2)
);

use tmp_innofin;
insert overwrite table vqq_tmp_flight
select a.uid,
	if(b.uid is null,0,1),
	b.activate_amt,
	a.avg_order_amt
from
(select get_json_object(content, '$.Uid') as uid  avg(get_json_object(content,'$.ActualOrderAmount')) as avg_order_amt from bbz_oi_orderdb.bbz_oi_order_flight 
	where dt >= add_months(current_date,-12) and get_json_object(content, '$.BizCategory') = 'FlightInternate' 
	and get_json_object(content,'$.OrderDescription') in('已支付出票中','已扣款出票中','已出票','出票中(预付)')
	 group by get_json_object(content, '$.Uid')) a
left join ods_innofin.nqh_user_active_date b on lower(a.uid) = lower(b.uid)
------------
总数：5362044
已激活：110768
额度覆盖平均单价的 102174

select 	case when credit_amt >= 2000 and credit_amt < 5000 then '2k~5k'
		 when credit_amt >= 5000 and credit_amt < 10000 then '5k~10k'
		 when credit_amt >= 10000 and credit_amt < 15000 then '10k~15k'
		 when credit_amt >= 15000 and credit_amt < 20000 then '15k~20k'
		 else '20k+' end, count(1) from 
tmp_innofin.vqq_tmp_flight  where nqh = 1 group by case when credit_amt >= 2000 and credit_amt < 5000 then '2k~5k'
		 when credit_amt >= 5000 and credit_amt < 10000 then '5k~10k'
		 when credit_amt >= 10000 and credit_amt < 15000 then '10k~15k'
		 when credit_amt >= 15000 and credit_amt < 20000 then '15k~20k'
		 else '20k+' end

额度	人数
2k~5k	22029
5k~10k	25012
10k~15k	19838
15k~20k	12668
20k+	31221
