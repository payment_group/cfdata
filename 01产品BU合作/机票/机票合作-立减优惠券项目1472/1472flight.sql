-- 3个月内曾点击“低价订阅”且满足*前置模型*条件的用户；
-- rn_flight_low_price_subscribe_one_click

use tmp_innofin;
create table if not exists vqq_rn_flight_low_price_subscribe_one_click
(
	uid string
);

insert overwrite table tmp_innofin.vqq_rn_flight_low_price_subscribe_one_click
select distinct a.uid from dw_mobdb.factmbpageview a
inner join tmp_innofin.cfbdb_vqq_user_prefix_model b on lower(a.uid) = lower(b.uid)
where d >= '${zdt.addDay(-90).format("yyyy-MM-dd")}'
and pagecode in ('rn_flight_low_price_subscribe_one_click') ;

