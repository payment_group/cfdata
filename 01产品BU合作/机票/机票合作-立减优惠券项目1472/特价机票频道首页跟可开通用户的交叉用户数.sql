use tmp_innofin;

create table tmp_innofin.vqq_flight_lowprice_home_rn as 
select distinct d,pagecode,uid from dw_mobdb.factmbpageview
where d > add_months(current_date,-1) and pagecode = 'flight_lowprice_home_rn';

create table tmp_innofin.vqq_flight_lowprice_home_rn_count as 
select d,count(a.uid),count(b.uid) from tmp_innofin.vqq_flight_lowprice_home_rn a
left join tmp_innofin.cfbdb_vqq_user_prefix_model b on lower(a.uid) = lower(b.uid) group by d;