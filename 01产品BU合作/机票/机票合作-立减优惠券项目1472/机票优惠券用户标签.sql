-- 满足*前置模型*条件的用户且未申请；
use tmp_innofin;
create table vqq_flight_curpon_users as 
select a.uid from cfbdb_vqq_user_prefix_model a 
left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id)
where b.user_id is null;
-- 用户三个月内机票的pv数据
use tmp_innofin;
create table vqq_flight_curpon_users_pv as
select a.uid,
	starttime,
	c.pagecode ,
	c.pagename ,
	c.category,
	c.categoryname ,
	countryname ,
	provincename ,
	cityname from tmp_innofin.vqq_flight_curpon_users a 
	inner join (select * from dw_mobdb.factmbpageview where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)
	inner join dim_mobdb.dimmbpagecode c on b.pagecode = c.pagecode 
where c.categoryname like '%机票%';

use tmp_innofin;
create table vqq_flight_curpon_users_flag(
	uid string,
	flag_value string
)partitioned by (flag string);

insert overwrite table tmp_innofin.vqq_flight_curpon_users_flag partition(flag='订单页')
select distinct uid ,1 from tmp_innofin.vqq_flight_curpon_users_pv where pagename like '%订单%';

insert overwrite table tmp_innofin.vqq_flight_curpon_users_flag partition(flag='特价页')
select distinct uid ,1 from tmp_innofin.vqq_flight_curpon_users_pv where (pagename like '%特价%' or pagename like '%低价%' or pagename like '%优惠%' or pagename like '%折扣%');

insert overwrite table tmp_innofin.vqq_flight_curpon_users_flag partition(flag='查询页')
select distinct uid ,1 from tmp_innofin.vqq_flight_curpon_users_pv where pagename like '%查询%';

insert overwrite table tmp_innofin.vqq_flight_curpon_users_flag partition(flag='城市')
select uid,cityname from (
select uid,cityname,row_number() over (partition by uid order by starttime desc) as rank from tmp_innofin.vqq_flight_curpon_users_pv) a
where a.rank = 1;

use tmp_innofin;
create table vqq_flight_curpon_users_flag_snap(
	uid string,
	order_page int,
	low_price_page int,
	query_page int,
	last_city string
);

insert overwrite table tmp_innofin.vqq_flight_curpon_users_flag_snap
select a.uid,
nvl(b.flag_value,0),
nvl(c.flag_value,0),
nvl(d.flag_value,0),
nvl(e.flag_value,'')
from tmp_innofin.vqq_flight_curpon_users a
left join (select * from tmp_innofin.vqq_flight_curpon_users_flag where flag='订单页') b on lower(a.uid) = lower(b.uid)
left join (select * from tmp_innofin.vqq_flight_curpon_users_flag where flag='特价页') c on lower(a.uid) = lower(c.uid)
left join (select * from tmp_innofin.vqq_flight_curpon_users_flag where flag='查询页') d on lower(a.uid) = lower(d.uid)
left join (select * from tmp_innofin.vqq_flight_curpon_users_flag where flag='城市') e on lower(a.uid) = lower(e.uid);

-- 计数
select flag,count(1) from tmp_innofin.vqq_flight_curpon_users_flag where flag in ('订单页','特价页','查询页') group by flag;
-- 查询页	4405680
-- 特价页	2046479
-- 订单页	2933148
select count(1) from tmp_innofin.vqq_flight_curpon_users_flag_snap where order_page*low_price_page*query_page = 1;
-- 1684578
select last_city,count(1) as c  from tmp_innofin.vqq_flight_curpon_users_flag_snap where last_city <> '' group by last_city order by c desc limit 1000;

use tmp_innofin;
create table vqq_flight_curpon_users_0925 as 
select uid from vqq_flight_curpon_users_flag_snap where order_page*low_price_page*query_page = 1;
