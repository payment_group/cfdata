select count(a.orderid)/7,count(b.user_id)/7 from 
ods_innofin.etl_oi_flight_order a left join 
(select * from dw_usercreditscoredb.loan_before_v2_user_risk_final_result_dt where prob <= 0.45 and dt = '${zdt.addDay(-2).format("yyyy-MM-dd")}') b on lower(a.uid) = lower(b.user_id)
where to_date(bookingdate) between date_add(current_date,-7) and date_add(current_date,-1); 
