-- 拿去花用户激活时间
-- 例行到公有云
-- tmp_innofin.nqh_uid_apl;

-- 公有云

use cfbdb;
create table if not exists vqq_nqh_user_active_date
(
	uid string,
	active_date string,
	active_ym string
);
insert overwrite table vqq_nqh_user_active_date
select uid, time_apl,substring(time_apl,1,7) as active_ym 
from nqh_uid_apl;

-- 拿去花用户所有订单，月统计数量
use tmp_innofin;
create table if not exists vqq_nqh_user_order_count_monthly
(
	uid string,
	ym string,
	bu string,
	order_count int,
	order_amount decimal(9,2)
);

insert overwrite table tmp_innofin.vqq_nqh_user_order_count_monthly
select uid, substring(orderdate,1,7) as ym , type as bu,
count(1) as order_count,sum(amount) as order_amount 
from temp_travelmoneydb.tmp_qmwang_nqhorders  
	group by uid, substring(orderdate,1,7), type;


select min(time_apl),max(time_apl) from tmp_innofin.nqh_uid_apl;	

--2016-06-23
--2017-11-08

-- 准备pv数据（防止数据太大）
use tmp_innofin;
create table vqq_nqh_flt_pv as 
select distinct d,uid from dw_mobdb.factmbpageview where d between '2016-03-23' and current_date and pagecode in ('flight_inland_singlelist','flight_inland_middle','flight_inland_order');

-- uv数据 ubt 
use tmp_innofin;
create table if not exists vqq_nqh_user_flt_uv
(
	uid string,
	active_date string,
	uv_date_count_before int,
	uv_date_count_after int
);

insert overwrite table  tmp_innofin.vqq_nqh_user_flt_uv
select a.uid,a.time_apl,
sum(case when b.d between add_months(a.time_apl,-3) and a.time_apl then 1 else 0 end) as uv_date_count_before,
sum(case when b.d between a.time_apl and add_months(a.time_apl,3) then 1 else 0 end) as uv_date_count_after
from tmp_innofin.nqh_uid_apl a
inner join tmp_innofin.vqq_nqh_flt_pv b on lower(a.uid) = lower(b.uid)
group by a.uid,a.time_apl;

-- 上传公有云

use cfbdb;
create table vqq_nqh_flt_order_count as 
select dt,uid,count(distinct orderid) from cfbdb.ctrip_cloud_uid_fltorder_detail where dt between '2016-03-23' and current_date group by dt,uid
-- 订单数据
use cfbdb;
drop table if exists vqq_nqh_user_flt_order;
create table if not exists vqq_nqh_user_flt_order
(
	uid string,
	active_date string,
	order_before int,
	order_after int
);

insert overwrite table  cfbdb.vqq_nqh_user_flt_order
select a.uid,a.active_date,
sum(case when b.dt between add_months(a.active_date,-3) and a.active_date then 1 else 0 end) as order_count_before,
sum(case when b.dt between a.active_date and add_months(a.active_date,3) then 1 else 0 end) as order_count_after
from cfbdb.vqq_nqh_user_flt_uv a
inner join cfbdb.vqq_nqh_flt_order_count b on lower(a.uid) = lower(b.uid)
group by a.uid,a.active_date;

-- 转化率
use cfbdb;
create table if not exists vqq_nqh_user_flt_rate
(
	uid string,
	active_date string,
	rate_before double,
	rate_after double
);
insert overwrite table cfbdb.vqq_nqh_user_flt_rate
select a.uid,a.active_date, 
if(uv_date_count_before= 0,0,order_before/uv_date_count_before),
if(uv_date_count_after=0,0,order_after/uv_date_count_after)
from vqq_nqh_user_flt_uv a 
inner join vqq_nqh_user_flt_order b on a.uid = b.uid;


-- 统计
select bu,avg(rate_before),avg(rate_after) from tmp_innofin.vqq_nqh_user_order_count_uv_rate where bu in('flight','hotel','train') and active_ym <= '2017-06' group by bu;