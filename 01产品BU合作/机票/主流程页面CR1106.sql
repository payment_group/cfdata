
-- 已开通
use cfbdb;
drop table if exists vqq_flt_active_user_nqh;
create table vqq_flt_active_user_nqh as 
select distinct a.uid from cfbdb.vqq_flt_active_user a 
inner join (select uid from cfbdb.vqq_user_contract where contract_status = 1) b on lower(a.uid) = lower(b.uid) 
-- 已开通 uv
use cfbdb;
select d,count(1) from cfbdb.vqq_flt_active_user a
inner join cfbdb.vqq_flt_active_user_nqh b on a.uid = b.uid group by d;
-- 已开通 订单
use cfbdb;
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_fltorder_detail_snap a
inner join cfbdb.vqq_flt_active_user_nqh b on lower(a.uid) = lower(b.uid) where to_date(createtime) >= '2017-10-22' group by to_date(createtime) ;


--可开通
use cfbdb;
drop table if exists vqq_flt_active_user_not_nqh;
create table vqq_flt_active_user_not_nqh as 
select distinct a.uid from cfbdb.vqq_flt_active_user a 
inner join cfbdb.vqq_user_model m on lower(a.uid) = lower(m.uid)
inner join cfbdb.vqq_user_prefix p on lower(a.uid) = lower(p.uid)
left join (select uid from cfbdb.vqq_user_contract where contract_status = 1) b on lower(a.uid) = lower(b.uid) 
where b.uid is null;
-- 可开通 uv
use cfbdb;
select d,count(1) from cfbdb.vqq_flt_active_user a
inner join cfbdb.vqq_flt_active_user_not_nqh b on a.uid = b.uid group by d;
-- 可开通 订单
use cfbdb;
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_fltorder_detail_snap a
inner join cfbdb.vqq_flt_active_user_not_nqh b on lower(a.uid) = lower(b.uid) where to_date(createtime) >= '2017-10-22' group by to_date(createtime) ;

-- 全部活跃用户uv
use cfbdb;
select d,count(1) from cfbdb.vqq_flt_active_user group by d;
-- 全部活跃用户订单
use cfbdb;
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_fltorder_detail_snap a
inner join cfbdb.vqq_flt_active_user b on lower(a.uid) = lower(b.uid) where to_date(createtime) >= '2017-10-22' group by to_date(createtime) ;
