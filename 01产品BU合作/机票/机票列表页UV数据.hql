-- 机票列表页最近一周日均UID
use tmp_innofin;
insert overwrite table vqq_tmp_flt_uv 
select distinct uid from dw_mobdb.factmbpageview
  	where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and pagecode in ('flight_inland_singlelist');
  
--总数 681583.4285714285
select count(1)/7 from tmp_innofin.vqq_tmp_flt_uv;

--实名绑卡 103679.0
select count(1)/7 from 
tmp_innofin.vqq_tmp_flt_uv a 
inner join (select distinct user_id from dw_usercreditscoredb.loan_before_v2_user_risk_final_result_dt where prob between 0.2 and 0.4 ) b on lower(a.uid) = lower(b.user_id)
inner join (select distinct source_uid from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap) c on lower(a.uid) = lower(c.source_uid)
inner join (select distinct uid from dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap) d on lower(a.uid) = lower(d.uid)
left join ods_innofin.nqh_user_active_date e on lower(a.uid) = lower(e.uid)
where e.uid is null;


--实名未绑卡 63536.71428571428
select count(1)/7 from 
tmp_innofin.vqq_tmp_flt_uv a 
inner join (select distinct user_id from dw_usercreditscoredb.loan_before_v2_user_risk_final_result_dt where prob between 0.2 and 0.4 ) b on lower(a.uid) = lower(b.user_id)
inner join (select distinct source_uid from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap) c on lower(a.uid) = lower(c.source_uid)
left join (select distinct uid from dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap) d on lower(a.uid) = lower(d.uid)
left join ods_innofin.nqh_user_active_date e on lower(a.uid) = lower(e.uid)
where e.uid is null and d.uid is null;

--未实名未绑卡 13784.285714285714
select count(1)/7 from 
tmp_innofin.vqq_tmp_flt_uv a 
inner join (select distinct user_id from dw_usercreditscoredb.loan_before_v2_user_risk_final_result_dt where prob between 0.2 and 0.4 ) b on lower(a.uid) = lower(b.user_id)
left join (select distinct source_uid from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap) c on lower(a.uid) = lower(c.source_uid)
left join (select distinct uid from dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap) d on lower(a.uid) = lower(d.uid)
left join ods_innofin.nqh_user_active_date e on lower(a.uid) = lower(e.uid)
where e.uid is null and c.source_uid is null and d.uid is null;
