
-- 拿去花用户国内酒店订单
select merchantname,substring(creationdate,1,7) as order_ym, count(1) as order_count
from ods_innofin.fact_loan_order a 
   left join dim_paydb.dimmerchant b on cast(a.merchantid as int)=b.merchantid
   inner join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id)
   where b.merchantname in ('国内酒店') and c.contract_status =1 group by merchantname,substring(creationdate,1,7) order by order_ym limit 20;

-- 订单总数 公有云
select substring(createtime,1,7) as ym,count(1) 
from cfbdb.ctrip_cloud_uid_htlorder_detail_snap a
inner join cfbdb.ctrip_cloud_dim_hotel b on a.hotel = b.hotel
where countryname= '中国' and  to_date(createtime) between '2016-09-01' and '2017-08-31'  group by  substring(createtime,1,7) order by ym limit 20;

--拿去花用户订单数
select substring(createtime,1,7) as ym,count(1) 
from cfbdb.ctrip_cloud_uid_htlorder_detail_snap a
inner join cfbdb.ctrip_cloud_dim_hotel b on a.hotel = b.hotel
inner join cfbdb.vqq_nqh_user_marrting_flag c on lower(a.uid) = lower(c.uid)
where countryname= '中国' and to_date(createtime) between '2016-09-01' and '2017-08-31' and c.active_flag = 1  group by  substring(createtime,1,7) order by ym limit 20;


select substring(createtime,1,7) as ym,count(1) 
from cfbdb.ctrip_cloud_uid_htlorder_detail_snap a
inner join cfbdb.ctrip_cloud_dim_hotel b on a.hotel = b.hotel
where countryname<> '中国' and  to_date(createtime) between '2016-09-01' and '2017-08-31'  group by  substring(createtime,1,7) order by ym limit 20;


-- 主流程页面
use tmp_innofin;
create table if not exists vqq_nqh_user_htl_uv_201703to06(
	d string,
	uid string,
	pagecode string,
	nqh_user int
);
insert overwrite table tmp_innofin.vqq_nqh_user_htl_uv_201703to06
select distinct d,a.uid,pagecode,if(b.uid is null,0,1) from dw_mobdb.factmbpageview a
left join (select user_id as uid from ods_innofin.user_contract where contract_status =1 )b on lower(a.uid) = lower(b.uid)
where d between '2017-01-01' and '2017-08-31' and pagecode in (
'hotel_inland_inquire','hotel_inland_list','hotel_inland_detail','hotel_inland_order');

-- 拿去花用户
-- 国内主流程
select substring(d,1,7) as ym,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201703to06 
where nqh_user = 1 and pagecode in ('hotel_inland_inquire','hotel_inland_list','hotel_inland_detail','hotel_inland_order') group by substring(d,1,7)  order by ym limit 20;

-- 全部用户
-- 国内主流程
select substring(d,1,7) as ym,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201703to06
 where  pagecode in ('hotel_inland_inquire','hotel_inland_list','hotel_inland_detail','hotel_inland_order') group by substring(d,1,7)  order by ym limit 20;
