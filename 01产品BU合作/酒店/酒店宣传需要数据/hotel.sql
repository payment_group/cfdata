-- 订单用户 公有云
use cfbdb;
create table vqq_hotel_uid_201708(
	uid string,
	actived int,
	activable int
);
insert overwrite table  cfbdb.vqq_hotel_uid_201708
select a.uid,nvl(b.active_flag,0),case when c.uid is null or d.uid is null then 0 else 1 end
from
(select distinct uid from cfbdb.ctrip_cloud_uid_htlorder_detail_snap where createtime >='2017-08-01' and orderstatus = 10) a
left join (select uid,active_flag from cfbdb.nqh_user_markting_flag where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and active_flag = 1) b on lower(a.uid) = lower(b.uid)
left join cfbdb.vqq_user_model_uid c on lower(a.uid) = lower(c.uid)
left join cfbdb.vqq_user_prefix_uid d on lower(a.uid) = lower(d.uid)

select count(1) as total,sum(actived) as actived,sum(activable) as activable from cfbdb.vqq_hotel_uid_201708
-- 活跃用户
use tmp_innofin;
create table if not exists vqq_htl_active_user_201708
(uid string, access_date string);

insert overwrite table tmp_innofin.vqq_htl_active_user_201708
select distinct uid,d from dw_mobdb.factmbpageview where d between '2017-07-01' and '2017-07-31' and ;
--
use tmp_innofin;
create table if not exists vqq_hotel_uid_201708(
	uid string,
	access_date string,
	actived int,
	activable int
);
insert overwrite table  tmp_innofin.vqq_hotel_uid_201708
select a.uid,a.access_date,nvl(b.active_flag,0),case when c.uid is null then 0 else 1 end
from tmp_innofin.vqq_htl_active_user_201708 a
left join (select uid,active_flag from dw_innofin.nqh_user_markting_flag where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and active_flag = 1) b on lower(a.uid) = lower(b.uid)
left join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid);

select access_date ,count(1) as total,sum(actived) as actived,sum(activable) as activable from tmp_innofin.vqq_hotel_uid_201708 group by access_date;

-- -- 活跃用户

use tmp_innofin;
create table if not exists vqq_htl_active_user_201708
(uid string,pagename string, access_date string);

insert overwrite table tmp_innofin.vqq_htl_active_user_201708
select distinct uid,pagename,d from 
dw_mobdb.factmbpageview a 
where a.d between '2017-08-01' and '2017-08-31' and a.pagename in('国内酒店详情页','海外酒店详情页');
--
use tmp_innofin;
create table if not exists vqq_hotel_uid_201708(
	uid string,
	pagename string,
	access_date string,
	actived int,
	activable int
);
insert overwrite table tmp_innofin.vqq_hotel_uid_201708 
select a.uid,a.pagename,a.access_date,nvl(b.active_flag,0) as actived,case when c.uid is null then 0 else 1 end as activable
from tmp_innofin.vqq_htl_active_user_201708 a
left join (select uid,active_flag from dw_innofin.nqh_user_markting_flag where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and active_flag = 1) b on lower(a.uid) = lower(b.uid)
left join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid);

use tmp_innofin;
create table tmp_innofin.vqq_hotel_uid_201708_count as 
select pagename,access_date,count(1) as total,sum(actived) as actived,sum(activable) as activable from tmp_innofin.vqq_hotel_uid_201708 group by pagename,access_date;

