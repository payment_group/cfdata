-- 口径1：订单量/各主流程页面UV（%），主流程页面包括：查询页、列表页、详情页、订单填写页，4个页面去重UV
-- 口径2：订单量/列表+详情UV（%）,列表页、详情页去重UV
-- 
-- 需要用到的Pagecode:
-- 
-- hotel_inland_inquire-国内酒店查询页
-- hotel_inland_list-国内酒店列表页
-- hotel_inland_detail-国内酒店详情页
-- hotel_inland_order-国内酒店订单填写页
-- 
-- hotel_oversea_inquire-海外酒店查询页
-- hotel_oversea_list-海外酒店列表页
-- hotel_oversea_detail-海外酒店详情页
-- hotel_oversea_order-海外酒店订单填写页
-- 
-- 分子有两种订单口径
-- 口径1、仅拿去花支付的订单
-- 口径2、不限支付方式的总订单

-- 拿去花订单量 fact_loan_order
use tmp_innofin;
create table if not exists vqq_htl_loan_order_201708
(
	merchantname string,
	order_date string,
	order_count int
);

insert overwrite table tmp_innofin.vqq_htl_loan_order_201708
select merchantname,to_date(creationdate), count(1)
from ods_innofin.fact_loan_order a 
   left join dim_paydb.dimmerchant b on cast(a.merchantid as int)=b.merchantid
   where b.merchantname in ('国内酒店','海外酒店') group by merchantname,to_date(creationdate);
   
select * from tmp_innofin.vqq_htl_loan_order_201708 where order_date between '2017-06-01' and '2017-08-31' order by merchantname,order_date limit 100;

-- 订单总数 公有云
use cfbdb;
drop table vqq_nqh_user_hotel_order_201708;
create table vqq_nqh_user_hotel_order_201708(
	uid string,
	order_id string,
	order_date string,
	domestic int,
	nqh_user int
);

insert overwrite table  cfbdb.vqq_nqh_user_hotel_order_201708
select 
a.uid,orderid, to_date(createtime),if(countryname = '中国',1,0),if(c.active_flag = 1,1,0) 
from cfbdb.ctrip_cloud_uid_htlorder_detail_snap a
inner join cfbdb.ctrip_cloud_dim_hotel b on a.hotel = b.hotel
left join cfbdb.nqh_user_markting_flag c on lower(a.uid) = lower(c.uid) and c.dt = '2017-09-07'
where to_date(createtime) between '2017-06-01' and '2017-08-31' ;

-- 拿去花用户订单
select domestic,order_date,count(1) from cfbdb.vqq_nqh_user_hotel_order_201708 where nqh_user = 1 group by domestic,order_date order by order_date limit 200;
-- 全体用户订单
select domestic,order_date,count(1) from cfbdb.vqq_nqh_user_hotel_order_201708 group by domestic,order_date order by order_date limit 200;


-- 主流程页面
use tmp_innofin;
drop table vqq_nqh_user_htl_uv_201708;
create table if not exists vqq_nqh_user_htl_uv_201708(
	d string,
	uid string,
	pagecode string,
	nqh_user int
);
insert overwrite table tmp_innofin.vqq_nqh_user_htl_uv_201708
select distinct d,uid,pagecode,if(contract_status=1,1,0) from dw_mobdb.factmbpageview a
left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id)
where d between '2017-06-01' and '2017-08-31' and pagecode in (
'hotel_inland_inquire','hotel_inland_list','hotel_inland_detail','hotel_inland_order',
'hotel_oversea_inquire','hotel_oversea_list','hotel_oversea_detail','hotel_oversea_order');

-- 拿去花用户
-- 国内主流程
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where nqh_user = 1 and pagecode in ('hotel_inland_inquire','hotel_inland_list','hotel_inland_detail','hotel_inland_order') group by d order by d limit 200;
-- 国际主流程
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where nqh_user = 1 and pagecode in ('hotel_oversea_inquire','hotel_oversea_list','hotel_oversea_detail','hotel_oversea_order') group by d order by d limit 200;
--国内详情
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where nqh_user = 1 and pagecode in ('hotel_inland_list','hotel_inland_detail') group by d order by d limit 200;
--国际详情
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where nqh_user = 1 and pagecode in ('hotel_oversea_list','hotel_oversea_detail') group by d order by d limit 200;

-- 全部用户
-- 国内主流程
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where pagecode in ('hotel_inland_inquire','hotel_inland_list','hotel_inland_detail','hotel_inland_order') group by d order by d limit 200;
-- 国际主流程
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where pagecode in ('hotel_oversea_inquire','hotel_oversea_list','hotel_oversea_detail','hotel_oversea_order') group by d order by d limit 200;
--国内详情
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where pagecode in ('hotel_inland_list','hotel_inland_detail') group by d order by d limit 200;
--国际详情
select d,count(distinct uid) from tmp_innofin.vqq_nqh_user_htl_uv_201708 where pagecode in ('hotel_oversea_list','hotel_oversea_detail') group by d order by d limit 200;
