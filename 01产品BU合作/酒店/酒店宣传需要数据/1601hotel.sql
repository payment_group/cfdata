-- 1、国内酒店查询页面-总用户数；
-- 2、国内酒店查询页面-可开通拿去花用户数；
-- 3、海外酒店查询页面-总用户数；
-- 4、海外酒店查询页面-可开通拿去花用户数；
-- 5、国内酒店查询页面-支持一键开通的用户数；
-- 6、海外酒店查询页面-支持一键开通的用户数；
-- hotel_inland_inquire -国内酒店查询页
-- hotel_oversea_inquire -海外酒店查询页
-- 活跃用户
use tmp_innofin;
create table if not exists vqq_htl_querypage_uv_201708
(
	uid string,
	pagecode string, 
	access_date string
);

insert overwrite table tmp_innofin.vqq_htl_querypage_uv_201708
select distinct uid,pagecode,d from dw_mobdb.factmbpageview where d between '2017-08-01' and '2017-08-31' and pagecode in ('hotel_inland_inquire','hotel_oversea_inquire') ;
--
use tmp_innofin;
drop table tmp_innofin.vqq_htl_querypage_uv_201708_flags;
create table if not exists vqq_htl_querypage_uv_201708_flags(
	uid string,
	pagecode string, 
	access_date string,
	actived int,
	activable int,
	preauth int
);
insert overwrite table  tmp_innofin.vqq_htl_querypage_uv_201708_flags
select a.uid,pagecode,a.access_date,if(b.uid is null,0,1),if(c.uid is null,0,1),if(d.uid is null,0,1)
from tmp_innofin.vqq_htl_querypage_uv_201708 a
left join (select user_id as uid from ods_innofin.user_contract where contract_status = 1) b on lower(a.uid) = lower(b.uid)
left join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid)
left join (select user_id as uid  from ods_innofin.user_contract where contract_status = 4 and req_src = 'PRE_CREDIT_TASK') d on lower(a.uid) = lower(d.uid);

-- count
use tmp_innofin;
drop table vqq_htl_querypage_uv_201708_count;
create table if not exists vqq_htl_querypage_uv_201708_count(
	access_date string,
	pagecode string,
	total int,
	actived int,
	activable int,
	preauth int
);

insert overwrite table tmp_innofin.vqq_htl_querypage_uv_201708_count
select access_date,pagecode, count(1),sum(actived), sum(activable), sum(preauth) from  
tmp_innofin.vqq_htl_querypage_uv_201708_flags a group by access_date,pagecode;

select * from tmp_innofin.vqq_htl_querypage_uv_201708_count order by pagecode,access_date limit 100;
