use tmp_innofin;
drop table if exists tmp_innofin.vqq_temp_htl_active_15;
create table vqq_temp_htl_active_15 as 
select distinct d,uid from  dw_mobdb.factmbpageview where d between '2017-10-15' and '2017-10-31' 
and pagecode in('hotel_inland_detail','hotel_inland_list','hotel_inland_order','hotel_inland_inquire');

use tmp_innofin;
create table if not exists vqq_temp_1101 (uid string) STORED AS TEXTFILE;

-- 1、 酒店活跃用户中已开通拿去花的用户，每日CR
-- 2、 酒店活跃用户中未开通且可开通拿去花的用户， 每日CR
-- 取数时间段：10月15日至31日

-- 已开通
use tmp_innofin;
drop table if exists vqq_temp_htl_active_15_nqh;
create table vqq_temp_htl_active_15_nqh as 
select distinct uid from tmp_innofin.vqq_temp_htl_active_15 a 
inner join (select user_id from ods_innofin.user_contract where contract_status = 1) b on lower(a.uid) = lower(b.user_id) 

--可开通
use tmp_innofin;
drop table if exists vqq_temp_htl_active_15_not_nqh;
create table vqq_temp_htl_active_15_not_nqh as 
select distinct a.uid from tmp_innofin.vqq_temp_htl_active_15 a 
inner join tmp_innofin.vqq_temp_1101 c on lower(a.uid) = lower(c.uid)
left join (select user_id from ods_innofin.user_contract where contract_status = 1) b on lower(a.uid) = lower(b.user_id) 
where b.user_id is null;


-- 已开通 uv
use cfbdb;
create table vqq_temp_htl_active_15_nqh_uv as 
select d,count(1) from tmp_innofin.vqq_temp_htl_active_15 a
inner join tmp_innofin.vqq_temp_htl_active_15_nqh b on a.uid = b.uid group by d;

-- 可开通 uv
use tmp_innofin;
create table vqq_temp_htl_active_15_not_nqh_uv as 
select d,count(1) from tmp_innofin.vqq_temp_htl_active_15 a
inner join tmp_innofin.vqq_temp_htl_active_15_not_nqh b on a.uid = b.uid group by d;

--------公有云
-- 已开通 订单
use cfbdb;
drop table vqq_temp_htl_active_15_nqh_order;
create table vqq_temp_htl_active_15_nqh_order as 
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_htlorder_detail_snap a
inner join cfbdb.vqq_temp_htl_active_15_nqh b on lower(a.uid) = lower(b.uid) where orderstatus = 10 and  to_date(createtime) between '2017-10-15' and '2017-10-31' group by to_date(createtime) ;
-- 可开通 订单
use cfbdb;
drop table vqq_temp_htl_active_15_not_nqh_order;
create table vqq_temp_htl_active_15_not_nqh_order as 
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_htlorder_detail_snap a
inner join cfbdb.vqq_temp_htl_active_15_not_nqh b on lower(a.uid) = lower(b.uid) where orderstatus = 10 and to_date(createtime) between '2017-10-15' and '2017-10-31' group by to_date(createtime) ;

-- 全部活跃用户uv
use cfbdb;
create table vqq_temp_htl_active_15_uv as 
select d,count(1) from tmp_innofin.vqq_temp_htl_active_15 group by d;

-- 全部活跃用户订单
use cfbdb;
drop table vqq_temp_htl_active_15_order;
create table vqq_temp_htl_active_15_order as 
select to_date(createtime) as d,count(distinct orderid) as orders from ctrip_cloud_uid_htlorder_detail_snap a
inner join cfbdb.vqq_temp_htl_active_15 b on lower(a.uid) = lower(b.uid) where orderstatus = 10 and to_date(createtime) between '2017-10-15' and '2017-10-31' group by to_date(createtime) ;
