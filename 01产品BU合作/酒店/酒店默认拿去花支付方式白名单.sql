-- 用户总量
select count(1) from cfbdb.vqq_user_contract where contract_status = 1
-- 722003

-- 拿去花订单量
use tmp_innofin;
create table vqq_nqh_order_count_12m as 
select uid,count(distinct orderid) as order_count from ods_innofin.fact_loan_order
where to_date(creationdate) >= add_months(current_date,-12)
group by uid
;
-- 全部的订单
use cfbdb;
drop table if exists vqq_nqh_user_order_count_12m;
create table vqq_nqh_user_order_count_12m as 
select a.uid, 
nvl(b.c,0) as order_count_all
from (select uid from cfbdb.vqq_user_contract where contract_status = 1) a
left join (select uid,count(distinct orderid) as c from cfbdb.ctrip_cloud_uid_allorder_info_snap where channel in ('pkg','hotel','train','flight') 
	and orderstatus = 10 and to_date(orderdate)>= add_months(current_date,-12) group by uid) b on lower(a.uid) = lower(b.uid) 

-- 赋予坐标
use cfbdb;
drop table if exists vqq_nqh_user_order_count_xy;
create table vqq_nqh_user_order_count_xy as 
select a.uid,
case when b.order_count_all is null then 0 when b.order_count_all>20 then 21 else b.order_count_all end as x,
case when c.order_count is null then 0 when c.order_count>20 then 21 else c.order_count end as y 
from (select distinct uid from cfbdb.vqq_user_contract where contract_status = 1) a
left join (select distinct uid,order_count_all from cfbdb.vqq_nqh_user_order_count_12m) b on lower(a.uid) = lower(b.uid)
left join (select distinct uid,order_count from cfbdb.vqq_nqh_order_count_12m) c on lower(a.uid) = lower(c.uid);


use cfbdb;
select x,y,count(1) from vqq_nqh_user_order_count_xy group x,y;

select x,
sum(if(y=0,1,0)) as `0`,
sum(if(y=1,1,0)) as `1`,
sum(if(y=2,1,0)) as `2`,
sum(if(y=3,1,0)) as `3`,
sum(if(y=4,1,0)) as `4`,
sum(if(y=5,1,0)) as `5`,
sum(if(y=6,1,0)) as `6`,
sum(if(y=7,1,0)) as `7`,
sum(if(y=8,1,0)) as `8`,
sum(if(y=9,1,0)) as `9`,
sum(if(y=10,1,0)) as `10`,
sum(if(y=11,1,0)) as `11`,
sum(if(y=12,1,0)) as `12`,
sum(if(y=13,1,0)) as `13`,
sum(if(y=14,1,0)) as `14`,
sum(if(y=15,1,0)) as `15`,
sum(if(y=16,1,0)) as `16`,
sum(if(y=17,1,0)) as `17`,
sum(if(y=18,1,0)) as `18`,
sum(if(y=19,1,0)) as `19`,
sum(if(y=20,1,0)) as `20`,
sum(if(y=21,1,0)) as `21`
from cfbdb.vqq_nqh_user_order_count_xy
group by x;