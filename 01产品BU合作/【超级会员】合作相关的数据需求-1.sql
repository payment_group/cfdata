--【超级会员】合作相关的数据需求-1
--1、 已开通拿去花的用户中
--（1） 已购超级会员的用户数及占比；
select count(1) from ods_innofin.user_contract a 
inner join dw_pubsharedb.super_member b on lower(a.user_id) = lower(b.uid) and a.contract_status = 1 and b.d = date_add(current_date,-1);
-- 28377
--（2） 访问过超级会员购买页的UV及占比；
select count(a.uid),count(b.uid)
from 
(select distinct uid from dw_mobdb.factmbpageview  where d >= date_add(current_date,-7) and pagecode in ('10320664636','10320664637','10320664638')) a 
left join (select user_id as uid from ods_innofin.user_contract where contract_status = 1) b on lower(a.uid) = lower(b.uid)  
--207207/14190
--
--2、 已开通超级会员的用户（数据sff已提供路径）中：
--（1）这些超级会员中，未申请过开通拿去花，且具备开通拿去花资质的用户数及占比；
select count(1) from dw_pubsharedb.super_member a 
inner join tmp_innofin.cfbdb_vqq_user_prefix_model b on lower(a.uid) = lower(b.uid) 
left join ods_innofin.user_contract c on  lower(a.uid) = lower(c.user_id) where c.user_id is null and a.d = date_add(current_date,-1);;
 
 --- 42184
