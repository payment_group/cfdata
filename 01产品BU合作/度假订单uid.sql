use tmp_innofin;
create table if not exists vqq_uid_pkorder_20170728_detail(
	uid string,
	orderdate string,
	contract_status int,
	prefix_model int
);
insert overwrite table tmp_innofin.vqq_uid_pkorder_20170728_detail
select a.uid,orderdate,case when contract_status is null then -1 when contract_status = 1 then 1 else 0 end,
	if(c.uid is null,0,1)
from
(select distinct uid ,orderdate from tmp_innofin.cfbdb_vqq_uid_pkorder_20170728) a 
left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id) 
left join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid);

select orderdate, contract_status, count(1) from tmp_innofin.vqq_uid_pkorder_20170728_detail group by orderdate, contract_status order by orderdate, contract_status limit 100;

select orderdate, count(1) from tmp_innofin.vqq_uid_pkorder_20170728_detail where contract_status = -1 and prefix_model = 1 group by orderdate;

--度假首页uid
use tmp_innofin;
create table if not exists vqq_uid_vac_c20170728(
	uid string,
	d string
);
insert overwrite table tmp_innofin.vqq_uid_vac_c20170728
select distinct uid,d from dw_mobdb.factmbpageview where d between '2017-07-10' and '2017-07-17' and pagecode='vac_home' and uid is not null and uid  <> '';

use tmp_innofin;
create table if not exists vqq_uid_vac_c20170728_detail(
	uid string,
	orderdate string,
	contract_status int,
	prefix_model int
);
insert overwrite table tmp_innofin.vqq_uid_vac_c20170728_detail
select a.uid,a.d,case when contract_status is null then 99 when contract_status = 1 then 1 else 0 end,
	if(c.uid is null,0,1)
from
tmp_innofin.vqq_uid_vac_c20170728 a 
left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id) 
left join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid);

select orderdate, contract_status, count(1) from tmp_innofin.vqq_uid_vac_c20170728_detail group by orderdate, contract_status order by orderdate, contract_status limit 100;
select orderdate, count(1) from tmp_innofin.vqq_uid_vac_c20170728_detail where contract_status = -1 and prefix_model = 1 group by orderdate;

--- 金额分布（公有云）
select min(amount),max(amount) from cfbdb.ctrip_cloud_uid_pkgorder_detail_snap where to_date(createtime) between '2017-07-10' and '2017-07-17' and orderstatus='10'
-- 50 ~~ 500050
-- 1k,3k,5k,10k,20k,50k
use cfbdb;
create table if not exists vqq_pkgorder_amount(
	uid string,
	orderid string,
	orderdate string,
	amount int,
	amount_group string
);
insert overwrite table cfbdb.vqq_pkgorder_amount
select uid,orderid,to_date(createtime),amount,
	case when amount <= 1000 then 'g1_1k' 
		when amount > 1000 and amount <=3000 then 'g2_3k'
		when amount > 3000 and amount <=5000 then 'g3_5k'
		when amount > 5000 and amount <=10000 then 'g4_10k'
		when amount > 10000 and amount <=20000 then 'g5_20k'
		when amount > 20000 and amount <=50000 then 'g6_50k'
		else 'g7_50k+' end
from cfbdb.ctrip_cloud_uid_pkgorder_detail_snap where to_date(createtime) between '2017-07-10' and '2017-07-17' and orderstatus='10';

select orderdate,amount_group,count(1) from cfbdb.vqq_pkgorder_amount group by orderdate,amount_group 
