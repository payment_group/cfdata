-- uv数据 ubt 
use tmp_innofin;
create table vqq_prefix_model_users_activity_pv as
select a.uid,
	starttime,
	c.pagecode,
	c.pagename,
	c.category,
	c.categoryname ,
	countryname,
	provincename ,
	cityname from tmp_innofin.cfbdb_user_prefix_model a 
	inner join (select * from dw_mobdb.factmbpageview where d >= '2017-01-01') b on lower(a.uid) = lower(b.uid)
	inner join dim_mobdb.dimmbpagecode c on b.pagecode = c.pagecode 
where c.categoryname like '%活动%';

use tmp_innofin;
create table if not exists vqq_prefix_model_users_activity_uv_monthly
(
	uid string,
	ym string,
	uv_date_count int
);


insert overwrite table  tmp_innofin.vqq_prefix_model_users_activity_uv_monthly
select uid,substring(starttime,1,7),count(distinct to_date(starttime))
from tmp_innofin.vqq_prefix_model_users_activity_pv a
group by uid,substring(starttime,1,7);

--计算分数
use tmp_innofin;
create table if not exists vqq_prefix_model_users_activity_score_monthly
(
	uid string,
	ym string,
	score_30 double,
	score_plus1 double
);

insert overwrite table  tmp_innofin.vqq_prefix_model_users_activity_score_monthly
select uid,ym,uv_date_count/30,uv_date_count/(uv_date_count+1)
from tmp_innofin.vqq_prefix_model_users_activity_uv_monthly;

use tmp_innofin;
create table if not exists vqq_prefix_model_users_activity_score_total
(
	uid string,
	score_30 double,
	score_plus1 double
);

insert overwrite table  tmp_innofin.vqq_prefix_model_users_activity_score_total
select uid,sum(score_30),sum(score_plus1)
from tmp_innofin.vqq_prefix_model_users_activity_score_monthly where uid is not null and trim(uid) <> '' group by uid ;

-- 导出数据，看分布

select min(score_30),max(score_30),min(score_plus1),max(score_plus1) from tmp_innofin.vqq_prefix_model_users_activity_score_total

