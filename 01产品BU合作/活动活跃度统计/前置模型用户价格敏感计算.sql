-- uv数据 ubt 
use tmp_innofin;
create table vqq_prefix_model_users_low_price_pv as
select a.uid,
	starttime,
	pagecode,
	pagename from tmp_innofin.cfbdb_user_prefix_model a 
	inner join (select * from dw_mobdb.factmbpageview where d >= '2017-01-01' and (pagename like '%特价%' or pagename like '%低价%' or pagename like '%优惠%' or pagename like '%折扣%')) 
		b on lower(a.uid) = lower(b.uid);
		

use tmp_innofin;
create table vqq_prefix_model_users_low_price_pv_daily as
select uid,to_date(starttime) as dt ,count(distinct pagecode) as pagecount
from tmp_innofin.vqq_prefix_model_users_low_price_pv a
group by uid,to_date(starttime);

--计算分数
use tmp_innofin;
create table vqq_prefix_model_users_low_price_score_daily as 
select uid,dt,pagecount/(pagecount+1) as score
from tmp_innofin.vqq_prefix_model_users_low_price_pv_daily;

use tmp_innofin;
create table vqq_prefix_model_users_low_price_score as
select uid,sum(score) as totalscore
from tmp_innofin.vqq_prefix_model_users_low_price_score_daily where uid is not null and trim(uid) <> '' group by uid ;



