use ods_innofin;
create table if not exists bbz_oi_order_hotel(
	dt	string,
	orderid	bigint,
	serverfrom	string,
	contactnumber	string,
	guestname	string,
	servertype	string,
	arrivaltime	string,
	departuretime	string,
	hotelid	string,
	hotelname	string,
	price	string,
	quantity	string,
	cityname	string,
	cityid	string,
	roomname	string,
	uid	string,
	bookingdate	string,
	orderstatus	string,
	orderdescription	string,
	ordertype	string,
	operatetime	string,
	message_createtime_raw	string,
	message_createtime	string,
	sourcefromcode	string,
	actualorderamount	string
);


use ods_innofin;
insert overwrite table ods_innofin.bbz_oi_order_hotel
select distinct dt,orderid,
       regexp_extract(content,'ServerFrom":"([^"]+)"',1) as serverfrom,
       regexp_extract(content,'ContactNumber":"[0](1[0-9]{10})"',1) as contactnumber,
       regexp_extract(content,'UserNames":"([^"]+)"',1) as guestname,
       regexp_extract(content,'ServerType":"([^"]+)"',1) as servertype,
       from_unixtime(cast(regexp_extract(content,'ArrivalTime":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd HH:mm:ss') as arrivaltime,
       from_unixtime(cast(regexp_extract(content,'DepartureTime":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd HH:mm:ss') as departuretime,
       regexp_extract(content,'HotelID":"([^"]+)"',1) as hotelid,
       regexp_extract(content,'Hotelname":"([^"]+)"',1) as hotelname,
       regexp_extract(content,'Price":"([^"]+)"',1) as price,
       regexp_extract(content,'Quantity":([0-9.]+),',1) as quantity,
       regexp_extract(content,'CityName":"([^"]+)"',1) as cityname,
       regexp_extract(content,'CityId":"([^"]+)"',1) as cityid,
       regexp_extract(content,'RoomName":"([^"]+)"',1) as roomname,
       regexp_extract(content,'Uid":"([^"]+)"',1) as uid,
       from_unixtime(cast(regexp_extract(content,'BookingDate":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd HH:mm:ss') as bookingdate,
       regexp_extract(content,'OrderStatus":"([^"]+)",',1) as orderstatus,
       regexp_extract(content,'OrderDescription":"([^"]+)",',1) as orderdescription,
       regexp_extract(content,'OrderType":"([^"]+)",',1) as ordertype,
       from_unixtime(cast(regexp_extract(content,'OperateTime":"/Date\\(([0-9]{10})[0-9]{3}\\+',1) as bigint),'yyyy-MM-dd HH:mm:ss') as operatetime,
       regexp_extract(content,'Message_CreateTime":"([^"]+)"',1) as message_createtime_raw,
       from_unixtime(unix_timestamp(regexp_extract(content,'Message_CreateTime":"([^"]+)"',1),'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd HH:mm:ss') as message_createtime,
       regexp_extract(content,'SourceFromCode":"([^"]+)",',1) as sourcefromcode,
       regexp_extract(content,'ActualOrderAmount":([0-9\\.]+)',1) as actualorderamount
from bbz_oi_orderdb.bbz_oi_order_hotel
where dt >= '2017-01-01'
and dt < '${zdt.format("yyyy-MM-dd")}';
