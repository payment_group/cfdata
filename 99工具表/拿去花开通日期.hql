-- 生成日期列表
use tmp_innofin;
create table if not exists vqq_tmp_nqh_active_date(
	dt string
);
insert overwrite table vqq_tmp_nqh_active_date
select date_add(t.start_date,pe.i) as dt
from  (select min(active_date) as start_date, date_add(current_date,-1) as end_date from ods_innofin.nqh_user_active_date) t
      lateral view 
      posexplode(split(space(datediff(t.end_date,t.start_date)),' ')) pe as i,x;

use ods_innofin;
create table if not exists nqh_user_active_date(
	uid string,
	open_id string,
	active_date string,
	tpp_code string,
	user_name string,
	activate_amt decimal(9,2)
);

insert overwrite table ods_innofin.nqh_user_active_date
select a.user_id,a.open_id,b.active_date,b.tpp_code,b.user_name,b.activate_amt from
ods_innofin.user_contract a
inner join (select distinct user_id, to_date(finish_time) as active_date,tpp_code,user_name,activate_amt from source_mobdb.ctrip_tbl_credit_activate_snap
	where d= '${zdt.addDay(-1).format("yyyy-MM-dd")}' and org_channel = 'CTRIP' and credit_type=1 and product_no = 'IOUS'  and accounting_status = 2) b 
	on a.open_id = b.user_id;
---
use ods_innofin;
create table if not exists nqh_user_active_daily(
	dt string,
	active_user int
);

insert overwrite table nqh_user_active_daily
select a.dt,sum(if(b.uid is null,0,1)) from
tmp_innofin.vqq_tmp_nqh_active_date a 
left join ods_innofin.nqh_user_active_date b on a.dt = b.active_date
group by a.dt;

--- 
use ods_innofin;
create table if not exists nqh_user_active_total(
	dt string,
	active_user_total int
);

insert overwrite table ods_innofin.nqh_user_active_total
select dt,sum(active_user) over (order by dt rows between unbounded preceding and current row) cumulative_count from ods_innofin.nqh_user_active_daily;


-- 激活过拿去花用户
use ods_innofin;
create table if not exists nqh_user_activated(
	uid string,
	open_id string,
	original_uid string
);

insert overwrite table ods_innofin.nqh_user_activated
select distinct a.user_id,a.open_id,c.original_uid from
ods_innofin.user_contract a
inner join (select distinct user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap
	where org_channel = 'CTRIP' and credit_type=1 and product_no = 'IOUS'  and activate_status = 2) b 
	on lower(a.open_id) = lower(b.user_id)
inner join (select distinct lower(user_id) as user_id,lower(original_uid) as original_uid from cfbdb.userid_all_sign_up_mapping) c on lower(a.user_id) = c.user_id;
