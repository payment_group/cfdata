use ods_innofin;
create table if not exists nqh_mkt_message_push_event(
	scene_name string,
	scene_code string,
	mktype string,
	min_push_date string,
	max_push_date string
);
-- 
insert overwrite table nqh_mkt_message_push_event values
('母亲节拉新','Pyou180511c1','Pyou180511c1','2018-05-09','2018-05-11'),
('开通流失挽回-1首页流失','Pyou180509c1','push0508','2018-05-08','2018-05-31'),
('开通流失挽回-2绑卡流失','Pyou180510c1','push0508','2018-05-08','2018-05-31'),
('开通流失挽回-3信息核实流失','Pyou180508c1','push0508','2018-05-08','2018-05-31'),

('开通流失挽回-1首页流失','Dfig509a','Dfig509a','2018-05-08','2018-05-31'),
('开通流失挽回-2绑卡流失','Dfig509b','Dfig509b','2018-05-08','2018-05-31'),
('开通流失挽回-3信息核实流失','Dfig509c','Dfig509c','2018-05-08','2018-05-31'),
('预授信通过短信','Pfig180510c1','Pfig180510c1','2018-05-07','2018-05-31'),
('母亲节拉新','Pfig180509c1','Pfig180509c1','2018-05-09','2018-05-11'),

('机票弹窗营销活动','Dfig509d','AppPush_FlightPop','2018-05-14','2018-06-30'),

('目标营销库全量拉新','Mfif180507f1','msg0507','2018-05-07','2018-05-31'),
('目标营销库全量拉新','Mfif180518f1','msg0518','2018-05-07','2018-05-31'),
('目标营销库全量拉新','Mfif180525f1','msg0525','2018-05-07','2018-05-31'),
('目标营销库全量拉新','Mfif180531f1','msg0531','2018-05-07','2018-05-31'),

('儿童节-门票营销活动','Dfig525a','children_Dfig525a','2018-05-07','2018-06-02'),
('儿童节-门票营销活动','Dfig525b','children_Dfig525b','2018-05-07','2018-06-02'),
('儿童节-门票营销活动','Dfig525c','children_Dfig525c','2018-05-07','2018-06-02');

use ods_innofin;
create table if not exists nqh_mkt_message_push_plan(
	scene_name string,
	mktype string
)partitioned by(
	scene_code string,
	push_date string,
	push_time string
);
-- history
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Pyou180511c1',push_date = '2018-05-08',push_time = '15:00',) values('母亲节拉新','Pyou180511c1');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Pyou180509c1',push_date = '2018-05-08',push_time = '15:00',) values('开通流失挽回-1首页流失','push0508');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Pyou180510c1',push_date = '2018-05-08',push_time = '15:00',) values('开通流失挽回-2绑卡流失','push0508');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Pyou180508c1',push_date = '2018-05-08',push_time = '15:00',) values('开通流失挽回-3信息核实流失','push0508');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509a',push_date = '2018-05-11',push_time = '15:00',) values('开通流失挽回-1首页流失','Dfig509a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509b',push_date = '2018-05-11',push_time = '15:00',) values('开通流失挽回-2绑卡流失','Dfig509b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509c',push_date = '2018-05-11',push_time = '15:00',) values('开通流失挽回-3信息核实流失','Dfig509c');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Pfig180510c1',push_date = '2018-05-11',push_time = '15:00',) values('预授信通过短信','Pfig180510c1');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Pfig180509c1',push_date = '2018-05-11',push_time = '15:00',) values('母亲节拉新','Pfig180509c1');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-05-14',push_time = '15:00',) values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509a',push_date = '2018-05-16',push_time = '15:00',) values('开通流失挽回-1首页流失','Dfig509a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509b',push_date = '2018-05-16',push_time = '15:00',) values('开通流失挽回-2绑卡流失','Dfig509b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509c',push_date = '2018-05-16',push_time = '15:00',) values('开通流失挽回-3信息核实流失','Dfig509c');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-05-16',push_time = '15:00',) values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509a',push_date = '2018-05-18',push_time = '15:00',) values('开通流失挽回-1首页流失','Dfig509a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509b',push_date = '2018-05-18',push_time = '15:00',) values('开通流失挽回-2绑卡流失','Dfig509b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509c',push_date = '2018-05-18',push_time = '15:00',) values('开通流失挽回-3信息核实流失','Dfig509c');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Mfif180507f1',push_date = '2018-05-07',push_time = '15:00',) values('目标营销库全量拉新','msg0507');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Mfif180518f1',push_date = '2018-05-18',push_time = '15:00',) values('目标营销库全量拉新','msg0518');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Mfif180525f1',push_date = '2018-05-25',push_time = '15:00',) values('目标营销库全量拉新','msg0525');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Mfif180531f1',push_date = '2018-05-31',push_time = '15:00',) values('目标营销库全量拉新','msg0531');
--
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-05-29',push_time='12:00') values('儿童节-门票营销活动','children_Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-05-29',push_time='15:00',) values('儿童节-门票营销活动','children_Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-05-29',push_time='17:00') values('儿童节-门票营销活动','children_Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-05-30',push_time='12:00') values('儿童节-门票营销活动','children_Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-05-30',push_time='15:00',) values('儿童节-门票营销活动','children_Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-05-30',push_time='17:00') values('儿童节-门票营销活动','children_Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-05-31',push_time='12:00') values('儿童节-门票营销活动','children_Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-05-31',push_time='15:00',) values('儿童节-门票营销活动','children_Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-05-31',push_time='17:00') values('儿童节-门票营销活动','children_Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-06-01',push_time='12:00') values('儿童节-门票营销活动','children_Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-06-01',push_time='15:00',) values('儿童节-门票营销活动','children_Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-06-01',push_time='17:00') values('儿童节-门票营销活动','children_Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-06-02',push_time='12:00') values('儿童节-门票营销活动','children_Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-06-02',push_time='15:00',) values('儿童节-门票营销活动','children_Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-06-02',push_time='17:00') values('儿童节-门票营销活动','children_Dfig525c');


use ods_innofin;
create table if not exists nqh_mkt_message_push_result(
	scene_code string,
	push_date string,
	push_time string,
	actual_push_time string
);
-- history 
insert into table ods_innofin.nqh_mkt_message_push_result values
('Pyou180511c1','2018-05-08','15:00','2018-05-08 15:00:00'),
('Pyou180509c1','2018-05-08','15:00','2018-05-08 15:00:00'),
('Pyou180510c1','2018-05-08','15:00','2018-05-08 15:00:00'),
('Pyou180508c1','2018-05-08','15:00','2018-05-08 15:00:00'),
('Dfig509a','2018-05-11','15:00','2018-05-11 15:00:00'),
('Dfig509b','2018-05-11','15:00','2018-05-11 15:00:00'),
('Dfig509c','2018-05-11','15:00','2018-05-11 15:00:00'),
('Pfig180510c1','2018-05-11','15:00','2018-05-11 15:00:00'),
('Pfig180509c1','2018-05-11','15:00','2018-05-11 15:00:00'),
('Dfig509d','2018-05-14','15:00','2018-05-14 15:00:00'),
('Dfig509a','2018-05-16','15:00','2018-05-16 15:00:00'),
('Dfig509b','2018-05-16','15:00','2018-05-16 15:00:00'),
('Dfig509c','2018-05-16','15:00','2018-05-16 15:00:00'),
('Dfig509d','2018-05-16','15:00','2018-05-16 15:00:00'),
('Dfig509a','2018-05-18','15:00','2018-05-18 15:00:00'),
('Dfig509b','2018-05-18','15:00','2018-05-18 15:00:00'),
('Dfig509c','2018-05-18','15:00','2018-05-18 15:00:00'),
('Mfif180507f1','2018-05-07','15:00','2018-05-07 15:00:00'),
('Mfif180518f1','2018-05-18','15:00','2018-05-18 15:00:00'),
('Mfif180525f1','2018-05-25','15:00','2018-05-25 15:00:00'),
('Mfif180531f1','2018-05-31','15:00','2018-05-31 15:00:00');


use ods_innofin;
create table if not exists vqq_rpt_active_process_pages_mktype(
	uid string,
	mktype string,
	apply int,
	cardguide int,
	card_number int,
	card int,
	setpass int,
	active int,
	finish int,
	success int
) partitioned by (
	dt string
);

insert overwrite table ods_innofin.vqq_rpt_active_process_pages_mktype partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,mktype,apply,cardguide,card_number,card,setpass,active,finish,success from 
(select
	uid,
	regexp_extract(query,'&mktype=([^&]+)',1) as mktype,
	max(case when pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431','10650001007')  then 1 else 0 end) as apply,		--进入激活广告页
    max(case when pagecode in ('10320643008','10320643007') and query like '%bustype=9907%'  then 1 else 0 end) as cardguide, --绑卡引导页
	max(case when pagecode in ('10320643027','10320643028')  then 1 else 0 end) as card_number,
	max(case when pagecode in ('10320642997','10320642998')  then 1 else 0 end) as card,
    max(case when pagecode in ('271074','272092')  then 1 else 0 end) as setpass,
	max(case when pagecode in ('10320657063','10320657064') then 1 else 0 end) as active,
	max(case when pagecode in ('10320607112','10320607172','10320607113','10320607173','10320607111','10320607171') then 1 else 0 end) as finish,
	max(case when pagecode in ('10320607113','10320607173') then 1 else 0 end) as success
from dw_mobdb.factmbpageview 
where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
group by uid,regexp_extract(query,'&mktype=([^&]+)',1)
) t where apply = 1;

--- 依赖ubt不是log
use ods_innofin;
create table if not exists nqh_mkt_message_push_direct_success(
 	dt string,
 	uid string,
 	mktype string,
 	activate int,
 	activate_success int
);
insert overwrite table ods_innofin.nqh_mkt_message_push_direct_success
select 
 	distinct c.dt,
 	b.uid,
 	a.mktype,
 	if(c.uid is null,0,1),
 	if(d.uid is null,0,1)
  from 
  	(select distinct mktype from ods_innofin.nqh_mkt_message_push_event) a
  	inner join (select * from ods_innofin.vqq_rpt_active_process_pages_mktype where dt >='2018-05-01') b on a.mktype = b.mktype
  	left join (select distinct user_id as uid,to_date(request_time) as dt from ods_innofin.user_activate_req where to_date(request_time) >='2018-05-01') c on c.dt = b.dt and lower(b.uid) = lower(c.uid)
 	left join (select distinct user_id as uid from ods_innofin.user_contract where contract_status = 1) d on lower(b.uid) = lower(d.uid)


-- use ods_innofin;
-- create table if not exists nqh_mkt_message_push_direct_success(
-- 	dt string,
-- 	uid string,
-- 	mktype string,
-- 	activate int,
-- 	activate_success int
-- );
-- insert overwrite table ods_innofin.nqh_mkt_message_push_direct_success
-- select 
-- 	distinct to_date(create_time) as dt,
-- 	b.user_id,
-- 	a.mktype,
-- 	if(c.osid is null, 0,1),
-- 	if(c.action_desc = 'CREDIT_SUCCESS',1,0)
--  from 
--  	(select distinct mktype from ods_innofin.nqh_mkt_message_push_event) a
--  	inner join (select user_id,osid,get_json_object(action_desc,'$.mktype') as mktype,create_time from ods_innofin.user_action_info where create_time >= '2018-05-01' and action_type = 'INDEX') b
--  		on  a.mktype = b.mktype
-- 	left join (select osid, get_json_object(action_desc,'$.creditStatus') as action_desc from ods_innofin.user_action_info where create_time >= '2018-05-01' and action_type = 'ACTIVATE') c on b.osid = c.osid;

-- history 
insert overwrite table ods_innofin.vqq_rpt_active_process_pages_mktype partition(dt)
select 	uid ,
	mktype ,
	apply ,
	cardguide ,
	card_number ,
	card ,
	setpass ,
	active ,
	finish ,
	success,
	dt from 
(select
	d as dt,
	uid,
	regexp_extract(query,'&mktype=([^&]+)',1) as mktype,
	max(case when pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431','10650001007')  then 1 else 0 end) as apply,		--进入激活广告页
    max(case when pagecode in ('10320643008','10320643007') and query like '%bustype=9907%'  then 1 else 0 end) as cardguide, --绑卡引导页
	max(case when pagecode in ('10320643027','10320643028')  then 1 else 0 end) as card_number,
	max(case when pagecode in ('10320642997','10320642998')  then 1 else 0 end) as card,
    max(case when pagecode in ('271074','272092')  then 1 else 0 end) as setpass,
	max(case when pagecode in ('10320657063','10320657064') then 1 else 0 end) as active,
	max(case when pagecode in ('10320607112','10320607172','10320607113','10320607173','10320607111','10320607171') then 1 else 0 end) as finish,
	max(case when pagecode in ('10320607113','10320607173') then 1 else 0 end) as success
from dw_mobdb.factmbpageview 
where d >= '2018-05-01'
group by d,uid,regexp_extract(query,'&mktype=([^&]+)',1)
) t where apply = 1;


-- 市场部数据
use tmp_innofin;
create table if not exists vqq_rpt_push_mkt_result(
	scene_name string,
	scene_code string,
	push_channel string,

	push_count_sbu int,
	push_count_mkt int,
	push_count_cti int
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_rpt_push_mkt_result
select scene_name,scene_code,sendchannel,
	sum(sbusendstatus) as push_count_sbu,
	sum(mktsendstatus) as push_count_mkt,
	sum(ctisendstatus) as push_count_cti
from (select distinct scene_name,scene_code from ods_innofin.nqh_mkt_message_push_event) a 
	inner join (select scenecode,sendchannel,uid,max(if(sbusendstatus = 'T',1,0)) as sbusendstatus,max(if(mktsendstatus = 'T',1,0)) as mktsendstatus,max(if(ctisendstatus = 'T',1,0)) as ctisendstatus 
		from dw_pubsharedb.factmktmembersenddetail_fin where d>= '2018-05-01' group by scenecode,sendchannel,uid) b
	on lower(a.scene_code) = lower(b.scenecode)
group by a.scene_name,a.scene_code,b.sendchannel;

-- 直接
use tmp_innofin;
create table if not exists vqq_rpt_push_direct_uv(
	scene_name string,
	scene_code string,
	send_channel string,
	apply int,
	finish int,
	db_success int
) partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_rpt_push_direct_uv partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.scene_name,a.scene_code,b.sendchannel,count(distinct d.uid),nvl(sum(activate),0),nvl(sum(success),0)
from ods_innofin.nqh_mkt_message_push_event a 
inner join (select distinct scenecode,sendchannel,uid from dw_pubsharedb.factmktmembersenddetail_fin where d>= '2018-05-01') b on a.scene_code = b.scenecode
left join (select mktype,uid, max(activate) as activate,max(activate_success) as success from ods_innofin.nqh_mkt_message_push_direct_success group by mktype,uid) d on a.mktype = d.mktype and lower(b.uid) = lower(d.uid)
group by a.scene_name,a.scene_code,b.sendchannel
;

-- 间接
use tmp_innofin;
create table if not exists vqq_rpt_push_indirect_uv(
	scene_name string,
	scene_code string,
	send_channel string,
	apply int,
	finish int,
	db_success int
) partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_rpt_push_indirect_uv partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.scene_name,a.scene_code,b.sendchannel,sum(apply),sum(finish),count(distinct d.uid)
from ods_innofin.nqh_mkt_message_push_event a 
inner join (select scenecode,sendchannel,uid,min(d) as min_d,max(d) as max_d from dw_pubsharedb.factmktmembersenddetail_fin where d>= '2018-05-01' group by scenecode,sendchannel,uid) b on a.scene_code = b.scenecode
left join (select dt,uid,max(apply) as apply, max(finish) as finish from ods_innofin.vqq_rpt_active_process_pages_mktype where dt >= '2018-05-01' group by dt,uid) c 
		on lower(b.uid) = lower(c.uid)
left join ods_innofin.nqh_user_active_date d on lower(c.uid) = lower(d.uid)
where c.dt between b.min_d and date_add(b.max_d,15) and (d.active_date is null or d.active_date between b.min_d and date_add(b.max_d,15))
group by scene_name,scene_code,mktype,sendchannel
;


use tmp_innofin;
create table if not exists vqq_rpt_push_performance(
	scene_name string comment '推送活动',
	push_channel string comment 'PUSH通道',

	push_count_sbu int comment 'BU成功推送数',
	push_count_mkt int comment '市场成功推送数',
	push_count_cti int comment 'CTI成功推送数',
	
	apply int comment '间接转化-首页uv',
	finish int comment '间接转化-完成页uv',
	success int comment '间接转化-激活成功',
	
	direct_apply int comment '直接转化-首页uv',
	direct_finish int comment '直接转化-完成页uv',
	direct_success int  comment '直接转化-激活成功'
)partitioned by (
	dt string comment '统计日期'
);

use tmp_innofin;
insert overwrite table vqq_rpt_push_performance partition (dt = '${zdt.format("yyyy-MM-dd")}')
select t1.scene_name,
	t1.push_channel,
	t1.push_count_sbu,
	t1.push_count_mkt,
	t1.push_count_cti,
	nvl(t2.apply,0),
	nvl(t2.finish,0),
	nvl(t2.db_success,0),
	nvl(t3.apply,0),
	nvl(t3.finish,0),
	nvl(t3.db_success,0)
from 
(select scene_name,push_channel,sum(push_count_sbu) as push_count_sbu,sum(push_count_mkt) as push_count_mkt,sum(push_count_cti) as push_count_cti
	from tmp_innofin.vqq_rpt_push_mkt_result group by scene_name,push_channel) t1 
left join (select scene_name,send_channel,sum(apply) as apply,sum(finish) as finish,sum(db_success) as db_success 
	from tmp_innofin.vqq_rpt_push_indirect_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' group by scene_name,send_channel) t2 
	on t1.scene_name = t2.scene_name and t1.push_channel = t2.send_channel
left join (select scene_name,send_channel,sum(apply) as apply,sum(finish) as finish,sum(db_success) as db_success 
	from tmp_innofin.vqq_rpt_push_direct_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' group by scene_name,send_channel) t3 
	on t1.scene_name = t3.scene_name and t1.push_channel = t3.send_channel
;