
--- 公有云
use tmp_cfbdb;
create if not exists vqq_cloud_result(
	uid string,
	u_date string,
	u_group string,
	u_value string
)partitioned by (
	dt string
);

insert into table vqq_cloud_result partition (dt = '${zdt.format("yyyy-MM-dd")}')
values select a.uid,active_date as u_date,
bu as u_group, b.activate_prob as u_value
from (select * from cfbdb.vqq_nqh_beforeafter_uv where dt = '${zdt.format("yyyy-MM-dd")}') a 
inner join cfbdb.ctrip_credit_user_info_to_credit_4 b on lower(a.uid) = lower(b.user_id);
 
-- "1) 年龄在（25-45）age 
-- 2) 节假日消费意愿高 vacation_day_desire
-- 3)家庭账号概率高 family_prob
-- 4)价格敏感度高 total_sensitive
-- 5)激活响应概率高" activate_prob
-- 取200万数据

insert into table tmp_cfbdb.vqq_cloud_result partition (dt = '${zdt.format("yyyy-MM-dd")}')
select a.user_id,'${zdt.format("yyyy-MM-dd")}' as u_date,
'201805_PUSH_MOTHERS_DAY' as u_group, concat(age , ':' , vacation_day_desire ,':' , family_prob , ':' , total_sensitive , ':' , activate_prob) as u_value
from cfbdb.ctrip_credit_user_info_to_credit_4 a
where age between 25 and 45
	and vacation_day_desire >= 20
	and family_prob >= 0.2
	and total_sensitive >= 10
	and activate_prob >= 0.3
;

---- 导入内网
-- tmp_innofin.vqq_cloud_result
-- 取数推送
---- 按活动分表
use tmp_innofin;
create table if not exists vqq_cloud_result_his(
	uid string,
	u_date string,
	u_value string
)partitioned by 
(
	dt string,
	u_group string
);

INSERT OVERWRITE TABLE vqq_cloud_result_his PARTITION (dt, u_group)
SELECT uid,u_date,u_value,dt,u_group FROM vqq_cloud_result;

-- 0508  Pyou180511c1 100000
-- 排除半个月内申请过
use ods_innofin;
create table if not exists nqh_mkt_message_push_manual(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Pyou180511c1',dt = '2018-05-08')
select a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from  (select * from tmp_innofin.vqq_cloud_result where u_group = '201805_PUSH_MOTHERS_DAY') a
left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
left join (select uid from ods_innofin.nqh_mkt_message_push_manual where dt = '2018-05-08') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by rand() limit 100000;

 