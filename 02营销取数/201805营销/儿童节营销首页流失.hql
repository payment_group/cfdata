-- 儿童节营销首页流失
use tmp_innofin;
create table if not exists vqq_temp_61_fail(
	scene_name string,
	flow_name string,
	uid string
);

insert overwrite table tmp_innofin.vqq_temp_61_fail
select distinct scene_name,
	case when log_creditProcess = 0 and log_realName = 1 then '已实名需绑卡'
		when log_creditProcess = 0 and log_realName = 0 then '未实名'
		when log_creditProcess = 1 then '已实名无需绑卡'
		when log_creditProcess = 2 then '已实名一键开通'
 		else '' end as flow_name,
	uid 
	from (select * from tmp_innofin.nqh_mkt_rpt_event_details where scene_name in ('儿童节-门票营销活动','儿童节-门票营销活动-banner') and page_apply = 1 and button_apply = 0) a 
	left join (select distinct user_id,open_id from ods_innofin.user_contract where product_no = 'IOUS' and contract_status = 1) b on lower(a.uid) = lower(b.user_id)
	where b.user_id is null
 ;



-- use tmp_innofin;
-- select distinct scene_name,log_creditProcess,a.uid,c.error_code,c.error_msg,d.error_msg,
-- 	0,'' 
-- from (select distinct scene_name,log_creditProcess,uid from tmp_innofin.nqh_mkt_rpt_event_details where scene_name in ('儿童节-门票营销活动','儿童节-门票营销活动-banner') and db_active = 1 and db_success = 0 and log_creditProcess in(1,2)) a
-- inner join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id) 
-- left join (select distinct user_id as user_id,error_code,error_msg from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap  
-- 		where to_date(finish_time)>= '2018-06-08'
-- 		and product_no = 'IOUS' and org_channel = 'CTRIP') c on lower(b.open_id) = lower(c.user_id)
-- left join vqq_credit_fail_msg d on c.error_code = d.error_code
-- ;
-- 
use tmp_innofin;
select * from (
select scene_name,
uid,open_id,
case when c.req_status is null then '无授信记录'  when req_status = 0 then '处理中' when  req_status = 1 then '授信成功' else '授信失败' end  as credit,
case when d.activate_status is null then '' when activate_status = 0 then '初始状态' when  activate_status = 1 then '处理中' when activate_status = 2 then '成功' else '激活失败' end as activate
from  tmp_innofin.vqq_temp_61_fail a
	inner join (select distinct user_id,open_id from ods_innofin.user_activate_req where product_no = 'IOUS' and req_source not in ('CFB_TASK','PRE_CREDIT_TASK')) b on lower(a.uid) = lower(b.user_id)
	left join (select distinct user_id,req_status from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where product_no = 'IOUS') c on lower(b.open_id) = lower(c.user_id)
	left join (select distinct user_id,activate_status from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap where product_no = 'IOUS' and activate_status <> 2) d on lower(b.open_id) = lower(d.user_id)) t
where credit in ('无授信记录','处理中','授信失败') or (credit = '授信成功' and activate in ('初始状态','处理中','激活失败')) and not(credit = '授信失败' and activate = '激活失败'); 
