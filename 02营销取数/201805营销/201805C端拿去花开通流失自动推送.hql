use tmp_innofin;
create table if not exists vqq_active_process_lost(
	uid string,
	flow string,
	lost_step string
)partitioned by(
	dt string comment '��������'
);

insert overwrite table vqq_active_process_lost partition (dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,
	case when log_creditProcess = 0 and log_realName = 1 then 'card'
		when log_creditProcess = 0 and log_realName = 0 then 'noname'
		when log_creditProcess = 1 then 'nocard'
		when log_creditProcess = 2 then 'rapid'
		else 'na' end,
	case when page_apply = 1 and button_apply = 0 then '1-lost-step_apply'
		when page_cardguide = 1 and page_card = 0 then '2-lost-step_card'
		when page_active = 1 and button_active = 0 then '3-lost-step_active'
		else 'others' end 
from vqq_rpt_active_process_detail where (dt >= '${zdt.addDay(-1).format("yyyy-MM-dd")}') and db_active = 0;

use ods_innofin;
create table if not exists nqh_mkt_message_push_auto(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);
insert overwrite table ods_innofin.nqh_mkt_message_push_auto partition (SceneCode = 'Dfig509a',dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from 
	(select uid from tmp_innofin.vqq_active_process_lost where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and lost_step = '1-lost-step_apply') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
where c.uid is null
;

insert overwrite table ods_innofin.nqh_mkt_message_push_auto partition (SceneCode = 'Dfig509b',dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from 
	(select uid from tmp_innofin.vqq_active_process_lost where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and lost_step = '2-lost-step_card') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
where c.uid is null
;

insert overwrite table ods_innofin.nqh_mkt_message_push_auto partition (SceneCode = 'Dfig509c',dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from 
	(select uid from tmp_innofin.vqq_active_process_lost where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and lost_step = '3-lost-step_active') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
where c.uid is null
;









