-- 推送用户访问及开通
use tmp_innofin;
create table if not exists vqq_temp_push_user_direct_uv(
	uid string,
	mktype string,
	apply int,
	cardguide int,
	card_number int,
	card int,
	setpass int,
	active int,
	finish int,
	success int,
	scenecode string,
	pushdate string,
	db_success int
) partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_temp_push_user_direct_uv partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.uid,a.mktype,apply,cardguide,card_number,card,setpass,active,finish,success,b.scenecode,pushdate,if(c.uid is null,0,1)
from
(select
	uid,
	regexp_extract(query,'&mktype=([^&]+)',1) as mktype,
	max(case when pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431','10650001007')  then 1 else 0 end) as apply,		--进入激活广告页
    max(case when pagecode in ('10320643008','10320643007') and query like '%bustype=9907%'  then 1 else 0 end) as cardguide, --绑卡引导页
	max(case when pagecode in ('10320643027','10320643028')  then 1 else 0 end) as card_number,
	max(case when pagecode in ('10320642997','10320642998')  then 1 else 0 end) as card,
    max(case when pagecode in ('271074','272092')  then 1 else 0 end) as setpass,
	max(case when pagecode in ('10320657063','10320657064') then 1 else 0 end) as active,
	max(case when pagecode in ('10320607112','10320607172','10320607113','10320607173','10320607111','10320607171') then 1 else 0 end) as finish,
	max(case when pagecode in ('10320607113','10320607173') then 1 else 0 end) as success
from dw_mobdb.factmbpageview 
where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
group by uid,regexp_extract(query,'&mktype=([^&]+)',1)) a
inner join (select scenecode,dt as pushdate,uid from  ods_innofin.nqh_mkt_message_push_manual where dt >= '2018-05-08'
	union all select scenecode,dt as pushdate,uid from ods_innofin.nqh_mkt_message_push where dt >= '2018-05-07'
	union all select scenecode,dt as pushdate,uid from ods_innofin.nqh_mkt_message_push_auto where dt >= '2018-05-07'
) b on lower(a.uid) = lower(b.uid) 
inner join (select scenecode,uid from dw_pubsharedb.factmktmembersenddetail_fin where d>= '2018-05-08' and ctisendstatus = 'T') d on lower(b.scenecode) = lower(d.scenecode) and lower(a.uid) = lower(d.uid) 
left join (select distinct uid from ods_innofin.nqh_user_active_date where active_date ='${zdt.addDay(-1).format("yyyy-MM-dd")') c on lower(a.uid) = lower(c.uid) 
where a.apply=1 and b.pushdate <= '${zdt.addDay(-1).format("yyyy-MM-dd")}';

-- 推送表
use tmp_innofin;
create table if not exists vqq_temp_mkt_push_group(
	push_date string,
	scene_name string,
	scene_code string,
	mktype string,
	push_count int,
	push_channel string
);
insert overwrite table vqq_temp_mkt_push_group values
-- 0508
('2018-05-08','母亲节拉新','Pyou180511c1','Pyou180511c1',100000,'APP_PUSH'),
('2018-05-08','开通流失挽回-1首页流失','Pyou180509c1','push0508',5000,'APP_PUSH'),
('2018-05-08','开通流失挽回-2绑卡流失','Pyou180510c1','push0508',3902,'APP_PUSH'),
('2018-05-08','开通流失挽回-3信息核实流失','Pyou180508c1','push0508',951,'APP_PUSH'),
-- 0511
('2018-05-11','开通流失挽回-1首页流失','Dfig509a','Dfig509a',15000,'APP_PUSH'),
('2018-05-11','开通流失挽回-2绑卡流失','Dfig509b','Dfig509b',10000,'APP_PUSH'),
('2018-05-11','开通流失挽回-3信息核实流失','Dfig509c','Dfig509c',3000,'APP_PUSH'),
('2018-05-11','预授信通过短信','Pfig180510c1','Pfig180510c1',50000,'SMS'),
('2018-05-11','母亲节拉新','Pfig180509c1','Pfig180509c1',100000,'APP_PUSH'),
-- 0514
('2018-05-14','机票弹窗营销活动','Dfig509d','AppPush_FlightPop',500000,'APP_PUSH'),
-- 0516
('2018-05-16','开通流失挽回-1首页流失','Dfig509a','Dfig509a',30000,'APP_PUSH'),
('2018-05-16','开通流失挽回-2绑卡流失','Dfig509b','Dfig509b',6007,'APP_PUSH'),
('2018-05-16','开通流失挽回-3信息核实流失','Dfig509c','Dfig509c',283,'APP_PUSH'),
('2018-05-16','机票弹窗营销活动','Dfig509d','AppPush_FlightPop',500000,'APP_PUSH'),
-- 0518
('2018-05-18','开通流失挽回-1首页流失','Dfig509a','Dfig509a',17411,'APP_PUSH'),
('2018-05-18','开通流失挽回-2绑卡流失','Dfig509b','Dfig509b',9552,'APP_PUSH'),
('2018-05-18','开通流失挽回-3信息核实流失','Dfig509c','Dfig509c',2370,'APP_PUSH'),
-- 目标营销库全量拉新
('2018-05-07','目标营销库全量拉新','Mfif180507f1','msg0507',1000000,'MSG'),
('2018-05-18','目标营销库全量拉新','Mfif180518f1','msg0518',1000000,'MSG'),
('2018-05-25','目标营销库全量拉新','Mfif180525f1','msg0525',1000000,'MSG'),
('2018-05-31','目标营销库全量拉新','Mfif180531f1','msg0531',1000000,'MSG')
-- 0529
('2018-05-29','儿童节-门票营销活动','Dfig525a','Dfig525a',1000000,'APP_PUSH'),
('2018-05-29','儿童节-门票营销活动','Dfig525b','Dfig525b',1000000,'APP_PUSH'),
('2018-05-29','儿童节-门票营销活动','Dfig525c','Dfig525c',1000000,'APP_PUSH'),
;

-- 市场部数据
use tmp_innofin;
create table if not exists vqq_rpt_push_mkt_result(
	scene_name string,
	push_channel string,

	push_count_sbu int,
	push_count_mkt int,
	push_count_cti int
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_rpt_push_mkt_result
select scene_name,sendchannel,
	sum(sbusendstatus) as push_count_sbu,
	sum(mktsendstatus) as push_count_mkt,
	sum(ctisendstatus) as push_count_cti
from (select distinct scene_name,scene_code from tmp_innofin.vqq_temp_mkt_push_group) a 
	inner join (select scenecode,sendchannel,uid,max(if(sbusendstatus = 'T',1,0)) as sbusendstatus,max(if(mktsendstatus = 'T',1,0)) as mktsendstatus,max(if(ctisendstatus = 'T',1,0)) as ctisendstatus 
		from dw_pubsharedb.factmktmembersenddetail_fin where d>= '2018-05-01' group by scenecode,sendchannel,uid) b
	on lower(a.scene_code) = lower(b.scenecode)
group by a.scene_name,b.sendchannel;


use tmp_innofin;
create table if not exists vqq_rpt_push_performance(
	scene_name string comment '场景',
	push_channel string comment 'PUSH通道',

	push_count_sbu int comment 'BU成功推送数',
	push_count_mkt int comment '市场成功推送数',
	push_count_cti int comment 'CTI成功推送数',
	
	apply int comment '间接转化-首页uv',
	finish int comment '间接转化-完成页uv',
	success int comment '间接转化-激活成功',
	
	direct_apply int comment '直接转化-首页uv',
	direct_finish int comment '直接转化-完成页uv',
	direct_success int  comment '直接转化-激活成功'
)partitioned by (
	dt string comment '统计日期'
);

use tmp_innofin;
insert overwrite table vqq_rpt_push_performance partition (dt = '${zdt.format("yyyy-MM-dd")}')
select t1.scene_name,
	t1.push_channel,
	t1.push_count_sbu,
	t1.push_count_mkt,
	t1.push_count_cti,
	nvl(t2.apply,0),
	nvl(t2.finish,0),
	nvl(t2.success,0),
	nvl(t2.direct_apply,0),
	nvl(t2.direct_finish,0),
	nvl(t2.direct_success,0)
from 
tmp_innofin.vqq_rpt_push_mkt_result t1 
inner join (
	select a.scene_name,sum(b.apply) as apply,sum(b.finish) as finish,sum(b.success) as success,
	sum(c.apply) as direct_apply,sum(c.finish) as direct_finish,sum(c.success) as direct_success
	from 
	(select distinct scene_name,scene_code,mktype from tmp_innofin.vqq_temp_mkt_push_group) a
	left join (select scenecode,sum(apply) as apply,sum(finish) as finish,sum(success) as success from tmp_innofin.vqq_temp_push_user_direct_uv where dt>='2018-05-01' group by scenecode) b on lower(a.scene_code) = lower(b.scenecode)
	left join (select scenecode,mktype,sum(apply) as apply,sum(finish) as finish,sum(success) as success from tmp_innofin.vqq_temp_push_user_direct_uv where dt>='2018-05-01' group by scenecode,mktype) c on lower(a.scene_code) = lower(b.scenecode) and lower(a.mktype) = lower(c.mktype)
	group by a.scene_name
	) t2 on t1.scene_name = t2.scene_name
;
