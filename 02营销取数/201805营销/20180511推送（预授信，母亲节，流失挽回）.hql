use ods_innofin;
create table if not exists nqh_mkt_message_push_manual(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);
-- 预授信
-- APP_PUSH已发送两周，2周内活跃，但未访问首页
-- Pfig180510c1
-- 5万

insert overwrite table ods_innofin.nqh_mkt_message_push_manual  partition (SceneCode = 'Pfig180510c1',dt = '2018-05-11')
select distinct a.user_id,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from (select user_id,update_time,row_number() over (partition by user_id order by update_time desc) as rn from ods_innofin.busi_notice where notice_type = 9) a
inner join (select distinct uid from dw_mobdb.factmbpageview where d >= date_add(current_date,-14)) b on lower(a.user_id) = lower(b.uid)
left join (select distinct uid from tmp_innofin.apply_process_detail where d >= date_add(current_date,-14) and apply = 1) c on lower(a.user_id) = lower(c.uid)
left join (select distinct uid from ods_innofin.nqh_user_active_date) d on lower(a.user_id) = lower(d.uid)
where a.rn = 1 and to_date(a.update_time) < date_add(current_date,-14) and c.uid is null and d.uid is null
limit 50000;

--- 流失挽回
-- 1. Dfig509a 首页流失 15000
-- 2. Dfig509b 绑卡流失 10000
-- 3. Dfig509c 激活流失 3000

-- 2-lost-step_card	16167
-- 1-lost-step_apply	49594
-- 3-lost-step_active	3350
insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509a',dt = '2018-05-11')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select uid,u_value from tmp_innofin.vqq_cloud_result_his where dt = '2018-05-11' and u_group = '1-lost-step_apply') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt = '2018-05-08') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 15000) a
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509b',dt = '2018-05-11')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result_his where dt = '2018-05-11' and u_group = '2-lost-step_card') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt = '2018-05-08') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 10000)a
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509c',dt = '2018-05-11')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result_his where dt = '2018-05-11' and u_group = '3-lost-step_active') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt = '2018-05-08') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 3000)a
;

--- 母亲节
insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Pfig180509c1',dt = '2018-05-11')
select a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from  (select * from tmp_innofin.vqq_cloud_result_his where u_group = '201805_PUSH_MOTHERS_DAY') a
left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
left join (select uid from ods_innofin.nqh_mkt_message_push_manual where dt >= '2018-05-08') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by rand() limit 100000;

