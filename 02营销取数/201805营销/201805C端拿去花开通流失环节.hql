-- 6)总表
-- use tmp_innofin;
-- create table if not exists vqq_rpt_active_process_detail (
-- 	uid string,
-- 
-- 	enter_ctrip int,
-- 	enter_realname int,
-- 	enter_wallet int,
-- 	enter_more_func int,
-- 	enter_others_channel int,
-- 	
-- 	page_apply	int,	
-- 	page_cardguide	int,	
-- 	page_card_number	int,	
-- 	page_card	int,	
-- 	page_setpass	int,	
-- 	page_active	int,
-- 	page_finish	int,	
-- 	page_success	int,
-- 	
-- 	button_apply int,
-- 	button_contract int,
-- 	button_active int,
-- 
-- 	db_active int,
-- 	db_success int,	
-- 
-- 	log_creditProcess int,
-- 	log_existPwd int,
-- 	log_hasMobile int,
-- 	log_preCreditStatus int,
-- 	log_realName int
-- 	
-- ) partitioned by (
-- 	dt string
-- );
-- 

use tmp_innofin;
create table if not exists vqq_active_process_lost(
	uid string,
	flow string,
	lost_step string
)partitioned by(
	dt string comment '数据日期'
);

insert overwrite table vqq_active_process_lost partition (dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,
	case when log_creditProcess = 0 and log_realName = 1 then 'card'
		when log_creditProcess = 0 and log_realName = 0 then 'noname'
		when log_creditProcess = 1 then 'nocard'
		when log_creditProcess = 2 then 'rapid'
		else 'na' end,
	case when page_apply = 1 and button_apply = 0 then '1-lost-step_apply'
		when page_cardguide = 1 and page_card = 0 then '2-lost-step_card'
		when page_active = 1 and button_active = 0 then '3-lost-step_active'
		else 'others' end 
from vqq_rpt_active_process_detail where (dt >= '${zdt.addDay(-3).format("yyyy-MM-dd")}') and db_active = 0;

-- 推送公有云
use tmp_innofin;
insert into table tmp_innofin.vqq_nqh_beforeafter_uv partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.uid as uid,
	a.dt as active_date,
	0,0,
	lost_step) as bu 
from (select uid,dt,row_numner() over (partition by uid,order by lost_step desc) as rn from vqq_active_process_lost a where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and lost_step <> 'others') a
where rn = 1;

-- 第二天上传公有云

--- 公有云
use tmp_cfbdb;
create table if not exists vqq_cloud_result(
	uid string,
	u_date string,
	u_group string,
	u_value string
)partitioned by (
	dt string
);

insert into table vqq_cloud_result partition (dt = '${zdt.format("yyyy-MM-dd")}')
select a.uid,active_date as u_date,
bu as u_group, b.activate_prob as u_value
from (select * from cfbdb.vqq_nqh_beforeafter_uv where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}') a 
inner join cfbdb.ctrip_credit_user_info_to_credit_4 b on lower(a.uid) = lower(b.user_id);
---------------
-- 取数推送
-- 0508 推送，page_apply 5000，page_active 1000,page_card 4000
-- 排除已开通
use ods_innofin;
create table if not exists nqh_mkt_message_push_manual(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);

alter table ods_innofin.nqh_mkt_message_push_manual drop if exists partition (dt = '2018-05-08'); 

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Pyou180508c1',dt = '2018-05-08')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(
	select a.uid,b.u_value from
	(select distinct uid from tmp_innofin.vqq_active_process_lost where dt = '2018-05-06' and lost_step = 'page_active') a
	inner join (select distinct uid,u_value from tmp_innofin.vqq_cloud_result where u_group = 'vqq_active_process_lost') b on lower(a.uid) = lower(b.uid)
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt = '2018-05-08') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(b.u_value as double) desc limit 1000)
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Pyou180510c1',dt = '2018-05-08')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(
	select distinct a.uid,b.u_value from
	(select * from tmp_innofin. vqq_active_process_lost where dt = '2018-05-06' and lost_step = 'page_card') a
	inner join (select distinct uid,u_value from tmp_innofin.vqq_cloud_result where u_group = 'vqq_active_process_lost') b on lower(a.uid) = lower(b.uid)
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt = '2018-05-08') d on lower(a.uid) = lower(d.uid)
	where c.uid is null and d.uid is null order by cast(b.u_value as double) desc limit 4000
)
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Pyou180509c1',dt = '2018-05-08')
select distinct uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
 from 
(
	select distinct a.uid,b.u_value from
	(select * from tmp_innofin.vqq_active_process_lost where dt = '2018-05-06' and lost_step = 'page_apply') a
	inner join (select distinct uid,u_value from tmp_innofin.vqq_cloud_result where u_group = 'vqq_active_process_lost') b on lower(a.uid) = lower(b.uid)
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt = '2018-05-08') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(b.u_value as double) desc limit 5000
)
;









