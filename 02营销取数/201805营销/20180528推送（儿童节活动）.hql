use ods_innofin;
create table if not exists nqh_mkt_message_push_plan(
	scene_name string,
	mktype string
)partitioned by(
	scene_code string,
	push_date string,
	push_time string
);

use ods_innofin;
create table if not exists nqh_mkt_message_push_data(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
)partitioned by(
	scene_code string,
	push_date string,
	push_time string
);

------------------------
-- 儿童节
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-05-29',push_time='12:00') values('儿童节-门票营销活动','Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-05-29',push_time='15:00') values('儿童节-门票营销活动','Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-05-29',push_time='17:00') values('儿童节-门票营销活动','Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-05-30',push_time='12:00') values('儿童节-门票营销活动','Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-05-30',push_time='15:00') values('儿童节-门票营销活动','Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-05-30',push_time='17:00') values('儿童节-门票营销活动','Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-05-31',push_time='12:00') values('儿童节-门票营销活动','Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-05-31',push_time='15:00') values('儿童节-门票营销活动','Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-05-31',push_time='17:00') values('儿童节-门票营销活动','Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-06-01',push_time='12:00') values('儿童节-门票营销活动','Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-06-01',push_time='15:00') values('儿童节-门票营销活动','Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-06-01',push_time='17:00') values('儿童节-门票营销活动','Dfig525c');

insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525a',push_date = '2018-06-02',push_time='12:00') values('儿童节-门票营销活动','Dfig525a');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525b',push_date = '2018-06-02',push_time='15:00') values('儿童节-门票营销活动','Dfig525b');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig525c',push_date = '2018-06-02',push_time='17:00') values('儿童节-门票营销活动','Dfig525c');
--- 

use tmp_innofin;
create table if not exists vqq_event_61(
	uid string,
	u_group int
);
insert overwrite table vqq_event_61
select distinct a.uid, floor(rand()*15+1)
from 
( select uid from tmp_dw_temp.tmp_xwt7130_marketing_target2
	union all 
	select user_id as uid from tmp_dw_temp.profile_have_child_count where pkg_avg_childcount>0 or child_count>0
) a
left join (select distinct user_id as uid from ods_innofin.busi_notice where notice_type = 9 and notice_status = 1) c on lower(a.uid) = lower(c.uid)
left join (select distinct uid from ods_innofin.nqh_user_active_date) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
;
------------------
-- Dfig525a
-- Dfig525b
-- Dfig525c
-- 5/29--6/2
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525a',push_date = '2018-05-29',push_time = '12:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 1;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525b',push_date = '2018-05-29',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 2;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525c',push_date = '2018-05-29',push_time = '17:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 3;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525a',push_date = '2018-05-30',push_time = '12:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 4;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525b',push_date = '2018-05-30',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 5;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525c',push_date = '2018-05-30',push_time = '17:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 6;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525a',push_date = '2018-05-31',push_time = '12:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 7;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525b',push_date = '2018-05-31',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 8;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525c',push_date = '2018-05-31',push_time = '17:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 9;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525a',push_date = '2018-06-01',push_time = '12:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 10;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525b',push_date = '2018-06-01',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 11;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525c',push_date = '2018-06-01',push_time = '17:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 12;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525a',push_date = '2018-06-02',push_time = '12:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 13;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525b',push_date = '2018-06-02',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 14;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig525c',push_date = '2018-06-02',push_time = '17:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_61 a where u_group = 15;

-- pushing
use tmp_innofin;
create table if not exists nqh_mkt_message_push_pushing(
	scenecode string,
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string,
 	
	push_date string,
	push_time string
);

------------- do the push
use ods_innofin;
insert overwrite table tmp_innofin.nqh_mkt_message_push_pushing
select a.scene_code,a.uid,a.did,a.mobilephone,a.email,a.extinfo,a.createdt,a.push_date,a.push_time from ods_innofin.nqh_mkt_message_push_data a
	inner join nqh_mkt_message_push_test b on a.scene_code = b.scene_code
	left join nqh_mkt_message_push_result c on a.scene_code = c.scene_code and a.push_date = c.push_date and a.push_time = c.push_time
	where concat(a.push_date,' ',a.push_time) <= '${zdt.format("yyyy-MM-dd HH:mm")}' and c.actual_push_time is null;
-- push by shell
-- after push
insert into ods_innofin.nqh_mkt_message_push_result
select distinct a.scenecode,a.push_date,a.push_time,from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') from tmp_innofin.nqh_mkt_message_push_pushing a;
TRUNCATE TABLE tmp_innofin.nqh_mkt_message_push_pushing;
------------- 
use ods_innofin;
create table if not exists nqh_mkt_message_push_test(
	test_ok_time string
)partitioned by(
	scene_code string
);

insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Dfig525a') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Dfig525b') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Dfig525c') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');


use ods_innofin;
create table if not exists nqh_mkt_message_push_result(
	scene_code string,
	push_date string,
	push_time string,
	actual_push_time string
);




