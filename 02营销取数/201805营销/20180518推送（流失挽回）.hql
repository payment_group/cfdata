use ods_innofin;
create table if not exists nqh_mkt_message_push_manual(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);

--- ��ʧ���
-- scenecode	count(1)
-- Dfig509a	17411
-- Dfig509b	9552
-- Dfig509c	2370

-- u_group	count(1)
-- 1-lost-step_apply	54675
-- 2-lost-step_card	16884
-- 3-lost-step_active	2960
-- 
insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509a',dt = '2018-05-18')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select uid,u_value from tmp_innofin.vqq_cloud_result where u_group = '1-lost-step_apply') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt in ('2018-05-08','2018-05-11','2018-05-16')) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 30000) a
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509b',dt = '2018-05-18')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result where u_group = '2-lost-step_card') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt in ('2018-05-08','2018-05-11','2018-05-16')) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 10000)a
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509c',dt = '2018-05-18')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result where u_group = '3-lost-step_active') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt in ('2018-05-08','2018-05-11','2018-05-16')) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 3000)a
;

