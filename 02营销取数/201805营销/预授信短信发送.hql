use tmp_innofin;
create table vqq_tmp_sms_001 as 
select b.user_id,a.mobile
 from 
(select user_id,mobile from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap
where org_channel = 'CTRIP' and product_no = 'IOUS'
and req_status = 1 and activate_status = 0 
and mobile is not null and trim(mobile) <> '' and end_eff_time > current_date
and  datediff(current_date,finish_time) <= 7) a 
inner join ods_innofin.user_contract b on a.user_id = b.open_id


use ods_innofin;
create table if not exists nqh_mkt_message_push(
	SceneCode string,
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	dt string comment 'push date'
);

insert overwrite table ods_innofin.nqh_mkt_message_push partition (dt = '2018-04-25')
select 'Dfig424a' as scenecode,
  b.user_id as uid,
  '' as did,
  a.mobile as mobilephone,
  '' as email,
  '{}' as extinfo,
  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from 
(select user_id,mobile from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap
	where org_channel = 'CTRIP' and product_no = 'IOUS'
	and req_status = 1 and activate_status = 0 
	and mobile is not null and trim(mobile) <> '' and end_eff_time > current_date
	and  datediff(current_date,finish_time) <= 7) a 
	inner join ods_innofin.user_contract b on a.user_id = b.open_id
order by user_id limit 100000
;
