use ods_innofin;
create table if not exists nqh_mkt_message_push_manual(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);

insert overwrite table ods_innofin.nqh_mkt_message_push_manual  partition (SceneCode = 'Dfig509d',dt = '2018-05-14')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from (select distinct uid from tmp_dw_temp.tmp_xwt7130_part_zy_to_flt where dt between '2018-05-02' and '2018-05-14') a
left join (select distinct user_id as uid from ods_innofin.busi_notice where notice_type = 9 and notice_status = 1) c on lower(a.uid) = lower(c.uid)
left join (select distinct uid from ods_innofin.nqh_user_active_date) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
limit 500000;

