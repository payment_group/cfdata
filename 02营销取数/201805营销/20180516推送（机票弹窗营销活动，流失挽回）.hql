use ods_innofin;
create table if not exists nqh_mkt_message_push_manual(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);

-- 机票弹窗营销活动
-- 5/14-5/20本周推送数据（吴双5.2-5.14推给机票的UID去掉已激活用户，去掉史柯APP Push已发送成功的用户，剩余的本周内分2天推送出去）
-- Dfig509d
-- 50万

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509d',dt = '2018-05-16')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from (select distinct uid from tmp_dw_temp.tmp_xwt7130_part_zy_to_flt where dt between '2018-05-02' and '2018-05-14') a
left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where SceneCode = 'Dfig509d' and dt = '2018-05-14') b on lower(a.uid) = lower(b.uid)
left join (select distinct user_id as uid from ods_innofin.busi_notice where notice_type = 9 and notice_status = 1) c on lower(a.uid) = lower(c.uid)
left join (select distinct uid from ods_innofin.nqh_user_active_date) d on lower(a.uid) = lower(d.uid)
where b.uid is null and c.uid is null and d.uid is null
limit 500000;

--- 流失挽回
-- 1. Dfig509a 首页流失 30000
-- 2. Dfig509b 绑卡流失 10000
-- 3. Dfig509c 激活流失 5000

-- 2-lost-step_card	106434
-- 1-lost-step_apply	34734
-- 3-lost-step_active	7178
insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509a',dt = '2018-05-16')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select uid,u_value from tmp_innofin.vqq_cloud_result_his where dt = '2018-05-11' and u_group = '1-lost-step_apply') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt in ('2018-05-08','2018-05-11')) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 30000) a
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509b',dt = '2018-05-16')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result_his where dt = '2018-05-11' and u_group = '2-lost-step_card') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt in ('2018-05-08','2018-05-11')) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 10000)a
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509c',dt = '2018-05-16')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select a.uid,a.u_value from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result_his where dt = '2018-05-11' and u_group = '3-lost-step_active') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt in ('2018-05-08','2018-05-11')) d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
order by cast(a.u_value as double) desc limit 5000)a
;

