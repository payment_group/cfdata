
use ods_innofin;
create table if not exists nqh_mkt_message_push(
	SceneCode string,
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	dt string comment 'push date'
);

insert overwrite table ods_innofin.nqh_mkt_message_push partition (dt = '2018-05-07')
select 'Mfif180507f1' as scenecode,
  a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from tmp_dw_temp.tmp_xwt7130_marketing_target a where realname = 1 and bindcard = 1
order by rand() limit 1000000
;

insert overwrite table ods_innofin.nqh_mkt_message_push partition (dt = '2018-05-18')
select 'Mfif180518f1' as scenecode,
  a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from (select * from tmp_dw_temp.tmp_xwt7130_marketing_target a where realname = 1 and bindcard = 1) a 
left join (select uid from ods_innofin.nqh_mkt_message_push where dt in ('2018-05-07')) b on a.uid = b.uid 
where b.uid is null 
order by rand() limit 1000000
;

insert overwrite table ods_innofin.nqh_mkt_message_push partition (dt = '2018-05-25')
select 'Mfif180525f1' as scenecode,
  a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from (select * from tmp_dw_temp.tmp_xwt7130_marketing_target a where realname = 1 and bindcard = 1) a 
left join (select uid from ods_innofin.nqh_mkt_message_push where dt in ('2018-05-07', '2018-05-18')) b on a.uid = b.uid 
where b.uid is null 
order by rand() limit 1000000
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (dt = '2018-05-31')
select 'Mfif180531f1' as scenecode,
  a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from (select * from tmp_dw_temp.tmp_xwt7130_marketing_target a where realname = 1 and bindcard = 1) a 
left join (select distinct uid from dw_pubsharedb.factmktmembersenddetail_fin where d>= '2018-05-15' and ctistatus = 'T') b on a.uid = b.uid 
where b.uid is null 
order by rand() limit 1000000
;

select dt,SceneCode,count(1) from ods_innofin.nqh_mkt_message_push where dt > '' group by dt,SceneCode;
select uid,count(distinct scenecode) from  ods_innofin.nqh_mkt_message_push where dt >= '2018-05-07' group by uid having count(distinct scenecode)>1;
