use ods_innofin;
create table if not exists nqh_mkt_message_push_manual(
	uid string,
	Did string,
 	MobilePhone string,
 	Email string,
 	ExtInfo string,
 	CreateDT string
) partitioned by (
	SceneCode string,
	dt string comment 'push date'
);

--- 流失挽回
--- 全量发
-- scenecode	count(1)
-- Dfig509a	17411
-- Dfig509b	9552
-- Dfig509c	2370

-- u_group	count(1)
-- 1-lost-step_apply	44667
-- 2-lost-step_card	11415
-- 3-lost-step_active	1995
-- 
insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509a',dt = '2018-05-22')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from 
	(select uid,u_value from tmp_innofin.vqq_cloud_result where u_group = '1-lost-step_apply') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt >= '2018-05-07') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509b',dt = '2018-05-22')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result where u_group = '2-lost-step_card') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt >= '2018-05-07') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
;

insert overwrite table ods_innofin.nqh_mkt_message_push_manual partition (SceneCode = 'Dfig509c',dt = '2018-05-22')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd hh:mm:ss') as CreateDT
from
(select distinct uid,u_value from tmp_innofin.vqq_cloud_result where u_group = '3-lost-step_active') a
	left join (select uid from ods_innofin.nqh_user_active_date) c on lower(a.uid) = lower(c.uid)
	left join (select distinct uid from ods_innofin.nqh_mkt_message_push_manual where dt >= '2018-05-07') d on lower(a.uid) = lower(d.uid)
where c.uid is null and d.uid is null
;

