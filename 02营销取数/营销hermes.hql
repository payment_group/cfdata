use tmp_innofin;
create table if not exists vqq_mkt_hermes(
	SceneCode string comment '场景号',
	Uid string comment '用户Id 至少选填一个',
	Did string comment 'App设备号',
	MobilePhone string  comment '手机号',
	Email string comment '邮箱',
	ExtInfo string  comment '扩展信息, 用于存在场景配置时所使用的标签信息({"[自定义标签]": "value"}格式) 否',
	RecommendInfo string  comment '推荐信息, 用于定制化时使用 否',
	CreateDT string comment '创建时间'
) partitioned by (
	dt string
);

-- test
use tmp_innofin;
insert overwrite table vqq_mkt_hermes partition (dt='${zdt.format("yyyy-MM-dd")}')
values
('Mfif180116test','_WeChat546894665','','','','','','2018-01-16 14:25:00'),
('Mfif180116test','_WeChat353954292','','','','','','2018-01-16 14:25:00')
;

--topic mkt.messagepush.manualbatchmsg