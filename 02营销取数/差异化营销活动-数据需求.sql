-- 拟针对不同的用户群做差异化营销政策，需要数据支持：
use tmp_innofin;
create table if not exists vqq_precise_marketing_user(
	string uid
)partitioned by (
	user_group string
);


-- 辅助表
-- 近3个月内有携程APP打开记录的用户
use tmp_innofin;
create table vqq_tmp_ctrip_active_3m as 
select distinct uid from dw_mobdb.factmbpageview where d >= add_months(current_date,-3); 
-- ①进入开通首页但未走完开通流程的用户
use tmp_innofin;
create table vqq_tmp_not_finish as 
select distinct uid from tmp_innofin.apply_process_detail a
left join (select distinct user_id from ods_innofin.user_activate_req) b on lower(a.uid) = lower(b.user_id)
where a.d < current_date and b.user_id is null; 
-- 近1个月内有查询机票/酒店/火车票但未预定的用户 
-- 有预定用户从公有云导出 -- 
use cfbdb;
create table vqq_tmp_with_order_1m (
	uid string
) partitioned by (
	bu string
);
insert overwrite table vqq_tmp_with_order_1m partition (bu='train')
select distinct uid from ctrip_cloud_uid_trnorder_detail where dt >= add_months(current_date,-1) and orderstatus = 10;
insert overwrite table vqq_tmp_with_order_1m partition (bu='flight')
select distinct uid from ctrip_cloud_uid_fltorder_detail where dt >= add_months(current_date,-1) and orderstatus = 10;
insert overwrite table vqq_tmp_with_order_1m partition (bu='hotel')
select distinct uid from ctrip_cloud_uid_htlorder_detail where dt >= add_months(current_date,-1) and orderstatus = 10;

use tmp_innofin;
create table vqq_user_with_order_1m(uid string
) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "'",
   "escapeChar"    = "\\"
)  
STORED AS TEXTFILE;
-- 第一类：
-- ①未到访过拿去花开通首页的用户
-- ②具有拿去花开通资质
-- ③近3个月内有携程APP打开记录的用户
use tmp_innofin;
insert overwrite table vqq_precise_marketing_user partition(user_group = 'group_1')
select distinct a.user_id 
from ods_innofin.tbl_campaign_audience a 
inner join vqq_tmp_ctrip_active_3m b on lower(a.user_id) = lower(b.uid)
left join (select distinct uid from tmp_innofin.apply_process_detail where d < current_date ) c on lower(a.user_id) = lower(c.uid)
where c.uid is null;

-- 第二类：
-- ①进入开通首页但未走完开通流程的用户
-- ②具有拿去花开通资质
-- ③近3个月内有携程APP打开记录的用户
-- ④剔除下面第三类用户
use tmp_innofin;
insert overwrite table vqq_precise_marketing_user partition(user_group = 'group_2')
select distinct a.user_id 
from ods_innofin.tbl_campaign_audience a 
inner join vqq_tmp_ctrip_active_3m b on lower(a.user_id) = lower(b.uid)
left join (select uid from vqq_precise_marketing_user where user_group = 'group_3') c on lower(a.user_id) = lower(c.uid)
where c.uid is null;


-- 第三类：
-- ①进入开通首页但未走完开通流程的用户
-- ②具有拿去花开通资质
-- ③近1个月内有查询机票/酒店/火车票但未预定的用户

use tmp_innofin;
insert overwrite table vqq_precise_marketing_user partition(user_group = 'group_3') 
select distinct a.user_id 
from ods_innofin.tbl_campaign_audience a 
inner join vqq_tmp_not_finish b on lower(a.user_id) = lower(b.uid)
inner join (select distinct uid from dw_mobdb.factmbpageview where d >= add_months(current_date,-1) and pagecode in (
'train_inquire',
'hotel_inland_inquire','hotel_inland_list','hotel_inland_detail','hotel_inland_order',
'flight_inland_inquire','flight_int_inquire','flight_int_inquire2','flight_inland_inquire2')) c on lower(a.user_id) = lower(c.uid)
left join vqq_user_with_order_1m d on lower(a.user_id) = lower(d.uid)
where d.uid is null;


-- 第四类：
-- ①三个月内打开过站内信
-- ②未走完开通流程的用户
-- ③具有拿去花开通资质
use tmp_innofin;
insert overwrite table vqq_precise_marketing_user partition(user_group = 'group_4')
select distinct a.user_id 
from ods_innofin.tbl_campaign_audience a 
inner join vqq_tmp_not_finish b on lower(a.user_id) = lower(b.uid)
inner join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where d >= add_months(current_date,-3) and sendchannel = 'MIM' and ctisendstatus = 'T' ) c on lower(a.user_id) = lower(c.uid);

-- 第五类：
-- ①历史营销短信到达用户
-- ②未走完开通流程的用户
-- ③具有拿去花开通资质
use tmp_innofin;
insert overwrite table vqq_precise_marketing_user partition(user_group = 'group_5')
select distinct a.user_id 
from ods_innofin.tbl_campaign_audience a 
inner join vqq_tmp_not_finish b on lower(a.user_id) = lower(b.uid)
inner join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'SMS' and ctisendstatus = 'T' ) c on lower(a.user_id) = lower(c.uid);

-- PS：具有拿去花开通资质的用户均用4000W用户来套；

