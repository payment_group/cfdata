use ods_innofin;
insert into table nqh_mkt_message_push_event values
('预授信Push过但未激活的用户','Sfif180618e1','Sfif180618e1','2018-06-18','2018-06-18'),
('预授信Push过但未激活的用户','Sfif180627e1','Sfif180627e1','2018-06-27','2018-06-27');

-- 1）预授信Push过但未激活的用户

use tmp_innofin;
create table if not exists vqq_event_0615_precredit(
	uid string,
	u_group int
);
insert overwrite table vqq_event_0615_precredit
select distinct a.user_id, floor(rand()*2+1)
from (select user_id,update_time,row_number() over (partition by user_id order by update_time desc) as rn 
	from ods_innofin.busi_notice where notice_type = 9) a
left join (select distinct user_id from ods_innofin.user_contract where contract_status = 1) d on lower(a.user_id) = lower(d.user_id)
where a.rn = 1 and d.user_id is null
;

------------------------
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Sfif180618e1',push_date = '2018-06-18',push_time='12:00') values('预授信Push过但未激活的用户','Sfif180618e1');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Sfif180627e1',push_date = '2018-06-27',push_time='12:00') values('预授信Push过但未激活的用户','Sfif180627e1');
--- 


insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Sfif180618e1',push_date = '2018-06-18',push_time = '12:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_precredit a where u_group = 1 limit 100000;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Sfif180627e1',push_date = '2018-06-27',push_time = '12:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_precredit a where u_group = 2 limit 100000;


insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Sfif180618e1') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Sfif180627e1') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
