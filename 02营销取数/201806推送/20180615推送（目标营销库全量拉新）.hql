use ods_innofin;
insert into table nqh_mkt_message_push_event values
('目标营销库全量拉新','Mfif180616f1','Mfif180616f1','2018-06-16','2018-06-30'),
('目标营销库全量拉新','Mfif180622f1','Mfif180622f1','2018-06-16','2018-06-30'),
('目标营销库全量拉新','Mfif180629f1','Mfif180629f1','2018-06-16','2018-06-30');

-- 1）预授信Push过但未激活的用户

use tmp_innofin;
create table if not exists vqq_event_0615_mkt_all(
	uid string,
	u_group int
);
insert overwrite table vqq_event_0615_mkt_all
select distinct a.user_id, floor(rand()*3+1)
from (select uid as user_id from tmp_dw_temp.tmp_xwt7130_marketing_target a where realname = 1 and bindcard = 1) a
left join (select distinct user_id from ods_innofin.user_contract where contract_status = 1) d on lower(a.user_id) = lower(d.user_id)
where d.user_id is null
;

------------------------
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Mfif180616f1',push_date = '2018-06-16',push_time='10:00') values('目标营销库全量拉新','Mfif180616f1');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Mfif180622f1',push_date = '2018-06-22',push_time='10:00') values('目标营销库全量拉新','Mfif180622f1');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Mfif180629f1',push_date = '2018-06-29',push_time='10:00') values('目标营销库全量拉新','Mfif180629f1');
--- 


insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Mfif180616f1',push_date = '2018-06-16',push_time = '10:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_mkt_all a where u_group = 1 limit 1000000;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Mfif180622f1',push_date = '2018-06-22',push_time = '10:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_mkt_all a where u_group = 2 limit 1000000;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Mfif180629f1',push_date = '2018-06-29',push_time = '10:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_mkt_all a where u_group = 3 limit 1000000;


insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Mfif180616f1') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Mfif180622f1') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Mfif180629f1') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
