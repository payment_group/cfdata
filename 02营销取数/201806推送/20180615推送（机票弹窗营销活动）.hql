-- 5月弹窗推送后所有未激活的用户滚动推送
-- 把总量池子洗出来，发送15天

use tmp_innofin;
create table if not exists vqq_event_0615_flight(
	uid string,
	u_group int
);
insert overwrite table vqq_event_0615_flight
select distinct a.uid, floor(rand()*15+1)
from 
(select distinct uid from tmp_dw_temp.tmp_xwt7130_part_zy_to_flt where dt between '2018-05-01' and '2018-05-31') a
left join (select distinct uid from ods_innofin.nqh_user_active_date) d on lower(a.uid) = lower(d.uid)
where d.uid is null
;

------------------------
-- 机票弹窗5月，未开通
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-16',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-17',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-18',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-19',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-20',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-21',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-22',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-23',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-24',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-25',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-26',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-27',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-28',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-29',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
insert overwrite table ods_innofin.nqh_mkt_message_push_plan partition(scene_code = 'Dfig509d',push_date = '2018-06-30',push_time='15:00') values('机票弹窗营销活动','AppPush_FlightPop');
--- 


insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-16',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 1;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-17',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 2;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-18',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 3;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-19',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 4;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-20',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 5;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-21',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 6;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-22',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 7;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-23',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 8;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-24',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 9;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-25',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 10;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-26',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 11;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-27',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 12;

insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-28',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 13;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-29',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 14;
insert overwrite table ods_innofin.nqh_mkt_message_push_data partition (scene_code = 'Dfig509d',push_date = '2018-06-30',push_time = '15:00')
select distinct a.uid,
  '' as did, '' as mobilephone,  '' as email,  '{}' as extinfo,  from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as CreateDT
from tmp_innofin.vqq_event_0615_flight a where u_group = 15;

------------- 
use ods_innofin;
create table if not exists nqh_mkt_message_push_test(
	test_ok_time string
)partitioned by(
	scene_code string
);

insert overwrite table ods_innofin.nqh_mkt_message_push_test partition(scene_code = 'Dfig509d') select from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss');
