-- 速度较慢，做成job
use dw_innofin;
create table markting_batch_result_month(
	month string comment '月',
	channel string comment '通道',
	sentall int comment '发送总条目',

	sent1 int comment '营销到达',
	sent1_apply int comment '申请数',
	sent1_finish int comment '完成流程数',
	sent1_success int comment '申请成功数',

	sent0 int comment '营销未到达',
	sent0_apply int comment '申请数',
	sent0_finish int comment '完成流程数',
	sent0_success int comment'申请成功数'
) comment '营销批次统计结果'
partitioned by (dt string comment '数据日期=T-1'); 
--
select 
	concat(substring(`月`,1,2),'年',substring(`月`,3,2),'月') as `月`,
	case when `通道` = 'E' then '邮件' when `通道` = 'S' then '短信' else '站内信' end as `通道`,
	`发送总条目`,
	`营销到达`,
	`进入申请页`,
	`完成流程`,
	`激活成功`,
	concat(format_number(100*`进入申请页`/`营销到达`,2),'%') as `申请数/营销到达%`,
	concat(format_number(100*`完成流程`/`营销到达`,2),'%') as `完成流程/营销到达%`,
	concat(format_number(100*`激活成功`/`营销到达`,2),'%') as `激活成功/营销到达%`,
	`营销未到达`,
	`申请数`,
	`完成流程数`,
	`申请成功数`,
	concat(format_number(100*`申请数`/`营销未到达`,2),'%') as `申请数/营销未到达%`,
	concat(format_number(100*`完成流程数`/`营销未到达`,2),'%') as `完成流程/营销未到达%`,
	concat(format_number(100*`申请成功数`/`营销未到达`,2),'%') as `激活成功/营销未到达%`
from (
select 
	substring(plancode,5,4) as `月`,
	upper(substring(plancode,1,1)) as `通道`,
	sum(sentall) as `发送总条目`,
	sum(sent1) as `营销到达`,
	sum(sent1_apply)  as `进入申请页`,
	sum(sent1_finish)  as `完成流程`,
	sum(sent1_success)  as `激活成功`,
	sum(sent0)  as `营销未到达`,
	sum(sent0_apply)  as `申请数`,
	sum(sent0_finish)  as `完成流程数`,
	sum(sent0_success)  as `申请成功数`
from dw_innofin.markting_batch_result where (dt=#YESTERDAY#) and sent1_finish < sent1_apply
group by substring(plancode,5,4), upper(substring(plancode,1,1))
) a order by `月`,`通道` limit 1000
            