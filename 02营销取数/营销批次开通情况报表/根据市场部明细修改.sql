-- 营销明细
use tmp_innofin;
create table if not exists vqq_markting_batch_detail(
 	plancode string comment '批次',
 	uid string comment 'uid',
 	sent int comment '是否成功flag',
 	d string comment '发送日期'
 ) comment '营销发送批次结果明细';
insert overwrite table tmp_innofin.vqq_markting_batch_detail
 select scenecode, uid, if(ctisendstatus = 'F',0,1), sendchannel,d from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel in ('EDM','MIM','SMS') and 
 lower(a.scenecode) in (select lower(plancode) from tmp_innofin.tmp_gpush)  and uid is not null and trim(uid)<>'';
 
-- 去重，批次内有重复uid的，取最后一条 
use tmp_innofin;
create table if not exists vqq_markting_batch_detail_last(
 	plancode string comment '批次',
 	uid string comment 'uid',
 	sent int comment '是否成功flag',
 	sent_date string comment '发送日期',
 	status_date string comment '开通状态日期'
 ) comment '营销发送批次结果明细';
insert overwrite table tmp_innofin.vqq_markting_batch_detail_last
select a.plancode, a.uid, a.sent, a.d, date_add(a.d,15) from tmp_innofin.vqq_markting_batch_detail a 
inner join (select plancode,uid,max(d) as d from tmp_innofin.vqq_markting_batch_detail group by plancode,uid) b on a.plancode = b.plancode and a.uid = b.uid and a.d = b.d;

-- 营销标记：批次间有重复的
use tmp_innofin;
create table if not exists vqq_markting_batch_detail_cross(
 	plancode string comment '批次',
 	uid string comment 'uid',
 	markting_flag int comment '营销标记，以发送日期为准（此日期前没有收到过更早的营销消息）'
 ) ;
insert overwrite table tmp_innofin.vqq_markting_batch_detail_cross
select distinct a.plancode,a.uid, 1 as markting_flag
from tmp_innofin.vqq_markting_batch_detail_last a 
inner join tmp_innofin.vqq_markting_batch_detail_last b on lower(a.uid) = lower(b.uid) 
where b.sent_date < a.sent_date;
-- 7730183

-- 申请标记
use tmp_innofin;
drop table if exists vqq_markting_user_apply_flag;
create table vqq_markting_user_apply_flag(
	plancode string comment 'plancode',
	uid string comment 'uid',
	apply_flag int comment '申请状态，以状态日为准'
);

insert overwrite table tmp_innofin.vqq_markting_user_apply_flag
select distinct a.plancode,a.uid, 1 as apply_flag
from tmp_innofin.vqq_markting_batch_detail_last a 
inner join (select * from tmp_innofin.wqm_process_basic01 where d<'${zdt.format("yyyy-MM-dd")}') b on lower(a.uid) = lower(b.uid)
where b.d between a.sent_date and a.status_date;

-- 开通标记
use tmp_innofin;
drop table if exists vqq_markting_user_active_flag;
create table vqq_markting_user_active_flag(
	plancode string comment 'plancode',
	uid string comment 'uid',
	active_flag int comment '开通状态，以状态日为准'
) ;
insert overwrite table tmp_innofin.vqq_markting_user_active_flag
select distinct a.plancode,a.uid,c.contract_status as active_flag 
from tmp_innofin.vqq_markting_batch_detail_last a 
inner join (select uid,contract_status,apply_active_date from dw_innofin.nqh_user_apply_active_date where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}') c on lower(a.uid) = lower(c.uid) 
where c.apply_active_date between a.sent_date and a.status_date; 

-- 所有标记
use tmp_innofin;
create table if not exists vqq_markting_user_flags(
 	plancode string comment '批次',
 	uid string comment 'uid',
 	sent int comment '是否成功flag',
 	sent_date string comment '发送日期',
 	status_date string comment '开通状态日期',

 	markting_flag int comment '营销标记，以发送日期为准（此日期前没有收到过更早的营销消息）',
	apply_flag int comment '申请状态，以状态日为准',
	active_flag int comment '开通状态，以状态日为准'
) comment '营销用户的开通，营销标记';
insert overwrite table tmp_innofin.vqq_markting_user_flags
select a.plancode,a.uid,a.sent,a.sent_date,a.status_date,b.markting_flag,c.apply_flag,d.active_flag
from tmp_innofin.vqq_markting_batch_detail_last a
left join tmp_innofin.vqq_markting_batch_detail_cross b on lower(a.plancode) = lower(b.plancode) and lower(a.uid) = lower(b.uid)
left join tmp_innofin.vqq_markting_user_apply_flag c on lower(a.plancode) = lower(c.plancode) and lower(a.uid) = lower(c.uid)
left join tmp_innofin.vqq_markting_user_active_flag d on lower(a.plancode) = lower(d.plancode) and lower(a.uid) = lower(d.uid);
-- 统计数据
use dw_innofin;
create table markting_batch_result(
	plancode string comment '批次号',
	sentall int comment '发送总条目',

	sent1 int comment '营销到达',
	sent1_apply int comment '申请数',
	sent1_finish int comment '完成流程数',
	sent1_success int comment '申请成功数',

	sent0 int comment '营销未到达',
	sent0_apply int comment '申请数',
	sent0_finish int comment '完成流程数',
	sent0_success int comment'申请成功数'
) comment '营销批次统计结果'
partitioned by (dt string comment '数据日期=T-1'); 

insert overwrite table dw_innofin.markting_batch_result partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select plancode,count(1) as total,
sum(case when sent = 1 then 1 else 0 end) as sent1,
sum(case when sent = 1 and apply_flag = 1 then 1 else 0 end) as sent1_apply,
sum(case when sent = 1 and apply_flag = 1 and active_flag is not null then 1 else 0 end) as sent1_finish,
sum(case when sent = 1 and apply_flag = 1 and active_flag = 1 then 1 else 0 end) as sent1_success,
sum(case when sent = 0 and markting_flag is null then 1 else 0 end) as sent0,
sum(case when sent = 0 and markting_flag is null and apply_flag = 1 then 1 else 0 end) as sent0_apply,
sum(case when sent = 0 and markting_flag is null and apply_flag = 1 and active_flag is not null then 1 else 0 end) as sent0_finish,
sum(case when sent = 0 and markting_flag is null and apply_flag = 1 and active_flag = 1  then 1 else 0 end) as sent0_success
from tmp_innofin.vqq_markting_user_flags a
group by plancode;
--ART
select 
	plancode as `批次号`,
	sentall as `发送总条目`,
	sent1 as `营销到达`,
	sent1_apply  as `进入申请页`,
	sent1_finish  as `完成流程`,
	sent1_success  as `激活成功`,
	if(sent1=0,'-',concat(format_number(100*sent1_apply/sent1,2),'%')) as `申请数/营销到达%`,
	if(sent1=0,'-',concat(format_number(100*sent1_finish/sent1,2),'%')) as `完成流程/营销到达%`,
	if(sent1=0,'-',concat(format_number(100*sent1_success/sent1,2),'%')) as `激活成功/营销到达%`,
	sent0  as `营销未到达`,
	sent0_apply  as `申请数`,
	sent0_finish  as `完成流程数`,
	sent0_success  as `申请成功数`,
	if(sent0=0,'-',concat(format_number(100*sent0_apply/sent0,2),'%')) as `申请数/营销未到达%`,
	if(sent0=0,'-',concat(format_number(100*sent0_finish/sent0,2),'%')) as `完成流程/营销未到达%`,
	if(sent0=0,'-',concat(format_number(100*sent0_success/sent0,2),'%')) as `激活成功/营销未到达%`
from dw_innofin.markting_batch_result where (dt=#YESTERDAY#)
and concat('2017-',substring(plancode,7,2),'-',substring(plancode,9,2)) < #20DAYS#