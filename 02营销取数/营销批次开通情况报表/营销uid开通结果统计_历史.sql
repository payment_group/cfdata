use tmp_innofin;
create table if not exists marketing_user_status(
  uid string,
  plan_code string,
  channel_type string,
  contract_status int,
  result_date string
)
PARTITIONED BY(d STRING COMMENT 'calculate date');

alter table tmp_innofin.marketing_user_status drop partition(d='2017-07-19');

insert overwrite table tmp_innofin.marketing_user_status partition (d='2017-07-01')
select a.uid,a.plancode,a.channeltype,b.contract_status,'${zdt.addDay(-1).format("yyyy-MM-dd")}' 
  from tmp_innofin.factmarketingsenddetail a
  left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id)
  where a.d <'${zdt.format("yyyy-MM-dd")}' and (plancode like '%fif1705%' or plancode like '%fif1706%');
-------------- 
use tmp_innofin;
create table if not exists marketing_user_count(
  plan_code string,
  channel_type string,
  total_count int,
  apply_count int,
  success_count int,
  result_date string
)
PARTITIONED BY(d STRING COMMENT 'calculate date');
alter table tmp_innofin.marketing_user_count drop partition(d='2017-07-19');
insert overwrite table tmp_innofin.marketing_user_count partition (d='2017-07-01')
select a.plan_code,a.channel_type,
	count(1),
	sum(if(contract_status is not null,1,0)),
	sum(if(contract_status = 1,1,0)),
	'${zdt.addDay(-1).format("yyyy-MM-dd")}' 
  from tmp_innofin.marketing_user_status a 
  where a.d='${zdt.format("yyyy-MM-dd")}' group by plan_code,channel_type;
