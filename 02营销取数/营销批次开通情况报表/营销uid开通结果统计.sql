use tmp_innofin;
create table if not exists marketing_user_status(
  uid string,
  plan_code string,
  channel_type string,
  contract_status int,
  result_date string
)
PARTITIONED BY(d STRING COMMENT 'calculate date');

insert overwrite table tmp_innofin.marketing_user_status partition (d='${zdt.format("yyyy-MM-dd")}')
select a.uid,a.plancode,a.channeltype,b.contract_status,'${zdt.addDay(-1).format("yyyy-MM-dd")}' 
  from tmp_innofin.factmarketingsenddetail a
  left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id)
  where a.d <'${zdt.format("yyyy-MM-dd")}' and (substring(plancode,10,6) ='${zdt.addDay(-31).format("yyMMdd")}');
-------------- 
use tmp_innofin;
drop table if exists marketing_user_count;
create table if not exists  marketing_user_count(
  plan_code string,
  channel_type string,
  total_count int,
  apply_count int,
  success_count int,
  result_date string
)
PARTITIONED BY(d STRING COMMENT 'calculate date');

insert overwrite table tmp_innofin.marketing_user_count partition (d='${zdt.format("yyyy-MM-dd")}')
select a.plan_code,a.channel_type,
	count(1),
	sum(if(contract_status is not null,1,0)),
	sum(if(contract_status = 1,1,0)),
	'${zdt.addDay(-1).format("yyyy-MM-dd")}' 
  from tmp_innofin.marketing_user_status a 
  where a.d='${zdt.format("yyyy-MM-dd")}' group by plan_code,channel_type;

-------------- ������
use tmp_innofin;
create table if not exists no_markting_users(
 uid string
) partitioned by (plancode string);
use tmp_innofin;
create table if not exists  no_marketing_user_count(
  total_count int,
  apply_count int,
  success_count int
)
PARTITIONED BY(plancode STRING);

--  0615
insert overwrite table tmp_innofin.no_markting_users partition(plancode='Sfif170615a1')
select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname_card a
left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d<'${zdt.format("yyyy-MM-dd")}') b on a.uid = b.uid
left join (select user_id from ods_innofin.user_contract_daily where d = '2017-06-15') c on lower(a.uid) = lower(c.user_id)
 where b.uid is null and c.user_id is null;

insert overwrite table tmp_innofin.no_marketing_user_count partition (plancode='Sfif170615a1')
select count(1),
	sum(if(contract_status is not null,1,0)),
	sum(if(contract_status = 1,1,0)) from
(select a.uid,	b.contract_status 
  from tmp_innofin.no_markting_users a 
  left join ods_innofin.user_contract b on a.uid = b.user_id
  where a.plancode='Sfif170615a1' 
) c;

--  0624
insert overwrite table tmp_innofin.no_markting_users partition(plancode='Sfif170624a1')
select a.uid from tmp_innofin.cfbdb_user_prefix_model a
left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d<'${zdt.format("yyyy-MM-dd")}') b on a.uid = b.uid
left join (select user_id from ods_innofin.user_contract_daily where d = '2017-06-24') c on lower(a.uid) = lower(c.user_id)
 where b.uid is null and c.user_id is null;

insert overwrite table tmp_innofin.no_marketing_user_count partition (plancode='Sfif170624a1')
select count(1),
	sum(if(contract_status is not null,1,0)),
	sum(if(contract_status = 1,1,0)) from
(select a.uid,b.contract_status 
  from tmp_innofin.no_markting_users a 
  left join ods_innofin.user_contract b on a.uid = b.user_id
  inner join (select uid from dw_mobdb.factmbpageview where d > '2017-06-24') c on a.uid=c.uid
  where a.plancode='Sfif170624a1' 
) c;

