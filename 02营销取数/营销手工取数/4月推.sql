--目前4月份还余留的需要数据端口帮忙手工取数的批次为：
--（1）4月17日推送，需要在4月12日完成手工取数：
--·短信，30W——Sfif170417a1；预授信成功用户；——针对新用户活动
--（2）4月20日推送，需要在4月14日完成手工取数：
--·EDM，5W——Efif170420a1；未参加过周四返礼活动的已成功激活用户；——针对老用户活动
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail add if not exists partition (d='2017-04-14');
insert overwrite table tmp_innofin.factmarketingsenddetail partition(d='2017-04-14')
select 'Efif170420a1',user_id ,chn_name,'EDM',email,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from(
  select a.user_id ,b.chn_name,b.email,a.update_time
  from ods_innofin.user_contract a
  inner join dw_innofin.dim_user b on a.user_id = b.uid
  left join ods_innofin.activity_thursday_rebate c on a.user_id = c.user_id
  where a.contract_status='1' and b.email REGEXP '^.+@.+$'
  and c.user_id is null order by a.update_time desc
  limit 100000
) e;

alter table tmp_innofin.factmarketingconfiguredetail add if not exists  partition (d='2017-04-14');
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-04-14')
select 'Efif170420a1','EDM','FIN',
0,1,1,0,1,1,0,0,
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
;

--·短信，10W——Sfif170420a1；4月15日前新开通但订单量较少的用户；——针对老用户活动
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail add if not exists partition (d='2017-04-14');
insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-04-14')
select 'Sfif170420a1',user_id ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from(
  select a.user_id,a.update_time,b.ordercount
  from ods_innofin.user_contract a
  left join (select uid as uid,count(1) as ordercount from ods_innofin.fact_loan_order group by uid) b on lower(a.user_id) = lower(b.uid)
  where a.contract_status='1' and a.update_time<'2017-04-15'
  order by a.update_time,b.ordercount desc
  limit 200000
) e;

alter table tmp_innofin.factmarketingconfiguredetail add if not exists  partition (d='2017-04-14');
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-04-14')
select 'Sfif170420a1','SMS','FIN',
0,1,1,0,1,1,0,0,
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
;

--（3）4月27日推送，需要在4月21日完成手工取数：
--·短信，10W——Sfif170427a1；4月15日之后新开通但订单量较少的用户；——针对老用户活动/下单赢免单活动
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail add if not exists partition (d='2017-04-21');
insert overwrite table tmp_innofin.factmarketingsenddetail partition(d='2017-04-21')
select 'Sfif170427a1',user_id ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from(
  select a.user_id,a.update_time,b.ordercount
  from ods_innofin.user_contract a
  left join (select uid as uid,count(1) as ordercount from ods_innofin.fact_loan_order group by uid) b on lower(a.user_id) = lower(b.uid)
  left join (select uid as sentuid from tmp_innofin.factmarketingsenddetail where d='2017-04-14') f on lower(a.user_id) = lower(f.sentuid)
  where a.contract_status='1' 
  and f.sentuid is null
  order by a.update_time,b.ordercount desc
  limit 200000
) e;

alter table tmp_innofin.factmarketingconfiguredetail add if not exists  partition (d='2017-04-21');
insert overwrite table tmp_innofin.factmarketingconfiguredetail partition(d='2017-04-21')
select 'Sfif170427a1','SMS','FIN',
0,1,1,0,1,1,0,0,
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
;


