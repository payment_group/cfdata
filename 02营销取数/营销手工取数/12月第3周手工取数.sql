-- 第3周批次要求入下：
-- 
-- 数据量：
-- EDM各5W；MSG各50W；
-- 
-- 批次号
-- EDM：Efif171216d1；
-- MSG：Mfif171216f1；
-- 
-- 取数条件：
-- EDM：请选取之前洗出来未推送过EDM的用户；
-- MSG：请选取之前洗出来未推送过MSG的用户；
-- 
-- 剔除条件：
-- 已完成申请流程剔重；
-- 高端客户等硬性剔重要求。

use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171216d1',a.uid ,'','EDM', a.value, 'FIN', CURRENT_DATE,CURRENT_DATE
from (select distinct uid,value from tmp_innofin.factmarketingsenddetail where d <= current_date and channeltype = 'EDM' and value REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' ) a
	left join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM') b on lower(a.uid) = lower(b.uid)
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where b.uid is null 
  and d.user_id is null 
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171216d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;


insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171216f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from tmp_innofin.factmarketingsenddetail where d <= current_date and channeltype = 'MIM') a
  left join	(select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM') b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171216f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Efif171216d1','2017-12-16','2017-12-26'),
('Mfif171216f1','2017-12-16','2017-12-26');