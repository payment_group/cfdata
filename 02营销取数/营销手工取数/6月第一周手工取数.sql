-- 描述
-- 
-- 在魔方数据的拿去花标签开发完毕之前，群分消息推送需要手工取数支持。
-- 
-- 【需要额外注意数据的剔重】问题：
-- 1、同一数据包内，同一手机号不得重复出现；
-- 2、15天内，同一渠道数据包内，用户不得重复出现，需剔重；
-- 
-- 6月第一周手工取数需求如下：
-- 本次的a1全部改为a2
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='2017-05-24');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d='2017-05-24');

alter table tmp_innofin.factmarketingsenddetail add if not exists partition (d='2017-05-24');
alter table tmp_innofin.factmarketingconfiguredetail add if not exists  partition (d='2017-05-24');
-- ·EDM：
-- 取数条件：选取“前置模型实名”用户；
-- 企划号及数据量：Efif170601a2——5W； Efif170602a2——50W；
-- 需要剔重的数据包：无；
use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-24')
select 'Efif170601a2',uid ,chn_name,'EDM',email,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from(
  select a.uid,b.chn_name,b.email
  from tmp_innofin.cfbdb_user_prefix_model_relname a
  inner join dw_innofin.dim_user b on a.uid = b.uid
  where b.email REGEXP '^.+@.+$'
  limit 50000
) e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-24')
select 'Efif170601a2','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-24')
select 'Efif170602a2',uid ,chn_name,'EDM',email,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from(
  select a.uid,b.chn_name,b.email
  from tmp_innofin.cfbdb_user_prefix_model_relname a
  inner join dw_innofin.dim_user b on a.uid = b.uid
  left join tmp_innofin.factmarketingsenddetail c on a.uid=c.uid and c.d <='2017-05-24' and
  c.plancode in ('Efif170601a2') where c.uid is null and a.uid is not null and trim(a.uid) <> ''
  and b.email REGEXP '^.+@.+$'
  limit 500000
) e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-24')
select 'Efif170602a2','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·短信：
-- 取数条件：选取“前置模型实名绑卡”用户；
-- 企划号及数据量：Sfif170601a2——40W； Sfif170603a2——20W；
-- 需要剔重的数据包：Sfif170518a1；Sfif170520a1；Sfif170525a1；Sfif170527a1；
insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-24')
select 'Sfif170601a2',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-05-24' and
  b.plancode in ('Sfif170518a1','Sfif170520a1','Sfif170525a1','Sfif170527a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 400000
) e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-24')
select 'Sfif170601a2','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-24')
select 'Sfif170603a2',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-05-24' and 
  b.plancode in ('Sfif170518a1','Sfif170520a1','Sfif170525a1','Sfif170527a1','Sfif170601a2') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 200000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-24')
select 'Sfif170603a2','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·站内信：
-- 取数条件：选取“前置模型实名”用户；
-- 企划号及数据量：Mfif170601a2——100W； Mfif170603a2——100W；
-- 需要剔重的数据包：Mfif170518a1；Mfif170520a1；Mfif170525a1；Mfif170527a1；
-- 
-- 以上数据包共计6个，需5月24日完成取数工作，感谢！
insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-24')
select 'Mfif170601a2',uid ,'','MIM',e.uid,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-05-24' and 
  b.plancode in ('Mfif170518a1','Mfif170520a1','Mfif170525a1','Mfif170527a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 1000000
)e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-24')
select 'Mfif170601a2','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-24')
select 'Mfif170603a2',e.uid ,'','MIM',e.uid,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-05-24' and 
  b.plancode in ('Mfif170518a1','Mfif170520a1','Mfif170525a1','Mfif170527a1','Mfif170601a2') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-24')
select 'Mfif170603a2','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');




