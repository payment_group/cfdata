-- 第2周批次要求入下：
-- 
-- 数据量：
-- EDM各5W；MSG各50W；
-- 
-- 批次号：
-- EDM：Efif171209d1；Efif171211d1
-- MSG：Mfif171209f1；Mfif171211f1
-- 
-- 取数条件：
-- EDM：请选取之前洗出来未推送过EDM的用户；
-- MSG：请选取之前洗出来未推送过MSG的用户；
-- 
-- 剔除条件：
-- 已完成申请流程剔重；
-- 高端客户等硬性剔重要求。
-- 

use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- EDM：Efif171209d1；Efif171211d1
-- 取数条件：
-- EDM：请选取之前洗出来未推送过EDM的用户；
-- 剔除条件：
-- 已完成申请流程剔重；

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171209d1',a.uid ,'','EDM', a.value, 'FIN', CURRENT_DATE,CURRENT_DATE
from (select distinct uid,value from tmp_innofin.factmarketingsenddetail where d < current_date and channeltype = 'EDM' and value REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' ) a
	left join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM') b on lower(a.uid) = lower(b.uid)
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where b.uid is null 
  and d.user_id is null 
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171209d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171211d1',a.uid ,'','EDM', a.value, 'FIN', CURRENT_DATE,CURRENT_DATE
from (select distinct uid,value from tmp_innofin.factmarketingsenddetail where d < current_date and channeltype = 'EDM' and value REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)') a
	left join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM') b on lower(a.uid) = lower(b.uid)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d = current_date and channeltype = 'EDM') c on lower(a.uid) = lower(c.uid)
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where b.uid is null and c.uid is null and d.user_id is null 
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171211d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

-- 取数条件：
-- MSG：请选取之前洗出来未推送过MSG的用户；
-- MSG：Mfif171209f1；Mfif171211f1
-- 剔除条件：
-- 已完成申请流程剔重；
-- 高端客户等硬性剔重要求。
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171209f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from tmp_innofin.factmarketingsenddetail where d < current_date and channeltype = 'MIM') a
  left join	(select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM') b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171209f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171211f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from tmp_innofin.factmarketingsenddetail where d < current_date and channeltype = 'MIM') a
  left join	(select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM') b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d = current_date and channeltype = 'MIM') d on lower(a.uid) = lower(d.uid)
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171211f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Efif171209d1','2017-12-09','2017-12-19'),
('Efif171211d1','2017-12-11','2017-12-21'),
('Mfif171209f1','2017-12-09','2017-12-19'),
('Mfif171211f1','2017-12-11','2017-12-21');