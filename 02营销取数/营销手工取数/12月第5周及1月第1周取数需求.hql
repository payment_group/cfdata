-- EDM：针对前6个月内发送但未打开过的用户推送
-- Efif171230d1——5W
-- 剔除：完成申请流程用户；剔除Efif171209d1；Efif171211d1；Efif171216d1；Efif171222d1；Efif171224d1；Efif171225d1
-- 
-- MSG:针对前6个月内发送但未打开过的用户推送
-- Mfif171230f1——50W
-- Mfif180102f1——100W
-- 剔除：完成申请流程用户；剔除Mfif171209f1；Mfif171211f1；Mfif171216f1；Mfif171222f1；Mfif171224f1；Mfif171225f1
-- 
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- EDM：针对前6个月内发送但未打开过的用户推送
-- Efif171230d1——5W
-- 剔除：完成申请流程用户；剔除Efif171209d1；Efif171211d1；Efif171216d1；Efif171222d1；Efif171224d1；Efif171225d1

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171230d1',a.uid ,'','EDM', sendaddress, 'FIN', CURRENT_DATE,CURRENT_DATE
from (select distinct uid,sendaddress from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM' and d >= add_months(current_date,-6) and ctisendstatus = 'T') a
	left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-6)) b on lower(a.uid) = lower(b.uid)  
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where a.uid is not null and a.sendaddress REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and d.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171230d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

-- MSG:针对前6个月内发送但未打开过的用户推送
-- Mfif171230f1——50W
-- Mfif180102f1——100W
-- 剔除：完成申请流程用户；剔除Mfif171209f1；Mfif171211f1；Mfif171216f1；Mfif171222f1；Mfif171224f1；Mfif171225f1

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171230f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and  d >= add_months(current_date,-6) and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171209f1','Mfif171211f1','Mfif171216f1','Mfif171222f1','Mfif171224f1','Mfif171225f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171230f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180102f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and  d >= add_months(current_date,-6) and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171209f1','Mfif171211f1','Mfif171216f1','Mfif171222f1','Mfif171224f1','Mfif171225f1','Mfif171230f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 1000000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180102f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;


--trace
insert into table tmp_innofin.tmp_gpush values
('Efif171230d1','2017-12-30','2018-01-10'),
('Mfif171230f1','2017-12-30','2018-01-10'),
('Mfif180102f1','2018-01-02','2018-01-12');