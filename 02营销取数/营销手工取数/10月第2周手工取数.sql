-- 10月第2周取数需求如下：
-- （1）Efif171015d1——5W——预授信成功且未推送过的用户
-- （2）Mfif171015f1——50W——3个月内发送成功但未打开过MSG的用户
-- 
-- 剔除注意：
-- EDM请剔除 Efif170927d1、 Efif170930d1、 Efif171004d1、 Efif171008d1、 Efif171011d1；
-- MSG请剔除Mfif170927f1、 Mfif170930f1、 Mfif171004f1、 Mfif171008f1、 Mfif171011f1；
-- 以上两个数据包均需剔除已申请开通拿去花的用户；


use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- （1）Efif171015d1——5W——预授信成功且未推送过的用户
-- EDM请剔除 Efif170927d1、 Efif170930d1、 Efif171004d1、 Efif171008d1、 Efif171011d1；
-- 以上两个数据包均需剔除已申请开通拿去花的用户；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171015d1',a.uid ,'','EDM', email, 'FIN',current_date,current_date
from dw_innofin.dim_user a
  inner join (select distinct a.user_id from ods_innofin.user_activate_req a 
  				inner join ods_innofin.user_contract b on a.user_id = b.user_id where contract_status = 4 and req_source = 'PRE_CREDIT_TASK') b on lower(a.uid) = lower(b.user_id)
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
  				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Efif170927d1','Efif170930d1','Efif171004d1','Efif171008d1','Efif171011d1')) c on lower(a.uid) = lower(c.uid)  
  where a.email REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and c.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171015d1','EDM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;


-- ·MSG：
-- （2）Mfif171015f1——50W——3个月内发送成功但未打开过MSG的用户
-- MSG请剔除Mfif170927f1、 Mfif170930f1、 Mfif171004f1、 Mfif171008f1、 Mfif171011f1；
-- 以上两个数据包均需剔除已申请开通拿去花的用户；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171015f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,7,2) in ('07','08','09') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d >= '${zdt.addDay(100).format("yyyy-MM-dd")}') b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif170927f1','Mfif170930f1','Mfif171004f1','Mfif171008f1','Mfif171011f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171015f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

--trace
insert into table tmp_innofin.tmp_gpush select 'Efif171015d1','2017-10-15','2017-10-25';
insert into table tmp_innofin.tmp_gpush select 'Mfif171015f1','2017-10-15','2017-10-25';
