-- 有邮箱的老客户 -- 无订单 --Efif170406a1
-- 0405补数据
alter table tmp_innofin.factmarketingsenddetail add partition (d='2017-04-05');

insert table tmp_innofin.factmarketingsenddetail partition(d='2017-04-05')
select 'Efif170406a1',a.user_id,b.chn_name,'EDM',b.email,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from ods_innofin.user_contract  a
inner join dw_innofin.dim_user b
on a.user_id = b.uid
left join (select uid, max(billtime)  from ods_innofin.fact_loan_order  group by  uid) c on lower(a.user_id) = lower(c.uid)
where a.contract_status=1 and b.email REGEXP '^.+@.+$'
and c.uid is null

-- 配置表
alter table tmp_innofin.factmarketingconfiguredetail add partition (d='2017-04-05');

insert table tmp_innofin.factmarketingconfiguredetail partition(d='2017-04-05')
values('Efif170406a1','EDM','FIN',
0,1,1,0,1,1,0,0,
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
);

--------------------------------------


-- 激活客户
select count(1) from ods_innofin.user_contract where contract_status=1

-- 有邮箱
select * from dw_innofin.dim_user where email REGEXP '^.+@.+$' limit 100
--最后一次下单
select uid, max(billtime)  from ods_innofin.fact_loan_order  group by uid

-- 激活有下单
select a.*,b.uid from ods_innofin.user_contract a
inner join (select uid, max(billtime)  from ods_innofin.fact_loan_order  group by  uid) b on lower(a.user_id) = lower(b.uid)
where contract_status=1
-- Time taken: 125.968 seconds, Fetched: 67777 row(s)


-- 激活未下单
select a.*,b.uid from ods_innofin.user_contract a
left join (select uid, max(billtime) from ods_innofin.fact_loan_order  group by  uid) b
on lower(a.user_id) = lower(b.uid)
where contract_status=1
and b.uid is null

-- Time taken: 116.764 seconds, Fetched: 174051 row(s)


-- 激活未下单 额度，时间
select a.*,b.uid,c.contract_amount from ods_innofin.user_contract a
inner join dw_innofin.dim_user c
on lower(a.user_id) = lower(c.uid)
left join (select uid, max(billtime) from ods_innofin.fact_loan_order  group by  uid) b
on lower(a.user_id) = lower(b.uid)
where contract_status=1
and b.uid is null
order by c.contract_amount desc,a.update_time limit 100000

-- 发送数据1 table
use tmp_innofin;
drop table tmp_innofin.tmp_Efif170406a1;
create table tmp_innofin.tmp_Efif170406a1(
  plancode 	string 	comment	'数据包号' ,
	uid 	string 	comment	'用户ID' ,
	username 	string 	comment	'用户名称' ,
	channeltype 	string 	comment	'渠道类型' ,
	value 	string 	comment	'值' ,
	businesstype 	string 	comment	'业务类型' ,
	createtime 	string 	comment	'创建时间' ,
	updatetime 	string 	comment	'更改时间' )
partitioned by(
	d 	string 	comment	'数据日期'
);

-- 发送数据2
use tmp_innofin;
drop table tmp_innofin.tmp_Sfif170406a1;
create table tmp_innofin.tmp_Sfif170406a1(
  plancode 	string 	comment	'数据包号' ,
	uid 	string 	comment	'用户ID' ,
	username 	string 	comment	'用户名称' ,
	channeltype 	string 	comment	'渠道类型' ,
	value 	string 	comment	'值' ,
	businesstype 	string 	comment	'业务类型' ,
	createtime 	string 	comment	'创建时间' ,
	updatetime 	string 	comment	'更改时间' )
partitioned by(
	d 	string 	comment	'数据日期'
);


