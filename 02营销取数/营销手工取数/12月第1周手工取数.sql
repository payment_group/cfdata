-- 第一周批次要求入下：
-- 
-- 数据量：
-- EDM各5W；MSG各50W；
-- 
-- 批次号：
-- EDM：Efif171202d1；Efif171203d1
-- MSG：Mfif171202f1；Mfif171203f1
-- 
-- 取数条件：
-- EDM：请选取可开通用户中最近1个月内有携程APP打开记录的用户；
-- MSG：请选取以往发送用户中未打开过MSG的用户；
-- 
-- 剔除条件：
-- 过去20天，同渠道发送剔重；
-- 已完成申请流程剔重；
-- 高端客户等硬性剔重要求。
-- 
-- 完成时间：
-- 周二下班前完成取数。

use tmp_innofin;
create table vqq_ctrip_active_user_1m as 
select distinct uid from dw_mobdb.factmbpageview where d >= add_months(current_date,-1);

use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- EDM：Efif171202d1；Efif171203d1
-- 取数条件：
-- EDM：请选取可开通用户中最近1个月内有携程APP打开记录的用户；
-- 剔除条件：
-- 过去20天，同渠道发送剔重；
-- 已完成申请流程剔重；
-- 高端客户等硬性剔重要求。

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171202d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	inner join vqq_ctrip_active_user_1m b on lower(a.uid) = lower(b.uid) 
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d >= date_add(current_date,-20) and channeltype = 'EDM') e on lower(a.uid) = lower(e.uid)
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and d.user_id is null 
  and e.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171202d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171203d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	inner join vqq_ctrip_active_user_1m b on lower(a.uid) = lower(b.uid) 
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d >= date_add(current_date,-20) and channeltype = 'EDM') e on lower(a.uid) = lower(e.uid)
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and d.user_id is null 
  and e.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171203d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

-- MSG：请选取以往发送用户中未打开过MSG的用户；
-- MSG：Mfif171202f1；Mfif171203f1
-- 剔除条件：
-- 过去20天，同渠道发送剔重；
-- 已完成申请流程剔重；
-- 高端客户等硬性剔重要求。
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171202f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d >= date_add(current_date,-20) and channeltype = 'MIM') d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171202f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171203f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d >= date_add(current_date,-20) and channeltype = 'MIM') d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171203f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Efif171202d1','2017-12-02','2017-12-12'),
('Efif171203d1','2017-12-03','2017-12-13'),
('Mfif171202f1','2017-12-02','2017-12-12'),
('Mfif171203f1','2017-12-03','2017-12-13');