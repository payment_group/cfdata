--EDM批次需求详情——各5W：
--Efif171222d1
--Efif171224d1
--Efif171225d1
--取数条件：未发送过的用户；
--剔除批次：Efif171203d1；Efif171209d1；Efif171211d1；Efif171216d1
--
--MSG批次需求详情——各50W：
--Mfif171222f1
--Mfif171224f1
--Mfif171225f1
--取数条件：未发送过的用户；
--剔除批次：Mfif171203f1；Mfif171209f1；Mfif171211f1；Mfif171216f1
--
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

--Efif171222d1
--Efif171224d1
--Efif171225d1
--取数条件：未发送过的用户；
--剔除批次：Efif171203d1；Efif171209d1；Efif171211d1；Efif171216d1

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171222d1',a.uid ,'','EDM', a.value, 'FIN', CURRENT_DATE,CURRENT_DATE
from (select distinct uid,value from tmp_innofin.factmarketingsenddetail where d < current_date and channeltype = 'EDM' and value REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' ) a
	left join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM') b on lower(a.uid) = lower(b.uid)
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where b.uid is null 
  and d.user_id is null 
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171222d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171224d1',a.uid ,'','EDM', a.value, 'FIN', CURRENT_DATE,CURRENT_DATE
from (select distinct uid,value from tmp_innofin.factmarketingsenddetail where d < current_date and channeltype = 'EDM' and value REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)') a
	left join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM') b on lower(a.uid) = lower(b.uid)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d = current_date and channeltype = 'EDM') c on lower(a.uid) = lower(c.uid)
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where b.uid is null and c.uid is null and d.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171224d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171225d1',a.uid ,'','EDM', a.value, 'FIN', CURRENT_DATE,CURRENT_DATE
from (select distinct uid,value from tmp_innofin.factmarketingsenddetail where d <= current_date and channeltype = 'EDM' and value REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)') a
	left join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM') b on lower(a.uid) = lower(b.uid)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d = current_date and channeltype = 'EDM') c on lower(a.uid) = lower(c.uid)
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where b.uid is null and c.uid is null and d.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171225d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

--MSG批次需求详情——各50W：
--Mfif171222f1
--Mfif171224f1
--Mfif171225f1
--取数条件：未发送过的用户；
--剔除批次：Mfif171203f1；Mfif171209f1；Mfif171211f1；Mfif171216f1
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171222f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from tmp_innofin.factmarketingsenddetail where d <= current_date and channeltype = 'MIM') a
  left join	(select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM') b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171222f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171224f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from tmp_innofin.factmarketingsenddetail where d <= current_date and channeltype = 'MIM') a
  left join	(select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM') b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d = current_date and channeltype = 'MIM') d on lower(a.uid) = lower(d.uid)
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171224f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

-- 数量不够，取全量
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171225f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select a.uid from tmp_innofin.cfbdb_vqq_user_prefix_model) a
  left join	(select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM') b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d = current_date and channeltype = 'MIM') d on lower(a.uid) = lower(d.uid)
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171225f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;


--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Efif171222d1','2017-12-22','2018-01-02'),
('Efif171224d1','2017-12-24','2017-12-04'),
('Efif171225d1','2017-12-25','2017-12-05'),
('Mfif171222f1','2017-12-22','2017-12-02'),
('Mfif171224f1','2017-12-24','2017-12-04'),
('Mfif171225f1','2017-12-25','2017-12-05');