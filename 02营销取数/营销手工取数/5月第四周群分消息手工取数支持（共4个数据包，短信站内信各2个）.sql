use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail add if not exists partition (d='2017-05-19');
alter table tmp_innofin.factmarketingconfiguredetail add if not exists  partition (d='2017-05-19');

-- 1） 20.00W——Sfif170525a1（短信）
	-- 满足前置条件及模型条件的用户（需有手机号的用户）
	-- 排除 Sfif170518a1,Sfif170520a1; partition (d='2017-05-15') 
insert overwrite table tmp_innofin.factmarketingsenddetail partition(d='2017-05-19')
select 'Sfif170525a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.user_prefix_model_relname_card_amount_feature a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d='2017-05-15' and 
  b.plancode in ('Sfif170518a1','Sfif170520a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 200000
) e;
insert overwrite table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-19')
select 'Sfif170525a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');
-- 2）20.00W——Sfif170527a1（短信）
	-- 满足前置条件及模型条件的用户（需有手机号的用户）
	-- 排除 Sfif170518a1,Sfif170520a1; partition (d='2017-05-15')
	-- 排除 Sfif170525a1; partition (d='2017-05-19')
--
insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-19')
select 'Sfif170527a1',e.uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.user_prefix_model_relname_card_amount_feature a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d='2017-05-15' and b.plancode in ('Sfif170518a1','Sfif170520a1')
  left join tmp_innofin.factmarketingsenddetail c on a.uid=c.uid and c.d='2017-05-19' and c.plancode in ('Sfif170525a1') 
  where b.uid is null and a.uid is not null and c.uid is null and trim(a.uid) <> ''
  limit 200000
)e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-19')
select 'Sfif170527a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 3）80.00W——Mfif170525a1（站内信）
	-- 要求：已实名用户；
	-- 排斥：Mfif170518a1，Mfif170520a1 partition (d='2017-05-15') 
insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-19')
select 'Mfif170525a1',uid ,'','MIM',e.uid,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d='2017-05-15' and 
  b.plancode in ('Mfif170518a1','Mfif170520a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 800000
)e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-19')
select 'Mfif170525a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 4）80.00W——Mfif170527a1（站内信）
	-- 要求：已实名用户；
	-- 排斥：Mfif170518a1，Mfif170520a1 partition (d='2017-05-15') 
	-- 排除：Mfif170525a1 (d='2017-05-19')
insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-05-19')
select 'Mfif170527a1',e.uid ,'','MIM',e.uid,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d='2017-05-15' and 
  b.plancode in ('Mfif170518a1','Mfif170520a1') 
  left join tmp_innofin.factmarketingsenddetail c on a.uid=c.uid and c.d='2017-05-19' and 
  c.plancode in ('Mfif170525a1') 
  where b.uid is null and a.uid is not null and c.uid is null and trim(a.uid) <> ''
  limit 800000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-05-19')
select 'Mfif170527a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 以上共计4个数据包，需在本周五完成取数，感谢！
-- 
select * from tmp_innofin.factmarketingconfiguredetail where d='2017-05-12';

-- 数据验证
-- 1)条数
select plancode,count(*) from tmp_innofin.factmarketingsenddetail where d='2017-05-19' group by plancode;
-- 2)批号内无重复
select plancode,count(distinct uid) from tmp_innofin.factmarketingsenddetail where d='2017-05-19' group by plancode;
-- 3)同类批号之间无重复
select count(1) from 
	(select uid from tmp_innofin.factmarketingsenddetail where d='2017-05-19' and plancode = 'Sfif170525a1') a 
	inner join 
	(select uid from tmp_innofin.factmarketingsenddetail where d='2017-05-19' and plancode = 'Sfif170527a1') b 
	on a.uid = b.uid;
select count(1) from 
	(select uid from tmp_innofin.factmarketingsenddetail where d='2017-05-19' and plancode = 'Mfif170525a1') a 
	inner join 
	(select uid from tmp_innofin.factmarketingsenddetail where d='2017-05-19' and plancode = 'Mfif170527a1') b 
	on a.uid = b.uid;
-- 4)有邮件
-- 5)无空
select count(uid) from tmp_innofin.factmarketingsenddetail where d='2017-05-19' and uid is null or trim(uid) = '';
-- 6)config
select * from tmp_innofin.factmarketingconfiguredetail where(d='2017-05-19');


