-- ·EDM：
-- 企划号：Efif170803d1，Efif170804d1
-- 数据量：均为5W；
-- 剔除数据包：Efif170722a1，Efif170726a1，Efif170728a1，Efif170729a1，Efif170802a1；
-- 
-- ·SMS：
-- 企划号：Sfif170804e1
-- 数据量：30W；
-- 剔除数据包：Sfif170722a1，Sfif170726a1，Sfif170729a1；
-- 
-- ·MSG：
-- 企划号：Mfif170803f1，Mfif170804f1，Mfif170806f1
-- 数据量：均为100W；
-- 剔除数据包：Mfif170722a1，Mfif170722a2，Mfif170726a1，Mfif170728a1，Mfif170729a1
-- 
-- 补充取数要求如下：
-- 
-- 短信：
-- 1、通过了双模型的用户；2、机票“大方小气”标签中的“一般大方”；（二者关系为“且”）
-- EMS：
-- 邮箱存量用户；
-- MSG：
-- 1、已绑卡用户；2、价格敏感用户；（二者关系为“且”）
--
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 1、EDM——库存邮箱客户；
-- 
-- 数据量 企划号 
-- 企划号：Efif170803d1，Efif170804d1
-- 数据量：均为5W；
-- 剔除数据包：Efif170722a1，Efif170726a1，Efif170728a1，Efif170729a1，Efif170802a1；

-- data 
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170803d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170722a1','Efif170726a1','Efif170728a1','Efif170729a1','Efif170802a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
-- config
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170803d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170804d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170722a1','Efif170726a1','Efif170728a1','Efif170729a1','Efif170802a1','Efif170803d1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170804d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 2、SMS
-- 
-- 企划号：Sfif170804e1
-- 数据量：30W；
-- 剔除数据包：Sfif170722a1，Sfif170726a1，Sfif170729a1；
-- 1、通过了双模型的用户；
-- 2、机票“大方小气”标签中的“一般大方”；（二者关系为“且”）这个条件不要了，数据太少
use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif170804e1',a.uid ,'','SMS','','FIN', from_unixtime(unix_timestamp(),'yyyy-MM-dd'), from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from
	tmp_innofin.cfbdb_vqq_user_prefix_model a 
	left join ods_innofin.user_contract b on a.uid = b.user_id 
    left join tmp_innofin.factmarketingsenddetail d on a.uid = d.uid and d.d <='${zdt.format("yyyy-MM-dd")}' and d.plancode in ('Sfif170722a1','Sfif170726a1','Sfif170729a1') 
	where b.user_id is null and d.uid is null
	order by rand()
  	limit 300000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170804e1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·MSG：
-- 企划号：Mfif170803f1，Mfif170804f1，Mfif170806f1
-- 数据量：均为100W；
-- 剔除数据包：Mfif170722a1，Mfif170722a2，Mfif170726a1，Mfif170728a1，Mfif170729a1
-- -- 1、已绑卡用户；
-- 2、价格敏感用户；（二者关系为“且”）这个条件不要了，数据太少

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170803f1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170722a1','Mfif170722a2','Mfif170726a1','Mfif170728a1','Mfif170729a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170803f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170804f1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170722a1','Mfif170722a2','Mfif170726a1','Mfif170728a1','Mfif170729a1','Mfif170803f1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170804f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170806f1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170722a1','Mfif170722a2','Mfif170726a1','Mfif170728a1','Mfif170729a1','Mfif170803f1','Mfif170804f1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170806f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

--trace
insert into table tmp_innofin.tmp_gpush select 'Efif170803d1','2017-08-03','2017-09-03';
insert into table tmp_innofin.tmp_gpush select 'Efif170804d1','2017-08-04','2017-09-04';
insert into table tmp_innofin.tmp_gpush select 'Sfif170804e1','2017-08-04','2017-09-04';
insert into table tmp_innofin.tmp_gpush select 'Mfif170803f1','2017-08-03','2017-09-03';
insert into table tmp_innofin.tmp_gpush select 'Mfif170804f1','2017-08-04','2017-09-04';
insert into table tmp_innofin.tmp_gpush select 'Mfif170806f1','2017-08-06','2017-09-06';
