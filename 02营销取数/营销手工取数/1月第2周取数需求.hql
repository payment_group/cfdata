-- MSG:针对前6个月内发送但未打开过的用户推送
-- 
-- Mfif180106f1——100W
-- Mfif180109f1——100W
-- 
-- 剔除：完成申请流程用户；剔除Mfif171211f1；Mfif171216f1；Mfif171222f1；Mfif171224f1；Mfif171225f1；Mfif171230f1；Mfif180102f1
-- 
-- 
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');


-- MSG:针对前6个月内发送但未打开过的用户推送
-- 
-- Mfif180106f1——100W
-- Mfif180109f1——100W
-- 
-- 剔除：完成申请流程用户；剔除Mfif171211f1；Mfif171216f1；Mfif171222f1；Mfif171224f1；Mfif171225f1；Mfif171230f1；Mfif180102f1

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180106f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and  d >= add_months(current_date,-6) and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-6)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171211f1','Mfif171216f1','Mfif171222f1','Mfif171224f1','Mfif171225f1','Mfif171230f1','Mfif180102f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 1000000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180106f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180109f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and  d >= add_months(current_date,-6) and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-6)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171211f1','Mfif171216f1','Mfif171222f1','Mfif171224f1','Mfif171225f1','Mfif171230f1','Mfif180102f1','Mfif180106f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 1000000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180109f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;


--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Mfif180106f1','2018-01-06','2018-01-16'),
('Mfif180109f1','2018-01-09','2018-01-19');