-- 1、EDM——新一轮提取出邮箱的客户；
-- 
-- 数据量 企划号 
-- 5W Efif170714a2
-- 50W Efif170715a2
-- 5W Efif170719a2
-- 
-- EDM需剔除的数据包号：Efif170701a1,Efif170705a1,Efif170707a1,Efif170708a2,Efif170712a2;
-- 
-- 2、SMS
-- 
-- 数据量 企划号
-- 20W Sfif170714a1——要求：未申请过开通拿去花的用户；已通过前置模型的用户；6个月内在携程APP有交易的用户；
-- 20W Sfif170719a1——要求：针对拿去花流失用户，发送活动营销短信；
-- 
-- SMS需剔除的数据包号：Sfif170701a1,Sfif170708a1;
-- 
-- 3、MSG——要求：未申请过开通拿去花的用户；已通过前置模型的用户；
-- 
-- 数据量 企划号
-- 100W Mfif170714a1
-- 100W Mfif170715a1
-- 100W Mfif170719a1
-- 
-- MSG数据包需剔除：Mfif170701a1,Mfif170705a1,Mfif170707a1,Mfif170708a1,Mfif170712a1;
-- 
--
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 1、EDM——新一轮提取出邮箱的客户；
-- 
-- 数据量 企划号 
-- 5W Efif170714a2
-- 50W Efif170715a2
-- 5W Efif170719a2
-- 
-- EDM需剔除的数据包号：Efif170701a1,Efif170705a1,Efif170707a1,Efif170708a2,Efif170712a2;

-- 5W Efif170714a2
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170714a2',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170707a1','Efif170708a2','Efif170712a2','Efif170701a1','Efif170705a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170714a2','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 50W Efif170715a2 -- 改为魔方取数
---- insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
---- select 'Efif170715a2',a.uid ,'','EDM', mail_plain, 'FIN',
---- from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
---- from_unixtime(unix_timestamp(),'yyyy-MM-dd')
---- from tmp_innofin.cfbdb_user_prefix_model_mail a
----   inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
----   left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170707a1','Efif170708a2','Efif170712a2','Efif170701a1','Efif170705a1','Efif170714a2') 
----   left join ods_innofin.user_contract c on a.uid = c.user_id 
----   where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
----   and b.uid is null and c.user_id is null
----   Order by rand()
----   limit 500000;
---- 
---- insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
---- select 'Efif170715a2','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');
-- 5W Efif170719a2
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170719a2',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170707a1','Efif170708a2','Efif170712a2','Efif170701a1','Efif170705a1','Efif170714a2','Efif170715a2') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170719a2','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 2、SMS
-- 
-- 数据量 企划号
-- 20W Sfif170714a1——要求：未申请过开通拿去花的用户；已通过前置模型的用户；6个月内在携程APP有交易的用户；
-- 20W Sfif170719a1——要求：针对拿去花流失用户，发送活动营销短信；
-- 
-- SMS需剔除的数据包号：Sfif170701a1,Sfif170708a1;


-- 20W Sfif170714a1——要求：未申请过开通拿去花的用户；已通过前置模型的用户；6个月内在携程APP有交易的用户；
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif170714a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Sfif170701a1','Sfif170708a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  order by rand()
  limit 200000
) e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170714a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 20W Sfif170719a1——要求：针对拿去花流失用户，发送活动营销短信；
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif170719a1',a.uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from
	(select uid,max(id) as maxid from tmp_innofin.qmwang_lostclients_1 group by uid) a
	inner join tmp_innofin.qmwang_lostclients_1 b on a.uid = b.uid and a.maxid = b.id
	where b.ord_loanpay = 0 
	order by rand()
  	limit 200000;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170719a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 
-- 3、MSG——要求：未申请过开通拿去花的用户；已通过前置模型的用户；
-- 
-- 数据量 企划号
-- 100W Mfif170714a1
-- 100W Mfif170715a1
-- 100W Mfif170719a1
-- 
-- MSG数据包需剔除：Mfif170701a1,Mfif170705a1,Mfif170707a1,Mfif170708a1,Mfif170712a1;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170714a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170701a1','Mfif170705a1','Mfif170707a1','Mfif170708a1','Mfif170712a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170714a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170715a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170701a1','Mfif170705a1','Mfif170707a1','Mfif170708a1','Mfif170712a1','Mfif170714a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170715a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170719a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170701a1','Mfif170705a1','Mfif170707a1','Mfif170708a1','Mfif170712a1','Mfif170714a1','Mfif170715a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170719a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

