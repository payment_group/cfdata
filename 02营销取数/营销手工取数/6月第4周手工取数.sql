-- 【需要额外注意数据的剔重】问题：
-- 1、同一数据包内，同一手机号不得重复出现；
-- 2、15天内，同一渠道数据包内，用户不得重复出现，需剔重；
-- 3、需剔除【已申请开通用户uid】
-- 
-- 6月第4周手工取数需求如下：
-- 
-- ·短信（拉新）：
-- 取数条件：选取“前置模型实名绑卡”用户+“近3个月内在携程有过订单的用户”；
-- 企划号及数据量：Sfif170622a1——40W； Sfif170624a1——20W；
-- 需要剔重的数据包：Sfif170608a1；Sfif170610a1；Sfif170615a1；Sfif170617a1；
-- 
-- ·站内信（拉新）：
-- 取数条件：选取“前置模型实名”用户；
-- 企划号及数据量：Mfif170622a1——100W； Mfif170624a1——100W；
-- 需要剔重的数据包：Mfif170608a1；Mfif170610a1；Mfif170615a1；Mfif170617a1；
-- 
-- 以上数据包共计4个，需*6月19日*完成取数工作，感谢！

-- 1) 近3个月内在携程有过订单的用户（使用order_index数据）
use tmp_innofin;
create table if not exists ctrip_order_index(
	dt string,
	uid string,
	order_id string,
	content  string,
	type string
);

insert into table tmp_innofin.ctrip_order_index
select dt,regexp_extract(content,'Uid":"([^"]+)",',1) as uid,orderid,content,'flight'
  from bbz_oi_orderdb.bbz_oi_order_flight
 where dt >= '${zdt.addDay(-30).format("yyyy-MM-dd")}'
   and dt <  '${zdt.format("yyyy-MM-dd")}';

insert into table tmp_innofin.ctrip_order_index
select dt,regexp_extract(content,'Uid":"([^"]+)",',1) as uid,orderid,content,'trains'
  from bbz_oi_orderdb.bbz_oi_order_trains
 where dt >= '${zdt.addDay(-30).format("yyyy-MM-dd")}'
   and dt <  '${zdt.format("yyyy-MM-dd")}';

insert into table tmp_innofin.ctrip_order_index
select dt,regexp_extract(content,'Uid":"([^"]+)",',1) as uid,orderid,content,'hotel'
  from bbz_oi_orderdb.bbz_oi_order_hotel
 where dt >= '${zdt.addDay(-30).format("yyyy-MM-dd")}'
   and dt <  '${zdt.format("yyyy-MM-dd")}';
 
 insert into table tmp_innofin.ctrip_order_index
select dt,regexp_extract(content,'Uid":"([^"]+)",',1) as uid,orderid,content,'vacation'
  from bbz_oi_orderdb.bbz_oi_order_vacation
 where dt >= '${zdt.addDay(-30).format("yyyy-MM-dd")}'
   and dt <  '${zdt.format("yyyy-MM-dd")}';

--
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- ·短信（拉新）：
-- 取数条件：选取“前置模型实名绑卡”用户+“近3个月内在携程有过订单的用户”；
-- 企划号及数据量：Sfif170622a1——40W； Sfif170624a1——20W；
-- 需要剔重的数据包：Sfif170608a1；Sfif170610a1；Sfif170615a1；Sfif170617a1；
-- 

insert overwrite table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170622a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Sfif170615a1','Sfif170617a1','Sfif170608a1','Sfif170610a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 400000
) e;

insert overwrite table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170622a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170624a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Sfif170615a1','Sfif170617a1','Sfif170608a1','Sfif170610a1','Sfif170622a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 200000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170624a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·站内信（拉新）：
-- 取数条件：选取“前置模型实名”用户；
-- 企划号及数据量：Mfif170622a1——100W； Mfif170624a1——100W；
-- 需要剔重的数据包：Mfif170608a1；Mfif170610a1；Mfif170615a1；Mfif170617a1；
-- 

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170622a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170615a1','Mfif170617a1','Mfif170608a1','Mfif170610a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170622a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170624a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170615a1','Mfif170617a1','Mfif170608a1','Mfif170610a1','Mfif170622a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170624a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');
