-- 拟针对预授信成功但1个月内未申请开通的用户单独进行站内信营销信息推送，需要强哥这边帮助取下数据~
-- 条件如下：
-- 1、预授信成功的用户；
-- 2、剔除近1个月内主动申请过开通的用户；
-- 3、剔除过去20天营销过的用户（批次号为：mfif180116f1；mfif180113f1；mfif180111f1；mfif180109f1；mfif171230f1；mfif180102f1；mfif171225f1；）
-- 取200W数据吧，数据包号如下：
-- Mfif180118f1
-- Mfif180120f1
-- 
-- -- 

-- 拟针对预授信成功但1个月内未申请开通的用户单独进行站内信营销信息推送，需要强哥这边帮助取下数据~
-- 条件如下：
-- 1、预授信成功的用户；
-- 2、剔除近1个月内主动申请过开通的用户；
-- 3、剔除过去20天营销过的用户（批次号为：mfif180116f1；mfif180113f1；mfif180111f1；mfif180109f1；mfif171230f1；mfif180102f1；mfif171225f1；）
-- 取200W数据吧，数据包号如下：
-- Mfif180118f1
-- Mfif180120f1
-- 


insert overwrite table tmp_innofin.vqq_mkt_hermes partition (dt='2018-01-18')
select 'Mfif180118f1',a.uid ,'','','','','','2018-01-18 00:00:00'
from (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source = 'PRE_CREDIT_TASK' and to_date(request_time) < '2018-01-01' and activate_status = 'PRECREDIT_SUCCESS') a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') b on lower(a.uid) = lower(b.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and lower(plancode) in ('mfif180116f1','mfif180113f1','mfif180111f1','mfif180109f1','mfif171230f1','mfif180102f1','mfif171225f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 1000000;

use tmp_innofin;
insert into table vqq_mkt_hermes partition (dt='2018-01-18') values
('Mfif180118f1','_WeChat546894665','','','','','','2018-01-18 00:25:00'),
('Mfif180118f1','_WeChat353954292','','','','','','2018-01-18 00:25:00')
;

insert overwrite table tmp_innofin.vqq_mkt_hermes partition (dt='2018-01-20')
select 'Mfif180120f1',a.uid ,'','','','','','2018-01-20 00:00:00'
from (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source = 'PRE_CREDIT_TASK' and to_date(request_time) < '2018-01-01' and activate_status = 'PRECREDIT_SUCCESS') a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') b on lower(a.uid) = lower(b.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and lower(plancode) in ('mfif180116f1','mfif180113f1','mfif180111f1','mfif180109f1','mfif171230f1','mfif180102f1','mfif171225f1','mfif180118f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 1000000;

use tmp_innofin;
insert into table vqq_mkt_hermes partition (dt='2018-01-20') values
('Mfif180120f1','_WeChat546894665','','','','','','2018-01-20 00:25:00'),
('Mfif180120f1','_WeChat353954292','','','','','','2018-01-20 00:25:00')
;

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Mfif180118f1','2018-01-18','2018-01-28'),
('Mfif180120f1','2018-01-20','2018-01-30');