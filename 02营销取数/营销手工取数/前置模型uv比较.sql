-- 1 7天uv数据明细（带路径）
use tmp_innofin;
create table if not exists uv_7d(
	prepagecode string,
	pagecode string,
	uid string
);
insert overwrite table tmp_innofin.uv_7d
select distinct prepagecode,pagecode,uid from dw_mobdb.factmbpageview where (d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}');
-- 2 求合计
use tmp_innofin;
drop table if exists uv_count_7d;
create table if not exists uv_count_7d(
	prepagecode string,
	pagecode string,
	uvcount int
);
insert overwrite table tmp_innofin.uv_count_7d
select prepagecode,pagecode,count(uid) as uvcount from tmp_innofin.uv_7d group by prepagecode,pagecode;

-- 3 前1000uv page
use tmp_innofin;
create table if not exists uv_count_max_1000(
	pagecode string,
	uvcount int
);
insert overwrite table tmp_innofin.uv_count_max_1000
select pagecode,sum(uvcount)  as uvcount  
from tmp_innofin.uv_count_7d group by pagecode order by uvcount desc limit 1000;

-- 前1000pagecode 的prepagecode前200 
use tmp_innofin;
create table if not exists prepagecode_max_200(
	prepagecode string,
	uvcount int
); 
insert overwrite table tmp_innofin.prepagecode_max_200
select prepagecode,sum(uvcount) as uvcount from (
select b.prepagecode,b.uvcount from
tmp_innofin.uv_count_max_1000 a
inner join tmp_innofin.uv_count_7d b on a.pagecode = b.pagecode
) c group by prepagecode order by uvcount desc limit 200;

-- 取页面名称
use tmp_innofin;
create table if not exists uv_count_result_7d(
	prepagecode string,
	prepagename string,
	preuvcount int,
	pagecode string,
	pagename string,
	uvcount int
);
insert overwrite table tmp_innofin.uv_count_result_7d
select a.prepagecode,d.pagename,a.uvcount as preuvcount,b.pagecode,e.pagename,b.uvcount from 
	tmp_innofin.prepagecode_max_200 a
	inner join tmp_innofin.uv_count_7d b on a.prepagecode = b.prepagecode
	inner join tmp_innofin.uv_count_max_1000 c on b.pagecode = c.pagecode
	inner join dim_mobdb.dimmbpagecode d on a.prepagecode = d.pagecode
	inner join dim_mobdb.dimmbpagecode e on c.pagecode = e.pagecode	
;

-- 分组取前5
use tmp_innofin;
create table if not exists uv_count_result_group_5(
	prepagecode string,
	prepagename string,
	preuvcount int,
	pagecode string,
	pagename string,
	uvcount int,
	od int
);
insert overwrite table tmp_innofin.uv_count_result_group_5
select * from(
select *, row_number() over (partition by prepagecode order by preuvcount desc ,uvcount desc ) as od from tmp_innofin.uv_count_result_7d 
) a
where a.od <= 5;
------------ 前置模型7天uv(1000-200)路径内
use tmp_innofin;
create table if not exists target_uv_7d(
	prepagecode string,
	pagecode string,
	uid string
);

insert overwrite table tmp_innofin.target_uv_7d
select a.prepagecode,a.pagecode,a.uid from tmp_innofin.uv_7d a 
inner join tmp_innofin.cfbdb_vqq_user_prefix_model b on a.uid = b.uid
left join ods_innofin.user_contract c on a.uid = c.user_id
inner join tmp_innofin.uv_count_result_group_5 d on a.prepagecode = d.prepagecode and a.pagecode = d.pagecode
where c.user_id is null;

-- 
select a.*,b.uvcount as pre_uv,c.uvcount as uv 
from tmp_innofin.uv_count_result_group_5 a 
inner join (select prepagecode,count(uid)  as uvcount from tmp_innofin.target_uv_7d group by prepagecode) b on a.prepagecode = b.prepagecode
inner join (select prepagecode,pagecode,count(uid)  as uvcount from tmp_innofin.target_uv_7d group by prepagecode,pagecode) c on a.prepagecode = c.prepagecode and a.pagecode = c.pagecode  


