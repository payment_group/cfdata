-- 拟针对预授信成功但1个月内未申请开通的用户单独进行站内信营销信息推送，需要强哥这边帮助取下数据~
-- 条件如下：
-- 1、预授信成功的用户；
-- 2、剔除近1个月内主动申请过开通的用户；
-- 3、剔除过去20天营销过的用户（批次号为：mfif180116f1；mfif180113f1；mfif180111f1；mfif180109f1；mfif171230f1；mfif180102f1；mfif171225f1；）
-- 取200W数据吧，数据包号如下：
-- Mfif180118f1
-- Mfif180120f1
-- 
-- -- 
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 拟针对预授信成功但1个月内未申请开通的用户单独进行站内信营销信息推送，需要强哥这边帮助取下数据~
-- 条件如下：
-- 1、预授信成功的用户；
-- 2、剔除近1个月内主动申请过开通的用户；
-- 3、剔除过去20天营销过的用户（批次号为：mfif180116f1；mfif180113f1；mfif180111f1；mfif180109f1；mfif171230f1；mfif180102f1；mfif171225f1；）
-- 取200W数据吧，数据包号如下：
-- Mfif180118f1
-- Mfif180120f1
-- 

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180118f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source = 'PRE_CREDIT_TASK' and to_date(request_time) < '2018-01-01' and activate_status = 'PRECREDIT_SUCCESS') a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') b on lower(a.uid) = lower(b.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and lower(plancode) in ('mfif180116f1','mfif180113f1','mfif180111f1','mfif180109f1','mfif171230f1','mfif180102f1','mfif171225f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 1000000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180118f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180120f1',a.uid ,'','MIM','','FIN',current_date,current_date

from (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source = 'PRE_CREDIT_TASK' and to_date(request_time) < '2018-01-01' and activate_status = 'PRECREDIT_SUCCESS') a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') b on lower(a.uid) = lower(b.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and lower(plancode) in ('mfif180116f1','mfif180113f1','mfif180111f1','mfif180109f1','mfif171230f1','mfif180102f1','mfif171225f1','mfif180118f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 1000000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif180120f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Mfif180118f1','2018-01-18','2018-01-28'),
('Mfif180120f1','2018-01-20','2018-01-30');