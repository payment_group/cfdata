-- 【需要额外注意数据的剔重】问题：
-- 1、同一数据包内，同一手机号不得重复出现；
-- 2、15天内，同一渠道数据包内，用户不得重复出现，需剔重；
-- 3、需剔除【已申请开通用户uid】
-- 
-- 6月第3周手工取数需求如下：
-- 
-- ·短信：
-- 取数条件：选取“前置模型实名绑卡”用户；
-- 企划号及数据量：Sfif170615a1——40W； Sfif170617a1——20W；
-- 需要剔重的数据包：Sfif170601a2；Sfif170603a2；Sfif170608a1；Sfif170610a1；
-- 
-- ·站内信：
-- 取数条件：选取“前置模型实名”用户；
-- 企划号及数据量：Mfif170615a1——100W； Mfif170617a1——100W；
-- 需要剔重的数据包：Mfif170601a2；Mfif170603a2；Mfif170608a1；Mfif170610a1；
-- 
-- 以上数据包共计4个，需*6月12日*完成取数工作，感谢！
-- 

-- 以上数据包共计4个，需6月5日完成取数工作，感谢！
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='2017-06-12');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d='2017-06-12');

-- ·短信：
-- 取数条件：选取“前置模型实名绑卡”用户；
-- 企划号及数据量：Sfif170615a1——40W； Sfif170617a1——20W；
-- 需要剔重的数据包：Sfif170601a2；Sfif170603a2；Sfif170608a1；Sfif170610a1；

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-12')
select 'Sfif170615a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='2017-06-12' and b.plancode in ('Sfif170601a2','Sfif170603a2','Sfif170608a1','Sfif170610a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 400000
) e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-12')
select 'Sfif170615a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-12')
select 'Sfif170617a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='2017-06-12' and b.plancode in ('Sfif170601a2','Sfif170603a2','Sfif170608a1','Sfif170610a1','Sfif170615a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 200000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-12')
select 'Sfif170617a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·站内信：
-- 取数条件：选取“前置模型实名”用户；
-- 企划号及数据量：Mfif170615a1——100W； Mfif170617a1——100W；
-- 需要剔重的数据包：Mfif170601a2；Mfif170603a2；Mfif170608a1；Mfif170610a1；
-- 
-- 以上数据包共计4个，需*6月12日*完成取数工作，感谢！
-- 

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-12')
select 'Mfif170615a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='2017-06-12' and b.plancode in ('Mfif170601a2','Mfif170603a2','Mfif170608a1','Mfif170610a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-12')
select 'Mfif170615a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-12')
select 'Mfif170617a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='2017-06-12' and b.plancode in ('Mfif170601a2','Mfif170603a2','Mfif170608a1','Mfif170610a1','Mfif170615a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-12')
select 'Mfif170617a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');
