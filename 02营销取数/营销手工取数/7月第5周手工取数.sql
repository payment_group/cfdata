-- 1、EDM——库存邮箱客户；
-- 
-- 数据量 企划号 
-- 5W Efif170729a1
-- 5W Efif170802a1
-- 
-- EDM需剔除的数据包号：Efif170714a2,Efif170715a1,Efif170719a2；Efif170722a1,Efif170726a1
-- 
-- 2、SMS
-- 
-- 数据量 企划号
-- 20W Sfif170729a1——要求：未申请过开通拿去花的用户；已通过前置模型的用户；3个月内在携程APP有火车票查询记录的用户；
-- 
-- SMS需剔除的数据包号：Sfif170714a1,Sfif170719a1;Sfif170722a1,Sfif170726a1;
-- 
-- 3、MSG——要求：未申请过开通拿去花的用户；已通过前置模型的用户；
-- 
-- 数据量 企划号
-- 100W Mfif170728a1
-- 100W Mfif170729a1
-- 100W Mfif170802a1
-- 
-- MSG数据包需剔除：Mfif170714a1,Mfif170715a1,Mfif170719a1；Mfif170722a1,Mfif170722a2,Mfif170726a1;
--
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 1、EDM——库存邮箱客户；
-- 
-- 数据量 企划号 
-- 5W Efif170729a1
-- 5W Efif170802a1
-- 
-- EDM需剔除的数据包号：Efif170714a2,Efif170715a1,Efif170719a2；Efif170722a1,Efif170726a1
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170729a1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170714a2','Efif170715a1','Efif170719a2','Efif170722a1','Efif170726a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170729a1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170802a1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170714a2','Efif170715a1','Efif170719a2','Efif170722a1','Efif170726a1','Efif170729a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170802a1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- 2、SMS
-- 
-- 数据量 企划号
-- 20W Sfif170729a1——要求：未申请过开通拿去花的用户；已通过前置模型的用户；3个月内在携程APP有火车票查询记录的用户；
-- 
-- SMS需剔除的数据包号：Sfif170714a1,Sfif170719a1;Sfif170722a1,Sfif170726a1;
use tmp_innofin;
create table if not exists query_trn_3m(
	uid string
);
insert overwrite table tmp_innofin.query_trn_3m
select distinct uid from dw_mobdb.factmbpageview a
inner join dim_mobdb.dimmbpagecode b on a.pagecode = b.pagecode
where a.d >='${zdt.addDay(-90).format("yyyy-MM-dd")}' and b.pagename like '%火车票查询%';

insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif170729a1',a.uid ,'','SMS','','FIN', from_unixtime(unix_timestamp(),'yyyy-MM-dd'), from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from
	tmp_innofin.cfbdb_vqq_user_prefix_model a 
	left join ods_innofin.user_contract b on a.uid = b.user_id 
	inner join tmp_innofin.query_trn_3m c on a.uid = c.uid
    left join tmp_innofin.factmarketingsenddetail d on a.uid = d.uid and d.d <='${zdt.format("yyyy-MM-dd")}' and d.plancode in ('Sfif170714a1','Sfif170719a1','Sfif170722a1','Sfif170726a1') 
	where b.user_id is null and d.uid is null
	order by rand()
  	limit 200000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170729a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');


-- 3、MSG——要求：未申请过开通拿去花的用户；已通过前置模型的用户；
-- 
-- 数据量 企划号
-- 100W Mfif170728a1
-- 100W Mfif170729a1
-- 100W Mfif170802a1
-- 
-- MSG数据包需剔除：Mfif170714a1,Mfif170715a1,Mfif170719a1；Mfif170722a1,Mfif170722a2,Mfif170726a1;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170728a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_vqq_user_prefix_model a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170714a1','Mfif170715a1','Mfif170719a1','Mfif170722a1','Mfif170722a2','Mfif170726a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170728a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170729a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_vqq_user_prefix_model a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170714a1','Mfif170715a1','Mfif170719a1','Mfif170722a1','Mfif170722a2','Mfif170726a1','Mfif170728a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170729a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170802a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_vqq_user_prefix_model a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170714a1','Mfif170715a1','Mfif170719a1','Mfif170722a1','Mfif170722a2','Mfif170726a1','Mfif170728a1','Mfif170729a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170802a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

