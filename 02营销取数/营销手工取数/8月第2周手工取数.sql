-- 本次取数，请注意统一批次号的首字母大小写问题，统一采用“大写”；
-- 
-- ——EDM
-- ·取数要求:以往发送过短信但发送失败且未申请开通的用户
-- ·批次号：Efif170811d1 Efif170816d1
-- ·数量：均为5W
-- ·剔除数据包：efif170726a1，efif170728a1，efif170729a1，efif170802a1；efif170803d1；efif170804d1；efif170806d1；efif170809d1；
-- 
-- ——SMS
-- ·取数要求：以往成功发送短信但未点击且未申请开通的用户
-- ·批次号：Sfif170811e1
-- ·数量：30W
-- ·剔除数据包：Sfif170722a1,Sfif170726a1,sfif170729a1,sfif170804e1
-- 
-- ——MSG
-- ·取数要求：以往发送过站内信但未点击且未申请开通的用户
-- ·批次号：Mfif170811f1，Mfif170813f1，Mfif170816f1
-- ·数量：均为100W
-- ·剔除数据包：mfif170728a1，mfif170729a1，mfif170802a1，mfif170803f1，mfif170804f1，mfif170806f1，mfif170809f1
-- 
-- 以上为本批次取数要求，有问题欢迎随时沟通，谢谢~
--
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');
-- 准备
-- 短信发送失败uid
use tmp_innofin;
create table if not exists vqq_sms_failed_uid(
	uid string
);
insert overwrite table tmp_innofin.vqq_sms_failed_uid
select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel in ('SMS') and 
lower(a.scenecode) in (select lower(plancode) from tmp_innofin.tmp_gpush) and ctisendstatus = 'F' and uid is not null and trim(uid) <> '';
-- 短信发送成功uid
use tmp_innofin;
create table if not exists vqq_sms_success_uid(
	uid string
);
insert overwrite table tmp_innofin.vqq_sms_success_uid
select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel in ('SMS') and 
lower(a.scenecode) in (select lower(plancode) from tmp_innofin.tmp_gpush) and ctisendstatus = 'T' and uid is not null and trim(uid) <> '';

-- ——EDM
-- ·取数要求:以往发送过短信但发送失败且未申请开通的用户
-- ·批次号：Efif170811d1 Efif170816d1
-- ·数量：均为5W
-- ·剔除数据包：efif170726a1，efif170728a1，efif170729a1，efif170802a1；efif170803d1；efif170804d1；efif170806d1；efif170809d1；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170811d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and lower(b.plancode) in ('efif170726a1','efif170728a1','efif170729a1','efif170802a1','efif170803d1','efif170804d1','efif170806d1','efif170809d1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  inner join tmp_innofin.vqq_sms_failed_uid d on a.uid = d.uid
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170811d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170816d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and lower(b.plancode) in ('efif170726a1','efif170728a1','efif170729a1','efif170802a1','efif170803d1','efif170804d1','efif170806d1','efif170809d1','efif170811d1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id
  inner join tmp_innofin.vqq_sms_failed_uid d on a.uid = d.uid
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170816d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ——SMS
-- ·取数要求：以往成功发送短信但未点击且未申请开通的用户(未点击无法实现)
-- ·批次号：Sfif170811e1
-- ·数量：30W
-- ·剔除数据包：Sfif170722a1,Sfif170726a1,sfif170729a1,sfif170804e1
use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif170811e1',a.uid ,'','SMS','','FIN', from_unixtime(unix_timestamp(),'yyyy-MM-dd'), from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from
	tmp_innofin.cfbdb_vqq_user_prefix_model a 
	left join ods_innofin.user_contract b on a.uid = b.user_id 
    left join tmp_innofin.factmarketingsenddetail d on a.uid = d.uid and d.d <='${zdt.format("yyyy-MM-dd")}' and lower(d.plancode) in ('sfif170722a1','sfif170726a1','sfif170729a1','sfif170804e1') 
	inner join tmp_innofin.vqq_sms_success_uid c on a.uid = c.uid
	where b.user_id is null and d.uid is null
	order by rand()
  	limit 300000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170811e1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ——MSG
-- ·取数要求：以往发送过站内信但未点击且未申请开通的用户(未点击无法实现)
-- ·批次号：Mfif170811f1，Mfif170813f1，Mfif170816f1
-- ·数量：均为100W
-- ·剔除数据包：mfif170728a1，mfif170729a1，mfif170802a1，mfif170803f1，mfif170804f1，mfif170806f1，mfif170809f1
-- 
-- 以上为本批次取数要求，有问题欢迎随时沟通，谢谢~

-- 站内信发送成功uid
use tmp_innofin;
create table if not exists vqq_mim_success_uid(
	uid string
);
insert overwrite table tmp_innofin.vqq_mim_success_uid
select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel in ('MIM') and 
lower(a.scenecode) in (select lower(plancode) from tmp_innofin.tmp_gpush) and ctisendstatus = 'T' and uid is not null and trim(uid) <> '';

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170811f1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and lower(b.plancode) in ('mfif170728a1','mfif170729a1','mfif170802a1','mfif170803f1','mfif170804f1','mfif170806f1','mfif170809f1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  inner join tmp_innofin.vqq_mim_success_uid d on a.uid = d.uid
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170811f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170813f1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and lower(b.plancode) in ('mfif170728a1','mfif170729a1','mfif170802a1','mfif170803f1','mfif170804f1','mfif170806f1','mfif170809f1','mfif170811f1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  inner join tmp_innofin.vqq_mim_success_uid d on a.uid = d.uid
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170813f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170816f1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and lower(b.plancode) in ('mfif170728a1','mfif170729a1','mfif170802a1','mfif170803f1','mfif170804f1','mfif170806f1','mfif170809f1','mfif170811f1','mfif170813f1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  inner join tmp_innofin.vqq_mim_success_uid d on a.uid = d.uid
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170816f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

--trace
insert into table tmp_innofin.tmp_gpush select 'Efif170811d1','2017-08-11','2017-09-03';
insert into table tmp_innofin.tmp_gpush select 'Efif170816d1','2017-08-16','2017-09-04';
insert into table tmp_innofin.tmp_gpush select 'Sfif170811e1','2017-08-11','2017-09-04';
insert into table tmp_innofin.tmp_gpush select 'Mfif170811f1','2017-08-11','2017-09-03';
insert into table tmp_innofin.tmp_gpush select 'Mfif170813f1','2017-08-13','2017-09-04';
insert into table tmp_innofin.tmp_gpush select 'Mfif170816f1','2017-08-16','2017-09-06';
