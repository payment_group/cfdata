-- preapare
use tmp_innofin;
CREATE TABLE nqh_user_hasorder(
	uid string,
	activedate string,
	lastordertime string,
	totalorder int,
	totalamount double
);
insert overwrite table tmp_innofin.nqh_user_hasorder
select a.user_id,a.update_time,b.lastbilltime,b.billcount,b.amount 
from ods_innofin.user_contract a
inner join (
	select uid ,max(billtime) as lastbilltime ,count(1) as billcount,sum(totalamount) as amount 
		from ods_innofin.fact_loan_order
	group by uid) b
 on lower(a.user_id) = lower(b.uid)
where a.contract_status=1; 

-- 
use tmp_innofin;
CREATE TABLE nqh_user_noorder(
	uid string,
	activedate string
);
insert overwrite table tmp_innofin.nqh_user_noorder
select a.user_id,a.update_time 
from ods_innofin.user_contract a
left join (select distinct uid as uid from ods_innofin.fact_loan_order) c
 on lower(a.user_id) = lower(c.uid)
where a.contract_status=1 
and c.uid is null;

-- data table
use tmp_innofin;
CREATE TABLE factmarketingsenddetail(
	plancode string, -- 数据包号
	uid string, -- 用户ID
	username string, -- 用户姓名
	channeltype string, -- 渠道类型
	value string, -- 值 
	businesstype string, -- 业务类型
	createtime string, -- 创建时间
	updatetime string -- 更改时间
) PARTITIONED BY ( 
	d string -- 数据日期
);
alter table tmp_innofin.factmarketingsenddetail add partition (d='2017-04-01');

insert table tmp_innofin.factmarketingsenddetail partition(d='2017-04-01')
select 'Efif170406a1',a.user_id,'','EDM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from ods_innofin.user_contract  a
where a.contract_status=1

insert table tmp_innofin.factmarketingsenddetail partition(d='2017-04-01')
select 'Sfif170406a1',a.user_id,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from ods_innofin.user_contract  a
where a.contract_status=1

-- 配置表
use tmp_innofin;
drop table tmp_innofin.factmarketingconfiguredetail;
CREATE TABLE factmarketingconfiguredetail (
	plancode string, -- 数据包号
	channeltype string, -- 渠道类型
	businesstype string, -- 业务类型
	isrejcorpuser tinyint, -- 是否剔除商旅用户
	isrejagentuser tinyint, -- 是否剔除代理用户
	isrejunsubuser tinyint, -- 是否剔除退订用户
	isrejinteruser tinyint, -- 是否剔除国际网站用户
	isrejblackuser tinyint, -- 是否剔除黑名单用户
	isrejtopuser tinyint, -- 是否剔除顶级用户
	isrejbossuser tinyint, -- 是否剔除BOSS用户
	isrejxmsuser tinyint, -- 是否剔除小秘书用户
	createtime string, -- 创建时间
	updatetime string -- 更改时间
) PARTITIONED BY ( 
	d string -- 数据日期
);
alter table tmp_innofin.factmarketingconfiguredetail add partition (d='2017-04-01');

use tmp_innofin;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-04-01')
values('Efif170406a1','EDM','FIN',
0,1,1,0,1,1,0,0,
'2017-04-01',
'2017-04-01');

use tmp_innofin;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-04-01')
values('Sfif170406a1','SMS','FIN',
0,1,1,0,1,1,0,0,
'2017-04-01',
'2017-04-01');


