--  描述
--  
--  1、EDM——新一轮提取出邮箱的客户；
--  
--  数据量 企划号 
--  50W Efif170707a2
--  5W Efif170708a2
--  5W Efif170712a2
--  
--  需剔除的数据包号：Efif170622a1,Efif170623a1,Efif170628a1,Efif170701a1,Efif170705a1
--  
--  2、SMS——要求：未申请过开通拿去花的用户；已通过前置模型的用户；6个月内在携程APP有交易的用户；
--  
--  数据量 企划号
--  20W Sfif170708a1
--  
--  需剔除的数据包号：Sfif170622a1,Sfif170624a1,Sfif170629a1,Sfif170701a1,
--  
--  3、MSG——要求：未申请过开通拿去花的用户；已通过前置模型的用户；
--  
--  数据量 企划号
--  100W Mfif170707a1
--  100W Mfif170708a1
--  100W Mfif170712a1
--  
--  补充一点：MSG数据包需剔除：Mfif170622a1,Mfif170624a1,Mfif170629a1,Mfif170701a1,Mfif170705a1

-- 1) 近6个月内在携程有过订单的用户（使用order_index数据）
-- 已有表

--
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

--  1、EDM——新一轮提取出邮箱的客户；
--  
--  数据量 企划号 
--  50W Efif170707a2 --- 走魔方
--  5W Efif170708a2
--  5W Efif170712a2
--  
--  需剔除的数据包号：Efif170622a1,Efif170623a1,Efif170628a1,Efif170701a1,Efif170705a1--
insert overwrite table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170708a2',a.uid ,'','EDM',mail_plain,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170622a1','Efif170623a1','Efif170628a1','Efif170701a1','Efif170705a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain not like '%,%' and a.mail_plain REGEXP '^.+@.+$' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
--  mail check: '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'

insert overwrite table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170708a2','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170712a2',a.uid ,'','EDM',mail_plain,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170622a1','Efif170623a1','Efif170628a1','Efif170701a1','Efif170705a1','Efif170708a2') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain not like '%,%' and a.mail_plain REGEXP '^.+@.+$' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170712a2','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

--  2、SMS——要求：未申请过开通拿去花的用户；已通过前置模型的用户；6个月内在携程APP有交易的用户；
--  
--  数据量 企划号
--  20W Sfif170708a1
--  
--  需剔除的数据包号：Sfif170622a1,Sfif170624a1,Sfif170629a1,Sfif170701a1,

insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif170708a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model a
  inner join (select distinct uid from tmp_innofin.ctrip_order_index) e on lower(a.uid) = lower(trim(e.uid))
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Sfif170629a1','Sfif170701a1','Sfif170622a1','Sfif170624a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 200000
) e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif170708a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

--  3、MSG——要求：未申请过开通拿去花的用户；已通过前置模型的用户；
--  
--  数据量 企划号
--  100W Mfif170707a1
--  100W Mfif170708a1
--  100W Mfif170712a1
--  
--  补充一点：MSG数据包需剔除：Mfif170622a1,Mfif170624a1,Mfif170629a1,Mfif170701a1,Mfif170705a1

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170707a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170629a1','Mfif170701a1','Mfif170705a1','Mfif170622a1','Mfif170624a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170707a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170708a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170629a1','Mfif170701a1','Mfif170705a1','Mfif170622a1','Mfif170624a1','Mfif170707a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170708a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170712a1',uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid = b.uid and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Mfif170629a1','Mfif170701a1','Mfif170705a1','Mfif170622a1','Mfif170624a1','Mfif170707a1','Mfif170708a1') 
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170712a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

