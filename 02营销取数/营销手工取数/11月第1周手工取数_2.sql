-- EDM——均为5W——取数条件：6/7/8/9四个月内发送过但未打开过EDM的用户；剔除10月已发送批次；
-- Efif171105d1
-- 
-- MSG——均为50W——取数条件：6/7/8/9四个月内发送过但未打开过MSG的用户；剔除10月已发送批次；
-- Mfif171105f1


use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 1、EDM
-- EDM——均为5W——取数条件：6/7/8/9四个月内发送过但未打开过EDM的用户；剔除10月已发送批次；
-- Efif171105d1
-- 6,7,8,9数量不够 条件去除and substring(scenecode,5,4) in ('1706','1707','1708','1709')
-- 已打开条件去除
-- 排除机票优惠券用户
-- 数据不够，去除发送成功条件
-- 数据不够，改用未发送过的邮箱数据


insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171105d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	left join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM') b on lower(a.uid) = lower(b.uid)
	left join tmp_innofin.vqq_flt_curpon_1029 c on lower(a.uid) = lower(c.uid)
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and d.user_id is null and c.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171105d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

-- 3、MSG
-- MSG——均为50W——取数条件：6/7/8/9四个月内发送过但未打开过MSG的用户；剔除10月已发送批次；
-- Mfif171105f1

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171105f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,5,4) in ('1706','1707','1708','1709') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d < '${zdt.format("yyyy-MM-dd")}' and channeltype = 'MIM' and (substring(plancode,5,4) = '1710' or plancode = 'Mfif171101f1' )) d on lower(a.uid) = lower(d.uid)  
  left join tmp_innofin.vqq_flt_curpon_1029 e on lower(a.uid) = lower(e.uid)
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null and e.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171105f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

--trace
insert into table tmp_innofin.tmp_gpush values
('Efif171105d1','2017-11-05','2017-11-15'),
('Mfif171105f1','2017-11-05','2017-11-15');