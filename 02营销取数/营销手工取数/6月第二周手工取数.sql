-- 在魔方数据的拿去花标签开发完毕之前，群分消息推送需要手工取数支持。
-- 
-- 【需要额外注意数据的剔重】问题：
-- 1、同一数据包内，同一手机号不得重复出现；
-- 2、15天内，同一渠道数据包内，用户不得重复出现，需剔重；
-- 
-- 6月第2周手工取数需求如下：
-- 
-- 

-- 以上数据包共计4个，需6月5日完成取数工作，感谢！
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='2017-06-05');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d='2017-06-05');

-- ·短信：
-- 取数条件：选取“前置模型实名绑卡”用户；
-- 企划号及数据量：Sfif170608a1——40W； Sfif170610a1——20W；
-- 需要剔重的数据包：Sfif170601a2；Sfif170603a2；Sfif170525a1；Sfif170527a1；

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-05')
select 'Sfif170608a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-06-05' and
  b.plancode in ('Sfif170601a2','Sfif170603a2','Sfif170525a1','Sfif170527a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 400000
) e;

insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-05')
select 'Sfif170608a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-05')
select 'Sfif170610a1',uid ,'','SMS','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname_card a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-06-05' and 
  b.plancode in ('Sfif170601a2','Sfif170603a2','Sfif170525a1','Sfif170527a1','Sfif170608a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 200000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-05')
select 'Sfif170610a1','SMS','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·站内信：
-- 取数条件：选取“前置模型实名”用户；
-- 企划号及数据量：Mfif170608a1——100W； Mfif170610a1——100W；
-- 需要剔重的数据包：Mfif170601a2；Mfif170603a2；Mfif170525a1；Mfif170527a1；

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-05')
select 'Mfif170608a1',uid ,'','MIM',e.uid,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_prefix_model_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-06-05' and 
  b.plancode in ('Mfif170601a2','Mfif170603a2','Mfif170525a1','Mfif170527a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 1000000
)e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-05')
select 'Mfif170608a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d='2017-06-05')
select 'Mfif170610a1',e.uid ,'','MIM',e.uid,'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select a.uid from tmp_innofin.cfbdb_user_relname a
  left join tmp_innofin.factmarketingsenddetail b on a.uid=b.uid and b.d <='2017-06-05' and 
  b.plancode in ('Mfif170601a2','Mfif170603a2','Mfif170525a1','Mfif170527a1','Mfif170608a1') where b.uid is null and a.uid is not null and trim(a.uid) <> ''
  limit 1000000
) e;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d='2017-06-05')
select 'Mfif170610a1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');




