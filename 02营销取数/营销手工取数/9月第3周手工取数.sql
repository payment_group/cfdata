-- 第3周取数事宜9月19/24日群分消息推广需要数据取数支持，请于周三下班前完成取数工作~
--
--·Efif170919d1、Efif170924d1——均为5W——存货；
--·Mfif170919f1、Mfif170924f1——均为50W——之前两个月发送成功但未点击的用户；
--
--注意：
--1、剔除已申请开通用户；
--2、EDM剔除efif170905d1、Efif170910d1、efif170912d1、Efif170917d1；
--3、MSG剔除mfif170905f1、mfif170910f1、mfif170912f1、mfif170917f1；
-- 
-- --
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 1、EDM——库存邮箱客户；
-- 
--·Efif170919d1、Efif170924d1——均为5W——存货；
--2、EDM剔除efif170905d1、Efif170910d1、efif170912d1、Efif170917d1；


insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170919d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on lower(a.uid) = lower(b.uid) and b.d <='${zdt.format("yyyy-MM-dd")}' and lower(b.plancode) in ('efif170905d1','efif170910d1','efif170912d1','efif170917d1') 
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170919d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170924d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on lower(a.uid) = lower(b.uid) and b.d <='${zdt.format("yyyy-MM-dd")}' and lower(b.plancode) in ('efif170905d1','efif170910d1','efif170912d1','efif170917d1','efif170919d1') 
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170924d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·MSG：
--·Mfif170919f1、Mfif170924f1——均为50W——之前两个月发送成功但未点击的用户；
--3、MSG剔除mfif170905f1、mfif170910f1、mfif170912f1、mfif170917f1；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170919f1',a.uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,7,2) in ('07','08') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d >= '${zdt.addDay(60).format("yyyy-MM-dd")}') b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  left join tmp_innofin.factmarketingsenddetail d on lower(a.uid) = lower(d.uid) and d.d <='${zdt.format("yyyy-MM-dd")}' and lower(d.plancode) in ('mfif170905f1','mfif170910f1','mfif170912f1','mfif170917f1')
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170919f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');


insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170924f1', a.uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,7,2) in ('07','08') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 where d >= '${zdt.addDay(60).format("yyyy-MM-dd")}') b on lower(a.uid) = lower(b.uid)    
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  left join tmp_innofin.factmarketingsenddetail d on lower(a.uid) = lower(d.uid) and d.d <='${zdt.format("yyyy-MM-dd")}' and lower(d.plancode) in ('mfif170905f1','mfif170910f1','mfif170912f1','mfif170917f1','mfif170919f1')
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 500000
;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170924f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');


--trace
insert into table tmp_innofin.tmp_gpush select 'Efif170919d1','2017-09-19','2017-10-09';
insert into table tmp_innofin.tmp_gpush select 'Efif170924d1','2017-09-24','2017-10-14';
insert into table tmp_innofin.tmp_gpush select 'Mfif170919f1','2017-09-19','2017-10-09';
insert into table tmp_innofin.tmp_gpush select 'Mfif170924f1','2017-09-24','2017-10-14';
