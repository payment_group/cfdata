-- 9月12日及9月17日拟推送EDM及MSG，需要在本周四配合完成取数工作，具体批次及取数需求如下：
-- 
-- ·Efif170912d1、Efif170917d1——均为5W——存货；剔除efif170905d1、efif170910d1；
-- ·Mfif170912f1、Mfif170917f1——均为50W——之前两个月发送成功但未点击的用户；剔除mfif170905f1、mfif170910f1；
-- 
-- 
-- --
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 1、EDM——库存邮箱客户；
-- 
-- ·Efif170912d1、Efif170917d1——均为5W——存货；剔除efif170905d1、efif170910d1；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170912d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on lower(a.uid) = lower(b.uid) and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170905d1','Efif170910d1') 
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170912d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170917d1',a.uid ,'','EDM', mail_plain, 'FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from tmp_innofin.cfbdb_user_prefix_model_mail a
  left join tmp_innofin.factmarketingsenddetail b on lower(a.uid) = lower(b.uid) and b.d <='${zdt.format("yyyy-MM-dd")}' and b.plancode in ('Efif170905d1','Efif170910d1','Efif170912d1') 
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  where a.d<= '${zdt.format("yyyy-MM-dd")}' and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and b.uid is null and c.user_id is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif170917d1','EDM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');

-- ·MSG：
-- ·Mfif170912f1、Mfif170917f1——均为50W——之前两个月发送成功但未点击的用户；剔除mfif170905f1、mfif170910f1；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170912f1',a.uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,7,2) in ('07','08') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d >= '${zdt.addDay(60).format("yyyy-MM-dd")}') b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  left join tmp_innofin.factmarketingsenddetail d on lower(a.uid) = lower(d.uid) and d.d <='${zdt.format("yyyy-MM-dd")}' and d.plancode in ('Mfif170905f1','Mfif170910f1')
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170912f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');


insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170917f1', a.uid ,'','MIM','','FIN',
from_unixtime(unix_timestamp(),'yyyy-MM-dd'),
from_unixtime(unix_timestamp(),'yyyy-MM-dd')
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,7,2) in ('07','08') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 where d >= '${zdt.addDay(60).format("yyyy-MM-dd")}') b on lower(a.uid) = lower(b.uid)    
  left join ods_innofin.user_contract c on lower(a.uid) = lower(c.user_id) 
  left join tmp_innofin.factmarketingsenddetail d on lower(a.uid) = lower(d.uid) and d.d <='${zdt.format("yyyy-MM-dd")}' and d.plancode in ('Mfif170905f1','Mfif170910f1','Mfif170912f1')
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> ''
  Order by rand()
  limit 500000
;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif170917f1','MIM','FIN',0,1,1,0,1,1,0,0,from_unixtime(unix_timestamp(),'yyyy-MM-dd'),from_unixtime(unix_timestamp(),'yyyy-MM-dd');


--trace
insert into table tmp_innofin.tmp_gpush select 'Efif170912d1','2017-09-12','2017-09-22';
insert into table tmp_innofin.tmp_gpush select 'Efif170917d1','2017-09-17','2017-09-27';
insert into table tmp_innofin.tmp_gpush select 'Mfif170912f1','2017-09-12','2017-09-22';
insert into table tmp_innofin.tmp_gpush select 'Mfif170917f1','2017-09-17','2017-09-27';
