-- 1 7天uv数据明细
use tmp_innofin;
drop table if exists uv_7d;
create table if not exists uv_7d(
	pagecode string,
	uid string
);

insert overwrite table tmp_innofin.uv_7d
select distinct pagecode,uid from dw_mobdb.factmbpageview where d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}';

-- 2 前1000uv page
use tmp_innofin;
drop table uv_count_max_1000;
create table if not exists uv_count_max_1000(
	pagecode string,
	uvcount int
);
insert overwrite table tmp_innofin.uv_count_max_1000
select pagecode,count(uid) as uvcount from tmp_innofin.uv_7d group by pagecode order by uvcount desc limit 1000;

-- 3 前1000页面中的prefix_model uid
use tmp_innofin;
drop table prefix_model_uv_7d;
create table if not exists prefix_model_uv_7d(
	pagecode string,
	uid int
);
insert overwrite table tmp_innofin.prefix_model_uv_7d
select distinct a.pagecode,a.uid from tmp_innofin.uv_7d a
inner join tmp_innofin.uv_count_max_1000 d on a.pagecode = d.pagecode
inner join tmp_innofin.cfbdb_vqq_user_prefix_model b on a.uid = b.uid
left join ods_innofin.user_contract c on a.uid = c.user_id
where c.user_id is null;

use tmp_innofin;
create table if not exists prefix_model_uv_7d_dis(
	pagecode string,
	uid int
);
insert overwrite table tmp_innofin.prefix_model_uv_7d_dis
select distinct a.pagecode,a.uid from tmp_innofin.prefix_model_uv_7d a;

-- 取页面名称
use tmp_innofin;
drop table uv_count_result_7d;
create table if not exists uv_count_result_7d(
	pagecode string,
	pagename string,
	uvcount int,
	prefix_model_uvcount int
);
insert overwrite table tmp_innofin.uv_count_result_7d
select a.pagecode,c.pagename,a.uvcount,b.prefix_model_uvcount from 
	tmp_innofin.uv_count_max_1000 a
	inner join (select pagecode,count(uid) as prefix_model_uvcount from tmp_innofin.prefix_model_uv_7d_dis group by pagecode) b on a.pagecode = b.pagecode
	inner join dim_mobdb.dimmbpagecode c on a.pagecode = c.pagecode;


