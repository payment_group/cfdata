
use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif171212d1',a.uid ,'','SMS','','FIN', CURRENT_DATE,CURRENT_DATE
from vqq_temp_1211 a;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif171212d1','SMS','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;


--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Sfif171212d1','2017-12-12','2017-12-22');