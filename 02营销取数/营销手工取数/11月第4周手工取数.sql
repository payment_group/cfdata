-- EDM取数条件：26与29批次，请取火车票活跃用户中符合拿去花开通条件的用户；
-- Efif171126d1——5W
-- Efif171129d1——5W
-- 站内信：请取酒店活跃用户中符合拿去花开通条件的用户（可通过前置模型，且已实名）
-- Mfif171126f1——50W
-- Mfif171129f1——50W
-- 【以上四个批次，可在明天下班前取完】


--use tmp_innofin;
--create table vqq_tmp_active_user(
	--uid string
--)partitioned by (bu string);

insert overwrite table tmp_innofin.vqq_tmp_active_user partition (bu='trn')
select distinct uid from dw_mobdb.factmbpageview a 
inner join (select pagecode from dim_mobdb.dimmbpagecode where pagename like '%查询页%' and (categoryname like '%火车票%')) b on a.pagecode = b.pagecode 
where a.d between add_months(current_date,-6) and current_date;


use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- EDM取数条件：26与29批次，请取火车票活跃用户中符合拿去花开通条件的用户；
-- Efif171126d1——5W
-- Efif171129d1——5W

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171126d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	inner join vqq_tmp_active_user b on lower(a.uid) = lower(b.uid) and b.bu='trn'
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d<='${zdt.format("yyyy-MM-dd")}' and plancode in ('Efif171119d1','Efif171122d1')) e on lower(a.uid) = lower(e.uid)
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and d.user_id is null 
  and e.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171126d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171129d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	inner join vqq_tmp_active_user b on lower(a.uid) = lower(b.uid) and b.bu='trn'
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d<='${zdt.format("yyyy-MM-dd")}' and plancode in ('Efif171119d1','Efif171122d1','Efif171126d1')) e on lower(a.uid) = lower(e.uid)
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and d.user_id is null 
  and e.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171129d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;


-- 站内信：请取酒店活跃用户中符合拿去花开通条件的用户（可通过前置模型，且已实名）
-- Mfif171126f1——50W
-- Mfif171129f1——50W
use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171126f1',a.uid ,'','MIM','','FIN',current_date,current_date
from vqq_tmp_active_user a	
  inner join (select distinct uid from cfbdb_vqq_user_prefix_model) b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d<='${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171119f1')) e on lower(a.uid) = lower(e.uid)
  where a.bu='htl' and c.user_id is null
  and e.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171126f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171129f1',a.uid ,'','MIM','','FIN',current_date,current_date
from vqq_tmp_active_user a	
  inner join (select distinct uid from cfbdb_vqq_user_prefix_model) b on lower(a.uid) = lower(b.uid)
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail where d<='${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171119f1','Mfif171126f1')) e on lower(a.uid) = lower(e.uid)
  where a.bu='htl' and c.user_id is null
  and e.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171129f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Efif171126d1','2017-11-26','2017-12-06'),
('Efif171129d1','2017-11-29','2017-12-09'),
('Mfif171126f1','2017-11-26','2017-12-06'),
('Mfif171129f1','2017-11-29','2017-12-09');