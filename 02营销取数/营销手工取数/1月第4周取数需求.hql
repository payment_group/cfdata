--取数条件：
--1、已通过前置模型；
--2、近6个月内到访过拿去花开通页但未走完开通流程；
--3、取数数量：均为100W，共计400W
--
--批次号如下：
--Mfif180123f1
--Mfif180125f1
--Mfif180127f1
--Mfif180129f1
--
--需剔除批次号：
--Mfif180109f1；Mfif180111f1；Mfif180113f1；Mfif180116f1；Mfif180118f1；Mfif180120f1

insert overwrite table tmp_innofin.vqq_mkt_hermes partition (dt='2018-01-23')
select 'Mfif180123f1',a.uid ,'','','','','','2018-01-23 00:00:00'
from tmp_innofin.cfbdb_vqq_user_prefix_model a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') c on lower(a.uid) = lower(c.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif180109f1','Mfif180111f1','Mfif180113f1','Mfif180116f1','Mfif180118f1','Mfif180120f1'))d on lower(a.uid) = lower(d.uid)  
  left join (select distinct uid from tmp_innofin.vqq_mkt_hermes where dt between '2018-01-09' and '2018-01-20') e on lower(a.uid) = lower(e.uid)
  where a.uid is not null and trim(a.uid) <> '' and c.uid is null and d.uid is null and e.uid is null
  limit 1000000;

insert overwrite table tmp_innofin.vqq_mkt_hermes partition (dt='2018-01-25')
select 'Mfif180125f1',a.uid ,'','','','','','2018-01-25 00:00:00'
from tmp_innofin.cfbdb_vqq_user_prefix_model a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') c on lower(a.uid) = lower(c.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif180109f1','Mfif180111f1','Mfif180113f1','Mfif180116f1','Mfif180118f1','Mfif180120f1','Mfif180123f1')) d on lower(a.uid) = lower(d.uid)  
  left join (select distinct uid from tmp_innofin.vqq_mkt_hermes where dt between '2018-01-09' and '2018-01-23') e on lower(a.uid) = lower(e.uid)
  where a.uid is not null and trim(a.uid) <> '' and c.uid is null and d.uid is null and e.uid is null
  limit 1000000;

insert overwrite table tmp_innofin.vqq_mkt_hermes partition (dt='2018-01-27')
select 'Mfif180127f1',a.uid ,'','','','','','2018-01-27 00:00:00'
from tmp_innofin.cfbdb_vqq_user_prefix_model a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') c on lower(a.uid) = lower(c.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif180109f1','Mfif180111f1','Mfif180113f1','Mfif180116f1','Mfif180118f1','Mfif180120f1','Mfif180123f1','Mfif180125f1')) d on lower(a.uid) = lower(d.uid)  
  left join (select distinct uid from tmp_innofin.vqq_mkt_hermes where dt between '2018-01-09' and '2018-01-25') e on lower(a.uid) = lower(e.uid)
  where a.uid is not null and trim(a.uid) <> '' and c.uid is null and d.uid is null and e.uid is null
  limit 1000000;

insert overwrite table tmp_innofin.vqq_mkt_hermes partition (dt='2018-01-29')
select 'Mfif180129f1',a.uid ,'','','','','','2018-01-29 00:00:00'
from tmp_innofin.cfbdb_vqq_user_prefix_model a
  left join (select distinct user_id as uid from ods_innofin.user_activate_req  where req_source <> 'PRE_CREDIT_TASK') c on lower(a.uid) = lower(c.uid)  
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d <= '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif180109f1','Mfif180111f1','Mfif180113f1','Mfif180116f1','Mfif180118f1','Mfif180120f1','Mfif180123f1','Mfif180125f1','Mfif180127f1')) d on lower(a.uid) = lower(d.uid)  
  left join (select distinct uid from tmp_innofin.vqq_mkt_hermes where dt between '2018-01-09' and '2018-01-27') e on lower(a.uid) = lower(e.uid)
  where a.uid is not null and trim(a.uid) <> '' and c.uid is null and d.uid is null and e.uid is null
  limit 1000000;


-- yyc

use tmp_innofin;
insert into table vqq_mkt_hermes partition (dt='2018-01-23') values ('Mfif180123f1','_WeChat546894665','','','','','','2018-01-23 14:25:00');
insert into table vqq_mkt_hermes partition (dt='2018-01-25') values ('Mfif180123f1','_WeChat546894665','','','','','','2018-01-25 14:25:00');
insert into table vqq_mkt_hermes partition (dt='2018-01-27') values ('Mfif180123f1','_WeChat546894665','','','','','','2018-01-27 14:25:00');
insert into table vqq_mkt_hermes partition (dt='2018-01-29') values ('Mfif180123f1','_WeChat546894665','','','','','','2018-01-29 14:25:00');

--trace
use tmp_innofin;
insert into table tmp_innofin.tmp_gpush values
('Mfif180123f1','2018-01-23','2018-02-03'),
('Mfif180125f1','2018-01-25','2018-02-05'),
('Mfif180127f1','2018-01-27','2018-02-07'),
('Mfif180129f1','2018-01-29','2018-02-09');