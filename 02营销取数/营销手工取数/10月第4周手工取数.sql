-- 1、EDM
-- Efif171025d1——5W——前3个月内发送成功邮件但未打开的用户
-- Efif171029d1——5W——前3个月内发送成功邮件但未打开的用户
-- Efif171030d1——5W——前3个月内发送成功邮件但未打开的用户
-- 剔除：完成申请流程用户剔除； Efif171008d1、 Efif171011d1、Efif171015d1、Efif171019d1、 Efif171022d1四个批次的用户剔除；25/29及30号三个批次互相剔除；
-- 
-- 2、SMS
-- Sfif171025e1——10W——过去所有推送过短信的用户中，发送成功且未点击的用户；
-- 剔除：完成申请流程用户剔除；电信用户剔除，仅保留移动联通用户；
-- 
-- 3、MSG
-- Mfif171025f1——50W——3个月内发送成功但未打开过MSG的用户
-- Mfif171029f1——50W——3个月内发送成功但未打开过MSG的用户
-- Mfif171030f1——50W——3个月内发送成功但未打开过MSG的用户
-- 剔除：完成申请流程用户剔除； Mfif171008f1、 Mfif171011f1、Mfif171015f1、 Mfif171019f1、Mfif171022f1四个批次的用户剔除；25/29及30号三个批次互相剔除；


use tmp_innofin;
alter table tmp_innofin.factmarketingsenddetail DROP IF EXISTS partition (d='${zdt.format("yyyy-MM-dd")}');
alter table tmp_innofin.factmarketingconfiguredetail DROP IF EXISTS partition (d= '${zdt.format("yyyy-MM-dd")}');

-- 1、EDM
-- Efif171025d1——5W——前3个月内发送成功邮件但未打开的用户
-- Efif171029d1——5W——前3个月内发送成功邮件但未打开的用户
-- Efif171030d1——5W——前3个月内发送成功邮件但未打开的用户
-- 剔除：完成申请流程用户剔除； Efif171008d1、 Efif171011d1、Efif171015d1、Efif171019d1、 Efif171022d1四个批次的用户剔除；25/29及30号三个批次互相剔除；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171025d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	inner join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM' and substring(scenecode,5,4) in ('1707','1708','1709') and ctisendstatus = 'T') b on lower(a.uid) = lower(b.uid)
    left join (select distinct uid from tmp_innofin.wqm_process_basic01 where d > add_months(current_date,-3)) c on lower(b.uid) = lower(c.uid)    
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
  				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Efif171008d1','Efif171011d1','Efif171015d1','Efif171019d1','Efif171022d1')) e on lower(b.uid) = lower(e.uid) 
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and c.uid is null and user_id is null and e.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171025d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171029d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	inner join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM' and substring(scenecode,5,4) in ('1707','1708','1709') and ctisendstatus = 'T') b on lower(a.uid) = lower(b.uid)
    left join (select distinct uid from tmp_innofin.wqm_process_basic01 where d > add_months(current_date,-3)) c on lower(b.uid) = lower(c.uid)    
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
  				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Efif171008d1','Efif171011d1','Efif171015d1','Efif171019d1','Efif171022d1','Efif171025d1')) e on lower(b.uid) = lower(e.uid) 
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and c.uid is null and user_id is null and e.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171029d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171030d1',a.uid ,'','EDM', mail_plain, 'FIN', CURRENT_DATE,CURRENT_DATE
from tmp_innofin.cfbdb_user_prefix_model_mail a
	inner join (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin where sendchannel = 'EDM' and substring(scenecode,5,4) in ('1707','1708','1709') and ctisendstatus = 'T') b on lower(a.uid) = lower(b.uid)
    left join (select distinct uid from tmp_innofin.wqm_process_basic01 where d > add_months(current_date,-3)) c on lower(b.uid) = lower(c.uid)    
	left join ods_innofin.user_contract d on lower(a.uid) = lower(d.user_id)
	left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
  				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Efif171008d1','Efif171011d1','Efif171015d1','Efif171019d1','Efif171022d1','Efif171025d1','Efif171030d1')) e on lower(b.uid) = lower(e.uid) 
  where a.d<= CURRENT_DATE and a.mail_plain REGEXP '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)' 
  and c.uid is null and user_id is null and e.uid is null
  Order by rand()
  limit 50000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Efif171030d1','EDM','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

-- 2、SMS
-- Sfif171025e1——10W——过去所有推送过短信的用户中，发送成功且未点击的用户；
-- 剔除：完成申请流程用户剔除；电信用户剔除，仅保留移动联通用户；

use tmp_innofin;
insert into table tmp_innofin.factmarketingsenddetail partition(d='${zdt.format("yyyy-MM-dd")}')
select 'Sfif171025e1',a.uid ,'','SMS','','FIN', CURRENT_DATE,CURRENT_DATE
from
	(select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'SMS' and ctisendstatus = 'T') a
	left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id) 
	left join (select distinct uid from tmp_innofin.wqm_process_basic01 where d < current_date ) d on lower(a.uid) = lower(d.uid)
	where b.user_id is null and d.uid is null
	order by rand()
  	limit 100000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Sfif171025e1','SMS','FIN',0,1,1,0,1,1,0,0,CURRENT_DATE,CURRENT_DATE;

-- 3、MSG
-- Mfif171025f1——50W——3个月内发送成功但未打开过MSG的用户
-- Mfif171029f1——50W——3个月内发送成功但未打开过MSG的用户
-- Mfif171030f1——50W——3个月内发送成功但未打开过MSG的用户
-- 剔除：完成申请流程用户剔除； Mfif171008f1、 Mfif171011f1、Mfif171015f1、 Mfif171019f1、Mfif171022f1四个批次的用户剔除；25/29及30号三个批次互相剔除；

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171025f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,5,4) in ('1707','1708','1709') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171008f1','Mfif171011f1','Mfif171015f1','Mfif171019f1','Mfif171022f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171025f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171029f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,5,4) in ('1707','1708','1709') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171008f1','Mfif171011f1','Mfif171015f1','Mfif171019f1','Mfif171022f1','Mfif171025f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171029f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;

insert into table tmp_innofin.factmarketingsenddetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171030f1',a.uid ,'','MIM','','FIN',current_date,current_date
from (select distinct uid from Dw_pubsharedb.factmktmembersenddetail_fin a where a.sendchannel = 'MIM' and substring(scenecode,5,4) in ('1707','1708','1709') and ctisendstatus = 'T') a
  left join (select distinct uid from tmp_innofin.wqm_process_basic01 b where d > add_months(current_date,-3)) b on lower(a.uid) = lower(b.uid)  
  left join ods_innofin.user_contract c on a.uid = c.user_id 
  left join (select distinct uid from tmp_innofin.factmarketingsenddetail 
				where d < '${zdt.format("yyyy-MM-dd")}' and plancode in ('Mfif171008f1','Mfif171011f1','Mfif171015f1','Mfif171019f1','Mfif171022f1','Mfif171025f1','Mfif171029f1')) d on lower(a.uid) = lower(d.uid)  
  where b.uid is null and c.user_id is null and a.uid is not null and trim(a.uid) <> '' and d.uid is null
  Order by rand()
  limit 500000;
insert into table tmp_innofin.factmarketingconfiguredetail partition(d= '${zdt.format("yyyy-MM-dd")}')
select 'Mfif171030f1','MIM','FIN',0,1,1,0,1,1,0,0,current_date,current_date;


--trace
insert into table tmp_innofin.tmp_gpush values
('Efif171025d1','2017-10-25','2017-11-05'),
('Efif171029d1','2017-10-29','2017-11-09'),
('Efif171030d1','2017-10-30','2017-11-10'),
('Sfif171025e1','2017-10-25','2017-11-05'),
('Mfif171025f1','2017-10-25','2017-11-05'),
('Mfif171029f1','2017-10-29','2017-11-09'),
('Mfif171030f1','2017-10-30','2017-11-10');
