use cfbdb;

select avg(amount) from  
(select uid from nqh_uid_apl where to_date(time_apl) < add_months(current_date,-1)) b 
inner join (select uid,amount from ctrip_cloud_uid_trnorder_detail_snap where to_date(createtime) >= add_months(current_date,-6) and orderstatus = '10') a
on lower(a.uid) = lower(b.uid)
-- 287.3308149124351

use cfbdb;

select avg(amount) from  
vqq_user_model a 
inner join vqq_user_prefix b on lower(a.uid) = lower(b.uid)
left join nqh_uid_apl c on lower(a.uid) = lower(c.uid)
inner join (select uid,amount from ctrip_cloud_uid_trnorder_detail_snap where to_date(createtime) >= add_months(current_date,-6) and orderstatus = '10') d
on lower(a.uid) = lower(d.uid) 
where c.uid is null;

-- 295.52772589047095

