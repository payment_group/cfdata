--统计4.10-20日授信，激活，用信（限当日激活）用户的实名情况和A卡评分（v3）分布
use tmp_innofin;
create table if not exists vqq_tmp_acard_v3(
	dt string,
	uid string,
	open_id string,
	credit int,
	active int,
	loan int,
	realname int
);

insert overwrite table tmp_innofin.vqq_tmp_acard_v3
select a.dt,
	b.user_id as uid, -- 
	b.open_id,
	1 as credit,
	if(c.uid is null,0,1) as active,
	if(d.uid is null,0,1) as loan,
	if(e.uid is null,1,0) as realname	
from (select distinct user_id,to_date(finish_time) as dt from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap 
		where to_date(finish_time) between '2018-04-10' and '2018-04-20' and org_channel='CTRIP' and product_no='IOUS' and activate_status = 2 and credit_type=1) a
	inner join ods_innofin.user_contract b on a.user_id = b.open_id
	left join ods_innofin.nqh_user_active_date c on lower(b.user_id) = lower(c.uid)
	left join (select distinct uid,to_date(creationdate) as dt from ods_innofin.fact_loan_order where to_date(creationdate) between '2018-04-10' and '2018-04-20') d on lower(b.user_id) = lower(d.uid) and a.dt = d.dt
	left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_detail where dt between '2018-04-10' and '2018-04-20' and log_creditProcess = 0 and log_realName = 0) e on lower(b.user_id) = lower(e.uid) 
;

select dt,sum(credit) as credit,sum(active) as active,sum(active*loan) as loan,sum(active*loan*realname) as realname,sum(active*loan*(1-realname)) as no_realname from  tmp_innofin.vqq_tmp_acard_v3 group by dt;


insert overwrite table tmp_innofin.vqq_nqh_beforeafter_uv partition(dt = '2018-04-23')
select distinct uid,'',0,0,open_id from  tmp_innofin.vqq_tmp_acard_v3;

insert overwrite table tmp_innofin.vqq_nqh_beforeafter_uv partition(dt = '2018-04-24')
select distinct uid,'',0,0,open_id from  tmp_innofin.vqq_tmp_acard_v3;

use tmp_cfbdb;
create table vqq_acard_v3 as (
	uid string,
	open_id string
);
-- import 
use tmp_cfbdb;
create table vqq_acard_v3_1 as 
select a.uid,a.open_id,b.ious_a_card_prob_v3
from vqq_acard_v3 a inner join
cfbdb.ctrip_user_risk_all_to_management b on lower(a.uid) = lower(b.user_id);

-- import tmp_innofin.vqq_acard_v3_1
select a.uid,credit,active,loan,b.ious_a_card_prob_v3 
from tmp_innofin.vqq_tmp_acard_v3 a 
inner join tmp_innofin.vqq_acard_v3_1 b on lower(a.uid) = lower(b.uid);


