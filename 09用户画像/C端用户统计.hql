-- 0-0.2 读秒
-- 实名
use tmp_cfbdb;
drop table if exists vqq_tmp_acard_001;
use tmp_cfbdb;
create table vqq_tmp_acard_001 as 
select distinct c.user_id as uid,ious_a_card_prob_v3 
from cfbdb.tmp_realname_feature_trn_flt_selfbind  a
inner join cfbdb.ctrip_user_risk_all_to_management c on lower(a.uid) = lower(c.user_id)
where ious_a_card_prob_v3 < 0.5;

select count(1) from tmp_cfbdb.vqq_tmp_acard_001 where ious_a_card_prob_v3 < 0.2;
select count(1) from tmp_cfbdb.vqq_tmp_acard_001 where ious_a_card_prob_v3 >= 0.2;


-- 按授信状态分组
use tmp_cfbdb;
drop table vqq_tmp_acard_002;
create table vqq_tmp_acard_002 as 
select a.*, nvl(b.req_status,-1) as req_status,nvl(b.activate_status,-1) as activate_status,nvl(b.tpp_code,'NA') as tpp_code
from tmp_cfbdb.vqq_tmp_acard_001 a
left join (
	select user_id,req_status,activate_status,tpp_code,row_number() over (partition by user_id order by update_time desc) as ln
		from cfbdb.ctrip_tbl_credit_req_snap_t where dt = '2018-04-22' and org_channel = 'CTRIP') b  on lower(a.uid) = lower(b.user_id) and ln = 1

select req_status,count(1) from tmp_cfbdb.vqq_tmp_acard_002 where ious_a_card_prob_v3 < 0.2 group by req_status;
select req_status,count(1) from tmp_cfbdb.vqq_tmp_acard_002 where ious_a_card_prob_v3 >= 0.2 group by req_status;

select activate_status,count(1) from tmp_cfbdb.vqq_tmp_acard_002 where ious_a_card_prob_v3 < 0.2 and req_status = 1 group by activate_status;
select activate_status,count(1) from tmp_cfbdb.vqq_tmp_acard_002 where ious_a_card_prob_v3 >= 0.2 and req_status = 1 group by activate_status;

select tpp_code,count(1) from tmp_cfbdb.vqq_tmp_acard_002 where ious_a_card_prob_v3 < 0.2 and req_status = 1 and avtive_status = 2 group by tpp_code;
select tpp_code,count(1) from tmp_cfbdb.vqq_tmp_acard_002 where ious_a_card_prob_v3 >= 0.2 and req_status = 1 and avtive_status = 2 group by tpp_code;
-- 

select count(a.user_id) from 
(select distinct user_id from fin_basic_data.q_xj_credit_credit_user_credit_record where org_channel = 'CTRIP') a 
left join (select distinct user_id from fin_basic_data.c_xj_credit_rads_tbl_decide_pre_result) b on a.user_id = b.user_id
where b.user_id is null;