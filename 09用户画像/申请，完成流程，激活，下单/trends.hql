
use tmp_innofin;
drop table if exists vqq_tmp_user_profile_trends;
create table if not exists vqq_tmp_user_profile_trends(
	tpos int,
	tname string
)partitioned by (
	trends string	
);
insert overwrite table vqq_tmp_user_profile_trends partition (trends = 'flight_cabin_trend') values
(0,'普通舱'),
(1,'头等舱'),
(2,'商务舱');

insert overwrite table vqq_tmp_user_profile_trends partition (trends = 'hotel_area_trend')values
(0,'国外'),
(1,'国内一线'),
(2,'国内二线'),
(3,'国内滨海城市'),
(4,'国内热门旅游城市');

insert overwrite table vqq_tmp_user_profile_trends partition (trends = 'hotel_band_trend')values
(0,'经济'),
(1,'舒适'),
(2,'高档'),
(3,'豪华');


insert overwrite table vqq_tmp_user_profile_trends partition (trends = 'train_cabin_trend')values
(0,'硬座'),
(1,'一等座'),
(2,'二等座'),
(3,'商务'),
(4,'硬卧'),
(5,'软卧'),
(6,'高软');

insert overwrite table vqq_tmp_user_profile_trends partition (trends = 'holiday_city_tendency')values
(0,'海外'),
(1,'滨海'),
(2,'人文城市'),
(3,'热门旅游城市');

