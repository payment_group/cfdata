use tmp_innofin;
-- vqq_nqh_beforeafter_uv 
-- 用以前的表，避免申请

-- col_name	data_type	comment
-- uid	string	
-- active_date	string	
-- uv_date_count_before	int	
-- uv_date_count_after	int	
-- bu	string	
-- dt	string	
-- # Partition Information		
-- # col_name	data_type	comment
-- dt	string

-- 申请的、流程走完的、审核通过的和下单的用户，都需要的哈 

insert overwrite table tmp_innofin.vqq_nqh_beforeafter_uv partition(dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct uid,'',0,0,'button_apply' from tmp_innofin.vqq_rpt_active_process_detail where (dt >= add_months(current_date,-6)) and button_apply = 1;

insert into table tmp_innofin.vqq_nqh_beforeafter_uv partition(dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct uid,'',0,0,'db_active' from tmp_innofin.vqq_rpt_active_process_detail where (dt >= add_months(current_date,-6)) and db_active = 1;

insert into table tmp_innofin.vqq_nqh_beforeafter_uv partition(dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct uid,'',0,0,'db_success' from tmp_innofin.vqq_rpt_active_process_detail where (dt >= add_months(current_date,-6)) and db_success = 1;

insert into table tmp_innofin.vqq_nqh_beforeafter_uv partition(dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct uid,'',0,0,'loan_order' from ods_innofin.fact_loan_order where (to_date(creationdate) >= add_months(current_date,-6));

insert into table tmp_innofin.vqq_nqh_beforeafter_uv partition(dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct a.uid,'',0,0,
	(case when b.merchantname in ('国内机票','国际机票') then '机票'
		  when b.merchantname = ('火车票') then '火车票'
		  when b.merchantname in ('国内酒店','海外酒店') then '酒店'
		  when b.merchantname = ('团队游') then '度假'
		  when b.merchantname in ('团购糯米','酒店团购') then '团购'
		  else '其他' end) as bu
 from ods_innofin.fact_loan_order a
left join dim_paydb.dimmerchant b on a.merchantid = b.merchantid
 where (to_date(creationdate)='${zdt.addDay(-2).format("yyyy-MM-dd")}');


use tmp_cfbdb;
drop table if exists vqq_tmp_user_profile_1;
create table if not exists vqq_tmp_user_profile_1(
	uid string,
	user_name	string	comment '用户姓名',
	gender	int	comment '性别',
	birthday	string	comment '出生日期',
	age int,
	native_city	string	comment '籍贯',
	train_power	double	comment '火车票消费能力',
	train_price_sensitive	double	comment '火车票价格敏感度',
	train_area_trend	string	comment '火车票地域倾向暂时做不了,需要从日志表里获取',
	train_price_per_km	double	comment '火车票每公里价格',
	train_cabin_trend	string	comment '火车票席位倾向[-1,硬座:p1、一等座:p2、二等座:p3、商务:p4、硬卧:p5、软卧:p6、高软:p7]',
	flight_power	double	comment '机票消费能力',
	flight_price_sensitive	double	comment '机票价格敏感度',
	flight_area_trend	string	comment '机票地域倾向[小型机场:中型机场:大型机场 = p1 : p2 : p3]',
	flight_price_per_km	double	comment '机票每公里价格',
	flight_cabin_trend	string	comment '机票仓位倾向[普通舱:头等舱:商务舱 = p1 : p2 ： p3]',
	flight_avg_distance	double	comment '机票单次平均距离',
	hotel_power	double	comment '酒店消费能力',
	hotel_price_sensitive	double	comment '酒店价格敏感度',
	hotel_area_trend	string	comment '酒店地域倾向[国外,国内一线,国内二线,国内滨海城市,国内热门旅游城市]',
	hotel_price_per_night	double	comment '酒店消费平均间夜价格',
	hotel_band_trend	string	comment '酒店品牌倾向[-1,经济:p1、舒适:p2、高档:p3、豪华:p4]',
	family_prob	double	comment '家庭账号概率',
	credit_max_amount	double	comment '信用卡最大金额',
	holiday_avg_count	int	comment '度假平均人数',
	work_city	string	comment '工作城市',
	app_freq	double	comment 'app粘度',
	login_freq	double	comment '登录账号频率,半衰期90天',
	total_sensitive	double	comment '价格敏感度',
	whole_power	double	comment '整体消费能力',
	consumer_stability	double	comment '消费稳定性',
	internation_prob	double	comment '国际用户概率',
	pay_type	string	comment '支付倾向,最常使用的前三种支付方式分别占比',
	vacation_day_desire	double	comment '节假日消费意愿',
	holiday_power	double	comment '度假消费能力',
	holiday_sensitive	double	comment '度假价格敏感度',
	holiday_city_tendency	string	comment '度假地域倾向[海外，滨海，人文城市，热门旅游城市]',
	plan_prob	double	comment '出行规划完备度',
	cancel_ratio	double	comment '取消订单占比',
	order_platform	string	comment '下单平台倾向',
	refund_ratio	double	comment '退款订单占比',
	login_period	string	comment '登录账号时间段偏好[早上,中午,下午,晚上,凌晨 = p1,p2,...]',
	rest_regular	double	comment '作息规律度',
	friend_prob	double	comment '好友影响度'
);

insert overwrite table tmp_cfbdb.vqq_tmp_user_profile_1
select a.uid,
	user_name,
	gender,
	birthday,
	floor(datediff(current_date, to_date(birthday)) / 365.25),
	native_city,
	train_power,
	train_price_sensitive,
	train_area_trend,
	train_price_per_km,
	train_cabin_trend,
	flight_power,
	flight_price_sensitive,
	flight_area_trend,
	flight_price_per_km,
	flight_cabin_trend,
	flight_avg_distance,
	hotel_power,
	hotel_price_sensitive,
	hotel_area_trend,
	hotel_price_per_night,
	hotel_band_trend,
	family_prob,
	credit_max_amount,
	holiday_avg_count,
	work_city,
	app_freq,
	login_freq,
	total_sensitive,
	whole_power,
	consumer_stability,
	internation_prob,
	pay_type,
	vacation_day_desire,
	holiday_power,
	holiday_sensitive,
	holiday_city_tendency,
	plan_prob,
	cancel_ratio,
	order_platform,
	refund_ratio,
	login_period,
	rest_regular,
	friend_prob
from (select distinct uid from cfbdb.vqq_nqh_beforeafter_uv where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}') a
inner join cfbdb.ctrip_user_profile_output_temp b on lower(a.uid) = lower(b.user_id);

-- 导入到innofin;
use tmp_innofin;
drop table if exists vqq_tmp_user_profile_1;
create table if not exists vqq_tmp_user_profile_1(
	uid string,
	user_name	string	comment '用户姓名',
	gender	int	comment '性别',
	birthday	string	comment '出生日期',
	age int,
	native_city	string	comment '籍贯',
	train_power	double	comment '火车票消费能力',
	train_price_sensitive	double	comment '火车票价格敏感度',
	train_area_trend	string	comment '火车票地域倾向暂时做不了,需要从日志表里获取',
	train_price_per_km	double	comment '火车票每公里价格',
	train_cabin_trend	string	comment '火车票席位倾向[-1,硬座:p1、一等座:p2、二等座:p3、商务:p4、硬卧:p5、软卧:p6、高软:p7]',
	flight_power	double	comment '机票消费能力',
	flight_price_sensitive	double	comment '机票价格敏感度',
	flight_area_trend	string	comment '机票地域倾向[小型机场:中型机场:大型机场 = p1 : p2 : p3]',
	flight_price_per_km	double	comment '机票每公里价格',
	flight_cabin_trend	string	comment '机票仓位倾向[普通舱:头等舱:商务舱 = p1 : p2 ： p3]',
	flight_avg_distance	double	comment '机票单次平均距离',
	hotel_power	double	comment '酒店消费能力',
	hotel_price_sensitive	double	comment '酒店价格敏感度',
	hotel_area_trend	string	comment '酒店地域倾向[国外,国内一线,国内二线,国内滨海城市,国内热门旅游城市]',
	hotel_price_per_night	double	comment '酒店消费平均间夜价格',
	hotel_band_trend	string	comment '酒店品牌倾向[-1,经济:p1、舒适:p2、高档:p3、豪华:p4]',
	family_prob	double	comment '家庭账号概率',
	credit_max_amount	double	comment '信用卡最大金额',
	holiday_avg_count	int	comment '度假平均人数',
	work_city	string	comment '工作城市',
	app_freq	double	comment 'app粘度',
	login_freq	double	comment '登录账号频率,半衰期90天',
	total_sensitive	double	comment '价格敏感度',
	whole_power	double	comment '整体消费能力',
	consumer_stability	double	comment '消费稳定性',
	internation_prob	double	comment '国际用户概率',
	pay_type	string	comment '支付倾向,最常使用的前三种支付方式分别占比',
	vacation_day_desire	double	comment '节假日消费意愿',
	holiday_power	double	comment '度假消费能力',
	holiday_sensitive	double	comment '度假价格敏感度',
	holiday_city_tendency	string	comment '度假地域倾向[海外，滨海，人文城市，热门旅游城市]',
	plan_prob	double	comment '出行规划完备度',
	cancel_ratio	double	comment '取消订单占比',
	order_platform	string	comment '下单平台倾向',
	refund_ratio	double	comment '退款订单占比',
	login_period	string	comment '登录账号时间段偏好[早上,中午,下午,晚上,凌晨 = p1,p2,...]',
	rest_regular	double	comment '作息规律度',
	friend_prob	double	comment '好友影响度'
) ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
STORED AS TEXTFILE;


use tmp_innofin;
drop table if exists vqq_tmp_user_profile_1_trends;
create table if not exists vqq_tmp_user_profile_1_trends(
	uid string,
	tpos int,
	tvalue double
)partitioned by (
	trends string	
);

use tmp_innofin;
insert overwrite table vqq_tmp_user_profile_1_trends partition (trends = 'flight_cabin_trend')
	select uid,tpos,tvalue from tmp_innofin.vqq_tmp_user_profile_1 LATERAL VIEW posexplode(split(flight_cabin_trend,',')) a AS tpos,tvalue where flight_cabin_trend <> '';
insert overwrite table vqq_tmp_user_profile_1_trends partition (trends = 'hotel_area_trend')
	select uid,tpos,tvalue from tmp_innofin.vqq_tmp_user_profile_1 LATERAL VIEW posexplode(split(hotel_area_trend,',')) a AS tpos,tvalue where hotel_area_trend <> '';
insert overwrite table vqq_tmp_user_profile_1_trends partition (trends = 'hotel_band_trend')
	select uid,tpos,tvalue from tmp_innofin.vqq_tmp_user_profile_1 LATERAL VIEW posexplode(split(hotel_band_trend,',')) a AS tpos,tvalue where hotel_band_trend <> '';
insert overwrite table vqq_tmp_user_profile_1_trends partition (trends = 'train_cabin_trend')
	select uid,tpos,tvalue from tmp_innofin.vqq_tmp_user_profile_1 LATERAL VIEW posexplode(split(train_cabin_trend,',')) a AS tpos,tvalue where train_cabin_trend <> '';
insert overwrite table vqq_tmp_user_profile_1_trends partition (trends = 'holiday_city_tendency')
	select uid,tpos,tvalue from tmp_innofin.vqq_tmp_user_profile_1 LATERAL VIEW posexplode(split(holiday_city_tendency,',')) a AS tpos,tvalue where holiday_city_tendency <> '';


--- max trends
use tmp_innofin;
drop table if exists vqq_tmp_user_profile_1_maxtrends;
create table if not exists vqq_tmp_user_profile_1_maxtrends(
	trends string,	
	uid string,
	tpos_max int,
	tlabel string
);
use tmp_innofin;
insert overwrite table vqq_tmp_user_profile_1_maxtrends
select a.trends,a.uid,a.tpos,b.tname from 
(select trends,uid,tpos,row_number() over (partition by trends,uid order by tvalue desc) as c1 from vqq_tmp_user_profile_1_trends where trends <> '') a 
inner join (select * from vqq_tmp_user_profile_trends where trends <> '') b on a.trends = b.trends and a.tpos = b.tpos and a.c1 = 1;


-- tmp_innofin.vqq_tmp_user_profile_1_data
use tmp_innofin;
drop table if exists vqq_tmp_user_profile_1_data;
create table if not exists vqq_tmp_user_profile_1_data(
	bu string,
	uid string,
	user_name	string	comment '用户姓名',
	gender	string	comment '性别',
	birthday	string	comment '出生日期',
	age int,
	work_city	string	comment '工作城市',
	province	string	comment '省份',

	flight_power	double	comment '机票消费能力',
	flight_cabin_trend	string	comment '机票仓位倾向[普通舱:头等舱:商务舱 = p1 : p2 ： p3]',
	
	hotel_power	double	comment '酒店消费能力',
	hotel_area_trend	string	comment '酒店地域倾向[国外,国内一线,国内二线,国内滨海城市,国内热门旅游城市]',
	hotel_band_trend	string	comment '酒店品牌倾向[-1,经济:p1、舒适:p2、高档:p3、豪华:p4]',
	
	train_power	double	comment '火车票消费能力',
	train_cabin_trend	string	comment '火车票席位倾向[-1,硬座:p1、一等座:p2、二等座:p3、商务:p4、硬卧:p5、软卧:p6、高软:p7]',
	
	credit_max_amount	double	comment '信用卡最大金额',
	holiday_avg_count	int	comment '度假平均人数',
	holiday_power	double	comment '度假消费能力',
	holiday_city_tendency	string	comment '度假地域倾向[海外，滨海，人文城市，热门旅游城市]'
);

use tmp_innofin;
insert overwrite table vqq_tmp_user_profile_1_data
select bu.bu,
	a.uid,a.user_name,
	case when gender = 1 then '男' when  gender = 0 then '女' else '未知' end ,a.birthday,a.age,a.work_city,
	nvl(b.province,'未知'),
	a.flight_power,nvl(c.tlabel,'未知'),
	a.hotel_power,nvl(d.tlabel,'未知'),nvl(e.tlabel,'未知'),
	a.train_power,nvl(f.tlabel,'未知'),
	a.credit_max_amount,a.holiday_avg_count,a.holiday_power,nvl(g.tlabel,'未知')
from vqq_tmp_user_profile_1 a 
	inner join (select uid,bu from tmp_innofin.vqq_nqh_beforeafter_uv where dt = '2018-03-22') bu on a.uid = bu.uid
	left join ods_innofin.city_list b on a.work_city = b.city
	left join (select * from vqq_tmp_user_profile_1_maxtrends where trends = 'flight_cabin_trend') c on a.uid = c.uid
	left join (select * from vqq_tmp_user_profile_1_maxtrends where trends = 'hotel_area_trend') d on a.uid = d.uid
	left join (select * from vqq_tmp_user_profile_1_maxtrends where trends = 'hotel_band_trend') e on a.uid = e.uid
	left join (select * from vqq_tmp_user_profile_1_maxtrends where trends = 'train_cabin_trend') f on a.uid = f.uid
	left join (select * from vqq_tmp_user_profile_1_maxtrends where trends = 'holiday_city_tendency') g on a.uid = g.uid
;



--db_active
--loan_order
--db_success
--button_apply
hive -e "select a.* from tmp_innofin.vqq_tmp_user_profile_1_data a where bu = 'db_active';">/home/innofin/db_active.txt
tar -czf db_active.tar.gz /home/innofin/db_active.txt
curl --ftp-ssl -k -T db_active.tar.gz ftp://data_ctrip_cf:qX3yHHc0CFoovsn9ynjT@dataexchange.ctripcorp.com:53233/xfjr_sbu/ctrip/uids/

hive -e "select a.*,b.prob from tmp_innofin.vqq_tmp_user_profile_1_data a inner join dw_usercreditscoredb.loan_before_v2_user_risk_final_result_dt b on lower(a.uid) = lower(b.user_id) where b.dt = '2018-04-16' and bu = 'loan_order';">/home/innofin/loan_order.txt
tar -czf loan_order.tar.gz /home/innofin/loan_order.txt
curl --ftp-ssl -k -T loan_order.tar.gz ftp://data_ctrip_cf:qX3yHHc0CFoovsn9ynjT@dataexchange.ctripcorp.com:53233/xfjr_sbu/ctrip/uids/

hive -e "select a.* from tmp_innofin.vqq_tmp_user_profile_1_data a where bu = 'db_success';">/home/innofin/db_success.txt
tar -czf db_success.tar.gz /home/innofin/db_success.txt
curl --ftp-ssl -k -T db_success.tar.gz ftp://data_ctrip_cf:qX3yHHc0CFoovsn9ynjT@dataexchange.ctripcorp.com:53233/xfjr_sbu/ctrip/uids/

hive -e "select a.* from tmp_innofin.vqq_tmp_user_profile_1_data a where bu = 'button_apply';">/home/innofin/button_apply.txt
tar -czf button_apply.tar.gz /home/innofin/button_apply.txt
curl --ftp-ssl -k -T button_apply.tar.gz ftp://data_ctrip_cf:qX3yHHc0CFoovsn9ynjT@dataexchange.ctripcorp.com:53233/xfjr_sbu/ctrip/uids/


