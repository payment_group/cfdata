use tmp_innofin;
insert overwrite table vqq_tmp_os_10 
select a.uid,b.operationsystem,b.starttime from
ods_innofin.nqh_user_active_date a 
inner join 
(select uid,operationsystem,starttime
	 from dw_mobdb.factmbpageview where d >= '2017-01-01') b on lower(a.uid) = b.uid

use tmp_innofin;
insert overwrite table vqq_tmp_os_2 
select uid,operationsystem from
(select uid,operationsystem, row_number() over (partition by uid order by starttime desc) as c1 from tmp_innofin.vqq_tmp_os_10 where operationsystem is not null and trim(operationsystem)<> '') a
where c1 = 1;


select operationsystem,count(1) from tmp_innofin.vqq_tmp_os_2 group by operationsystem


