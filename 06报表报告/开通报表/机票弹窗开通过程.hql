use tmp_innofin;
create table if not exists vqq_entry_mktype_flight_pop(
	d string,
	uid	string	comment ''
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_entry_mktype_flight_pop
select distinct d,uid
from dw_mobdb.factmbpageview 
where d >= '2018-05-10' and pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431','10650001007') 
	and regexp_extract(query,'&mktype=([^&]+)',1) = 'flight_pop';


use tmp_innofin;
create table if not exists vqq_rpt_active_process_detail_mktype_flight_pop (
	dt string,
	uid string,

	enter_ctrip int,
	enter_realname int,
	enter_wallet int,
	enter_more_func int,
	enter_others_channel int,
	
	page_apply	int,	
	page_cardguide	int,	
	page_card_number	int,	
	page_card	int,	
	page_setpass	int,	
	page_active	int,
	page_finish	int,	
	page_success	int,
	
	button_apply int,
	button_contract int,
	button_active int,

	db_active int,
	db_success int,	

	log_creditProcess int,
	log_existPwd int,
	log_hasMobile int,
	log_preCreditStatus int,
	log_realName int
);

use tmp_innofin;
insert overwrite table vqq_rpt_active_process_detail_mktype_flight_pop
select a.dt,a.uid,
	enter_ctrip ,
	enter_realname ,
	enter_wallet ,
	enter_more_func ,
	enter_others_channel ,
	
	page_apply	,	
	page_cardguide	,	
	page_card_number	,	
	page_card	,	
	page_setpass	,	
	page_active	,
	page_finish	,	
	page_success	,
	
	button_apply ,
	button_contract ,
	button_active ,

	db_active ,
	db_success ,	

	log_creditProcess ,
	log_existPwd ,
	log_hasMobile ,
	log_preCreditStatus ,
	log_realName 
from vqq_entry_mktype_flight_pop a
inner join (select * from vqq_rpt_active_process_detail where dt >= '2018-05-10') b on lower(a.uid) = lower(b.uid) and a.d = b.dt;


use tmp_innofin;
create table if not exists vqq_tmp_entry_mktype_flight_pop
(
	dt string comment '日期',
	flow_name string comment '开通流程',
	
	page_apply    int comment  '开通首页UV',
	button_apply    int comment  '立即开通btn',

	button_contract    int comment  '协议页下一步btn',

	page_cardguide   int  comment  '支付绑卡引导页UV',
	page_card  int comment  '支付绑卡页UV',

	page_active    int comment  '核实信息页UV',
	button_active    int comment  '核实信息页提交btn',

	need_setpass   int comment  '需要设置支付密码',
	success_setpass int comment '成功设置支付密码',

	page_finish   int  comment  '开通完成页UV',
	db_success   int  comment  '授信成功数'
);


use tmp_innofin;
insert overwrite table vqq_tmp_entry_mktype_flight_pop
select dt,'已实名需绑卡',
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_card),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail_mktype_flight_pop where (log_creditProcess = 0 and log_realName = 1) 
	group by dt;
	
use tmp_innofin;
insert into table vqq_tmp_entry_mktype_flight_pop
select dt,'未实名',
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_card),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail_mktype_flight_pop where (log_creditProcess = 0 and log_realName = 0)
	group by dt;

use tmp_innofin;
insert into table vqq_tmp_entry_mktype_flight_pop
select dt,'已实名无需绑卡',
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail_mktype_flight_pop where (log_creditProcess = 1) 
	group by dt;

use tmp_innofin;
insert into table vqq_tmp_entry_mktype_flight_pop
select dt,'已实名一键开通',
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail_mktype_flight_pop where (log_creditProcess = 2)
	group by dt;

