
use tmp_innofin;
create table if not exists vqq_apply_process_detail_mktype_30(
	d string,
	uid	string	comment '',
	mktype string
)
Partitioned by (
	dt string
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_apply_process_detail_mktype_30 partition(dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct d,uid,regexp_extract(query,'&mktype=([^&]+)',1) as mktype
from dw_mobdb.factmbpageview 
where d >= '${zdt.addDay(-30).format("yyyy-MM-dd")}' and pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431','10650001007');

