use tmp_innofin;

-- tmp_innofin.apply_process_detail 
use ods_innofin;
create table if not exists user_action_info(
	id	bigint,
	user_id	string,
	osid		string,
	user_group		string,
	action_type		string,
	action_desc string,
	create_time	string,
	update_time	string
);
-- datax

use tmp_innofin;
create table if not exists user_action_info_index (
	uid	string,
	action_desc string
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.user_action_info_index partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct user_id as uid,action_desc 
from ods_innofin.user_action_info 
where to_date(create_time) = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and action_type = 'INDEX';

use tmp_innofin;
create table if not exists vqq_rpt_active_process_user_subprocess (
	uid string,
	creditProcess string,
	existPwd string,
	hasMobile string,
	preCreditStatus string,
	realName string	
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_user_subprocess partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,
	get_json_object(action_desc,'$.creditProcess'),
	get_json_object(action_desc,'$.existPwd'),
	get_json_object(action_desc,'$.hasMobile'),
	get_json_object(action_desc,'$.preCreditStatus'),
	get_json_object(action_desc,'$.realName')
from tmp_innofin.user_action_info_index 
where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}';

-- REPORT
use tmp_innofin;
create table if not exists vqq_rpt_active_process_main_detail (
	uid string,
	
	ctrip int,
	wallet int,
	realname int,
	more_func int,
	others_channel int,
	
	apply_page int,
	apply_click int,
	contract_click int,
	finish_page int,
	finish_db int,
	success_page int,
	success_db int	
) partitioned by (
	dt string
);

set DT = ${zdt.addDay(-1).format("yyyy-MM-dd")};

insert overwrite table tmp_innofin.vqq_rpt_active_process_main_detail partition (dt='${hiveconf:DT}')
select a.uid,ctrip,wallet,realname,more_func,others_channel,
	1,if(b.uid is null,0,1),if(c.uid is null,0,1),finish,if(e.uid is null,0,1),success,if(d.uid is null,0,1)
from (select * from tmp_innofin.apply_process_detail where d='${hiveconf:DT}') a 
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${hiveconf:DT}' and bid in ('101320','101321')) b on lower(a.uid) = lower(b.uid) 
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${hiveconf:DT}' and bid in ('101324')) c on lower(a.uid) = lower(c.uid)
left join (select uid from tmp_innofin.nqh_uid_apl where to_date(time_apl)='${hiveconf:DT}' ) d on lower(a.uid) = lower(d.uid)
left join (select user_id as uid from ods_innofin.user_contract where to_date(update_time) = '${hiveconf:DT}') e on lower(a.uid) = lower(e.uid) ;

use tmp_innofin;
create table if not exists vqq_rpt_active_process_main (
	apply_page int,
	apply_click int,
	apply_click_rate decimal(5,4),
	
	contract_click int,
	contract_click_rate decimal(5,4),
	finish_page int,
	finish_db int,
	success_page int,
	success_db int,
	success_rate decimal(5,4)	
) partitioned by (
	dt string,
	entrance string

);

set DT = ${zdt.addDay(-1).format("yyyy-MM-dd")};
insert overwrite table tmp_innofin.vqq_rpt_active_process_main partition (dt='${hiveconf:DT}',entrance='我携')
select sum(apply_page),sum(apply_click),sum(apply_click)/sum(apply_page),sum(contract_click),sum(contract_click)/sum(apply_click),sum(finish_page),sum(finish_db),sum(	success_page),sum(success_db),sum(success_db)/sum(apply_page)
from tmp_innofin.vqq_rpt_active_process_main_detail where dt='${hiveconf:DT}' and ctrip = 1;

insert overwrite table tmp_innofin.vqq_rpt_active_process_main partition (dt='${hiveconf:DT}',entrance='实名')
select sum(apply_page),sum(apply_click),sum(apply_click)/sum(apply_page),sum(contract_click),sum(contract_click)/sum(apply_click),sum(finish_page),sum(finish_db),sum(	success_page),sum(success_db),sum(success_db)/sum(apply_page)
from tmp_innofin.vqq_rpt_active_process_main_detail where dt='${hiveconf:DT}' and realname = 1;

insert overwrite table tmp_innofin.vqq_rpt_active_process_main partition (dt='${hiveconf:DT}',entrance='钱包')
select sum(apply_page),sum(apply_click),sum(apply_click)/sum(apply_page),sum(contract_click),sum(contract_click)/sum(apply_click),sum(finish_page),sum(finish_db),sum(	success_page),sum(success_db),sum(success_db)/sum(apply_page)
from tmp_innofin.vqq_rpt_active_process_main_detail where dt='${hiveconf:DT}' and wallet = 1;

insert overwrite table tmp_innofin.vqq_rpt_active_process_main partition (dt='${hiveconf:DT}',entrance='首页更多')
select sum(apply_page),sum(apply_click),sum(apply_click)/sum(apply_page),sum(contract_click),sum(contract_click)/sum(apply_click),sum(finish_page),sum(finish_db),sum(	success_page),sum(success_db),sum(success_db)/sum(apply_page)
from tmp_innofin.vqq_rpt_active_process_main_detail where dt='${hiveconf:DT}' and more_func = 1;

insert overwrite table tmp_innofin.vqq_rpt_active_process_main partition (dt='${hiveconf:DT}',entrance='其他')
select sum(apply_page),sum(apply_click),sum(apply_click)/sum(apply_page),sum(contract_click),sum(contract_click)/sum(apply_click),sum(finish_page),sum(finish_db),sum(	success_page),sum(success_db),sum(success_db)/sum(apply_page)
from tmp_innofin.vqq_rpt_active_process_main_detail where dt='${hiveconf:DT}' and others_channel = 1;

insert overwrite table tmp_innofin.vqq_rpt_active_process_main partition (dt='${hiveconf:DT}',entrance='总体')
select sum(apply_page),sum(apply_click),sum(apply_click)/sum(apply_page),sum(contract_click),sum(contract_click)/sum(apply_click),sum(finish_page),sum(finish_db),sum(	success_page),sum(success_db),sum(success_db)/sum(apply_page)
from tmp_innofin.vqq_rpt_active_process_main_detail where dt='${hiveconf:DT}';


-- ART
select dt as `日期`,
	entrance as `进入首页来源`,
	apply_page as `免费开通页UV`,
	apply_click as `免费开通键点击UV`,
	apply_click_rate as `免费开通建点击率%`,
	contract_click as `合同确认键点击UV`,
	contract_click_rate as `合同确认键点击率%`,
	finish_db as `完成流程数`,
	success_db as `开通成功数`,
	success_rate as `来源开通成功率%`
from tmp_innofin.vqq_rpt_active_process_main where dt >= '${zdt.addDay(-7).format("yyyy-MM-dd")}';


