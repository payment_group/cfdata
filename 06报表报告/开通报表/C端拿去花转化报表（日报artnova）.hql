-- 总表，
	-- 申请，走完流程，激活成功
use tmp_innofin;
create table if not exists vqq_rpt_ctrip_nqh_active_process_main(
	dt string,
	apply int,
	finish int,
	success int
);

use tmp_innofin;
insert overwrite table vqq_rpt_ctrip_nqh_active_process_main
select a.dt,apply,nvl(finish,0),nvl(success,0) from 
(select d as dt,sum(apply) as apply,sum(finish) as finish from tmp_innofin.apply_process_detail where d >='2017-12-01' group by d) a 
left join 
(select to_date(finish_time) as dt, count(distinct user_id) as success from source_mobdb.ctrip_tbl_credit_activate_snap
	where  org_channel = 'CTRIP' and credit_type=1 and to_date(finish_time) >= '2017-12-01' and product_no = 'IOUS' and accounting_status = 2 group by to_date(finish_time)) b on a.dt = b.dt
;

--ART
select dt as `日期`,apply as `申请人数`,finish as `完成流程`,success as `激活成功`,
	cast(cast(100*finish/apply as decimal(5,2)) as double) as `完成流程%`,
	cast(cast(100*success/finish as decimal(5,2)) as double) as `激活成功%`
	from tmp_innofin.vqq_rpt_ctrip_nqh_active_process_main order by `日期` limit 30


-- 2)页面	
-- ods_innofin.vqq_rpt_active_process_ubt
-- 3)按钮
use tmp_innofin;
create table if not exists vqq_rpt_active_process_click_uv(
	bid string,
	uid string
) partitioned by (dt string);

insert overwrite table tmp_innofin.vqq_rpt_active_process_click_uv partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct name as bid,env['UID'] as uid
from dw_mobdb.factmbmetriclog_sdk
where d = date_add(current_date,-1) and h between '0' and '23' and name between '101320' and '101328';

insert overwrite table ods_innofin.vqq_rpt_active_process_data partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}',ugroup='entrance' ,upoint='我携')
	select uid from tmp_innofin.apply_process_detail where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and ctrip = 1;

--- 新，协议弹窗页
use ods_innofin;
create table if not exists vqq_rpt_active_process_ubt(
	uid	string comment '用户登录帐号'
) partitioned by (
	dt string,
	sgroup string,
	skey string
);

insert overwrite table ods_innofin.vqq_rpt_active_process_ubt partition(dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}',sgroup = 'page',skey='协议弹窗页')
select distinct uid from dw_mobdb.factmbpageview where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and pagecode in ('10320678353','10320678341');

-- 4)后端埋点
use tmp_innofin;
create table if not exists user_action_info_index (
	uid	string,
	action_desc string
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.user_action_info_index partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select user_id as uid,action_desc from
(select user_id,action_desc,row_number() over (partition by user_id order by create_time desc) as no 
from ods_innofin.user_action_info 
where to_date(create_time) = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and action_type = 'INDEX') a
where a.no = 1;

use tmp_innofin;
create table if not exists vqq_rpt_active_process_user_subprocess (
	uid string,
	creditProcess string,
	existPwd string,
	hasMobile string,
	preCreditStatus string,
	realName string	
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_user_subprocess partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,
	get_json_object(action_desc,'$.creditProcess'),
	get_json_object(action_desc,'$.existPwd'),
	get_json_object(action_desc,'$.hasMobile'),
	get_json_object(action_desc,'$.preCreditStatus'),
	get_json_object(action_desc,'$.realName')
from tmp_innofin.user_action_info_index 
where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}';

-- 5)后端开通
-- left join (select uid from tmp_innofin.nqh_uid_apl where to_date(time_apl)='${zdt.addDay(-1).format("yyyy-MM-dd")}' ) d on lower(a.uid) = lower(d.uid)
-- left join (select user_id as uid from ods_innofin.user_contract where to_date(update_time) = '${zdt.addDay(-1).format("yyyy-MM-dd")}') e on lower(a.uid) = lower(e.uid) ;


-- 6)总表
use tmp_innofin;
create table if not exists vqq_rpt_active_process_detail (
	uid string,

	enter_ctrip int,
	enter_realname int,
	enter_wallet int,
	enter_more_func int,
	enter_others_channel int,
	
	page_apply	int,	
	page_cardguide	int,	
	page_card_number	int,	
	page_card	int,	
	page_setpass	int,	
	page_active	int,
	page_finish	int,	
	page_success	int,
	
	button_apply int,
	button_contract int,
	button_active int,

	db_active int,
	db_success int,	

	log_creditProcess int,
	log_existPwd int,
	log_hasMobile int,
	log_preCreditStatus int,
	log_realName int
	
) partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_rpt_active_process_detail partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.uid,
	a.ctrip,a.realname,a.wallet,a.more_func,a.others_channel, -- enter
	a.apply,a.cardguide,a.card_number,a.card,a.setpass,a.active,a.finish,a.success, -- page
	if(c.uid is null,0,1) as button_apply,if(d.uid is null,0,1) as button_contract,if(h.uid is null,0,1) as button_active, -- button
	if(f.uid is null,0,1) as db_active,if(e.uid is null,0,1) as db_success, -- db
	cast(nvl(g.creditProcess,-1) as int),if(existPwd = 'true',1,0),if(hasMobile = 'true',1,0),if(preCreditStatus = 'true',1,0),if(g.realName  = 'true',1,0) -- log
from (select * from tmp_innofin.apply_process_detail where d='${zdt.addDay(-1).format("yyyy-MM-dd")}') a 
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bid in ('101320','101321')) c on lower(a.uid) = lower(c.uid) 
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bid in ('101324')) d on lower(a.uid) = lower(d.uid)
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bid in ('101327')) h on lower(a.uid) = lower(h.uid)

left join  ods_innofin.nqh_user_active_date e on lower(a.uid) = lower(e.uid) and e.active_date = '${zdt.addDay(-1).format("yyyy-MM-dd")}' 

left join (select distinct user_id as uid from ods_innofin.user_activate_req where to_date(request_time) = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and req_source <>'PRE_CREDIT_TASK') f on lower(a.uid) = lower(f.uid)
left join (select * from tmp_innofin.vqq_rpt_active_process_user_subprocess where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}') g on lower(a.uid) = lower(g.uid);


-- datasource 
use tmp_innofin;
create table if not exists vqq_rpt_active_process_all
(
	dt string comment '日期',
	flow_name string comment '开通流程',
	
	page_apply_total  int comment  '开通首页UV(总)',
	page_finish_total  int  comment  '开通完成页UV(总)',
	db_success_total   int comment  '授信成功数(总)',
	
	page_apply    int comment  '开通首页UV',
	button_apply    int comment  '立即开通btn',
	button_apply_lost decimal(5,4) comment  '首页流失率%',

	button_contract    int comment  '协议页下一步btn',
	button_contract_lost decimal(5,4) comment  '协议页流失率',

	page_cardguide   int  comment  '支付绑卡引导页UV',
	page_cardguide_lost decimal(5,4)  comment  '绑卡引导流失率%',

	page_card    int comment  '支付绑卡页UV',
	page_card_lost decimal(5,4)  comment  '绑卡页流失率%',

	page_active    int comment  '核实信息页UV',
	button_active    int comment  '核实信息页提交btn',
	button_active_lost decimal(5,4)  comment  '核实信息流失率%',

	need_setpass   int comment  '需要设置支付密码',

	page_finish   int  comment  '开通完成页UV',
	page_finish_rate decimal(5,4)  comment  '流程完成率%',
	db_success   int  comment  '授信成功数',
	db_success_rate decimal(5,4)  comment  '授信成功数%'
);

-- ART
-- card 
use tmp_innofin;
insert overwrite table vqq_rpt_active_process_all
select 
	a.dt,
	b.flow_name,
	a.apply,
	a.finish,
	a.success,
	
	page_apply,
	button_apply,
	1-1.0*button_apply/page_apply,

	button_contract,
	1-1.0*button_contract/button_apply,

	page_cardguide,
	if(page_cardguide = 0,0,1-1.0*page_cardguide/button_contract),

	page_card,
	if(page_cardguide = 0,0,1-1.0*page_card/page_cardguide),

	page_active as `核实信息页UV`,
	button_active as `核实信息页提交btn`,
	1-1.0*button_active/page_active,

	need_setpass 	as `需要设置支付密码`,

	page_finish as `开通完成页UV`,
	1.0*page_finish/page_apply,
	db_success as `授信成功数`,
	1.0*db_success/page_finish
from vqq_rpt_ctrip_nqh_active_process_main a 
inner join 
(select '已实名需要绑卡' as flow_name,dt,page_apply,page_cardguide,page_card,page_active,button_apply,button_contract,button_active,need_setpass,success_setpass,page_finish,db_success 
	from tmp_innofin.vqq_rpt_active_process_card
union all
select '已实名无需绑卡' as flow_name, dt,page_apply,0 as page_cardguide,0 as page_card,page_active,button_apply,button_contract,button_active,need_setpass,success_setpass,page_finish,db_success 
	from tmp_innofin.vqq_rpt_active_process_nocard
union all
select '未实名' as flow_name, dt,page_apply,page_cardguide,page_card,page_active,button_apply,button_contract,button_active,need_setpass,success_setpass,page_finish,db_success 
	from tmp_innofin.vqq_rpt_active_process_noname
union all
select '已实名一键开通' as flow_name, dt,page_apply,0 as page_cardguide,0 as page_card,page_active,button_apply,button_contract,button_active,need_setpass,success_setpass,page_finish,db_success 
	from tmp_innofin.vqq_rpt_active_process_rapid
) b
on a.dt = b.dt;

-- 流程、入口
use tmp_innofin;
create table if not exists vqq_rpt_active_process_entry
(
	dt string comment '日期',
	flow_name string comment '开通流程',
	entry_name string comment '首页来源',
	
	page_apply    int comment  '开通首页UV',
	button_apply    int comment  '立即开通btn',

	button_contract    int comment  '协议页下一步btn',

	page_cardguide   int  comment  '支付绑卡引导页UV',
	page_card  int comment  '支付绑卡页UV',

	page_active    int comment  '核实信息页UV',
	button_active    int comment  '核实信息页提交btn',

	need_setpass   int comment  '需要设置支付密码',
	success_setpass int comment '成功设置支付密码',

	page_finish   int  comment  '开通完成页UV',
	db_success   int  comment  '授信成功数'
);

use tmp_innofin;
insert overwrite table vqq_rpt_active_process_entry
select dt,'已实名需绑卡',
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end,
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 0 and log_realName = 1) 
	group by dt,
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end;

use tmp_innofin;
insert into table vqq_rpt_active_process_entry
select dt,'未实名',
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end,
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 0 and log_realName = 0)
	group by dt,
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end;

use tmp_innofin;
insert into table vqq_rpt_active_process_entry
select dt,'已实名无需绑卡',
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end,
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 1) 
	group by dt,
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end;

use tmp_innofin;
insert into table vqq_rpt_active_process_entry
select dt,'已实名一键开通',
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end,
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 2)
	group by dt,
	case when enter_ctrip = 1 then '我携' when enter_realname  = 1 then '实名' when enter_wallet = 1 then '钱包' when enter_more_func = 1 then '首页更多' else '其他' end;

--- AppPush_FlightPop

use tmp_innofin;
insert into table vqq_rpt_active_process_entry
select dt,'已实名需绑卡',
	'机票立减30消息推送',
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from (select * from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 0 and log_realName = 1)) a 
	inner join (select uid,d from tmp_innofin.vqq_apply_process_detail_mktype_30 where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and mktype = 'AppPush_FlightPop') b 
		on lower(a.uid) = lower(b.uid) and a.dt = b.d
	group by a.dt;

use tmp_innofin;
insert into table vqq_rpt_active_process_entry
select dt,'未实名',
	'机票立减30消息推送',
	sum(page_apply),sum(button_apply),sum(button_contract),
	sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from (select * from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 0 and log_realName = 0)) a 
	inner join (select uid,d from tmp_innofin.vqq_apply_process_detail_mktype_30 where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and mktype = 'AppPush_FlightPop') b 
		on lower(a.uid) = lower(b.uid) and a.dt = b.d
	group by a.dt;

use tmp_innofin;
insert into table vqq_rpt_active_process_entry
select dt,'已实名无需绑卡',
	'机票立减30消息推送',
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from (select * from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 1) ) a 
	inner join (select uid,d from tmp_innofin.vqq_apply_process_detail_mktype_30 where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and mktype = 'AppPush_FlightPop') b 
		on lower(a.uid) = lower(b.uid) and a.dt = b.d
	group by a.dt;

use tmp_innofin;
insert into table vqq_rpt_active_process_entry
select dt,'已实名一键开通',
	'机票立减30消息推送',
	sum(page_apply),sum(button_apply),sum(button_contract),
	0,0,
	sum(page_active),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from (select * from tmp_innofin.vqq_rpt_active_process_detail where (dt >= '2017-12-01' and log_creditProcess = 2)) a 
	inner join (select uid,d from tmp_innofin.vqq_apply_process_detail_mktype_30 where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and mktype = 'AppPush_FlightPop') b 
		on lower(a.uid) = lower(b.uid) and a.dt = b.d
	group by a.dt;



