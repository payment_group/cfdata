select d as `日期`,
       cast(100.0*sum(realname*if(b.uid is null,0,1))/sum(realname) as double) as `实名认证(%)`,
       cast(100.0*sum(wallet*if(b.uid is null,0,1))/sum(wallet) as double) as `钱包(%)`,
       cast(100.0*sum(ctrip*if(b.uid is null,0,1))/sum(ctrip) as double) as `我携(%)`,
       cast(100.0*sum(more_func*if(b.uid is null,0,1))/sum(more_func) as double) as `首页更多(%)`,
       cast(100.0*sum(others_channel*if(b.uid is null,0,1))/sum(others_channel) as double) as `其他(%)`
from tmp_innofin.apply_process_detail a
left join ods_innofin.nqh_user_active_date b on a.uid = b.uid and a.d = b.active_date  
where datediff(current_date,d)<=30
group by d



select a.d as `日期`,a.type as `入口类型`,a.num as `进入入口新用户`,b.apply as `申请`,b.finish as `完成流程`,b.success as `开通成功`,
       cast(100 * apply / num as decimal(18,2)) as `入口转化率(%)`,
       cast(100 * finish / apply as decimal(18,2)) as `完成流程转化率(%)`,
       cast(100 * success / apply as decimal(18,2)) as `开通成功转化率(%)`
from 
(select d,'我携' as type,sum(new_ctrip) as num
from tmp_innofin.wqm_process_basic02 
where datediff(current_date,d)<=30
group by d
union all
select d,'钱包' as type,sum(new_wallet) as num
from tmp_innofin.wqm_process_basic02 
where datediff(current_date,d)<=30
group by d
union all
select d,'实名' as type,sum(new_realname) as num
from tmp_innofin.wqm_process_basic02 
where datediff(current_date,d)<=30
group by d
union all
select d,'首页更多' as type,sum(new_morefunc) as num
from tmp_innofin.wqm_process_basic02 
where datediff(current_date,d)<=30
group by d
union all
select d,'其他' as type,sum(others) as num
from tmp_innofin.wqm_process_basic02 
where datediff(current_date,d)<=30
group by d) a
join
(select d,'我携' as type,sum(ctrip) as apply,sum(ctrip*finish) as finish,sum(ctrip*if(b.uid is null,0,1)) as success
from tmp_innofin.apply_process_detail a left join ods_innofin.nqh_user_active_date b on a.uid = b.uid and a.d = b.active_date  
where datediff(current_date,d)<=30
group by d
union all
select d,'钱包' as type,sum(wallet) as apply,sum(wallet*finish) as finish,sum(wallet*if(b.uid is null,0,1)) as success
from tmp_innofin.apply_process_detail a left join ods_innofin.nqh_user_active_date b on a.uid = b.uid and a.d = b.active_date  
where datediff(current_date,d)<=30
group by d
union all
select d,'实名' as type,sum(realname) as apply,sum(realname*finish) as finish,sum(realname*if(b.uid is null,0,1)) as success
from tmp_innofin.apply_process_detail a left join ods_innofin.nqh_user_active_date b on a.uid = b.uid and a.d = b.active_date  
where datediff(current_date,d)<=30
group by d
union all
select d,'首页更多' as type,sum(more_func) as apply,sum(more_func*finish) as finish,sum(more_func*if(b.uid is null,0,1)) as success
from tmp_innofin.apply_process_detail a  left join ods_innofin.nqh_user_active_date b on a.uid = b.uid and a.d = b.active_date  
where datediff(current_date,d)<=30
group by d
union all
select d,'其他' as type,sum(others_channel) as apply,sum(others_channel*finish) as finish,sum(others_channel*if(b.uid is null,0,1)) as success
from tmp_innofin.apply_process_detail a left join ods_innofin.nqh_user_active_date b on a.uid = b.uid and a.d = b.active_date  
where datediff(current_date,d)<=30
group by d

) b on a.d=b.d and a.type=b.type
order by `入口类型` desc,`日期` desc limit 1000