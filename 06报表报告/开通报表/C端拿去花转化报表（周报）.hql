-- 总表，
	-- 申请，走完流程，激活成功
use tmp_innofin;
create table if not exists vqq_rpt_ctrip_nqh_active_process_main(
	dt string,
	apply int,
	finish int,
	success int
);

use tmp_innofin;
insert overwrite table vqq_rpt_ctrip_nqh_active_process_main
select a.dt,apply,nvl(finish,0),nvl(success,0) from 
(select d as dt,sum(apply) as apply,sum(finish) as finish from tmp_innofin.apply_process_detail where d >='2017-12-01' group by d) a 
left join 
(select to_date(finish_time) as dt, count(distinct user_id) as success from source_mobdb.ctrip_tbl_credit_activate_snap
	where  org_channel = 'CTRIP' and credit_type=1 and to_date(finish_time) >= '2017-12-01' and product_no = 'IOUS' and accounting_status = 2 
	group by to_date(finish_time)) b on a.dt = b.dt
;

-- 周报
use tmp_innofin;
create table if not exists vqq_rpt_ctrip_nqh_active_process_main_weekly(
	week string,
	apply int,
	finish int,
	success int
);

use tmp_innofin;
insert overwrite table vqq_rpt_ctrip_nqh_active_process_main_weekly
select b.week,sum(a.apply),sum(a.finish),sum(a.success) from 
vqq_rpt_ctrip_nqh_active_process_main a
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where datediff(current_date,a.dt) <= 12*7
group by b.week
;

--ART
select week as `星期`,apply as `申请人数`,finish as `完成流程`,success as `激活成功`,
	cast(100*finish/apply as decimal(5,2)) as `完成流程%`,
	cast(100*success/finish as decimal(5,2)) as `激活成功%`,
	active_user_total as `累计激活`,cast(100*active_user_total/2760974 as decimal(5,2)) as `目标完成%`
	from tmp_innofin.vqq_rpt_ctrip_nqh_active_process_main_weekly a 
	inner join ods_innofin.nqh_user_active_total b on substring(week,12,10) = b.dt
	order by `星期` limit 12;

-- 各流程
use tmp_innofin;
create table if not exists vqq_rpt_active_process_card_weekly(
	week string,
	page_apply int,
	page_cardguide int,
	page_card int,
	page_active int,
	
	button_apply int,
	button_contract int,
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int	
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_rpt_active_process_card_weekly
select b.week,
	sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a 
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 0 and log_realName = 1) group by week;


use tmp_innofin;
create table if not exists vqq_rpt_active_process_noname_weekly (
	week string,
	page_apply int,
	page_cardguide int,
	page_card int,
	page_active int,
	
	button_apply int,
	button_contract int,
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int	
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_noname_weekly
select b.week,
	sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 0 and log_realName = 0) group by week ;


use tmp_innofin;
create table if not exists vqq_rpt_active_process_nocard_weekly (
	week string,
	page_apply int,
	page_active int,
	
	button_apply int,
	button_contract int,
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int	
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_nocard_weekly
select b.week,
	sum(page_apply),sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 1) group by week ;


use tmp_innofin;
create table if not exists vqq_rpt_active_process_rapid_weekly (
	week string,
	page_apply int,
	page_active int,
	
	button_apply int,
	button_contract int,
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int	
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_rapid_weekly
select b.week,
	sum(page_apply),sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 2) group by week;

-- ART

-- card 
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	button_apply as `立即开通btn`,
	cast(100-100*button_apply/page_apply as decimal(5,2)) as `首页流失率%`,

	button_contract as `协议页下一步btn`,
	page_cardguide as `支付绑卡引导页UV`,
	cast(100-100*page_cardguide/page_apply as decimal(5,2)) as `绑卡引导流失率%`,

	page_card as `支付绑卡页UV`,
	cast(100-100*page_card/page_apply as decimal(5,2)) as `绑卡页流失率%`,

	page_active as `核实信息页UV`,
	button_active as `核实信息页提交btn`,
	cast(100-100*button_active/page_apply as decimal(5,2)) as `核实信息流失率%`,

	need_setpass 	as `需要设置支付密码`,

	page_finish as `开通完成页UV`,
	cast(100*page_finish/page_apply as decimal(5,2)) as `流程完成率%`,
	db_success as `授信成功数`,
	cast(100*db_success/page_finish as decimal(5,2)) as `授信成功数%`
from  tmp_innofin.vqq_rpt_active_process_card  order by `日期` desc limit 30


--nocard 
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	button_apply as `立即开通btn`,

	cast(100-100*button_apply/page_apply as decimal(5,2)) as `首页流失率%`,
	button_contract as `协议页下一步btn`,
	page_active as `核实信息页UV`,
	
	button_active as `核实信息页提交btn`,
	cast(100-100*button_active/page_apply as decimal(5,2)) as `核实信息流失率%`,
	need_setpass as `需要设置支付密码`,
	
	page_finish as `开通完成页UV`,
	cast(100*page_finish/page_apply as decimal(5,2)) as `流程完成率%`,
	db_success as `授信成功数`,
	cast(100*db_success/page_finish as decimal(5,2)) as `授信成功数%`
from  tmp_innofin.vqq_rpt_active_process_nocard  order by `日期` desc limit 10
-- rapid 
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	button_apply as `立即开通btn`,
	cast(100-100*button_apply/page_apply as decimal(5,2)) as `首页流失率%`,
	button_contract as `协议页下一步btn`,
	page_active as `核实信息页UV`,
	
	button_active as `核实信息页提交btn`,
	cast(100-100*button_active/page_apply as decimal(5,2)) as `核实信息流失率%`,
	
	page_finish as `开通完成页UV`,
	cast(100*page_finish/page_apply as decimal(5,2)) as `流程完成率%`,
	db_success as `授信成功数`,
	cast(100*db_success/page_finish as decimal(5,2)) as `授信成功数%`
from  tmp_innofin.vqq_rpt_active_process_rapid  order by `日期` desc limit 30

-- noname
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	button_apply as `立即开通btn`,
	cast(100-100*button_apply/page_apply as decimal(5,2)) as `首页流失率%`,
	button_contract as `协议页下一步btn`,
	page_cardguide as `支付绑卡引导页UV`,
	cast(100-100*page_cardguide/page_apply as decimal(5,2)) as `绑卡引导流失率%`,

	page_card as `支付绑卡页UV`,
	cast(100-100*page_card/page_apply as decimal(5,2)) as `绑卡页流失率%`,

	page_active as `核实信息页UV`,
	button_active as `核实信息页提交btn`,
	cast(100-100*button_active/page_apply as decimal(5,2)) as `核实信息流失率%`,

	need_setpass 	as `需要设置支付密码`,
	
	page_finish as `开通完成页UV`,
	cast(100*page_finish/page_apply as decimal(5,2)) as `流程完成率%`,
	db_success as `授信成功数`,
	cast(100*db_success/page_finish as decimal(5,2)) as `授信成功数%`
from tmp_innofin.vqq_rpt_active_process_noname  order by `日期` desc limit 30