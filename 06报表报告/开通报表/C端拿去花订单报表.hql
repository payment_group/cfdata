use tmp_innofin;
create table if not exists vqq_rpt_nqh_orders_details(
	uid string,
	o_id string,
	o_date string comment '日期',
	o_week string comment '星期',
	o_month string comment '月',
	merchant_id string,
	merchant_name string,
	bu_name string,
	amount decimal(18,2)
);

insert overwrite table tmp_innofin.vqq_rpt_nqh_orders_details
select uid,orderid,to_date(creationdate),
	c.week,
	concat(month(creationdate),'月'),	
	a.merchantid,b.merchantname,
	(case when merchantname in ('国内机票','国际机票') then '机票'
		  when merchantname = ('火车票') then '火车票'
		  when merchantname in ('国内酒店','海外酒店') then '酒店'
		  when merchantname= ('团队游') then '度假'
		  when merchantname in ('团购糯米','酒店团购') then '团购'
		  else '其他' end) as merchant_name,
	nvl(nvl(amount,totalamount),0) from ods_innofin.fact_loan_order a  
	left join dim_paydb.dimmerchant b on a.merchantid = b.merchantid
	left join tmp_innofin.vqq_rpt_weekofyear_2018 c on to_date(a.creationdate) = c.dt
    ;


use tmp_innofin;
create table if not exists vqq_rpt_nqh_orders_daily(
	dt string comment '日期',
	merchant_name string comment '事业部',
	o_count int comment '订单量', 
	amount decimal(18,2) comment '订单金额',
	avg_amount  decimal(18,2) comment '订单平均金额'
);
insert overwrite table tmp_innofin.vqq_rpt_nqh_orders_daily
select o_date,merchant_name,
	count(1),sum(amount),sum(amount)/count(1) 
from vqq_rpt_nqh_orders_details where datediff(current_date,o_date) <= 30  
group by o_date,merchant_name 

-- 总
select dt as `日期`,sum(o_count) as `订单量`,sum(amount) as `订单金额` from  tmp_innofin.vqq_rpt_nqh_orders_daily group by dt;
-- 分BU


-- 周报 -- 总
select o_week as `星期`,count(1) as `订单量`,sum(amount) as `订单金额` from  tmp_innofin.vqq_rpt_nqh_orders_details  where datediff(current_date,o_date) <= 12*7 group by o_week;
-- BU
select o_week as `星期`,merchant_name as `事业部`, count(1) as `订单量`,sum(amount) as `订单金额`, cast(sum(amount)/count(1) as decimal(18,2)) as `订单平均金额` 
	from tmp_innofin.vqq_rpt_nqh_orders_details  where datediff(current_date,o_date) <= 12*7 group by o_week,merchant_name;

-- 月报 -- 总
select substring(o_date,1,7) as `月`,count(1) as `订单量`,sum(amount) as `订单金额` from  tmp_innofin.vqq_rpt_nqh_orders_details  where add_months(o_date,12) >= current_date group by substring(o_date,1,7);

select substring(o_date,1,7) as `月`,merchant_name as `事业部`,count(1) as `订单量`,sum(amount) as `订单金额`,cast(sum(amount)/count(1) as decimal(18,2)) as `订单平均金额`  from  tmp_innofin.vqq_rpt_nqh_orders_details  where add_months(o_date,12) >= current_date group by substring(o_date,1,7),merchant_name;
