-- 各流程
use tmp_innofin;
create table if not exists vqq_rpt_active_process_flow_weekly(
	week string,
	page_apply int,
	page_cardguide int,
	page_card int,
	page_active int,
	
	button_apply int,
	button_contract int,
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int	
)partitioned by (
	flow string
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_rpt_active_process_flow_weekly partition(flow = '已实名需绑卡')
select b.week,
	sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a 
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 0 and log_realName = 1) group by week;


insert overwrite table tmp_innofin.vqq_rpt_active_process_flow_weekly partition(flow = '未实名')
select b.week,
	sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 0 and log_realName = 0) group by week ;

insert overwrite table tmp_innofin.vqq_rpt_active_process_flow_weekly partition(flow = '已实名无需绑卡')
select b.week,
	sum(page_apply),0,0,sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 1) group by week ;

insert overwrite table tmp_innofin.vqq_rpt_active_process_flow_weekly partition(flow = '已实名一键开通')
select b.week,
	sum(page_apply),0,0,sum(page_active),
	sum(button_apply),sum(button_contract),sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success)
from tmp_innofin.vqq_rpt_active_process_detail a
inner join vqq_rpt_weekofyear_2018 b on a.dt = b.dt
where (datediff(current_date,a.dt) <= 12*7 and log_creditProcess = 2) group by week;


-- 一周漏斗
use tmp_innofin;
create table if not exists  vqq_rpt_active_process_funnel_weekly(
	week string,
	step_name string,
	step_count int
)partitioned by (flow string);

use tmp_innofin;
insert overwrite table vqq_rpt_active_process_funnel_weekly partition (flow = '已实名需绑卡')
select week,
explode(map(
	'拿去花主动开通首页UV',page_apply,
	'立即开通btn',button_apply,
	'协议页下一步btn',button_contract,
	'支付绑卡引导页uv',page_cardguide,
	'支付绑卡页uv',page_card,
	'核实信息页uv',page_active,
	'核实信息页提交btn',button_active,
	'开通完成页uv',page_finish,
	'授信成功数',db_success
	)) as (step_name,step_count)
from (select * from tmp_innofin.vqq_rpt_active_process_flow_weekly where (flow = '已实名需绑卡') order by week desc limit 1) 
;

use tmp_innofin;
insert overwrite table vqq_rpt_active_process_funnel_weekly partition (flow = '未实名')
select week,
explode(map(
	'拿去花主动开通首页UV',page_apply,
	'立即开通btn',button_apply,
	'协议页下一步btn',button_contract,
	'支付绑卡引导页uv',page_cardguide,
	'支付绑卡页uv',page_card,
	'核实信息页uv',page_active,
	'核实信息页提交btn',button_active,
	'开通完成页uv',page_finish,
	'授信成功数',db_success
	)) as (step_name,step_count)
from (select * from tmp_innofin.vqq_rpt_active_process_flow_weekly where (flow = '未实名') order by week desc limit 1) 
;
use tmp_innofin;
insert overwrite table vqq_rpt_active_process_funnel_weekly partition (flow = '已实名无需绑卡')
select week,
explode(map(
	'拿去花主动开通首页UV',page_apply,
	'立即开通btn',button_apply,
	'协议页下一步btn',button_contract,
	'核实信息页uv',page_active,
	'核实信息页提交btn',button_active,
	'开通完成页uv',page_finish,
	'授信成功数',db_success
	)) as (step_name,step_count)
from (select * from tmp_innofin.vqq_rpt_active_process_flow_weekly where (flow = '已实名无需绑卡') order by week desc limit 1) 
;
use tmp_innofin;
insert overwrite table vqq_rpt_active_process_funnel_weekly partition (flow = '已实名一键开通')
select week,
explode(map(
	'拿去花主动开通首页UV',page_apply,
	'立即开通btn',button_apply,
	'协议页下一步btn',button_contract,
	'核实信息页uv',page_active,
	'核实信息页提交btn',button_active,
	'开通完成页uv',page_finish,
	'授信成功数',db_success
	)) as (step_name,step_count)
from (select * from tmp_innofin.vqq_rpt_active_process_flow_weekly where (flow = '已实名一键开通') order by week desc limit 1) 
;
