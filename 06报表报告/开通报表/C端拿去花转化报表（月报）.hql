-- 总表，
	-- 申请，走完流程，激活成功
-- 月报
use tmp_innofin;
create table if not exists vqq_rpt_ctrip_nqh_active_process_main_monthly(
	month string,
	apply int,
	finish int,
	success int
);

use tmp_innofin;
insert overwrite table vqq_rpt_ctrip_nqh_active_process_main_monthly
select substring(dt,1,7),sum(a.apply),sum(a.finish),sum(a.success) from 
vqq_rpt_ctrip_nqh_active_process_main a
where dt >= '2017-12-01'
group by substring(dt,1,7)
;

--ART
select month as `月份`,apply as `申请人数`,finish as `完成流程`,success as `激活成功`,
	cast(100*finish/apply as decimal(5,2)) as `完成流程%`,
	cast(100*success/finish as decimal(5,2)) as `激活成功%`,
	active_user_total as `累计激活`,cast(100*active_user_total/2760974 as decimal(5,2)) as `目标完成%`
	from tmp_innofin.vqq_rpt_ctrip_nqh_active_process_main_monthly a 
	inner join ods_innofin.nqh_user_active_total b on last_day(month) = b.dt
