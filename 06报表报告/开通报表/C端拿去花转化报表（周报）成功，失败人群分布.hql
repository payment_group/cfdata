-- 6)明细总表
use tmp_innofin;
create table if not exists vqq_rpt_active_process_detail (
	uid string,

	enter_ctrip int,
	enter_realname int,
	enter_wallet int,
	enter_more_func int,
	enter_others_channel int,
	
	page_apply	int,	
	page_cardguide	int,	
	page_card_number	int,	
	page_card	int,	
	page_setpass	int,	
	page_active	int,
	page_finish	int,	
	page_success	int,
	
	button_apply int,
	button_contract int,
	button_active int,

	db_active int,
	db_success int,	

	log_creditProcess int,
	log_existPwd int,
	log_hasMobile int,
	log_preCreditStatus int,
	log_realName int
	
) partitioned by (
	dt string
);

-- 成功人群
use tmp_innofin;
create table if not exists vqq_rpt_active_process_success_profile_weekly (
	uid string,
	tpp_code string,
	activate_amt decimal(9,2),
	amt_group string, 
	age int,
	age_group string, 
	a_score double,
	score_group string
);

use tmp_innofin;
insert overwrite table vqq_rpt_active_process_success_profile_weekly
select distinct a.uid,a.tpp_code,
	a.activate_amt,
	case when activate_amt >= 2000 and activate_amt < 5000 then '2k~5k'
		 when activate_amt >= 5000 and activate_amt < 10000 then '5k~10k'
		 when activate_amt >= 10000 and activate_amt < 15000 then '10k~15k'
		 when activate_amt >= 15000 and activate_amt < 20000 then '15k~20k'
		 else '20k+'
	end,
	floor(datediff(current_date,b.birthday)/365.24) as age, 
	case when floor(datediff(current_date,b.birthday)/365.24) > 0 and floor(datediff(current_date,b.birthday)/365.24) < 20 then '<20' 
		 when floor(datediff(current_date,b.birthday)/365.24) >= 20 and floor(datediff(current_date,b.birthday)/365.24) < 30 then '20~30'
		 when floor(datediff(current_date,b.birthday)/365.24) >= 30 and floor(datediff(current_date,b.birthday)/365.24) < 40 then '30~40'
		 when floor(datediff(current_date,b.birthday)/365.24) >= 40 and floor(datediff(current_date,b.birthday)/365.24) < 50 then '40~50'
		 when floor(datediff(current_date,b.birthday)/365.24) >= 50 and floor(datediff(current_date,b.birthday)/365.24) < 60 then '50~60'
		 when floor(datediff(current_date,b.birthday)/365.24) >= 60 and floor(datediff(current_date,b.birthday)/365.24) < 100 then '60+'
		 else 'unknow'
	end,
	0 as a_score,'' as score_group
from (select * from ods_innofin.nqh_user_active_date where active_date >= '${zdt.addDay(-7).format("yyyy-MM-dd")}') a
left join dw_app_user_profile_v1.c_corp_busi_cfbdb_output b on lower(a.uid) = lower(b.original_uid);

--- 失败人群
--- 


use tmp_innofin;
create table if not exists vqq_credit_fail_msg(
	error_code string,
	error_msg string
);
insert overwrite table vqq_credit_fail_msg values
('999999','系统异常'),
('G00024','未获取到征信报文'),
('000004','规则引擎计算错误'),
('100151','同一个用户ID不允许注册不同的身份证号码'),
('100152','身份证号码已经注册过'),
('700006','预授信被拒'),
('100153','USER_PRECREDIT_ACCESS_ERROR'),
('PRE_CREDIT_AMOUNT_E','授信额度异常'),
('200000','通道规则计算失败'),
('100148','该用户已经授信并激活'),
('G00005','风控检查失败'),
('IOUS8000011','授信中心不准入'),
('100149','综合授信拒绝'),
('G00007','高阳白条申请失败'),
('500017','服务器错误'),
('PRE_CREDIT_RATE_EXC','所有期数利率不合法'),
('QF600006','预授信准入被拒'),
('000020','缺少指标'),
('100062','无授信记录'),
('000022','指标数据错误:Q001025'),
('100063','个人信息不匹配'),
('999120','UNKNOWN_INVALID'),
('100002','userName is null'),
('100003','用户已经提交授信请求'),
('IOUS6000020','user loan amount is not bigger than 0 and set failed status'),
('000011','缺少指标'),
('000012','drools的计算结果中，没有需要的结果'),
('810001','用户已经提交授信请求'),
('810002','拒绝低分用户授信'),
('999997','查无此单(查不到相应的授信请求)'),
('100057','授信申请被拒绝'),
('999998','风控拒绝');

use tmp_innofin;
create table if not exists vqq_rpt_active_process_fail_profile_weekly (
	uid string,
	error_code string,
	error_msg string,
	error_msg_group string,
	a_score double,
	score_group string
);

use tmp_innofin;
insert overwrite table vqq_rpt_active_process_fail_profile_weekly
select distinct a.uid,c.error_code,c.error_msg,d.error_msg,
	0,'' 
from (select distinct uid from tmp_innofin.vqq_rpt_active_process_detail where dt >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' and page_finish = 1 and db_success = 0) a
inner join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id) 
left join (select distinct user_id as user_id,error_code,error_msg from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap  
		where to_date(finish_time)>= '${zdt.addDay(-7).format("yyyy-MM-dd")}'
		and product_no = 'IOUS' and org_channel = 'CTRIP') c on lower(b.open_id) = lower(c.user_id)
left join vqq_credit_fail_msg d on c.error_code = d.error_code
;

select uid from tmp_innofin.vqq_rpt_active_process_fail_profile_weekly where error_code is null


select uid,
	nvl(concat(error_code,'：',error_msg_group),'前决策未通过') as `原因`
from tmp_innofin.vqq_rpt_active_process_fail_profile_weekly


select a.uid,
	nvl(concat(error_code,'：',error_msg_group),'前决策未通过') as `原因`
from tmp_innofin.vqq_rpt_active_process_fail_profile_weekly 
inner join (select distinct uid from tmp_innofin.vqq_rpt_active_process_detail where dt >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' and log_creditProcess = 2) b
on lower(a.uid) = lower(b.uid)


