use ods_innofin;
create table if not exists vqq_rpt_active_process_settings(
	skey string
	svalue string
) partitioned by (
	sgroup string,
);
insert overwrite table ods_innofin.vqq_rpt_active_process_settings partition (sgroup = 'entrance') values
('我携','pagecode in \(10320607100,10320607163,10320663287,10320663286,10320670433,10320670431\) and prepagecode = \'myctrip_home\''),
('钱包', 'pagecode in \(\'10320607100\',\'10320607163\',\'10320663287\',\'10320663286\',\'10320670433\',\'10320670431\'\) and prepagecode in \(\'272040\',\'271022\'\)'),		--我的钱包进入开通页
('实名','pagecode in \(\'10320607100\',\'10320607163\',\'10320663287\',\'10320663286\',\'10320670433\',\'10320670431\'\) and prepagecode in \(\'600001514\',\'600001511\'\)'),		--实名认证进入开通页
('首页更多','pagecode in \(\'10320607100\',\'10320607163\',\'10320663287\',\'10320663286\',\'10320670433\',\'10320670431\'\) and prepagecode = \'210000\''),		--首页更多进入开通页
('其他', 'pagecode in \(\'10320607100\',\'10320607163\',\'10320663287\',\'10320663286\',\'10320670433\',\'10320670431\'\) and prepagecode not in \(\'10320643008\',\'10320642998\',\'10320657061\',\'272092\',\'10320643028\',\'0\',\'10320607112\',\'10320607172\',\'10320607113\',\'10320607173\',\'10320607111\',\'10320607171\',\'10320654471\',\'10320654472\',\'10320663291\',\'10320663289\',\'myctrip_home\',\'272040\',\'271022\',\'600001514\',\'600001511\',\'flight_lowprice_home_rn\',\'476001\',\'210000\',\'10320607163\',\'10320657064\',\'10320663286\'\)');

insert overwrite table ods_innofin.vqq_rpt_active_process_settings partition (sgroup = 'page') values
('激活首页','pagecode in \(\'10320607100\',\'10320607163\',\'10320663287\',\'10320663286\',\'10320670433\',\'10320670431\'\)'),
('绑卡引导页','pagecode in \(\'10320643008\',\'10320643007\'\) and query like \'%bustype=9907%\''),
('输入卡号页','pagecode in \(\'10320643027\',\'10320643028\'\)'),
('绑卡页','pagecode in \(\'10320642997\',\'10320642998\'\)'),
('密码设置页','pagecode in \(\'271074\',\'272092\'\)'),
('提交页','pagecode in \(\'10320657063\',\'10320657064\'\)'),
('完成页','pagecode in \(\'10320607112\',\'10320607172\',\'10320607113\',\'10320607173\',\'10320607111\',\'10320607171\'\) and prepagecode in \(\'10320657063\',\'10320657064\',\'10320637265\',\'10320637264\',\'10320660457\',\'10320660458\',\'10320668195\'\)'),
('激活成功页','pagecode in \(\'10320607113\',\'10320607173\'\)');

-- table 

use ods_innofin;
create table vqq_rpt_active_process_ubt(
	uid	string comment '用户登录帐号'
) partitioned by (
	dt string,
	sgroup string,
	skey string
);



-- sh
hive -e "set hive.cli.print.header=false;select sgroup,skey,svalue from ods_innofin.vqq_rpt_active_process_settings where sgroup <> ''">/home/innofin/export_vqq_ubt.txt
#curl --ftp-ssl -k -T /home/innofin/export_vqq_ubt.txt ftp://data_ctrip_cf:qX3yHHc0CFoovsn9ynjT@dataexchange.ctripcorp.com:53233/xfjr_sbu/ctrip/uids/

while IFS='' read -r line || [[ -n "$line" ]]; do
    #echo "Text read from file: $line"
    IFS=$'\t';array=($line)
    echo "insert overwrite table ods_innofin.vqq_rpt_active_process_ubt partition(dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}',sgroup='${array[0]}',skey='${array[1]}')
select distinct uid from dw_mobdb.factmbpageview 
where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
and ${array[2]};
"
done < /home/innofin/export_vqq_ubt.txt
