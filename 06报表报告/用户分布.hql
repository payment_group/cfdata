-- 
use tmp_innofin;
create table if not exists vqq_rpt_user_distribution_tag(
	user_id string
)partitioned  by (
	user_tag string
);



-- credit 已授信用户 
insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'credit')
select distinct user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS';
-- credit.user 主动申请 = 激活有记录+授信来源1，且失败的 credit_user
insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'credit_user')
select distinct user_id from 
(select user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS') a 
inner join (select open_id from ods_innofin.user_contract where req_src <> 'PRE_CREDIT_TASK') b on a.user_id = b.open_id;
--1.2 预授信 除1.1外 credit.task
-- insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag (user_tag = 'task')
-- select distinct a.user_id from 
-- (select user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS') a 
-- left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where  user_tag = 'user') b on a.user_id = b.user_id
-- where b.user_id is null;

-- credit.user.success 授信成功 credit_success
insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'credit_success')
select distinct user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1; 
-- credit.user.fail 主动申请授信失败(1.1-1.1.1) credit_fail
-- insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag (user_tag = 'credit_fail')
-- select distinct a.user_id from 
-- (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where  user_tag = 'credit') a 
-- left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where  user_tag = 'credit_success') b on a.user_id = b.user_id
-- where b.user_id is null;
-- 
-- active_success 激活成功 credit_user_success-active
insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'active')
select distinct user_id from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap where org_channel='CTRIP' and product_no='IOUS';

insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'active_success')
select distinct user_id from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap where org_channel='CTRIP' and product_no='IOUS' and activate_status = 2;

-- active_fail 主动申请授信成功-激活失败 
-- insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag (user_tag = 'active_fail')
-- select distinct a.user_id 
-- from (select user_id from fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap where org_channel='CTRIP' and product_no='IOUS') a
-- left join (select user_if from tmp_innofin.vqq_rpt_user_distribution_tag where  user_tag = 'active_success')  b on a.user_id = b.user_id
-- where b.user_id is null;
-- 
-- 授信可用
insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'credit_valid')
select distinct a.user_id 
from (select user_id from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and end_eff_time >= '${zdt.format("yyyy-MM-dd")}') a;
-- 授信过期
-- insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag (user_tag = 'credit_invalid')
-- select distinct a.user_id 
-- from (select user_id from fin_basic_data.fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS') a 
-- left join (select user_id from fin_basic_data.fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and end_eff_time >= '${zdt.format("yyyy-MM-dd")}') b
-- on a.user_id = b.user_id where b.user_id is null;
-- 
-- loan_order
insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'loan_order')
select distinct user_id from dw_subject_finance.a_xj_busi_pay_ious_tbl_ious_auth_apply_snap where org_channel='CTRIP' and product_no='IOUS';

-- extend_term
insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'extend_term')
select distinct user_id from dw_subject_finance.a_xj_busi_pay_ious_tbl_ious_auth_apply_snap where org_channel='CTRIP' and product_no='IOUS';

insert overwrite table tmp_innofin.vqq_rpt_user_distribution_tag partition(user_tag = 'extend_term_success')
select distinct user_id from dw_subject_finance.a_xj_busi_pay_ious_tbl_ious_auth_apply_snap where org_channel='CTRIP' and product_no='IOUS' and status = 2;

use tmp_innofin;
create table if not exists vqq_rpt_user_distribution(
	user_count int
)partitioned  by (
	user_group string,
	dt string
);

alter table tmp_innofin.vqq_rpt_user_distribution drop if exists partition  (dt = '${zdt.format("yyyy-MM-dd")}');

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信')
select count(1) from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit';

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-主动申请')
select count(1) from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user';

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-预授信')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit') a
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') b
on a.user_id = b.user_id where b.user_id is null
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-主动申请-授信成功')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') a
inner join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_success') b
on a.user_id = b.user_id
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-主动申请-授信失败')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') a
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_success') b
on a.user_id = b.user_id where b.user_id is null
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-主动申请-授信成功-激活成功')
select count(distinct user_id) from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'active_success';
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-主动申请-授信成功-激活失败')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'active') a
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'active_success') b on a.user_id = b.user_id
where b.user_id is null
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-主动申请-授信成功-激活成功-已用信')
select count(distinct user_id) from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'loan_order';
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-主动申请-授信成功-激活成功-未用信')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'active_success') a 
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'loan_order') b on a.user_id = b.user_id
where b.user_id is null
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-预授信-授信成功')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit') a
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') b on a.user_id = b.user_id 
inner join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_success') c on a.user_id = c.user_id 
where b.user_id is null
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-预授信-授信失败')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit') a
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') b on a.user_id = b.user_id 
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_success') c on a.user_id = c.user_id 
where b.user_id is null and c.user_id is null
;

-- insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-预授信-授信成功-激活成功')
-- select count(distinct a.user_id) from 
-- (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit') a
-- left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') b on a.user_id = b.user_id 
-- inner join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'active_success') c on a.user_id = c.user_id 
-- where b.user_id is null
-- ;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-预授信-授信成功-未过期')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit') a
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') b on a.user_id = b.user_id 
inner join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_success') c on a.user_id = c.user_id 
inner join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_valid') d on a.user_id = d.user_id 
where b.user_id is null
;

insert overwrite table tmp_innofin.vqq_rpt_user_distribution partition (dt = '${zdt.format("yyyy-MM-dd")}',user_group = '已授信-预授信-授信成功-已过期')
select count(distinct a.user_id) from 
(select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit') a
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_user') b on a.user_id = b.user_id 
inner join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_success') c on a.user_id = c.user_id 
left join (select user_id from tmp_innofin.vqq_rpt_user_distribution_tag where user_tag = 'credit_valid') d on a.user_id = d.user_id 
where b.user_id is null and d.user_id is null;


select * from tmp_innofin.vqq_rpt_user_distribution where (dt = '${zdt.format("yyyy-MM-dd")}') order by user_group;

-- 没有通道可用 且 展期失败

