-- 6)总表
use tmp_innofin;
create table if not exists vqq_rpt_active_process_detail (
	uid string,

	enter_ctrip int,
	enter_realname int,
	enter_wallet int,
	enter_more_func int,
	enter_others_channel int,
	
	page_apply	int,	
	page_cardguide	int,	
	page_card_number	int,	
	page_card	int,	
	page_setpass	int,	
	page_active	int,
	page_finish	int,	
	page_success	int,
	
	button_apply int,
	button_contract int,
	button_active int,

	db_active int,
	db_success int,	

	log_creditProcess int,
	log_existPwd int,
	log_hasMobile int,
	log_preCreditStatus int,
	log_realName int
	
) partitioned by (
	dt string
);
-- ART
select dt as `日期`,
	sum(page_apply) as `拿去花主动开通首页UV`,
	sum(db_active) as `完成流程数`,
	100*sum(db_active)/sum(page_apply) as	`页面开通率%`
from tmp_innofin.vqq_rpt_active_process_detail where dt >= date_add(current_date,-30) group by dt
