-- 每周一统计
-- 上周一到周日数据

use tmp_innofin;
create table if not exists vqq_qa_entry_cr(
	date_from string,
	date_to string,
	entry_type string,
	entry int,
	apply int,
	finish int,
	success int
) partitioned by (dt string comment '计算日');

insert overwrite table tmp_innofin.vqq_qa_entry_cr partition (dt = '${zdt.format("yyyy-MM-dd")}')
select 
	'${zdt.addDay(-7).format("yyyy-MM-dd")}' as date_from, 
	'${zdt.addDay(-1).format("yyyy-MM-dd")}' as date_to,
	a.type as entry_type,
	a.num as entry,
	b.apply,
	b.finish,
	b.success
from 
	(select '我携' as type,count(distinct uid) as num
		from tmp_innofin.wqm_process_basic02 
		where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and new_ctrip = 1
	union all
		select '钱包' as type,count(distinct uid) as num
		from tmp_innofin.wqm_process_basic02 
		where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and new_wallet = 1
	union all
		select '实名' as type, count(distinct uid) as num
		from tmp_innofin.wqm_process_basic02 
		where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and new_realname = 1
	union all
		select '首页更多' as type,count(distinct uid) as num
		from tmp_innofin.wqm_process_basic02 
		where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and new_morefunc = 1
	union all
		select '其他' as type,count(distinct uid) as num
		from tmp_innofin.wqm_process_basic02 
		where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and others = 1
	) a
join
	(
	select '我携' as type,
		count(1) as apply, sum(finish) as finish,sum(success) as success
		from 
		(select uid,max(finish) as finish, max(success) as success from tmp_innofin.apply_process_detail 
			where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and ctrip= 1 group by uid) a
	union all
		select '钱包' as type,
		count(1) as apply, sum(finish) as finish,sum(success) as success
		from
		(select uid,max(finish) as finish, max(success) as success from tmp_innofin.apply_process_detail 
			where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and wallet= 1 group by uid) a
	union all
		select '实名' as type,
		count(1) as apply, sum(finish) as finish,sum(success) as success
		from
		(select uid,max(finish) as finish, max(success) as success from tmp_innofin.apply_process_detail 
			where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname= 1 group by uid) a
	union all
		select '首页更多' as type,
		count(1) as apply, sum(finish) as finish,sum(success) as success
		from
		(select uid,max(finish) as finish, max(success) as success from tmp_innofin.apply_process_detail 
			where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and more_func= 1 group by uid) a
	union all
		select '其他' as type,
		count(1) as apply, sum(finish) as finish,sum(success) as success
		from
		(select uid,max(finish) as finish, max(success) as success from tmp_innofin.apply_process_detail 
			where d between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' and others_channel= 1 group by uid) a
	) b on a.type=b.type;

-- report
select 
	date_from as `起始日`,
	date_to as `终了日`,
	entry_type as `入口`,
	entry as `入口新用户`,
	apply as `申请人数`,
	finish as `完成流程人数`,
	success as `成功人数`,
    cast(100 * apply / entry as decimal(18,2)) as `入口转化率(%)`,
    cast(100 * finish / apply as decimal(18,2)) as `完成流程转化率(%)`,
    cast(100 * success / apply as decimal(18,2)) as `开通成功转化率(%)`
from
tmp_innofin.vqq_qa_entry_cr where dt = '${zdt.format("yyyy-MM-dd")}';

-- 趋势

select 