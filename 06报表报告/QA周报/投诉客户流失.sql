use tmp_innofin;
create table if not exists vqq_qa_user_last_complaint(
	id int,
	event_date string,
	uid string,
	complaint_id string,
	event_status string
);

insert overwrite table tmp_innofin.vqq_qa_user_last_complaint
select id,event_date,uid,complaint_id,event_status from
(select id,event_date,uid,complaint_id,event_status,row_number() over (partition by uid order by event_date desc) as rank from tmp_innofin.vqq_qa_customer_complaint_uid) a 
where a.rank = 1;

use tmp_innofin;
create table if not exists vqq_qa_complaint_user_orders(
	uid string,
	last_event_date string,
	order_id string
);
insert overwrite table tmp_innofin.vqq_qa_complaint_user_orders
select a.uid,event_date,b.orderid
from tmp_innofin.vqq_qa_user_last_complaint a 
inner join temp_travelmoneydb.tmp_qmwang_nqhorders b on lower(a.uid) = lower(b.uid)
where b.orderdate > a.event_date;

use tmp_innofin;
create table if not exists  vqq_qa_complaint_user_nqh_orders(
	uid string,
	last_event_date string,
	order_id string
);
insert overwrite table tmp_innofin.vqq_qa_complaint_user_nqh_orders
select a.uid,event_date,b.orderid
from tmp_innofin.vqq_qa_user_last_complaint a 
inner join dw_paypubdb.factpaymentorder_loanpay b on lower(a.uid) = lower(b.uid)
where b.creationdate>a.event_date;

use tmp_innofin;
create table if not exists  vqq_qa_complaint_user_lost(
	uid string,
	last_event_date string,
	order_count int,
	nqh_order_count int 
);
insert overwrite table tmp_innofin.vqq_qa_complaint_user_lost
select a.uid,event_date, nvl(order_count,0), nvl(nqh_order_count,0)
from tmp_innofin.vqq_qa_user_last_complaint a 
left join (select uid,count(1) as order_count from tmp_innofin.vqq_qa_complaint_user_orders group by uid ) b on  lower(a.uid) = lower(b.uid)
left join (select uid,count(1) as nqh_order_count from tmp_innofin.vqq_qa_complaint_user_nqh_orders group by uid ) c on  lower(a.uid) = lower(c.uid);

--投诉用户，其中三个月以上，之后有订单，之后无拿去花订单
select total_3m as `投诉超过3个月`, total_3m-with_order as `无携程订单`,  with_order as `有携程订单`,no_nqh_order as `有携程订单但无拿去花订单`,
(total_3m-with_order)/total_3m as `携程流失`,no_nqh_order/total_3m as `拿去花`
from(
select count(1) as total,
	sum(if(last_event_date <= add_months(current_date,-3),1,0)) as total_3m,
	sum(case when last_event_date <= add_months(current_date,-3) and order_count > 0 then 1 else 0 end) as with_order,
	sum(case when last_event_date <= add_months(current_date,-3) and order_count > 0 and nqh_order_count = 0 then 1 else 0 end) as no_nqh_order
from tmp_innofin.vqq_qa_complaint_user_lost) a;