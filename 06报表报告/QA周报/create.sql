use tmp_innofin;
drop table vqq_qa_customer_service;
create table vqq_qa_customer_service(
	date_from string,
	date_to string,
	usage int,
	repay int,
	active int,
	others int,
	refund int
);

use tmp_innofin;
drop table vqq_qa_customer_complaint;
create table vqq_qa_customer_complaint(
	date_from string,
	date_to string,
	user int,
	usage int,
	repay int,
	support int,
	collection int,
	risk int,
	others int
);

--- Ͷ��uid
use tmp_innofin;
drop table vqq_qa_customer_complaint_uid;
create table vqq_qa_customer_complaint_uid(
	id int,
	project string,
	event_date string,
	uid string,
	category string,
	operator string,
	source string,
	complaint_id string,
	event_status string
);
