use tmp_innofin;
create table if not exists vqq_pmo_issues(
	id	string  comment '关键字',
	summary	string  comment '主题',
	status	string  comment '状态',
	pm	string  comment '产品经理',
	fe	string  comment 'FE',
	dev	string  comment 'DEV',
	qa	string  comment 'QA',
	startdate_p	string  comment '计划开始日期',
	startdate_a	string  comment '实际开始日期',
	commitdate_p	string  comment '计划提测日期',
	commitdate_a	string  comment '实际提测日期',
	testdate_p	string  comment '计划测试开始日期',
	testdate_a	string  comment '实际测试开始日期',
	publishdate_p	string  comment '计划发布日期',
	publishtime_a	string  comment '实际发布时间'
) partitioned by (dt string comment '状态日')
;

insert into table  tmp_innofin.vqq_pmo_issues select '{id}','{summary}','{status}','{pm}','{fe}','{dev}','{qa}','{startdate_p}','{startdate_a}','{commitdate_p}','{commitdate_a}','{testdate_p}','{testdate_a}','{publishdate_p}','{publishtime_a}';
-- 0828增加备注
use tmp_innofin;
drop table tmp_innofin.vqq_pmo_issues_remark;
create table if not exists vqq_pmo_issues_remark(
	id	string  comment '关键字',
	remark	string  comment '备注',
	long_term int
) partitioned by (dt string comment '状态日')
;

insert overwrite table tmp_innofin.vqq_pmo_issues_remark partition (dt='${zdt.format("yyyy-MM-dd")}')
	select 'FINANCE-2294','',1;
insert into table tmp_innofin.vqq_pmo_issues_remark partition (dt='${zdt.format("yyyy-MM-dd")}')
	select 'FINANCE-2086','',1;
-- ART 日报
select a.id	as `PMO编号`,
	summary	as `主题`,
	status	as `状态`,
	pm	as `产品经理`,
	fe	as `FE`,
	dev	as `DEV`,
	qa	as `QA`,
	startdate_p	as `计划开始日期`,
	startdate_a	as `实际开始日期`,
	commitdate_p	as `计划提测日期`,
	commitdate_a	as `实际提测日期`,
	publishdate_p	as `计划发布日期`,
	substring(publishtime_a,1,10)	as `实际发布日期`,
	case when status in ('暂停','已关闭') then '' 
		when commitdate_p <> '' and commitdate_p < #TODAY# and commitdate_a = '' then '提测DELAY' 
		when publishdate_p <> '' and publishdate_p < #TODAY# and publishtime_a = '' then '发布DELAY' else '' end as `DELAY`,
	nvl(b.remark,nvl(c.remark,'')) as `备注`
from tmp_innofin.vqq_pmo_issues a
left join (select * from tmp_innofin.vqq_pmo_issues_remark where long_term = 0) b on a.id = b.id and a.dt = b.dt
left join (select * from (select *,rank() over (partition by id order by dt desc) as rank from tmp_innofin.vqq_pmo_issues_remark where dt <= #TODAY# and long_term = 1)
	 d where d.rank = 1) c on a.id = c.id
where a.dt = #TODAY# order by `状态`,`PMO编号` limit 50
-- 0830 合并状态和变动
-- 一周状态进展
-- select a.id as `PMO编号`, a.summary 	as `主题`, e.status as `周一`,d.status as `周二`,c.status as `周三`,b.status as `周四`,a.status as `周五`
-- from (select id,summary,status from tmp_innofin.vqq_pmo_issues where dt = date_add(#TODAY#,-3)) a
-- left join (select id,summary,status from tmp_innofin.vqq_pmo_issues where dt = date_add(#TODAY#,-4)) b on a.id = b.id
-- left join (select id,summary,status from tmp_innofin.vqq_pmo_issues where dt = date_add(#TODAY#,-5)) c on a.id = c.id
-- left join (select id,summary,status from tmp_innofin.vqq_pmo_issues where dt = date_add(#TODAY#,-6)) d on a.id = d.id
-- left join (select id,summary,status from tmp_innofin.vqq_pmo_issues where dt = date_add(#TODAY#,-7)) e on a.id = e.id
-- order by `PMO编号` limit 50
-- 一周计划变动
use tmp_innofin;
drop table tmp_innofin.vqq_pmo_issues_change;
create table if not exists vqq_pmo_issues_change(
	id	string  comment '关键字',
	change	string
) partitioned by (dt string comment '状态日',field string)
;

-- Tuesday--Friday
insert overwrite table tmp_innofin.vqq_pmo_issues_change partition (dt='${zdt.format("yyyy-MM-dd")}',field='commitdate_p')
select a.id, if(b.commitdate_p = '',concat('计划提测日期:',a.commitdate_p), concat('计划提测日期变更:',b.commitdate_p,'~',a.commitdate_p))
from (select id,commitdate_p,dt from tmp_innofin.vqq_pmo_issues where dt='${zdt.format("yyyy-MM-dd")}') a
inner join (select id,commitdate_p from tmp_innofin.vqq_pmo_issues where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}') b on a.id = b.id
where a.commitdate_p <> b.commitdate_p;

insert into table tmp_innofin.vqq_pmo_issues_change partition (dt='${zdt.format("yyyy-MM-dd")}',field='publishdate_p')
select a.id, if(b.publishdate_p = '',concat('计划发布日期:',a.publishdate_p), concat('计划发布日期变更:',b.publishdate_p,'~',a.publishdate_p))
from (select id,publishdate_p from tmp_innofin.vqq_pmo_issues where dt='${zdt.format("yyyy-MM-dd")}') a
inner join (select id,publishdate_p from tmp_innofin.vqq_pmo_issues where dt ='${zdt.addDay(-1).format("yyyy-MM-dd")}') b on a.id = b.id
where a.publishdate_p <> b.publishdate_p;

-- Monday
insert overwrite table tmp_innofin.vqq_pmo_issues_change partition (dt='${zdt.format("yyyy-MM-dd")}',field='commitdate_p')
select a.id, if(b.commitdate_p = '',concat('计划提测日期:',a.commitdate_p), concat('计划提测日期变更:',b.commitdate_p,'~',a.commitdate_p))
from (select id,commitdate_p from tmp_innofin.vqq_pmo_issues where dt='${zdt.format("yyyy-MM-dd")}') a
inner join (select id,commitdate_p from tmp_innofin.vqq_pmo_issues where dt ='${zdt.addDay(-3).format("yyyy-MM-dd")}') b on a.id = b.id
where a.commitdate_p <> b.commitdate_p;

insert into table tmp_innofin.vqq_pmo_issues_change partition (dt='${zdt.format("yyyy-MM-dd")}',field='publishdate_p')
select a.id, if(b.publishdate_p = '',concat('计划发布日期:',a.publishdate_p), concat('计划发布日期变更:',b.publishdate_p,'~',a.publishdate_p))
from (select id,publishdate_p from tmp_innofin.vqq_pmo_issues where dt='${zdt.format("yyyy-MM-dd")}') a
inner join (select id,publishdate_p from tmp_innofin.vqq_pmo_issues where dt ='${zdt.addDay(-3).format("yyyy-MM-dd")}') b on a.id = b.id
where a.publishdate_p <> b.publishdate_p;

-- every day
-- 增加状态和DELAY
insert overwrite table tmp_innofin.vqq_pmo_issues_change partition (dt='${zdt.format("yyyy-MM-dd")}',field='status')
select a.id,concat(a.status, case when status in ('暂停','已关闭') then '' 
		when commitdate_p <> '' and commitdate_p < '${zdt.format("yyyy-MM-dd")}' and commitdate_a = '' then '(提测DELAY)' 
		when publishdate_p <> '' and publishdate_p < '${zdt.format("yyyy-MM-dd")}' and publishtime_a = '' then '(发布DELAY)' else '' end)
from tmp_innofin.vqq_pmo_issues a where a.dt = '${zdt.format("yyyy-MM-dd")}';

insert overwrite table tmp_innofin.vqq_pmo_issues_change partition (dt='${zdt.format("yyyy-MM-dd")}',field='all')
select a.id,concat(a.change,if(b.change is null,'',concat('<br/>',b.change)),if(c.change is null,'',concat('<br/>',c.change))) from
(select id, change from  tmp_innofin.vqq_pmo_issues_change where dt='${zdt.format("yyyy-MM-dd")}' and field='status') a 
left join (select id, change from  tmp_innofin.vqq_pmo_issues_change where dt='${zdt.format("yyyy-MM-dd")}' and field='commitdate_p') b on a.id = b.id 
left join (select id, change from  tmp_innofin.vqq_pmo_issues_change where dt='${zdt.format("yyyy-MM-dd")}' and field='publishdate_p') c on a.id = c.id 
;

--ART
select a.id as `PMO编号`, a.summary as `主题`, nvl(e.change,'') as `周一`,nvl(d.change,'') as `周二`,nvl(c.change,'') as `周三`,nvl(b.change,'') as `周四`,nvl(f.change,'') as `周五`
from (select id,summary from tmp_innofin.vqq_pmo_issues where dt = date_add(#TODAY#,-3)) a
left join (select id,change from tmp_innofin.vqq_pmo_issues_change where dt = date_add(#TODAY#,-3) and field='all') f on a.id = f.id
left join (select id,change from tmp_innofin.vqq_pmo_issues_change where dt = date_add(#TODAY#,-4) and field='all') b on a.id = b.id
left join (select id,change from tmp_innofin.vqq_pmo_issues_change where dt = date_add(#TODAY#,-5) and field='all') c on a.id = c.id
left join (select id,change from tmp_innofin.vqq_pmo_issues_change where dt = date_add(#TODAY#,-6) and field='all') d on a.id = d.id
left join (select id,change from tmp_innofin.vqq_pmo_issues_change where dt = date_add(#TODAY#,-7) and field='all') e on a.id = e.id
order by `PMO编号` limit 50





