use tmp_innofin;
create table if not exists vqq_rpt_collection_performance_agent (
	company_code string comment 'company_code',
	repay_date string comment '还款日期',
	operator_id string comment '工号',
	username string comment '姓名',
    repay_amount decimal(18,2) comment '还款金额'
) partitioned by (
	dt string comment '报表日期'
);

insert overwrite table vqq_rpt_collection_performance_agent partition(dt='${zdt.format("yyyy-MM-dd")}')
select a.company_code,
       to_date(a.update_time),
       a.current_operater,
       b.username,
       sum(a.total_collected_amt - a.total_overdue_amt)
from ods_innofin.tbl_case_info a  
left join ods_innofin.sys_user b on a.current_operater= b.account
where a.company_code in ('CtripInner') and datediff(to_date(a.update_time),to_date(a.create_time))<=30 and collection_status=3 and datediff(current_date,to_date(a.update_time))<=30
group by a.company_code,to_date(a.update_time),a.current_operater,b.username;



-----------------
select 
	company_code as `company_code`,
	create_time as `入催日期`,
	operator_id as `工号`,
	username as `姓名`,
	org_channel as `通道方`,
	product_no as `产品`,
	case_count as `当日委案量`,
	case_left as `当日未处理量`,
	case_called as `当日处理案件数`,
	case_repay as `还款案件数`,
	case_repay_rate as `案件回收率%`,
	case_amount as `当日委案金额`,
    repay_amount as `还款金额`,
    reay_amount_rate as `金额回收率%`,
    call_count as `当日拨打数量`,
    connection_count as `接通数量`,
    connection_rate as `接通率%`,
    call_rate as `拨打频次`,
    call_seconds as `拨打时长-秒`
from tmp_innofin.vqq_rpt_collection_performance_data where dt = current_date

