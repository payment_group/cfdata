-- 对账结算单，
-- 还款明细
use tmp_innofin;
alter table vqq_rpt_DUMIAO_statement rename to vqq_rpt_DUMIAO_statement_bk;
use tmp_innofin;
create table if not exists vqq_rpt_DUMIAO_statement(
	product_no	string 	comment '产品编码',
	loan_provide_no string				comment  '借据号',
	repayment_date string,
	due_date string                    comment  '委案时间',
	loan_channel string                comment  '放款渠道',
	user_id string comment 'user_id', 
	open_id string comment 'open_id', 
	user_name string                   comment  '姓名',
	period int                         comment  '逾期期数',
	capital_amt decimal(15,2)          comment  '委案本金',
	interest_amt decimal(15,2)         comment  '委案息费',
	overdue_interest_amt decimal(15,2) comment  '委案违约',
	overdue_days int                   comment  '逾期天数',
	overdue_months string              comment '逾期阶段',
	actual_repay_amt decimal(15,2)      comment  '实际回款',
	actual_repayment_date string        comment  '回款时间'
) comment '读秒委托'
partitioned by (                       
	dt string comment 'dt'
);

-- 历史数据迁移
use tmp_innofin;
insert overwrite table tmp_innofin.vqq_rpt_DUMIAO_statement partition (dt)
select a.product_no,
	b.loan_provide_no,
	repayment_date,
	due_date,
	loan_channel,
	user_id,
	open_id,
	user_name,
	period,
	capital_amt,
	interest_amt,
	overdue_interest_amt,
	overdue_days,
	overdue_months,
	actual_repay_amt,
	actual_repayment_date,
	dt
from
(select distinct loan_provide_no,product_no from  ods_innofin.tbl_case_details) a 
inner join vqq_rpt_DUMIAO_statement_bk b on a.loan_provide_no = b.loan_provide_no;

use tmp_innofin;
alter table vqq_tmp_DUMIAO_statement_first_overdue rename to vqq_tmp_DUMIAO_statement_first_overdue_bk;
-- 最早逾期  
use tmp_innofin;
create table if not exists vqq_tmp_DUMIAO_statement_first_overdue(
	product_no	string 	comment '产品编码',
	loan_provide_no string		comment  '借据号',
	period int                  comment  '逾期期数',
	repayment_date string 		comment '到期日',
	actual_repayment_date string 		comment '实际支付日',
	due_date string 			comment '逾期日'
) comment '读秒委托对账单-最早逾期日'
partitioned by (                       
	dt string comment 'dt'
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_tmp_DUMIAO_statement_first_overdue partition (dt)
select a.product_no,
	b.loan_provide_no,
	period,
	repayment_date,
	actual_repayment_date,
	due_date,
	dt
from
(select distinct loan_provide_no,product_no from  ods_innofin.tbl_case_details) a 
inner join vqq_tmp_DUMIAO_statement_first_overdue_bk b on a.loan_provide_no = b.loan_provide_no;

-- 修改job
insert overwrite table tmp_innofin.vqq_tmp_DUMIAO_statement_first_overdue partition (dt = '${zdt.format("yyyy-MM-dd")}')
select product_no,loan_provide_no,period,repayment_date,actual_repayment_date,date_add(repayment_date,1) as due_date from (
select b.product_no,a.loan_provide_no,period,repayment_date,actual_repayment_date,row_number() over (partition by a.loan_provide_no,to_date(a.actual_repayment_date) order by period) as row_no 
from ods_innofin.tbl_repayment_plan a
inner join (select distinct case_no,loan_provide_no,product_no from ods_innofin.tbl_case_details) b on a.loan_provide_no = b.loan_provide_no
inner join (select distinct case_no,case_type from ods_innofin.tbl_case_info) c on b.case_no = c.case_no and c.case_type = 'APPOINT'
where to_date(a.actual_repayment_date) > to_date(a.repayment_date) 
) t where t.row_no = 1;

-- 逾期 --出催
insert overwrite table tmp_innofin.vqq_rpt_DUMIAO_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct b.product_no,a.loan_provide_no,a.repayment_date,
	b.due_date,
	'读秒委托' as loan_channel,
	d.user_id,
	d.open_id,
	c.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)),
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select product_no,loan_provide_no,actual_repayment_date,due_date from tmp_innofin.vqq_tmp_DUMIAO_statement_first_overdue where dt =  '${zdt.format("yyyy-MM-dd")}') b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
inner join ods_innofin.tbl_overdue_loan d on a.loan_provide_no = d.loan_provide_no
inner join (select distinct user_id,user_name from ods_innofin.user_info) c on d.user_id = c.user_id
where a.status = 2 and to_date(a.actual_repayment_date) > to_date(a.repayment_date) and to_date(a.actual_repayment_date) >= add_months( '${zdt.format("yyyy-MM-dd")}',-2);
-- 未逾期，同日还款
insert into table tmp_innofin.vqq_rpt_DUMIAO_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct b.product_no,a.loan_provide_no,a.repayment_date,
	b.due_date,
	'读秒委托' as loan_channel,
	a.user_id,
	b.open_id,
	b.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1 as overdue_days,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)) as overdue_months,
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt as actual_repay_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select distinct product_no,loan_provide_no,user_id,open_id,user_name,due_date,to_date(actual_repayment_date) as actual_repayment_date from tmp_innofin.vqq_rpt_DUMIAO_statement where dt =  '${zdt.format("yyyy-MM-dd")}') b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
where a.status = 2 and to_date(a.actual_repayment_date) <= to_date(a.repayment_date) 
and to_date(a.actual_repayment_date) >= add_months( '${zdt.format("yyyy-MM-dd")}',-2) and to_date(a.actual_repayment_date) >= b.due_date;
