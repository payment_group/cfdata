use tmp_innofin;
create table if not exists vqq_rpt_loan_cancel(
	loan_provide_no string comment '借据号',
	user_id string comment'UID',
	user_name string comment'姓名',
	org_channel string comment'平台',
	tpp_code string comment'产品',
	capital_amt decimal(18,2) comment'本金',
	interest_amt decimal(18,2) comment'利息',
	overdue_interest_amt decimal(18,2) comment'罚息',
	overdue_days int comment'逾期天数',
	cancel_reason string comment '核销原因',
	remark string comment '催记备注'
);

insert overwrite table vqq_rpt_loan_cancel
select 
loan_provide_no ,
a.user_id ,
b.user_name ,
a.org_channel ,
a.tpp_code ,
a.loan_amt ,
0,
0,
datediff('2018-03-31',due_date) ,
'1.逾期超360天' ,
'' 
from ods_innofin.tbl_overdue_loan a 
left join (select distinct user_id,user_name from fin_basic_data.a_xj_busi_pay_ious_tbl_ious_user_info_snap) b on lower(a.user_id) = lower(b.user_id)
where datediff('2018-03-31',a.due_date) > 360 and a.tpp_code in ('CQLOAN','CQCASH') and a.loan_status = 1;

insert into table vqq_rpt_loan_cancel
select 
a.loan_provide_no ,
a.user_id ,
c.user_name ,
a.org_channel ,
a.tpp_code ,
b.capital_amt ,
b.interest_amt ,
b.overdue_interest_amt ,
datediff('2018-03-31',due_date) ,
'3.死亡' ,
'' 
from (select loan_provide_no,user_id,org_channel,tpp_code,due_date from ods_innofin.tbl_overdue_loan where user_id in ('176149438','147979258','131319260') 
		and tpp_code in('CQCASH','CQLOAN','SHLOAN') and loan_status = 1) a 
inner join (select loan_provide_no,
		sum(capital_amt-actual_capital_amt) as capital_amt, 
		sum(interest_amt-actual_interest_amt) as interest_amt, 
		sum(overdue_interest_amt-actual_overdue_interest_amt) as overdue_interest_amt
		from ods_innofin.tbl_repayment_plan where user_id in ('176149438','147979258','131319260') group by loan_provide_no) b
	 on a.loan_provide_no = b.loan_provide_no
left join (select distinct user_id,user_name from fin_basic_data.a_xj_busi_pay_ious_tbl_ious_user_info_snap) c on lower(a.user_id) = lower(c.user_id);
