use tmp_innofin;
create table if not exists vqq_collection_delegate_2m_hd(
	delegate_name string,
	report_date string,
	create_date string,
	tpp_code string,
	  org_channel string,
	  init_count int,
	  ok_count int,
	  stop_count int,
	  init_amount decimal(9,2),
	  repay_amount decimal(9,2),
	  user_rate decimal(5,2),
	  amount_rate decimal(5,2)
);

--����
use tmp_innofin;
insert overwrite table vqq_collection_delegate_2m_hd
select '����',	current_date
       ,to_date(create_time)
       ,b.tpp_code
       ,b.org_channel
       ,count(1)
       ,sum(case when a.collection_status=3 then 1 else 0 end)
       ,0
       ,sum(a.total_collected_amt)
       ,sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end)
       ,sum(case when a.collection_status=3 then 1.0 else 0 end)/count(1)*100.0
       ,sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end)/sum(a.total_collected_amt)*100
from ods_innofin.tbl_case_info a  
   join (select case_no,max(tpp_code) as tpp_code,max(org_channel) as org_channel
         from ods_innofin.tbl_case_details 
         group by case_no) b on a.case_no=b.case_no
where a.company_code in ('QCQLOAN','QSHLOAN','QCQCASH','QJIMULOAN','VirHuaDao') and collection_type =2  and to_date(a.create_time) >= add_months(current_date,-2) 
group by to_date(create_time),b.tpp_code,b.org_channel;
-- ��̭
use tmp_innofin;
create table if not exists vqq_collection_delegate_2m_jt(
	delegate_name string,
	report_date string,
	create_date string,
	tpp_code string,
	  org_channel string,
	  init_count int,
	  ok_count int,
	  stop_count int,
	  init_amount decimal(9,2),
	  repay_amount decimal(9,2),
	  user_rate decimal(5,2),
	  amount_rate decimal(5,2)
);

use tmp_innofin;
insert overwrite table vqq_collection_delegate_2m_jt 
select '��̭',	current_date,
       to_date(create_time)
       ,b.tpp_code
       ,b.org_channel
       ,count(1)
       ,sum(case when a.collection_status=3 then 1 else 0 end)
       ,0
       ,sum(a.total_collected_amt)
       ,sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end)
       ,sum(case when a.collection_status=3 then 1.0 else 0 end)/count(1)*100.0
       ,sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end)/sum(a.total_collected_amt)*100
from ods_innofin.tbl_case_info a  
   join (select case_no,max(tpp_code) as tpp_code,max(org_channel) as org_channel
         from ods_innofin.tbl_case_details 
         group by case_no) b on a.case_no=b.case_no
where a.company_code in ('JUNTAI') and collection_type =2  and to_date(a.create_time) >= add_months(current_date,-2) 
group by to_date(create_time),b.tpp_code,b.org_channel;