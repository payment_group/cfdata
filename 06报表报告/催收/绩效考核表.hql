-- 绩效考核表，
use ods_innofin;
create table if not exists vqq_rpt_collention_performance_30_case_creation(
	case_no string,
	case_date	string	comment'入催日期',
	total_overdue_amt	decimal(15,2)	comment'逾期未还总金额',
	total_collected_amt	decimal(15,2) comment'入催金额'
) comment '绩效考核表-当天入催'
partitioned by (
	dt string comment '报表日期'
);

insert overwrite table ods_innofin.vqq_rpt_collention_performance_30_case_creation partition(dt = '${zdt.format("yyyy-MM-dd")}')
select case_no,to_date(create_time),total_overdue_amt,total_collected_amt from ods_innofin.tbl_case_info where to_date(create_time) = current_date;

--- 
use ods_innofin;
create table  if not exists vqq_rpt_collention_performance_30(
	case_date	string	comment'入催日期',
	employee_no	string	comment'工号',
	employee_name	string	comment'姓名',
	org_channel	string	comment'放款平台',
	product_no	string	comment'产品',
	case_count	int	comment'当日委案量',
	repay_count	int	comment'还款案件数',
	case_amount	decimal(15,2)	comment'当日委案金额',
	repay_amount	decimal(15,2)	comment'还款金额',
	repay_amount_percent	decimal(15,2)	comment'金额回收率%',
	collection_count int comment '处理量'
) comment '绩效考核表'
partitioned by (
	dt string comment '报表日期'
);

insert overwrite table ods_innofin.vqq_rpt_collention_performance_30 partition(dt = '${zdt.format("yyyy-MM-dd")}')
select nvl(a.case_date,b.case_date),--  as `入催日期`,
       nvl(a.employee_no,b.employee_no),--  as `工号`,
       nvl(a.employee_name,''),--  as `姓名`,
       a.org_channel,
	   a.product_no,
       nvl(a.case_count,0),--  as `当日委案量`,
       nvl(a.repay_count,0),--  as `还款案件数`,
       nvl(a.case_amount,0),--  as `当日委案金额`,
       nvl(a.repay_amount,0),--  as `还款金额`,
       nvl(cast(100*a.repay_amount/a.case_amount as decimal(18,2)),''),--  as `金额回收率%`,
       nvl(b.collection_count,0)--  as `当日处理案件数`,
from (
	select to_date(a.create_time) as case_date,
	       a.current_operater as employee_no,
	       b.org_channel,
	       b.product_no,
	       c.username as employee_name,
	       count(1) as case_count,
	       sum(case when a.collection_status=3 then 1 else 0 end) as repay_count,
	       sum(a.total_collected_amt) as case_amount,
	       sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end) as repay_amount
	from ods_innofin.tbl_case_info a  
	inner join (select distinct case_no,product_no,org_channel from ods_innofin.tbl_case_details) b on a.case_no = b.case_no
	left join ods_innofin.sys_user c on a.current_operater = c.account
	where to_date(a.create_time) >= '2018-02-26'
	group by to_date(a.create_time),a.current_operater, b.org_channel,b.product_no,c.username) a
left join (
	select to_date(a.create_time) as case_date,
	operator_id as employee_no,
	count(distinct a.case_no) as collection_count
	from ods_innofin.cas_action a
	     where to_date(a.create_time) >= '2018-02-26'
	group by to_date(a.create_time),operator_id
) b on a.case_date=b.case_date and a.employee_no=b.employee_no
;

select
	 dt  as `报表日期`,
	 case_date as `入催日期`,
	 employee_no as `工号`,
	 employee_name as `姓名`,
	 org_channel as `放款平台`,
	 product_no as `产品`,
	 case_count as `当日委案量`,
	 repay_count as `还款案件数`,
	 case_amount as `当日委案金额`,
	 repay_amount as `还款金额`,
	 repay_amount_percent as `金额回收率%`,
	 collection_count as  `处理量`
from  ods_innofin.vqq_rpt_collention_performance_30 where dt > ''

