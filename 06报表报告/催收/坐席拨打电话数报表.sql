select to_date(a.create_time) as `日期`,operator as `姓名`,
	sum(if(hour(a.create_time) = 8,1,0)) as `8:00-9:00`,
	sum(if(hour(a.create_time) = 9,1,0)) as `9:00-10:00`,
	sum(if(hour(a.create_time) = 10,1,0)) as `10:00-11:00`,
	sum(if(hour(a.create_time) = 11,1,0)) as `11:00-12:00`,
	sum(if(hour(a.create_time) = 12,1,0)) as `12:00-13:00`,
	sum(if(hour(a.create_time) = 13,1,0)) as `13:00-14:00`,
	sum(if(hour(a.create_time) = 14,1,0)) as `14:00-15:00`,
	sum(if(hour(a.create_time) = 15,1,0)) as `15:00-16:00`,
	sum(if(hour(a.create_time) = 16,1,0)) as `16:00-17:00`,
	sum(if(hour(a.create_time) = 17,1,0)) as `17:00-18:00`,
	sum(if(hour(a.create_time) = 18,1,0)) as `18:00-19:00`,
	sum(if(hour(a.create_time) = 19,1,0)) as `19:00-20:00`,
	sum(if(hour(a.create_time) >= 20,1,0)) as `>20:00`,
	count(1) as `总计`	 
 from 
ods_innofin.cas_action a 
where to_date(a.create_time) >= date_add(current_date,-7)
group by to_date(a.create_time),operator 
order by `日期`,`姓名` limit 999