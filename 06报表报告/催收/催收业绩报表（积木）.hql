use tmp_innofin;
create table if not exists vqq_rpt_collection_performance (
	company_code string comment 'company_code',
	create_time string comment '入催日期',
	operator_id string comment '工号',
	username string comment '姓名',
	case_count int comment '当日委案量',
	case_left int comment '当日未处理量',
	case_called int comment '当日处理案件数',
	case_repay int comment '还款案件数'
	case_repay_rate decimal(9,2) comment '案件回收率%'
	case_amount decimal(18,2) comment '当日委案金额',
    repay_amount decimal(18,2) comment '还款金额',
    reay_amount_rate decimal(9,2) comment '金额回收率%',
    call_count int comment '当日拨打数量',
    connection_count int comment '接通数量',
    connection_rate decimal(5,2) comment '接通率%',
    call_rate decimal(5,2) comment '拨打频次',
    call_seconds int comment '拨打时长-秒'
) partitioned by (
	dt string comment '报表日期',
	performance_report string
);

insert overwrite table tmp_innofin.vqq_rpt_collection_performance partition (dt = '${zdt.format("yyyy-MM-dd")}',performance_report = '积木')
select nvl(a.company_code,'CtripInner'),-- as company_code,
       nvl(a.create_time,b.create_time),--  as `入催日期`,
       nvl(a.current_operater,b.operator_id),--  as `工号`,
       nvl(nvl(a.username,b.username),''),--  as `姓名`,
       nvl(a.num,0),--  as `当日委案量`,
       nvl(c.num_caseno1-c.num_caseno2,0),--  as `当日未处理量`,
       nvl(b.num_caseno,0),--  as `当日处理案件数`,
       nvl(a.num_status_3,0),--  as `还款案件数`,
       nvl(cast(a.num_status_3*1.0/a.num*100 as decimal(18,2)),''),--  as `案件回收率%`,
       nvl(a.overdue_amt,0),--  as `当日委案金额`,
       nvl(a.repay_amount,0),--  as `还款金额`,
       nvl(cast(a.repay_amount*1.0/a.overdue_amt*100 as decimal(18,2)),''),--  as `金额回收率%`,
       nvl(b.num,0),--  as `当日拨打数量`,
       nvl(b.num_effect,0),--  as `接通数量`,
       nvl(cast(b.num_effect*1.0/b.num*100 as decimal(18,2)),''),--  as `接通率%`,
       nvl(cast(b.num*1.0/b.num_caseno*100 as decimal(18,2)),''),--  as `拨打频次`,
       nvl(d.TimeAnswer,0)--  as `拨打时长-秒`
from (
	select a.company_code,to_date(a.create_time) as create_time,
	       a.current_operater,c.username,
	       count(0) as num,
	       sum(case when a.collection_status=3 then 1 else 0 end) as num_status_3,
	       sum(a.total_collected_amt) as overdue_amt,
	       sum(case when a.total_collected_amt - a.total_overdue_amt>0 then a.total_collected_amt - a.total_overdue_amt else 0 end) as repay_amount
	from ods_innofin.tbl_case_info a  
	inner join (select distinct case_no from ods_innofin.tbl_case_details where tpp_code = 'JIMULOAN') b on a.case_no = b.case_no
	left join ods_innofin.sys_user c on a.current_operater = c.account
	where a.company_code in ('CtripInner' ) and to_date(a.create_time) >= add_months(current_date,-2)
	group by a.company_code,to_date(a.create_time),a.current_operater,c.username ) a
left join (
	select to_date(a.create_time) as create_time,operator_id,b.agent_id,b.username,
	       count(0) as num,
	       count(case when action_code <> 'WJT' then 1 end) as num_effect,
	       count(distinct a.case_no) as num_caseno
	from ods_innofin.cas_action a
	     left join ods_innofin.sys_user b on a.operator_id=b.account
	     inner join (select distinct case_no from ods_innofin.tbl_case_details where tpp_code = 'JIMULOAN') c on a.case_no = c.case_no
	     inner join (select distinct case_no from ods_innofin.tbl_case_info where company_code = 'CtripInner') d on a.case_no = d.case_no
	where to_date(a.create_time) >= add_months(current_date,-2)
	group by to_date(a.create_time),operator_id,b.agent_id,b.username
) b on a.create_time=b.create_time and a.current_operater=b.operator_id

left join (
	select to_date(a.create_time) as create_time,a.current_operater,count(distinct a.case_no) as num_caseno1,
	       count(distinct b.case_no) as num_caseno2
	from ods_innofin.tbl_case_info a
	   left join ods_innofin.cas_action b on a.case_no=b.case_no and to_date(a.create_time)=to_date(b.create_time)
	where a.collection_status<>3
	group by to_date(a.create_time),a.current_operater) c on c.create_time=a.create_time and c.current_operater=a.current_operater
left join (
	select to_date(time_in) as date,Agent_No,sum(Time_to_Answer) as TimeAnswer
	from ods_innofin.call_info_sync_process 
	where to_date(create_time) >= add_months(current_date,-2)
	group by to_date(time_in),Agent_No) d on d.date=b.create_time and d.Agent_No=b.agent_id;

