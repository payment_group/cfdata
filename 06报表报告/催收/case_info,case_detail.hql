--case_info case_detail
-- 留快照
use tmp_innofin;
create table tbl_case_details_daily(
	id	bigint	comment '主键Id',
	case_no	string	comment '案件号',
	loan_provide_no	string	comment '借据号',
	period	int	comment '还款计划第x期',
	user_id	string	comment '用户ID',
	org_channel	string	comment '渠道编码: QUNAR, CTRIP',
	tpp_code	string	comment '贷款通道',
	fund_code	string	comment 'NULL',
	product_no	string	comment '产品编码',
	capital_amt	decimal(15,2)	comment '应还本金',
	actual_capital_amt	decimal(15,2)	comment '已还本金',
	interest_amt	decimal(15,2)	comment '应还利息',
	actual_interest_amt	decimal(15,2)	comment '已还利息',
	overdue_interest_amt	decimal(15,2)	comment '应还罚息',
	actual_overdue_interest_amt	decimal(15,2)	comment '已还利息',
	repayment_date	string	comment '还款日期',
	actual_repayment_date	string	comment '实际还款日期',
	status	int	comment '状态，1逾期，2 已还清',
	create_time	string	comment '创建时间',
	update_time	string	comment '更新时间',
	datachange_lasttime	string	comment '最后更新时间'
) partitioned by (
	dt string
);

use tmp_innofin;
create table tbl_case_info_daily(
	id	bigint	comment '主键ID',
	case_no	string	comment '案件号',
	user_id	string	comment '用户ID',
	open_id	string	comment '携程集团的用户编号',
	case_type	string	comment '案件类型',
	due_date	string	comment '最早还款日期',
	total_overdue_amt	decimal(15,2)	comment '逾期未还总金额',
	total_collected_amt	decimal(15,2)	comment '入催金额',
	collection_type	tinyint	comment '催收类型：0:电催，1:法催，2:委外',
	collection_status	tinyint	comment '催收状态：0：初始状态，1：已分案 2：催收中，3：关案，4：停案',
	company_code	string	comment '催收组织编码',
	current_operater	string	comment '当前归属座席',
	user_group	tinyint	comment '用户群,0-普通用户,1-校园用户,2-初级版',
	create_time	string	comment '创建时间',
	update_time	string	comment '更新时间',
	datachange_lasttime	string	comment '最后更新时间'
) partitioned by (
	dt string
);
