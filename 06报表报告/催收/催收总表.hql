use olap_innofin;
create table vqq_rpt_collention_CtripInner(
	`company_code`	string	comment'company_code',
	`报表日期`	string	comment'报表日期',
	`入催日期`	string	comment'入催日期',
	`资金方`	string	comment'资金方',
	`放款平台`	string	comment'放款平台',
	`期初案件数`	int	comment'期初案件数',
	`满意案件数`	int	comment'满意案件数',
	`停催案件数`	int	comment'停催案件数',
	`期初案件金额`	decimal(15,2)	comment'期初案件金额',
	`还款金额合计`	decimal(15,2)	comment'还款金额合计',
	`用户回收率%`	decimal(15,2)	comment'用户回收率%',
	`金额回收率%`	decimal(15,2)	comment'金额回收率%'
) comment '催收总表'
partitioned by (
	dt string comment 'dt'
);

insert overwrite table olap_innofin.vqq_rpt_collention_CtripInner partition(dt = '${zdt.format("yyyy-MM-dd")}')
select a.company_code,current_date as `报表日期`,
       to_date(create_time) `入催日期`,b.tpp_code as `资金方`,b.org_channel as `放款平台`,
       count(1) as `期初案件数`,
       sum(case when a.collection_status=3 then 1 else 0 end) as `满意案件数`,0 as `停催案件数`,
       sum(a.total_collected_amt) as `期初案件金额`,
       sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end ) as `还款金额合计`,
       cast(sum(case when a.collection_status=3 then 1 else 0 end)*1.0/count(1)*100.0 as decimal(15,2)) as `用户回收率%`,
       cast(sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end )*1.0/sum(a.total_collected_amt)*100 as decimal(15,2)) as `金额回收率%`
from ods_innofin.tbl_case_info a  
   join (select case_no,max(tpp_code) as tpp_code,max(org_channel) as org_channel
         from ods_innofin.tbl_case_details where tpp_code <> 'ZYLOAN'
         group by case_no
         union all 
         select case_no,max(fund_code) as tpp_code,max(org_channel) as org_channel
         from ods_innofin.tbl_case_details where tpp_code = 'ZYLOAN'
         group by case_no
         ) b on a.case_no=b.case_no
where a.company_code in ('CtripInner') and to_date(a.create_time) >= add_months(current_date,-2)
group by a.company_code,to_date(create_time),b.tpp_code,b.org_channel;

select * from olap_innofin.vqq_rpt_collention_CtripInner  where dt = current_date 