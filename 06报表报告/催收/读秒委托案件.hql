-- 对账结算单，
-- 还款明细
use tmp_innofin;
create table if not exists vqq_rpt_DUMIAO_statement(
	loan_provide_no string				comment  '借据号',
	repayment_date string,
	due_date string                    comment  '委案时间',
	loan_channel string                comment  '放款渠道',
	user_id string comment 'user_id', 
	open_id string comment 'open_id', 
	user_name string                   comment  '姓名',
	period int                         comment  '逾期期数',
	capital_amt decimal(15,2)          comment  '委案本金',
	interest_amt decimal(15,2)         comment  '委案息费',
	overdue_interest_amt decimal(15,2) comment  '委案违约',
	overdue_days int                   comment  '逾期天数',
	overdue_months string              comment '逾期阶段',
	actual_repay_amt decimal(15,2)      comment  '实际回款',
	actual_repayment_date string        comment  '回款时间'
) comment '读秒委托'
partitioned by (                       
	dt string comment 'dt'
);

-- 最早逾期  
use tmp_innofin;
create table if not exists vqq_tmp_DUMIAO_statement_first_overdue(
	loan_provide_no string		comment  '借据号',
	period int                  comment  '逾期期数',
	repayment_date string 		comment '到期日',
	actual_repayment_date string 		comment '实际支付日',
	due_date string 			comment '逾期日'
) comment '读秒委托对账单-最早逾期日'
partitioned by (                       
	dt string comment 'dt'
);
insert overwrite table tmp_innofin.vqq_tmp_DUMIAO_statement_first_overdue partition (dt = '${zdt.format("yyyy-MM-dd")}')
select loan_provide_no,period,repayment_date,actual_repayment_date,date_add(repayment_date,1) as due_date from (
select a.loan_provide_no,period,repayment_date,actual_repayment_date,row_number() over (partition by a.loan_provide_no,to_date(a.actual_repayment_date) order by period) as row_no 
from ods_innofin.tbl_repayment_plan a
inner join (select distinct case_no,loan_provide_no from ods_innofin.tbl_case_details) b on a.loan_provide_no = b.loan_provide_no
inner join (select distinct case_no,case_type from ods_innofin.tbl_case_info) c on b.case_no = c.case_no and c.case_type = 'APPOINT'
where to_date(a.actual_repayment_date) > to_date(a.repayment_date) 
) t where t.row_no = 1;

-- 逾期 --出催
insert overwrite table tmp_innofin.vqq_rpt_DUMIAO_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.loan_provide_no,a.repayment_date,
	b.due_date,
	'读秒委托' as loan_channel,
	d.user_id,
	d.open_id,
	c.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)),
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select loan_provide_no,actual_repayment_date,due_date from tmp_innofin.vqq_tmp_DUMIAO_statement_first_overdue where dt = current_date) b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
inner join ods_innofin.tbl_overdue_loan d on a.loan_provide_no = d.loan_provide_no
inner join (select distinct user_id,user_name from ods_innofin.user_info) c on d.user_id = c.user_id
where a.status = 2 and to_date(a.actual_repayment_date) > to_date(a.repayment_date) and to_date(a.actual_repayment_date) >= add_months(current_date,-2);
-- 未逾期，同日还款
insert into table tmp_innofin.vqq_rpt_DUMIAO_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.loan_provide_no,a.repayment_date,
	b.due_date,
	'读秒委托' as loan_channel,
	a.user_id,
	b.open_id,
	b.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1 as overdue_days,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)) as overdue_months,
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt as actual_repay_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select distinct loan_provide_no,user_id,open_id,user_name,due_date,to_date(actual_repayment_date) as actual_repayment_date from tmp_innofin.vqq_rpt_DUMIAO_statement where dt = current_date) b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
where a.status = 2 and to_date(a.actual_repayment_date) <= to_date(a.repayment_date) 
and to_date(a.actual_repayment_date) >= add_months(current_date,-2) and to_date(a.actual_repayment_date) >= b.due_date;

-----
---2．	读秒委托案件的催记明细表

use tmp_innofin;
create table if not exists vqq_tmp_DUMIAO_last_action(
	open_id string		comment  '客户号',
	collection_date string                  comment  '催收日期',
	collection_result string 		comment '催收情况',
	collection_action string 		comment '行动码',
	overdue_reason string 			comment '逾期原因'
) comment '读秒委托-催记明细'
partitioned by (                       
	dt string comment 'dt'
);

use ods_innofin;
insert overwrite table tmp_innofin.vqq_tmp_DUMIAO_last_action partition (dt = '${zdt.format("yyyy-MM-dd")}')
select a.open_id,
	b.create_time,
	c.name,
	d.name,
	e.name
from tbl_case_info a 
inner join (select case_no,create_time, action_code,action_result,overdue_reason,row_number() over (partition by case_no order by create_time desc) as row_no from cas_action where caller_type = 'USER')
	 b on a.case_no = b.case_no and b.row_no = 1
inner join (select distinct code,name from  ods_innofin.cas_code_dict where name <> '金额、质量及退货') c on c.code = b.action_result
inner join (select distinct code,name from  ods_innofin.cas_code_dict where name <> '金额、质量及退货') d on d.code = b.action_code
inner join (select distinct code,name from  ods_innofin.cas_code_dict where name <> '金额、质量及退货') e on e.code = b.overdue_reason
where a.case_type = 'APPOINT';