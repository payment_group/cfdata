use tmp_innofin;
create table vqq_rpt_collention_JIMULOAN(
	`company_code`	string	comment'company_code',
	`报表日期`	string	comment'报表日期',
	`入催日期`	string	comment'入催日期',
	`资金方`	string	comment'资金方',
	`放款平台`	string	comment'放款平台',
	`期初案件数`	int	comment'期初案件数',
	`满意案件数`	int	comment'满意案件数',
	`停催案件数`	int	comment'停催案件数',
	`期初案件金额`	decimal(15,2)	comment'期初案件金额',
	`还款金额合计`	decimal(15,2)	comment'还款金额合计',
	`用户回收率%`	decimal(15,2)	comment'用户回收率%',
	`金额回收率%`	decimal(15,2)	comment'金额回收率%'
) comment '催收总表_JIMU'
partitioned by (
	dt string comment 'dt'
);

insert overwrite table tmp_innofin.vqq_rpt_collention_JIMULOAN partition(dt = '${zdt.format("yyyy-MM-dd")}')
select a.company_code,current_date as `报表日期`,
       to_date(create_time) `入催日期`,b.tpp_code as `资金方`,b.org_channel as `放款平台`,
       count(1) as `期初案件数`,
       sum(case when a.collection_status=3 then 1 else 0 end) as `满意案件数`,0 as `停催案件数`,
       sum(a.total_collected_amt) as `期初案件金额`,
       sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end ) as `还款金额合计`,
       cast(sum(case when a.collection_status=3 then 1 else 0 end)*1.0/count(1)*100.0 as decimal(15,2)) as `用户回收率%`,
       cast(sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end )*1.0/sum(a.total_collected_amt)*100 as decimal(15,2)) as `金额回收率%`
from ods_innofin.tbl_case_info a  
   join (select case_no, tpp_code,max(org_channel) as org_channel
         from ods_innofin.tbl_case_details where tpp_code = 'JIMULOAN'
         group by case_no, tpp_code
         ) b on a.case_no=b.case_no
where a.company_code in ('CtripInner') and to_date(a.create_time) >= add_months(current_date,-2)
group by a.company_code,to_date(create_time),b.tpp_code,b.org_channel;

select * from tmp_innofin.vqq_rpt_collention_JIMULOAN  where dt = current_date 

-- 明细
use ods_innofin;
select a.company_code,current_date as `报表日期`,a.user_id,
       to_date(create_time) as `入催日期`,b.case_no,b.tpp_code as `资金方`,b.org_channel as `放款平台`,
       b.capital_amt as `本金`,
       b.actual_capital_amt as `已还本金`,
       b.interest_amt as `利息`,
       b.actual_interest_amt as `已还利息`,
       b.overdue_interest_amt as `罚息`,
       b.actual_overdue_interest_amt as `已还罚息`
       
from tbl_case_info a inner join
(select case_no, tpp_code,org_channel,
	sum(capital_amt) as capital_amt,
	sum(actual_capital_amt) as actual_capital_amt,
	sum(interest_amt) as interest_amt,
	sum(actual_interest_amt) as actual_interest_amt,
	sum(overdue_interest_amt) as overdue_interest_amt,
	sum(actual_overdue_interest_amt) as actual_overdue_interest_amt
 from tbl_case_details
	where tpp_code = 'JIMULOAN'
	group by case_no,tpp_code,org_channel
) b on a.case_no = b.case_no 
where a.company_code in ('CtripInner') and to_date(a.create_time) >= add_months(current_date,-2) and b.actual_capital_amt > 0





SEQ	Field	Type	Null	Key	Default	Extra
1	id	bigint(20)	NO	PRI	NULL	auto_increment
2	case_no	varchar(30)	NO	MUL		
3	loan_provide_no	varchar(45)	NO	MUL		
4	period	int(6)	NO		1	
5	user_id	varchar(64)	NO			
6	org_channel	varchar(20)	NO			
7	tpp_code	varchar(20)	NO			
8	fund_code	varchar(20)	NO			
9	product_no	varchar(20)	NO		IOUS	
10	capital_amt	decimal(15,2)	NO		0.00	
11	actual_capital_amt	decimal(15,2)	NO		0.00	
12	interest_amt	decimal(15,2)	NO		0.00	
13	actual_interest_amt	decimal(15,2)	NO		0.00	
14	overdue_interest_amt	decimal(15,2)	NO		0.00	
15	actual_overdue_interest_amt	decimal(15,2)	NO		0.00	
16	repayment_date	datetime	NO		1971-01-01 00:00:00	
17	actual_repayment_date	datetime	YES		1971-01-01 00:00:00	
18	status	int(6)	NO		0	
19	create_time	datetime	NO	MUL	1971-01-01 00:00:00	
20	update_time	datetime	NO		1971-01-01 00:00:00	
21	DataChange_LastTime	datetime	NO	MUL	CURRENT_TIMESTAMP	on update CURRENT_TIMESTAMP