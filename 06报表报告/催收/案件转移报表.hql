
-- 案件转移报表
 select b.apply_time as `案件转移时间`,
	a.case_no as `案件号`,
	a.user_id as `用户id`,
	c.loan_provide_no as `借据号/订单号`,
	org_channel as `平台`,
	tpp_code as `放款渠道`
from 
	tbl_case_info a
	inner join case_transfer_apply b on a.case_no = b.case_no and b.apply_status in (#STATUS#) 
	inner join tbl_case_details c on a.case_no = c.case_no 
	where apply_time between  #FROM_DATE# and #TO_DATE# and collection_status = 2
	
	
