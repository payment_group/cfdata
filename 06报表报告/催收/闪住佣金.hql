use tmp_innofin;
create table if not exists vqq_rpt_collection_qflash(
	actual_repayment_date string,
	repayment_date string,
	overdue_days int,
	order_id string,
	overdue_amount decimal(15,2),
	repayment_amount decimal(15,2)
);

insert overwrite table tmp_innofin.vqq_rpt_collection_qflash
select to_date(update_time),
	to_date(due_time),
	datediff(update_time,due_time),
	order_no,
	order_amt,
	repay_amt
from ods_innofin.tbl_order_info where order_type = 'FLASH' and order_status = 2;


use tmp_innofin;
create table if not exists vqq_rpt_collection_qflash_commission (
	actual_repayment_date string comment '还款日期',
	repayment_date string comment '欠款日期',
	overdue_days int comment '欠款天数',
	order_id string comment '订单号',
	overdue_amount decimal(15,2) comment '欠款金额（含杂费）',
	repayment_amount decimal(15,2) comment '还款金额',
	overdue_days_type string comment '欠款天数类型',
	collection_commission decimal(9,4) comment '佣金'	
);

insert overwrite table tmp_innofin.vqq_rpt_collection_qflash_commission
select actual_repayment_date,
	repayment_date,
	overdue_days,
	order_id,
	overdue_amount,
	repayment_amount,
case when overdue_days <= 7 then '欠款7日内'
	when overdue_days <= 10 then '欠款10日内'
	when overdue_days <= 15 then '欠款15日内'
	when overdue_days <= 20 then '欠款20日内'
	when overdue_days <= 30 then '欠款30日内'
	when overdue_days <= 60 then '欠款60日内'
	when overdue_days <= 90 then '欠款90日内'
	when overdue_days <= 180 then '欠款180日内'
	else '欠款180日以上' end,
case when overdue_days <= 7 then repayment_amount*0.00
	when overdue_days <= 10 then repayment_amount*0.03
	when overdue_days <= 15 then repayment_amount*0.05
	when overdue_days <= 20 then repayment_amount*0.08
	when overdue_days <= 30 then repayment_amount*0.10
	when overdue_days <= 60 then repayment_amount*0.15
	when overdue_days <= 90 then repayment_amount*0.18
	when overdue_days <= 180 then repayment_amount*0.20
	else repayment_amount*0.25 end 
from tmp_innofin.vqq_rpt_collection_qflash;


case when overdue_days <= 7 then repayment_amount*0.00
	when overdue_days <= 10 then repayment_amount*0.03
	when overdue_days <= 15 then repayment_amount*0.05
	when overdue_days <= 20 then repayment_amount*0.08
	when overdue_days <= 30 then repayment_amount*0.10
	when overdue_days <= 60 then repayment_amount*0.15
	when overdue_days <= 90 then repayment_amount*0.18
	when overdue_days <= 180 then repayment_amount*0.20
	else repayment_amount*0.25 end 
	
case when overdue_days <= 7 then '欠款7日内'
	when overdue_days <= 10 then '欠款10日内'
	when overdue_days <= 15 then '欠款15日内'
	when overdue_days <= 20 then '欠款20日内'
	when overdue_days <= 30 then '欠款30日内'
	when overdue_days <= 60 then '欠款60日内'
	when overdue_days <= 90 then '欠款90日内'
	when overdue_days <= 180 then '欠款180日内'
	else '欠款180日以上' end 

select  vqq_rpt_collection_qflash.actual_repayment_date,  vqq_rpt_collection_qflash.repayment_date,  vqq_rpt_collection_qflash.order_id,  
vqq_rpt_collection_qflash.overdue_days as a,  vqq_rpt_collection_qflash.overdue_amount as b,  vqq_rpt_collection_qflash.repayment_amount as c, 
(cast(case when overdue_days <= 10 then repayment_amount*0.03
	when overdue_days <= 15 then repayment_amount*0.05
	when overdue_days <= 20 then repayment_amount*0.08
	when overdue_days <= 30 then repayment_amount*0.10
	when overdue_days <= 60 then repayment_amount*0.15
	when overdue_days <= 90 then repayment_amount*0.18
	when overdue_days <= 180 then repayment_amount*0.20
	else repayment_amount*0.25 end) as decimal(9,2)) as d  from tmp_innofin.vqq_rpt_collection_qflash   
	 order by  vqq_rpt_collection_qflash.actual_repayment_date asc 