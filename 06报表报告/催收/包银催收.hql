-- 回收率
select a.company_code,#REPORT_DATE# as `报表日期`,
       to_date(create_time) `入催日期`,b.tpp_code as `资金方`,b.org_channel as `放款平台`,
       count(1) as `期初案件数`,
       sum(case when a.collection_status=3 then 1 else 0 end) as `满意案件数`,0 as `停催案件数`,
       sum(a.total_collected_amt) as `期初案件金额`,
       sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end ) as `还款金额合计`,
       cast(sum(case when a.collection_status=3 then 1 else 0 end)*1.0/count(1)*100.0 as decimal(18,2)) as `用户回收率%`,
       cast(sum(case when a.total_collected_amt - a.total_overdue_amt > 0 then a.total_collected_amt - a.total_overdue_amt else 0 end )*1.0/sum(a.total_collected_amt)*100 as decimal(18,2)) as `金额回收率%`
from ods_innofin.tbl_case_info a  
   join (select case_no,max(tpp_code) as tpp_code,max(org_channel) as org_channel
         from ods_innofin.tbl_case_details 
         group by case_no) b on a.case_no=b.case_no
where a.company_code in ('CtripInner') and to_date(a.create_time) between add_months(#REPORT_DATE#,-2) and date_add(add_months(#REPORT_DATE#,-1),-1)
and tpp_code = 'BYXFCASH'
group by a.company_code,to_date(create_time),b.tpp_code,b.org_channel

-- 催记模板
select caller as `客户姓名`,
	a.user_id as `客户ID`,action_time as `拔打时间`,if(caller_type = 'USER','本人','干系人') as `与客户关系`,'电催'  as `催收方式`, 
	nvl(c.mobile,'')  as `拔打号码`, if(action_code = 'WJT','否','是') as `接通状态`,d.name as `行动码`,e.name as `状态码`, nvl(f.name,'') as `逾期原因`,remark  as `备注内容`
from 
(select caller,user_id, row_number() over (partition by user_id order by action_time desc) as row_no,action_time,caller_type,action_code,action_result, overdue_reason,remark from ods_innofin.cas_action 
	where action_time between add_months(#REPORT_DATE#,-1) and #REPORT_DATE#
) a
left join (select user_id,user_name,mobile,row_number() over (partition by user_id,user_name) as phone_id from ods_innofin.stakeholder_info) c on a.user_id = c.user_id and a.caller = c.user_name and c.phone_id = 1 
inner join (select distinct code,name from ods_innofin.cas_code_dict) d on a.action_code = d.code 
inner join (select distinct code,name from ods_innofin.cas_code_dict) e on a.action_result = e.code 
left join (select distinct code,name from ods_innofin.cas_code_dict) f on a.overdue_reason = f.code 
where a.row_no = 1 and c.phone_id = 1

-- 对账结算单
use olap_innofin;
create table vqq_rpt_BYXFCASH_statement(
	loan_provide_no string				comment  '借据号',
	due_date string                    comment  '委案时间',
	loan_channel string                comment  '放款渠道',
	user_id string comment 'user_id', 
	user_name string                   comment  '姓名',
	period int                         comment  '逾期期数',
	capital_amt decimal(15,2)          comment  '委案本金',
	interest_amt decimal(15,2)         comment  '委案息费',
	overdue_interest_amt decimal(15,2) comment  '委案违约',
	overdue_days int                   comment  '逾期天数',
	overdue_months string              comment '逾期阶段',
	actual_repay_amt decimal(15,2)      comment  '实际回款',
	actual_repayment_date string        comment  '回款时间'
) comment '包银对账单'
partitioned by (                       
	dt string comment 'dt'
);

-- 最早逾期  
use tmp_innofin;
create table if not exists vqq_tmp_BYXFCASH_statement_first_overdue(
	loan_provide_no string		comment  '借据号',
	period int                  comment  '逾期期数',
	repayment_date string 		comment '到期日',
	actual_repayment_date string 		comment '实际支付日',
	due_date string 			comment '逾期日'
) comment '包银对账单-最早逾期日'
partitioned by (                       
	dt string comment 'dt'
);
insert overwrite table tmp_innofin.vqq_tmp_BYXFCASH_statement_first_overdue partition (dt = '${zdt.format("yyyy-MM-dd")}')
select loan_provide_no,period,repayment_date,actual_repayment_date,date_add(repayment_date,1) as due_date from (
select a.loan_provide_no,period,repayment_date,actual_repayment_date,row_number() over (partition by a.loan_provide_no,to_date(a.actual_repayment_date) order by period) as row_no 
from ods_innofin.tbl_repayment_plan a
inner join ods_innofin.tbl_overdue_loan b on a.loan_provide_no = b.loan_provide_no
where to_date(a.actual_repayment_date) > to_date(a.repayment_date) and b.tpp_code = 'BYXFCASH' 
) t where t.row_no = 1;

-- 逾期 --出催
insert overwrite table olap_innofin.vqq_rpt_BYXFCASH_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.loan_provide_no,
	b.due_date,
	'包银消费' as loan_channel,
	a.user_id,
	c.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)),
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select loan_provide_no,actual_repayment_date,due_date from tmp_innofin.vqq_tmp_BYXFCASH_statement_first_overdue where dt = current_date) b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
inner join (select distinct user_id,user_name from ods_innofin.user_info) c on a.user_id = c.user_id
where a.status = 2 and to_date(a.actual_repayment_date) > to_date(a.repayment_date) and to_date(a.actual_repayment_date) >= add_months(current_date,-2);
-- 未逾期，同日还款
insert into table olap_innofin.vqq_rpt_BYXFCASH_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.loan_provide_no,
	b.due_date,
	'包银消费' as loan_channel,
	a.user_id,
	b.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1 as overdue_days,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)) as overdue_months,
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt as actual_repay_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select distinct loan_provide_no,user_id,user_name,due_date,to_date(actual_repayment_date) as actual_repayment_date from olap_innofin.vqq_rpt_BYXFCASH_statement where dt = current_date
			and cast(regexp_replace(overdue_months,'M','') as int) > 2 ) b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
where a.status = 2 and to_date(a.actual_repayment_date) <= to_date(a.repayment_date) 
and to_date(a.actual_repayment_date) >= add_months(current_date,-2) and to_date(a.actual_repayment_date) >= b.due_date
;


select
 loan_provide_no as `借据号`,
 due_date as `委案时间`,
 loan_channel as `放款渠道`,
 user_id as `user_id`, 
 user_name as `姓名`,
 period as `逾期期数`,
 capital_amt as `委案本金`,
 interest_amt as `委案息费`,
 overdue_interest_amt as `委案违约`,
 overdue_days as `逾期天数`,
 overdue_months as `逾期阶段`,
 actual_repay_amt as `实际回款`,
 actual_repayment_date as `回款时间`
from  olap_innofin.vqq_rpt_BYXFCASH_statement where dt = current_date 
