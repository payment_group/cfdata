-- 
use ods_innofin;
create table if not exists cas_code_dict(
	id	bigint,
	code	string,
	name	string,
	group	string,
	code_type	string,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);


select id,code,name,group,code_type,create_time,update_time,DataChange_LastTime from cas_code_dict;

use ods_innofin;
create table if not exists user_loan_info(

	id	bigint,
	user_id	string,
	open_id	string,
	order_time	string,
	tpp_code	string,
	tpp_name	string,
	org_channel	string,
	product_no	string,
	product_name	string,
	loan_amt	decimal(15,2),
	loan_term	int,
	order_no	string,
	loan_provide_no	string,
	repay_amt	decimal(15,2),
	setl_repay_amt	decimal(15,2),
	expire_amt	decimal(15,2),
	due_date	string,
	expire_count	int,
	loan_status	int,
	case_status	int,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);

select id,user_id,open_id,order_time,tpp_code,tpp_name,org_channel,product_no,product_name,loan_amt,loan_term,order_no,loan_provide_no,repay_amt,setl_repay_amt,expire_amt,due_date,expire_count,loan_status,case_status,create_time,update_time,DataChange_LastTime from user_loan_info;

use ods_innofin;
create table if not exists case_transfer_apply(
	id	bigint,
	case_no	string,
	transfer_from	string,
	transfer_to	string,
	transfer_role	string,
	remark	string,
	reason	string,
	applyer	string,
	applyer_id	string,
	apply_time	string,
	audit_time	string,
	apply_status	int,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);
select id,case_no,transfer_from,transfer_to,transfer_role,remark,reason,applyer,applyer_id,apply_time,audit_time,apply_status,create_time,update_time,DataChange_LastTime from case_transfer_apply;


use ods_innofin;
create table if not exists tbl_order_info(
	id	bigint,
	user_id	string,
	open_id	string,
	order_no	string,
	order_name	string,
	order_time	string,
	order_amt	decimal(15,2),
	order_type	string,
	repay_amt	decimal(15,2),
	due_time	string,
	due_amt	decimal(15,2),
	guest	string,
	org_channel	string,
	order_status	int,
	process_status	int,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);

select id,user_id,open_id,order_no,order_name,order_time,order_amt,order_type,repay_amt,due_time,due_amt,guest,org_channel,order_status,process_status,create_time,update_time,DataChange_LastTime from tbl_order_info;

use ods_innofin;
create table if not exists cas_call_info(
	id	bigint,
	call_id	string,
	operator	string,
	case_no	string,
	ani	string,
	dnis	string,
	agent_id	string,
	prefix_code	string,
	call_type	string,
	call_status	int,
	device_type	string,
	device_id	string,
	time_in	string,
	time_out	int,
	time_to_alert	int,
	time_to_answer	int,
	time_to_queue	int,
	term_reason	string,
	create_time	string,
	update_time	string,
	DataChange_LastTime	string
);

select id,call_id,operator,case_no,ani,dnis,agent_id,prefix_code,call_type,call_status,device_type,device_id,time_in,time_out,time_to_alert,time_to_answer,time_to_queue,term_reason,create_time,update_time,DataChange_LastTime from cas_call_info;
