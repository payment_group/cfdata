-- 还款明细日报
select 
	a.org_channel as `放款平台`,
	a.tpp_code as `放款渠道`,
	loan_provide_no as `借据号\业务订单号`,
	if(a.status = 1,'强扣','正常') as `类型`,
	a.user_id,
	c.user_name as `姓名`,
	period as `还款期数`,
	capital_amt as `还款本金`,
	interest_amt as `还款息费`,
	overdue_interest_amt as `违约金`,
	datediff(actual_repayment_date,due_date)+1 as `逾期天数`,
	concat('M',ceil((datediff(actual_repayment_date,due_date)+1)/30)) `逾期阶段`,
	actual_capital_amt + actual_interest_amt + actual_overdue_interest_amt as `实际回款`,
	actual_repayment_date as `回款时间`
from ods_innofin.tbl_case_details a
inner join ods_innofin.tbl_case_info b on a.case_no = b.case_no
inner join (select distinct user_id,user_name from ods_innofin.user_info) c on b.user_id = c.user_id
where actual_capital_amt + actual_interest_amt + actual_overdue_interest_amt > 0 and to_date(actual_repayment_date) = date_add(#REPORT_DATE#,-1)
union all 
select * from(
select 
	d.org_channel as `放款平台`,
	d.tpp_code as `放款渠道`,
	a.loan_provide_no as `借据号`,
	b.due_date as `委案时间`,
	a.user_id,
	c.user_name as `姓名`,
	period as `还款期数`,
	capital_amt as `还款本金`,
	interest_amt as `还款息费`,
	overdue_interest_amt as `违约金`,
	datediff(actual_repayment_date,b.due_date)+1 as `逾期天数`,
	concat('M',ceil((datediff(actual_repayment_date,b.due_date)+1)/30)) `逾期阶段`,
	actual_capital_amt + actual_interest_amt + actual_overdue_interest_amt as `实际回款`,
	actual_repayment_date as `回款时间`
from ods_innofin.tbl_repayment_plan a
inner join ods_innofin.tbl_case_info b on a.user_id = b.user_id
inner join (select distinct user_id,user_name from ods_innofin.user_info) c on b.user_id = c.user_id
inner join ods_innofin.tbl_overdue_loan d on a.loan_provide_no = d.loan_provide_no
where actual_capital_amt + actual_interest_amt + actual_overdue_interest_amt > 0 and actual_repayment_date <= repayment_date 
and to_date(actual_repayment_date) = date_add(#REPORT_DATE#,-1)) f
where f.`逾期天数` >=1


	
	
