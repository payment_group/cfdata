-- 对账结算单
use tmp_innofin;
create table if not exists vqq_rpt_FPJKLOAN_statement(
	loan_provide_no string				comment  '借据号',
	repayment_date string,
	due_date string                    comment  '委案时间',
	loan_channel string                comment  '放款渠道',
	user_id string comment 'user_id', 
	user_name string                   comment  '姓名',
	period int                         comment  '逾期期数',
	capital_amt decimal(15,2)          comment  '委案本金',
	interest_amt decimal(15,2)         comment  '委案息费',
	overdue_interest_amt decimal(15,2) comment  '委案违约',
	overdue_days int                   comment  '逾期天数',
	overdue_months string              comment '逾期阶段',
	actual_repay_amt decimal(15,2)      comment  '实际回款',
	actual_repayment_date string        comment  '回款时间'
) comment '凡普金科对账单'
partitioned by (                       
	dt string comment 'dt'
);

-- 最早逾期  
use tmp_innofin;
create table if not exists vqq_tmp_FPJKLOAN_statement_first_overdue(
	loan_provide_no string		comment  '借据号',
	period int                  comment  '逾期期数',
	repayment_date string 		comment '到期日',
	actual_repayment_date string 		comment '实际支付日',
	due_date string 			comment '逾期日'
) comment '凡普金科对账单-最早逾期日'
partitioned by (                       
	dt string comment 'dt'
);
insert overwrite table tmp_innofin.vqq_tmp_FPJKLOAN_statement_first_overdue partition (dt = '${zdt.format("yyyy-MM-dd")}')
select loan_provide_no,period,repayment_date,actual_repayment_date,date_add(repayment_date,1) as due_date from (
select a.loan_provide_no,period,repayment_date,actual_repayment_date,row_number() over (partition by a.loan_provide_no,to_date(a.actual_repayment_date) order by period) as row_no 
from ods_innofin.tbl_repayment_plan a
inner join ods_innofin.tbl_overdue_loan b on a.loan_provide_no = b.loan_provide_no
where to_date(a.actual_repayment_date) > to_date(a.repayment_date) and b.tpp_code = 'FPJKLOAN' 
) t where t.row_no = 1;

-- 逾期 --出催
insert overwrite table tmp_innofin.vqq_rpt_FPJKLOAN_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.loan_provide_no,a.repayment_date,
	b.due_date,
	'凡普金科' as loan_channel,
	a.user_id,
	c.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)),
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select loan_provide_no,actual_repayment_date,due_date from tmp_innofin.vqq_tmp_FPJKLOAN_statement_first_overdue where dt = current_date) b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
inner join (select distinct user_id,user_name from ods_innofin.user_info) c on a.user_id = c.user_id
where a.status = 2 and to_date(a.actual_repayment_date) > to_date(a.repayment_date) and to_date(a.actual_repayment_date) >= add_months(current_date,-2);
-- 未逾期，同日还款
insert into table tmp_innofin.vqq_rpt_FPJKLOAN_statement partition (dt = '${zdt.format("yyyy-MM-dd")}')
select distinct a.loan_provide_no,a.repayment_date,
	b.due_date,
	'凡普金科' as loan_channel,
	a.user_id,
	b.user_name,
	a.period,
	a.capital_amt,
	a.interest_amt,
	a.overdue_interest_amt,
	datediff(a.actual_repayment_date,b.due_date)+1 as overdue_days,
	concat('M',ceil((datediff(a.actual_repayment_date,b.due_date)+1)/30)) as overdue_months,
	a.actual_capital_amt + a.actual_interest_amt + a.actual_overdue_interest_amt as actual_repay_amt,
	a.actual_repayment_date
from ods_innofin.tbl_repayment_plan a
inner join (select distinct loan_provide_no,user_id,user_name,due_date,to_date(actual_repayment_date) as actual_repayment_date from tmp_innofin.vqq_rpt_FPJKLOAN_statement where dt = current_date) b 
	on a.loan_provide_no = b.loan_provide_no and to_date(a.actual_repayment_date) = to_date(b.actual_repayment_date)
where a.status = 2 and to_date(a.actual_repayment_date) <= to_date(a.repayment_date) 
and to_date(a.actual_repayment_date) >= add_months(current_date,-2) and to_date(a.actual_repayment_date) >= b.due_date
;


select
 loan_provide_no as `借据号`,
 due_date as `委案时间`,
 loan_channel as `放款渠道`,
 user_id as `user_id`, 
 user_name as `姓名`,
 period as `逾期期数`,
 capital_amt as `委案本金`,
 interest_amt as `委案息费`,
 overdue_interest_amt as `委案违约`,
 overdue_days as `逾期天数`,
 overdue_months as `逾期阶段`,
 actual_repay_amt as `实际回款`,
 actual_repayment_date as `回款时间`
from  tmp_innofin.vqq_rpt_FPJKLOAN_statement where dt = current_date 
