------------------------订单表更改 ----------------------------
--支付订单表中nqh_merchant_enable对订单的类型分类不够细致，因此换为dim_paydb.dimmerchant
--将olap_nqhuse改为olap_nqhuse_update,将buname改为bu


use tmp_innofin;

drop table tmp_innofin.report_use_uid_enable;
create table tmp_innofin.report_use_uid_enable as
select distinct lower(trim(user_id)) as uid,
       (case when substr(ious_begin_time,1,10)<substr(create_time,1,10) then update_time else ious_begin_time end) as createtime
  from ods_innofin.user_contract                  --依赖87282，每天01:10更新完毕
 where create_time >= '2016-10-26 18:00:00'
   and contract_status = 1;

use tmp_innofin;
drop table tmp_innofin.report_use_uid_liv;
create table tmp_innofin.report_use_uid_liv as
select distinct lower(trim(uid)) as uid
  from dw_mobdb.factmbpageview                   --依赖何蜀波的205，每天06:00更新完毕
 where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
   and starttime >= '${zdt.addDay(-1).format("yyyy-MM-dd")}'
   and starttime <  '${zdt.format("yyyy-MM-dd")}'
   and length(trim(uid)) >= 1;
--select d,count(*) from tmp_innofin.report_use_uid_liv group by d order by d desc limit 99

use tmp_innofin;
drop table tmp_innofin.report_use_uid_all;
create table tmp_innofin.report_use_uid_all as
select a.grp,a.uid,
       (case when loan.uid is not null or enb.createtime<'${zdt.addDay(-1).format("yyyy-MM-dd")}' then 1 else 0 end) as flag_enable,
       (case when pay.uid is null and loan.uid is null and liv.uid is null then 0 else 1 end) as flag_liv,
       (case when pay.uid is null and loan.uid is null then 0 else 1 end) as flag_pay,
       (case when loan.uid is null then 0 else 1 end) as flag_loan
  from ods_innofin.nqh_uid_cfb a
  left join (select uid,min(createtime) as createtime from tmp_innofin.report_use_uid_enable where createtime<='${zdt.format("yyyy-MM-dd")}' group by uid) enb
    on a.uid = enb.uid
  left join tmp_innofin.report_use_uid_liv liv
    on a.uid = liv.uid
  left join (select distinct lower(uid) as uid from dw_paypubdb.factnqhuserorder where ord_all>0 and orderdate='${zdt.addDay(-1).format("yyyy-MM-dd")}') pay     --依赖罗建香的12779，每天07:00更新完毕
    on a.uid = pay.uid
  left join (select distinct lower(uid) as uid from dw_paypubdb.factnqhuserorder where ord_loanpay>0 and orderdate='${zdt.addDay(-1).format("yyyy-MM-dd")}') loan
    on a.uid = loan.uid;

use tmp_innofin;
drop table tmp_innofin.report_use_uid_sta;
create table tmp_innofin.report_use_uid_sta as
select 
m.grp,
m.cnt_enable,
m.cnt_liv,
m.cnt_pay,
n.uid_count,
m.cnt_loan,
concat(cast(cast(100.0*m.cnt_loan/n.uid_count as decimal(10,1)) as string),'%') as pct_loan_pay
from
(select (case when grp='emp' then 'a_员工'
             when grp='train' then 'b_火车票'
             when grp='cross' then 'c_交叉用户'
             when grp='flt' then 'd_机票'
             when grp='vacation' then 'e_度假'
             when grp='train2' then 'f_火车票2'
             when grp='flt2' then 'g_机票2'
             when grp='nwl' then 'h_主动激活'
             else grp
        end
       ) as grp,
       sum(flag_enable) as cnt_enable,
       sum(flag_enable*flag_liv) as cnt_liv,
       sum(flag_enable*flag_pay) as cnt_pay,
       sum(flag_loan) as cnt_loan  
  from tmp_innofin.report_use_uid_all
   where grp in ('emp','train','cross','flt','vacation','train2','flt2','nwl')
   group by grp) m
  left join 
  (select 
  (case when grp='1_携程员工' then 'a_员工'
             when grp='2_火车票' then 'b_火车票'
             when grp='3_交叉用户' then 'c_交叉用户'
             when grp='4_机票' then 'd_机票'
             when grp='5_度假' then 'e_度假'
             when grp='6_火车票2' then 'f_火车票2'
             when grp='7_机票2' then 'g_机票2'
             when grp='8_主动激活' then 'h_主动激活'
             else grp
        end
       ) as grp,
  count(uid) as uid_count
  from dw_innofin.olap_nqhuse				----依赖90784
  where orderdate = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
  group by (case when grp='1_携程员工' then 'a_员工'
             when grp='2_火车票' then 'b_火车票'
             when grp='3_交叉用户' then 'c_交叉用户'
             when grp='4_机票' then 'd_机票'
             when grp='5_度假' then 'e_度假'
             when grp='6_火车票2' then 'f_火车票2'
             when grp='7_机票2' then 'g_机票2'
             when grp='8_主动激活' then 'h_主动激活'
             else grp
        end
       )) n
  on m.grp = n.grp;
--select * from tmp_innofin.report_use_uid_sta order by grp limit 99


---------------------------- 订单 ----------------------------
use tmp_innofin;
drop table report_use_order_sta;
create table report_use_order_sta as
select 
	a.orderdate as orderdate,
	a.bu as bu,
	a.cnt_ord as cnt_ord,
	b.ord_support as ord_support,
	a.cnt_nqh as cnt_nqh,
	concat(cast(cast(100.0*a.cnt_nqh/b.ord_support as decimal(10,1)) as string),'%') as pct_loan,
	concat(cast(cast(100.0*a.cnt_nqh/a.cnt_ord as decimal(10,1)) as string),'%') as pct_loanall
from
(select f.orderdate as orderdate,
       mct.merchantname as bu,
       sum(case when f.orderdate>=u.createtime then f.ord_all else 0 end) as cnt_ord,
       sum(f.ord_loanpay) as cnt_nqh
  from dw_paypubdb.factnqhuserorder f
  left join dim_paydb.dimmerchant mct
    on f.merchantid = mct.merchantid
  left join tmp_innofin.report_use_uid_enable u
    on lower(trim(f.uid)) = u.uid
 where f.orderdate >= '${zdt.addDay(-7).format("yyyy-MM-dd")}'
 group by f.orderdate,mct.merchantname) a
left join
(select 
	orderdate as orderdate,
	bu as bu,
  	sum(ord_support) as ord_support
  from dw_innofin.olap_nqhuse_update			--依赖90784
  where d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}'
  group by orderdate,bu) b
  on a.bu = b.bu
  and a.orderdate = b.orderdate
  where b.ord_support is not null;

insert into table report_use_order_sta
select orderdate,
       '总计',
       sum(cnt_ord) as cnt_ord,
       sum(ord_support) as ord_support,
       sum(cnt_nqh) as cnt_nqh,
       concat(cast(cast(100.0*sum(cnt_nqh)/sum(ord_support) as decimal(10,1)) as string),'%') as pct_loan,
       concat(cast(cast(100.0*sum(cnt_nqh)/sum(cnt_ord) as decimal(10,1)) as string),'%') as pct_loanall
  from report_use_order_sta
 group by orderdate;
 
--排序需要
drop table report_use_order_sta_01;
create table report_use_order_sta_01 as
select 
	a.*,
    b.number as number
  from report_use_order_sta a
  left join bu_order_0622 b
  on a.bu = b.BU
   order by a.orderdate desc,number asc
   limit 50
 
 
 


--select * from tmp_innofin.report_use_order_sta order by orderdate desc,buname limit 999