use tmp_innofin;
drop table if exists vqq_tmp_active_and_pay;
create table vqq_tmp_active_and_pay as 
select a.d,a.uid,case 
		when b.contract_status = 1 then 1 
		when b.contract_status = 4 and ious_end_time >= current_date then 4
		else 0 end as contract_status 
from (select distinct d,uid from dw_mobdb.factmbpageview where d between '2018-01-11' and '2018-01-17' and pagecode = 'widget_pay_main') a   
left join ods_innofin.user_contract b on lower(a.uid) = lower(b.user_id);


select d,contract_status,count(distinct uid) from tmp_innofin.vqq_tmp_active_and_pay group by d,contract_status;

select distinct uid from tmp_innofin.vqq_tmp_active_and_pay order by random() limit 10000;

use cfbdb;
create table vqq_active_and_pay_uid as 
select a.d,a.uid,b.openid from vqq_tmp_active_and_pay a 
inner join 