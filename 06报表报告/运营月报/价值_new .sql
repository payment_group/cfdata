-- 2）客户价值

-- 数据准备
-- 实际支付
use tmp_innofin;
drop table if exists tmp_innofin.vqq_user_value_payment;
CREATE TABLE if not exists tmp_innofin.vqq_user_value_payment (
  orderid string COMMENT '业务订单号',
  orderdate string comment '订单日期',
  gatheringtype string COMMENT '支付标记',
  totalamount float COMMENT '金额'
);
-- 拉有效数据
insert overwrite table tmp_innofin.vqq_user_value_payment
select orderid,to_date(main_createtime),gatheringtype,totalamount-nvl(payamt_coupon,0)-nvl(payamt_wallet,0) as totalamount from dw_paypubdb.FactPayGatheringMain_Loan
where applys_cnt>0;
-- 计算实际支付
use tmp_innofin;
drop table if exists  tmp_innofin.vqq_user_value_actualpayment;
CREATE TABLE if not exists tmp_innofin.vqq_user_value_actualpayment (
  order_id string COMMENT '业务订单号',
  order_date string comment '订单日期',
  actual_amount float COMMENT '金额'
);

insert overwrite table tmp_innofin.vqq_user_value_actualpayment
select orderid,orderdate,
	sum(case when gatheringtype='D' then totalamount else -totalamount end)
from tmp_innofin.vqq_user_value_payment group by orderid,orderdate; 

use tmp_innofin;
drop table if exists  tmp_innofin.vqq_user_value_orders;
CREATE TABLE if not exists tmp_innofin.vqq_user_value_orders (
  uid string,
  order_id string COMMENT '业务订单号',
  order_date string comment '订单日期',
  actual_amount float COMMENT '金额'
);
insert overwrite table tmp_innofin.vqq_user_value_orders
select uid,order_id,order_date,actual_amount
from ods_innofin.fact_loan_order a 
inner join tmp_innofin.vqq_user_value_actualpayment b on a.orderid = b.order_id and actual_amount>0;


-- 分期
use tmp_innofin;
drop table if exists  tmp_innofin.vqq_user_value_installment;
CREATE TABLE if not exists tmp_innofin.vqq_user_value_installment (
  uid string,
  order_id string COMMENT '业务订单号',
  order_date string comment '订单日期',
  install_amount float COMMENT '金额'
);

insert overwrite table tmp_innofin.vqq_user_value_installment
select uid,orderid,creationdate,amt_dr from ods_innofin.fact_loan_order_ist where installmentnum > 0;

--合并
-- 明细
use tmp_innofin;
create table vqq_user_value(
	uid string comment 'uid',
	active_date string,
	order_amount_value decimal(18,2) comment '实际支付0.3%',
	install_amount_value decimal(18,2) comment '分期金额0.36%',
	user_value decimal(18,2)
) comment '用户价值，截止到计算日'
partitioned by (
	dt string comment '计算日期'
);

insert overwrite table tmp_innofin.vqq_user_value partition(dt ='${zdt.format("yyyy-MM-dd")}')
select a.uid, a.time_apl,
	0.003 * nvl(b.actual_amount,0),
	0.0036 * nvl(c.install_amount,0),
	0.003 * nvl(b.actual_amount,0) + 0.0036 * nvl(c.install_amount,0)
	from tmp_innofin.nqh_uid_apl a
	left join (select uid,sum(actual_amount) as actual_amount from tmp_innofin.vqq_user_value_orders where order_date < '${zdt.format("yyyy-MM-dd")}' group by uid) b on lower(a.uid) = lower(b.uid)
	left join (select uid,sum(install_amount) as install_amount from tmp_innofin.vqq_user_value_installment where order_date < '${zdt.format("yyyy-MM-dd")}' group by uid) c on lower(a.uid) = lower(c.uid)
	where a.time_apl < '${zdt.format("yyyy-MM-dd")}';	
-- 分组
use tmp_innofin;
create table vqq_user_value_group(
	uid string comment 'uid',
	active_date string,
	active_date_group string,
	user_value decimal(18,2),
	user_value_group string
) comment '用户价值，截止到计算日'
partitioned by (
	dt string comment '计算日期'
);

insert overwrite table tmp_innofin.vqq_user_value_group partition(dt ='${zdt.format("yyyy-MM-dd")}')
select uid, active_date, substring(active_date,1,7),
	(user_value),
	case when (user_value) <= 0 then '0' 
		when (user_value) > 0 and  (user_value) <= 10 then '0-10'
		when (user_value) > 10 and (user_value) <= 30 then '10-30'
		when (user_value) > 30 and (user_value) <= 50 then '30-50'
		when (user_value) > 50 and (user_value) <= 100 then '50-100'
		when (user_value) > 100 then '>100' end
	from tmp_innofin.vqq_user_value where dt = '${zdt.format("yyyy-MM-dd")}';
	
-- report 表 
-- 区间分布
use tmp_innofin;
create table if not exists vqq_report_user_value_1(
	value_range string,
	user_count int,
	user_ratio double
) partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_report_user_value_1 partition(dt ='${zdt.format("yyyy-MM-dd")}')
select a.user_value_group, user_count, round(100.0*user_count/total_count,2)
	from (select user_value_group, 'this' as m,count(1) as user_count from tmp_innofin.vqq_user_value_group where dt = '${zdt.format("yyyy-MM-dd")}' group by user_value_group) a
	inner join 
	(select 'this' as m, count(1) as total_count from tmp_innofin.vqq_user_value_group where  dt = '${zdt.format("yyyy-MM-dd")}') b on a.m = b.m;

-- 平均价值按激活年月
--
use tmp_innofin;
create table if not exists vqq_report_user_value_2(
	active_ym string,
	user_count int,
	avg_value double
) partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_report_user_value_2 partition(dt ='${zdt.format("yyyy-MM-dd")}')
select active_date_group, count(1), round(avg(user_value),2) from tmp_innofin.vqq_user_value_group where dt = '${zdt.format("yyyy-MM-dd")}' group by active_date_group;

-- ART
-- 分布
select a.value_range as `区间`, a.user_count as `上月用户数`, b.user_count as `本月用户数`,
	a.user_ratio as `上月占比%`, b.user_ratio as `本月占比%`
	from 
	(select * from tmp_innofin.vqq_report_user_value_1 where dt = add_months(from_unixtime(unix_timestamp(),'yyyy-MM-01'),-1)) a
	inner join 
	(select * from tmp_innofin.vqq_report_user_value_1 where dt = from_unixtime(unix_timestamp(),'yyyy-MM-01')) b on a.value_range = b.value_range
 order by `区间` limit 10
 
-- 平均
select b.active_ym as `激活年月`,b.user_count as`用户数`,nvl(a.avg_value,0.0) as `上月平均价值`, b.avg_value as `本月平均价值` from 
	(select * from tmp_innofin.vqq_report_user_value_2 where dt = add_months(from_unixtime(unix_timestamp(),'yyyy-MM-01'),-1)) a
	right join 
	(select * from tmp_innofin.vqq_report_user_value_2 where dt = from_unixtime(unix_timestamp(),'yyyy-MM-01')) b on a.active_ym = b.active_ym 

	