select a.value_group as `区间`, 
b.user_count as `上期用户数`,
cast(cast(100*(b.user_count/d.totalcount) as decimal(9,2)) as double)  as `上期占比%`,
a.user_count as `本期用户数`, 
cast(cast(100*(a.user_count/c.totalcount)as decimal(9,2)) as double) as `本期占比%`,
cast(cast((100*(a.user_count-b.user_count)/b.user_count) as decimal(9,2))as double) as `增长率%`   
from (select dt,value_group, user_count  from dw_innofin.vqq_user_value_count where dt='2017-08-01') a
inner join (select dt,value_group, user_count from dw_innofin.vqq_user_value_count where dt=add_months('2017-08-01',-1)) b on a.value_group  = b.value_group 
inner join (select dt,sum(user_count) as totalcount from dw_innofin.vqq_user_value_count where dt='2017-08-01' group by dt) c on a.dt = c.dt 
inner join (select dt,sum(user_count) as totalcount from dw_innofin.vqq_user_value_count where dt=add_months('2017-08-01',-1) group by dt) d on b.dt=d.dt 
order by  `本期用户数` desc limit  10;

select a.value_group as `区间`, 
b.user_count as `上期用户数`,
cast(cast(100*(b.user_count/d.totalcount) as decimal(9,2)) as double)  as `上期占比%`,
a.user_count as `本期用户数`, 
cast(cast(100*(a.user_count/c.totalcount)as decimal(9,2)) as double) as `本期占比%`,
cast(cast((100*(a.user_count-b.user_count)/b.user_count) as decimal(9,2))as double) as `增长率%`   
from (select dt,value_group, user_count  from dw_innofin.vqq_user_value_count where dt=#TODAY#) a
inner join (select dt,value_group, user_count from dw_innofin.vqq_user_value_count where dt=add_months(#TODAY#,-1)) b on a.value_group  = b.value_group 
inner join (select dt,sum(user_count) as totalcount from dw_innofin.vqq_user_value_count where dt=#TODAY# group by dt) c on a.dt = c.dt 
inner join (select dt,sum(user_count) as totalcount from dw_innofin.vqq_user_value_count where dt=add_months(#TODAY#,-1) group by dt) d on b.dt=d.dt 
order by  `本期用户数` desc limit  10;
 