-- 2）客户价值

-- 数据准备
-- 实际支付
use tmp_innofin;
drop table if exists tmp_innofin.user_value_payment;
CREATE TABLE if not exists tmp_innofin.user_value_payment (
  orderid string COMMENT '业务订单号',
  gatheringtype string COMMENT '支付标记',
  totalamount float COMMENT '金额'
);
-- 拉有效数据
insert overwrite table tmp_innofin.user_value_payment
select orderid,gatheringtype,totalamount-COALESCE(payamt_coupon,0)-COALESCE(payamt_wallet,0) as totalamount from dw_paypubdb.FactPayGatheringMain_Loan
where applys_cnt>0;
-- 计算实际支付
use tmp_innofin;
drop table if exists  tmp_innofin.user_value_actualpayment;
CREATE TABLE if not exists tmp_innofin.user_value_actualpayment (
  orderid string COMMENT '业务订单号',
  damount float COMMENT '支付',
  ramount float COMMENT '退款',
  actualamount float COMMENT '金额'
);

insert overwrite table tmp_innofin.user_value_actualpayment
select orderid,
	sum(case when gatheringtype='D' then totalamount else 0 end), 
	sum(case when gatheringtype='R' then totalamount else 0 end), 
	sum(case when gatheringtype='D' then totalamount else -totalamount end)
from tmp_innofin.user_value_payment group by orderid; 

-- 明细数据
use tmp_innofin;
drop table if exists  tmp_innofin.user_value_order_actualpayment;
CREATE TABLE if not exists tmp_innofin.user_value_order_actualpayment (
  uid string,
  orderid string COMMENT '业务订单号',
  actualamount float COMMENT '金额'
);

insert overwrite table tmp_innofin.user_value_order_actualpayment
select b.uid,b.orderid,c.actualamount
	from 
	ods_innofin.user_contract a 
	inner join ods_innofin.fact_loan_order b on lower(trim(a.user_id)) = lower(trim(b.uid))
	inner join tmp_innofin.user_value_actualpayment c on b.orderid = c.orderid;

-- 2.1 按订单金额计算价值
use tmp_innofin;
drop table if exists  tmp_innofin.user_value_1;
create table if not exists user_value_1(
	uid string,
	user_value_amount decimal(18,2)
);

-----------
insert overwrite table tmp_innofin.user_value_1
select uid, sum(actualamount*0.003)
	from tmp_innofin.user_value_order_actualpayment 
	group by uid;

-- 2.2 按订单金额和分期金额（分期）
use tmp_innofin;
drop table if exists  tmp_innofin.user_value_2;

create table if not exists user_value_2(
	uid string,
	install_amount decimal(18,2)
);

insert overwrite table tmp_innofin.user_value_2
select b.uid,sum(amt_dr) * 0.0036 
	from ods_innofin.fact_loan_order_ist b
	where b.installmentnum > 0
	group by b.uid;

-- 2.3）合并
use dw_innofin;
create table vqq_user_value(
	uid string comment 'uid',
	user_value_amount decimal(18,2) comment '实际支付0.3%',
	install_amount decimal(18,2) comment '分期金额0.36%'
) comment '周报月报用户价值'
partitioned by (
	dt string comment '计算日期'
);

insert overwrite table dw_innofin.vqq_user_value partition(dt ='${zdt.format("yyyy-MM-dd")}')
select a.uid, a.user_value_amount,if(b.uid is null,0,b.install_amount) 
	from tmp_innofin.user_value_1 a
	left join tmp_innofin.user_value_2 b
	on lower(trim(a.uid)) = lower(trim(b.uid));
-- 0	
insert into table dw_innofin.vqq_user_value partition(dt ='${zdt.format("yyyy-MM-dd")}')
select a.user_id,0,0 from 
	(select user_id from ods_innofin.user_contract where contract_status = 1) a 
	left join (select * from dw_innofin.vqq_user_value where dt ='${zdt.format("yyyy-MM-dd")}') b on lower(trim(a.user_id)) = lower(trim(b.uid)) 
	where b.uid is null;

use dw_innofin;
create table vqq_user_value_count(
	value_group string comment '价值区间',
	user_count int comment '用户数'
) comment '周报月报用户价值-区间'
partitioned by (
	dt string comment '计算日期'
);

--分布
insert overwrite table dw_innofin.vqq_user_value_count partition(dt ='${zdt.format("yyyy-MM-dd")}')
select '0', count(1) from dw_innofin.vqq_user_value where dt ='${zdt.format("yyyy-MM-dd")}' and user_value_amount+install_amount = 0
union all
select '0-10' , count(1) from dw_innofin.vqq_user_value where dt ='${zdt.format("yyyy-MM-dd")}' and user_value_amount+install_amount > 0 and user_value_amount+install_amount <=10
union all
select '10~30' , count(1) from dw_innofin.vqq_user_value where dt ='${zdt.format("yyyy-MM-dd")}' and user_value_amount+install_amount > 10 and user_value_amount+install_amount <=30
union all
select '30~50' , count(1) from dw_innofin.vqq_user_value where dt ='${zdt.format("yyyy-MM-dd")}' and user_value_amount+install_amount > 30 and user_value_amount+install_amount <=50
union all
select '50~100' , count(1) from dw_innofin.vqq_user_value where dt ='${zdt.format("yyyy-MM-dd")}' and user_value_amount+install_amount > 50 and user_value_amount+install_amount <=100
union all
select '100+' , count(1) from dw_innofin.vqq_user_value where dt ='${zdt.format("yyyy-MM-dd")}' and user_value_amount+install_amount > 100;
---平均用户价值（包含0用户的）
use dw_innofin;
create table vqq_user_value_by_activetime(
	uid string comment 'uid',
	active_time string comment '激活时间',
	user_value_amount decimal(9,2) comment '客户价值,实际支付0.5%+分期金额0.3%' 
) comment '按激活时间计算客户价值' 
partitioned by (
	dt string comment '计算日期'
);

insert overwrite table dw_innofin.vqq_user_value_by_activetime partition(dt ='${zdt.format("yyyy-MM-dd")}')
select a.uid,b.active_date,a.user_value_amount + install_amount 
	from dw_innofin.vqq_user_value a
	inner join dw_innofin.vqq_nqh_user_active_date_snap b on lower(trim(a.uid)) = lower(trim(b.uid))
	where a.dt ='${zdt.format("yyyy-MM-dd")}';
----- 统计表
use dw_innofin;
create table vqq_user_value_by_activetime_count(
	active_month string comment '激活时间-年月',
	user_count int comment '用户数',
	user_value_amount decimal(9,2) comment '平均客户价值' 
) comment '按激活时间计算客户价值' 
partitioned by (
	dt string comment '计算日期'
);
insert overwrite table dw_innofin.vqq_user_value_by_activetime_count partition(dt ='${zdt.format("yyyy-MM-dd")}')
select substring(active_time,1,7),count(1),avg(user_value_amount)
from dw_innofin.vqq_user_value_by_activetime where dt='${zdt.format("yyyy-MM-dd")}';
group by substring(active_time,1,7); 
-- 报表
-- 图
select substring(active_time,1,7) as `激活时间（年月）`, count(1) ,cast(avg(user_value_amount) as double) as `平均价值(实际支付0.5%+分期金额0.3%)`
from dw_innofin.vqq_user_value_by_activetime where d= from_unixtime(unix_timestamp(),'yyyy-MM-dd') 
group by substring(active_time,1,7) 
-- 数据
select substring(active_time,1,7) as `激活时间（年月）`, count(1) ,cast(avg(user_value_amount) as double) as `平均价值(实际支付0.5%+分期金额0.3%)`
from dw_innofin.vqq_user_value_by_activetime where d= from_unixtime(unix_timestamp(),'yyyy-MM-dd') 
group by substring(active_time,1,7)
union all 
select '全部' as `激活时间（年月）`, count(1) ,cast(avg(user_value_amount) as double) as `平均价值(实际支付0.5%+分期金额0.3%)`
from dw_innofin.vqq_user_value_by_activetime where d= from_unixtime(unix_timestamp(),'yyyy-MM-dd') 


 -- sample data
insert overwrite table dw_innofin.vqq_user_value_by_activetime partition(dt ='2017-07-11')
select a.uid,b.apply_active_date,a.user_value_amount + install_amount 
	from dw_innofin.vqq_user_value a
	inner join dw_innofin.nqh_user_markting_flag b on lower(trim(a.uid)) = lower(trim(b.uid))
	where a.dt ='2017-07-11' and b.dt ='2017-08-13';
    
insert overwrite table dw_innofin.vqq_user_value_by_activetime_count partition(dt ='2017-07-11')
select substring(active_time,1,7),count(1),avg(user_value_amount)
from dw_innofin.vqq_user_value_by_activetime where dt='2017-07-11'
group by substring(active_time,1,7); 