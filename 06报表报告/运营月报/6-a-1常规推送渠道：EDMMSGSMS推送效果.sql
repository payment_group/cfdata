-- 6-a-1常规推送渠道：EDM/MSG/SMS推送效果
-- 每月1日出mon-2,mon-3数据并比对
-- use dw_innofin;
-- create table markting_batch_result(
-- 	plancode string comment '批次号',
-- 	sentall int comment '发送总条目',
-- 
-- 	sent1 int comment '营销到达',
-- 	sent1_apply int comment '申请数',
-- 	sent1_finish int comment '完成流程数',
-- 	sent1_success int comment '申请成功数',
-- 
-- 	sent0 int comment '营销未到达',
-- 	sent0_apply int comment '申请数',
-- 	sent0_finish int comment '完成流程数',
-- 	sent0_success int comment'申请成功数'
-- ) comment '营销批次统计结果'
-- partitioned by (dt string comment '数据日期=T-1'); 
-- 将以上数据按批次发生月分区存放

 use tmp_innofin;
 create table if not exists vqq_report_markting_batch_month(
 	month int,
 	ym string
 ) comment '月'; 
insert overwrite table tmp_innofin.vqq_report_markting_batch_month
select -3,concat(substring(add_months('${zdt.format("yyyy-MM-dd")}',-3),3,2),'年',substring(add_months('${zdt.format("yyyy-MM-dd")}',-3),6,2),'月');
insert into table tmp_innofin.vqq_report_markting_batch_month
select -2,concat(substring(add_months('${zdt.format("yyyy-MM-dd")}',-2),3,2),'年',substring(add_months('${zdt.format("yyyy-MM-dd")}',-2),6,2),'月');

 use tmp_innofin;
 create table if not exists vqq_report_markting_batch_result(
 	plancode string comment '批次号',
 	sentall int comment '发送总条目',
 
 	sent1 int comment '营销到达',
 	sent1_apply int comment '申请数',
 	sent1_finish int comment '完成流程数',
 	sent1_success int comment '申请成功数',
 
 	sent0 int comment '营销未到达',
 	sent0_apply int comment '申请数',
 	sent0_finish int comment '完成流程数',
 	sent0_success int comment'申请成功数',
 	channel string,
 	ym string
 ) comment '营销批次统计结果'; 
-- mon-3明细
insert overwrite table tmp_innofin.vqq_report_markting_batch_result
select plancode,sentall,sent1,sent1_apply,sent1_finish,sent1_success,sent0,sent0_apply,sent0_finish,sent0_success,
if(upper(substring(plancode,1,1)) = 'E','邮件',if(upper(substring(plancode,1,1)) = 'S','短信','站内信')),
concat(substring(plancode,5,2),'年',substring(plancode,7,2),'月') 
from dw_innofin.markting_batch_result a where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
and substring(plancode,5,4) = concat(substring(add_months('${zdt.format("yyyy-MM-dd")}',-3),3,2),substring(add_months('${zdt.format("yyyy-MM-dd")}',-3),6,2));
-- mon-2明细
insert into table tmp_innofin.vqq_report_markting_batch_result
select plancode,sentall,sent1,sent1_apply,sent1_finish,sent1_success,sent0,sent0_apply,sent0_finish,sent0_success,
if(upper(substring(plancode,1,1)) = 'E','邮件',if(upper(substring(plancode,1,1)) = 'S','短信','站内信')),
concat(substring(plancode,5,2),'年',substring(plancode,7,2),'月')  
from dw_innofin.markting_batch_result where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}'
and substring(plancode,5,4) = concat(substring(add_months('${zdt.format("yyyy-MM-dd")}',-2),3,2),substring(add_months('${zdt.format("yyyy-MM-dd")}',-2),6,2));
-- 统计值
 use tmp_innofin;
 create table if not exists vqq_report_markting_batch_result_total(
    month string,
 	channel string comment '渠道',
 	sentall int comment '发送总条目',
 
 	sent1 int comment '营销到达',
 	sent1_apply int comment '申请数',
 	sent1_finish int comment '完成流程数',
 	sent1_success int comment '申请成功数',
 
 	sent0 int comment '营销未到达',
 	sent0_apply int comment '申请数',
 	sent0_finish int comment '完成流程数',
 	sent0_success int comment'申请成功数'
);

insert overwrite table tmp_innofin.vqq_report_markting_batch_result_total
select ym,'全部渠道',sum(sentall),sum(sent1),sum(sent1_apply),sum(sent1_finish),sum(sent1_success),sum(sent0),sum(sent0_apply),sum(sent0_finish),sum(sent0_success) 
from tmp_innofin.vqq_report_markting_batch_result where sent1_finish <= sent1_apply and sent0_finish<= sent0_apply group by ym;

insert into table tmp_innofin.vqq_report_markting_batch_result_total
select ym,channel,sum(sentall),sum(sent1),sum(sent1_apply),sum(sent1_finish),sum(sent1_success),sum(sent0),sum(sent0_apply),sum(sent0_finish),sum(sent0_success) 
from tmp_innofin.vqq_report_markting_batch_result where sent1_finish <= sent1_apply and sent0_finish<= sent0_apply group by ym,channel;

