use tmp_innofin;
create table if not exists vqq_rpt_deposit_proof_page_app(
	d string,
	uid string,
	
	e_sms int,
	e_wallet int,
	e_nqh int,
	e_score int,
	
	p_first int,
	p_proof int,
	p_applicant int,
	p_reveiver int,
	p_finish int
)partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_rpt_deposit_proof_page_app partition(dt = '${zdt.format("yyyy-MM-dd")}')
select distinct d,userid,
		max(case when pageid in ('10320654208') and query like '%mktype=sms%' then 1 else 0 end)  as e_sms,
		max(case when pageid in ('10320654208') and prevpageid in ('272040') then 1 else 0 end) as e_wallet,
		max(case when pageid in ('10320654208') and query like '%mktype=nqh%' then 1 else 0 end) as e_nqh,
		max(case when pageid in ('10320654208') and query like '%mktype=creditscore%' then 1 else 0 end) as e_score,
		max(case when pageid in ('10320654208') then 1 else 0 end) as p_first,
		max(case when pageid in ('10320654213') then 1 else 0 end) as p_proof,
		max(case when pageid in ('10320654215') then 1 else 0 end) as p_applicant,
		max(case when pageid in ('10320673925') then 1 else 0 end) as p_reveiver,
		max(case when pageid in ('10320654217') then 1 else 0 end) as p_finish
from dw_ubtdb.factpageview
where d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' and pageid in ('10320654208','10320654213','10320654215','10320654217','10320673925')
group by d,userid;

use tmp_innofin;
create table if not exists vqq_rpt_deposit_proof_page_h5(
	d string,
	uid string,
	
	e_sms int,
	
	p_first int,
	p_proof int,
	p_applicant int,
	p_reveiver int,
	p_finish int
)partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_rpt_deposit_proof_page_h5 partition(dt = '${zdt.format("yyyy-MM-dd")}')
select distinct d,userid,
		max(case when pageid in ('600004889') and query like '%mktype=sms%' then 1 else 0 end)  as e_sms,
       max(if(pageid in ('600004889'),1,0)) as p_first,
       max(if(pageid in ('10320605068'),1,0)) as p_proof,
       max(if(pageid in ('10320605069'),1,0)) as p_applicant,
       max(if(pageid in ('10320673921'),1,0)) as p_reveiver,
       max(if(pageid in ('10320605071'),1,0)) as p_finish
from dw_ubtdb.factpageview
where d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' 
	and pageid in ('600004889','10320605068','10320605069','10320673921','10320605071')
group by d,userid;

use tmp_innofin;
create table if not exists vqq_rpt_deposit_proof_page_online(
	d string,
	uid string,
	p_first int,
	p_proof int,
	p_finish int
)partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_deposit_proof_page_online partition(dt = '${zdt.format("yyyy-MM-dd")}')
select distinct d,userid,
       max(if(pageid in ('102618'),1,0)) as p_first,
       max(if(pageid in ('102619'),1,0)) as p_proof,
       max(if(pageid in ('102620'),1,0)) as p_finish
from dw_ubtdb.factpageview
where d >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' 
 and pageid in ('102618','102619','102620')
group by d,userid;
