select CAST(a.orderid as string) as orderid,case when c.orderstatus='0' then '待支付' when c.orderstatus='1' then '支付中' when c.orderstatus='2' then '审核中'
       when c.orderstatus='3' then '审核通过' when c.orderstatus='4' then '已成交' when c.orderstatus='5' then '审核失败' when c.orderstatus='7' then '开证中' 
       when c.orderstatus='8' then '已赎回'  when c.orderstatus='9' then '未提交' when c.orderstatus='10' then '审核未通过' else '未知' end as `订单状态`,
       c.starttime as `开具日期`,c.bookingdate as `预订日期`,case c.FundingType when 0 then '银行出资' when 1 then '用户贷款-携程出资' when 2 then '用户出资' else '' end as `贷款出资方`,
       a.applyer as `个人客户名称`,case when substring(cardno,17,1)%2=0 then 2 else 1 end as `性别`,'101' as `客户证件类型`,cast(cardno as string) as `证件号码`,
       substring(cardno,7,8) as `出生日期`,to_date(cardendtime) as `证件截止日期`,authority as `发证机构`,'156' as `国籍`,'04' as `职业`
       ,case when c.ordertype=2 then e.contactphone else b.mobilephone end as `手机号码`,
       case when c.ordertype=2 then e.contactphone else b.mobilephone end as `电话号码`,'156' as `单位地址-国家和地区`,'200335' as `单位地址邮政编码`,'上海市金钟路968号' as `单位详细地址`,
       '156' as `家庭地址-国家和地区`,'100020' as `家庭地址邮政编码`,case when ordertype=2 then concat(e.cityname,e.zonename,e.address) else concat(b.cityname,b.zonename,b.address) end as `家庭详细地址`,'' as `监护人关系`,'' as `监护人姓名`,
       '' as `监护人证件类型`,'' as `监护人证件号码`,'001' as `货币数字代码/币种`,'' as `钞汇标识`,a.applyamount as `交易金额`,case when c.ordertype=1 then c.orderproductdesc else c.period end as `存期`,'' as `通知种类`,'' as `备注`
from ods_innofin.OrderApplyer a
left join ods_innofin.OrderReceiver b on a.orderid=b.orderid
left join (
	select a.*,b.StartTime,0 as ExtOrderInfoID,case periodtype when 0 then '7天至89天' when 1 then '3个月' when 2 then '6个月' else '' end as period
	from ods_innofin.DepositOrder a
	join ods_innofin.OrderItem b on a.OrderID=b.OrderID and a.OrderType=0
	union all
	select a.*,b.StartTime,ExtOrderInfoID,case periodtype when 0 then '7天至89天' when 1 then '3个月' when 2 then '6个月' else '' end as period
	from ods_innofin.DepositOrder a
	join ods_innofin.MultiOrderItem b on a.OrderID=b.OrderID and a.OrderType=2
	union all 
	select a.*,b.StartTime,ExtOrderInfoID,case periodtype when 0 then '7天至89天' when 1 then '3个月' when 2 then '6个月' else '' end as period
	from ods_innofin.DepositOrder a
	join ods_innofin.GuarantyOrderItem b on a.OrderID=b.OrderID and a.OrderType=1
) c on a.OrderID=c.OrderID
left join ods_innofin.ExtOrderInfo e on c.ExtOrderInfoID=e.extorderinfoid
where c.orderstatus<>6 and c.starttime between '2017-12-11' and '2017-12-12';
--where c.orderstatus<>6 and c.starttime between #startdate# and #enddate#
