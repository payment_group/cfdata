-- 
use ods_innofin;
create table if not exists OrderReceiver(
	OrderReceiverID	bigint,
	OrderID	bigint,
	ContactName	string,
	Mobilephone	string,
	Email	string,
	Remark	string,
	ProvinceName	string,
	CityName	string,
	ZoneName	string,
	Address	string,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	DataChange_LastTime	string,
	IsDeleted	boolean

);


select OrderReceiverID,OrderID,ContactName,Mobilephone,Email,Remark,ProvinceName,CityName,ZoneName,Address,CreateBy,CreateTime,UpdateBy,DataChange_LastTime,IsDeleted from OrderReceiver with(nolock);

use ods_innofin;
create table if not exists ExtOrderInfo(
	ExtOrderInfoID	bigint,
	ExOrderID	bigint,
	EXOrderName	string,
	StartTime	string,
	EndTime	string,
	ValidityDays	smallint,
	Departure	string,
	Destination	string,
	IsNeedCancelVisa	smallint,
	BelongCompany	string,
	ProvinceName	string,
	CityName	string,
	ZoneName	string,
	Address	string,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	DataChange_LastTime	string,
	ContactName	string,
	ContactPhone	string
);

select ExtOrderInfoID,ExOrderID,EXOrderName,StartTime,EndTime,ValidityDays,Departure,Destination,IsNeedCancelVisa,BelongCompany,ProvinceName,CityName,ZoneName,Address,IsDeleted,CreateBy,CreateTime,UpdateBy,DataChange_LastTime,ContactName,ContactPhone from ExtOrderInfo with (nolock);

