-- 
use ods_innofin;
create table if not exists OrderDelivery(
	OrderDeliveryID	bigint,
	OrderID	bigint,
	FlowCompanyName	string,
	FlowTicketNumber	string,
	FlowStatus	smallint,
	FlowRemark	string,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	DataChange_LastTime	string,
	OrderApplyerID	bigint
);


select OrderDeliveryID,OrderID,FlowCompanyName,FlowTicketNumber,FlowStatus,FlowRemark,IsDeleted,CreateBy,CreateTime,UpdateBy,DataChange_LastTime,OrderApplyerID from OrderDelivery with(nolock);
