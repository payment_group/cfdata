-- 
use ods_innofin;
create table if not exists GuarantyOrderItem(
 GuarantyOrderItemID bigint,
 OrderID bigint,
 ExtOrderInfoID bigint,
 ExOrderID bigint,
 ProductID bigint,
 ProductName string,
 PeriodType smallint,
 Rate decimal(18,8),
 ServiceFee Decimal(18,2),
 GuarantyRate decimal(18,8),
 GuarantyFee Decimal(18,2),
 InvoicePostage Decimal(18,2),
 MasterCode string,
 DataChange_LastTime string,
 IsDeleted boolean,
 CreateBy string,
 CreateTime string,
 UpdateBy string,
 StartTime string,
 EndTime string,
 ServiceDays smallint
);


select GuarantyOrderItemID, OrderID, ExtOrderInfoID, ExOrderID, ProductID, ProductName, PeriodType, Rate, ServiceFee, GuarantyRate, GuarantyFee, InvoicePostage, MasterCode, DataChange_LastTime, IsDeleted, CreateBy, CreateTime, UpdateBy, StartTime, EndTime, ServiceDays from GuarantyOrderItem with (nolock);


use ods_innofin;
create table if not exists multiorderitem(
	MultiOrderItemID	bigint,
	OrderID	bigint,
	ExtOrderInfoID	bigint,
	ExOrderID	bigint,
	Rate	decimal(18,8),
	ServiceFee	 Decimal(18,2),
	GuarantyRate	decimal(18,8),
	GuarantyFee	 Decimal(18,2),
	ProductID	bigint,
	ProductName	string,
	PeriodType	smallint,
	MaterialFee	 Decimal(18,2),
	Postage	 Decimal(18,2),
	InvoicePostage	Decimal(18,2),
	Quantity	int,
	IsNeedAccountList	smallint,
	MasterCode	string,
	DataChange_LastTime	string,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	StartTime	string,
	EndTime	string,
	ServiceDays	smallint
);
select MultiOrderItemID,OrderID,ExtOrderInfoID,ExOrderID,Rate,ServiceFee,GuarantyRate,GuarantyFee,ProductID,ProductName,PeriodType,MaterialFee,Postage,InvoicePostage,Quantity,IsNeedAccountList,MasterCode,DataChange_LastTime,IsDeleted,CreateBy,CreateTime,UpdateBy,StartTime,EndTime,ServiceDays from multiorderitem with (nolock);

use ods_innofin;
create table if not exists orderincome(
	OrderIncomeID	bigint,
	OrderID	bigint,
	IncomeAmount	decimal(18,2),
	RechargeReferenceID	string,
	RechargeStatus	smallint,
	RechargeRetryTimes	smallint,
	DataChange_LastTime	string,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	ExecNextTime	string
);
select OrderIncomeID,OrderID,IncomeAmount,RechargeReferenceID,RechargeStatus,RechargeRetryTimes,DataChange_LastTime,IsDeleted,CreateBy,CreateTime,UpdateBy,ExecNextTime from orderincome with (nolock);

use ods_innofin;
create table if not exists OrderItem(
	OrderItemID	bigint,
	OrderID	bigint,
	ProductID	bigint,
	ProductName	string,
	PeriodType	smallint,
	StartTime	string,
	EndTime	string,
	ServiceDays	smallint,
	MinAmount	decimal(18,2),
	MaxAmount	decimal(18,2),
	Rates	decimal(18,8),
	MaterialFee	decimal(18,2),
	Postage	decimal(18,2),
	ServiceFee	decimal(18,2),
	MasterCode	string,
	DataChange_LastTime	string,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	Quantity	int,
	IsNeedAccountDetails	smallint,
	InvoicePostage	decimal(18,2),
	IssueDate	string,
	DeductionTotalAmount	decimal(18,2),
	Purpose string
);

select OrderItemID,OrderID,ProductID,ProductName,PeriodType,StartTime,EndTime,ServiceDays,MinAmount,MaxAmount,Rates,MaterialFee,Postage,ServiceFee,MasterCode,DataChange_LastTime,IsDeleted,CreateBy,CreateTime,UpdateBy,Quantity,IsNeedAccountDetails,InvoicePostage,IssueDate,DeductionTotalAmount from OrderItem with (nolock);

use ods_innofin;
create table if not exists OrderPaymentDetail(

	OrderPaymentDetailID	bigint,
	OrderID	bigint,
	OrderPaymentID	bigint,
	PaymentWayID	string,
	CatalogName	string,
	PaidAmount	decimal(18,2),
	PaidStatus	string,
	Remark	string,
	DataChange_LastTime	string,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string
);
select OrderPaymentDetailID,OrderID,OrderPaymentID,PaymentWayID,CatalogName,PaidAmount,PaidStatus,Remark,DataChange_LastTime,IsDeleted,CreateBy,CreateTime,UpdateBy from OrderPaymentDetail with (nolock);

use ods_innofin;
create table if not exists Refund(
	RefundID	bigint,
	OrderID	bigint,
	RefundStatus	smallint,
	RefundTime	string,
	PaidTime	string,
	Amount	decimal(18,2),
	CreateBy	string,
	DataChange_LastTime	string,
	IsDeleted	boolean,
	UpdateBy	string,
	IsSettled	smallint
);

select RefundID,OrderID,RefundStatus,RefundTime,PaidTime,Amount,CreateBy,DataChange_LastTime,IsDeleted,UpdateBy,IsSettled from Refund with (nolock);

-- IncomeDetail
use ods_innofin;
create table if not exists IncomeDetail(

	IncomeDetailID	bigint,
	IncomeOptLogID	bigint,
	OrderID	string,
	Applyer	string,
	CardNo	string,
	Mobilephone	string,
	StartTime	string,
	EndTime	string,
	ServiceDays	string,
	Interest	string,
	MatchResult	smallint,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	DataChange_LastTime	string,
	ExcSerialID	string,
	Remark	string
);

select IncomeDetailID,IncomeOptLogID,OrderID,Applyer,CardNo,Mobilephone,StartTime,EndTime,ServiceDays,Interest,MatchResult,IsDeleted,CreateBy,CreateTime,UpdateBy,DataChange_LastTime,ExcSerialID,Remark from IncomeDetail with (nolock);



use ods_innofin;
create table if not exists IncomeOptLog(

	IncomeOptLogID	bigint,
	FileName	string,
	OptUser	string,
	OptTime	string,
	ProcessStatus	smallint,
	TotalCount	int,
	UnmatchCount	smallint,
	Remark	string,
	IsDeleted	boolean,
	CreateBy	string,
	CreateTime	string,
	UpdateBy	string,
	DataChange_LastTime	string,
	OptEmail	string
);


select IncomeOptLogID,FileName,OptUser,OptTime,ProcessStatus,TotalCount,UnmatchCount,Remark,IsDeleted,CreateBy,CreateTime,UpdateBy,DataChange_LastTime,OptEmail from IncomeOptLog with(nolock);
