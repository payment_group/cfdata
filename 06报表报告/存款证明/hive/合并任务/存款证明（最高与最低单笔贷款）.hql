﻿---最高与最低单笔贷款

select distinct date_add(current_date,-1) as `开具时间`,case when b.u is not null then '最高单笔贷款' when c.l is not null then '最低单笔贷款' end as `贷款额度`
		,a.`携程贷款金额`,a.`贷款天数`,a.`携程贷款利息`,a.`贷款利率`
from ods_innofin.vqq_rpt_deposit_proof_pp a
left join (
	select max(`携程贷款金额`) as u from ods_innofin.vqq_rpt_deposit_proof_pp 
		where`贷款出资方`='用户贷款-携程出资' and`订单状态` in ('已成交','审核通过','开证中') and to_date(`开具日期`)=date_add(current_date,-1))
 	b on a.`携程贷款金额`=b.u
left join (
	select min(`携程贷款金额`) as l from ods_innofin.vqq_rpt_deposit_proof_pp
		where`贷款出资方`='用户贷款-携程出资' and`订单状态` in ('已成交','审核通过','开证中') and to_date(`开具日期`)=date_add(current_date,-1))
 	c on a.`携程贷款金额`=c.l
where a.`贷款出资方`='用户贷款-携程出资' and`订单状态` in ('已成交','审核通过','开证中') and (b.u is not null or c.l is not null) and to_date(`开具日期`)=date_add(current_date,-1)
