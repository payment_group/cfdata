select date_add(current_date,-1) as date, case `存期` when '3m' then '3个月' when '6m' then '6个月' when '7' then '3个月内' else `存期` end as `存期`
          ,count(distinct `订单号`) as `回款订单数`,sum(`开证金额`) as `回款金额`,sum(`利息`) as `存款利息`
from 
(
SELECT distinct id.orderid as `订单号`,id.applyer as `客户名`,id.cardno as `证件号码`,id.mobilephone as `联系电话`,id.starttime as `订单申请日`
	  ,id.endtime as `订单到期日`,id.servicedays as `存期`,to_date(nvl(oi.starttime,moi.starttime)) as `存单起息日`
	  ,to_date(nvl(oi.endtime,moi.endtime)) as `存单到期日`,do.amount as `开证金额`
	  ,oa.BankInterest as `利息`,do.amount+oa.BankInterest as `本息合计`,nvl(oi.quantity,moi.quantity) as `开证数量`
	  ,od.FlowTicketNumber as `快递单号` 
	FROM 
		(select id.*,cast(substring(id.orderid,1,instr(id.orderid,'-')-1) as bigint) as oid
			from ods_innofin.IncomeDetail AS id
			INNER JOIN ods_innofin.IncomeOptLog AS iol ON id.IncomeOptLogID=iol.IncomeOptLogID
			WHERE iol.`filename` LIKE from_unixtime(unix_timestamp(date_add(current_date,-1),'yyyy-MM-dd'),'%yyyyMMdd%') and id.matchresult=1
		) id
		left join ods_innofin.OrderItem oi on id.oid=oi.orderid
	left join ods_innofin.DepositOrder do on id.oid=do.orderid
	left join ods_innofin.OrderApplyer oa on id.oid=oa.orderid
	left join ods_innofin.OrderDelivery  od on id.oid=od.orderid
	LEFT JOIN ods_innofin.MultiOrderItem AS moi ON id.oid=moi.OrderID
) a
group by `存期`
