---平均贷款期限与贷款利率

select date_add(current_date,-1) as `开具时间`,`总贷款金额`,sum(`贷款利率`*`携程贷款金额`)/b.`总贷款金额` as `贷款平均年利率`
from (select *,date_add(current_date,-1) as date
		from ods_innofin.vqq_rpt_deposit_proof_pp
		where `贷款出资方`='用户贷款-携程出资' and `订单状态` in ('已成交','审核通过','开证中') and to_date(`开具日期`)=date_add(current_date,-1) 
	) a
join (select sum(`携程贷款金额`) as `总贷款金额`,date_add(current_date,-1) as date
		from ods_innofin.vqq_rpt_deposit_proof_pp 
		where `贷款出资方`='用户贷款-携程出资' and `订单状态` in ('已成交','审核通过','开证中') and to_date(`开具日期`)=date_add(current_date,-1)
	) b on a.date=b.date
group by `总贷款金额`