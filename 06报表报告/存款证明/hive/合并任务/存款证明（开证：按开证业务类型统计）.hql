---按开证业务类型统计：存款证明与一金双用
select date_add(current_date,-1) as `开具时间`,case when `产品类型` like '存款证明（%' then '存款证明' else `产品类型` end as `产品类型`,
	count(distinct `旅行金融订单号`) as `订单数`,sum(case when `贷款出资方`='用户贷款-携程出资' then `携程贷款金额` when `贷款出资方`='用户出资' then `自有资金金额` else 0 end) as `开证金额`, 
		sum(`携程贷款利息`) as `贷款利息`,cast(sum(`服务费`) as decimal(18,2)) as `保证金`,sum(`携程快递费`) as `快递费`
from ods_innofin.vqq_rpt_deposit_proof_pp
where `订单状态` in ('已成交','审核通过','开证中') and to_date(`开具日期`)=date_add(current_date,-1)
group by case when `产品类型` like '存款证明（%' then '存款证明' else `产品类型` end
