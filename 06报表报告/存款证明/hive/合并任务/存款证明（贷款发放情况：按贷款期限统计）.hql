-----------------2 `贷款发放情况`
---`按贷款期限统计`
select date_add(current_date,-1) as `开具时间`,case when `贷款天数`<90 then '3个月以内' when `贷款天数`>=90 and `贷款天数`<=180 then '3个月' else '6个月' end as `贷款期限`,
		`贷款利率`,count(distinct `旅行金融订单号`) as `发放笔数`,sum(`携程贷款金额`) as `贷款发放金额`,sum (`携程贷款利息`) as `贷款利息`
from ods_innofin.vqq_rpt_deposit_proof_pp
where `订单状态` in ('已成交','审核通过','开证中') and `贷款出资方`='用户贷款-携程出资' and to_date(`开具日期`)=date_add(current_date,-1) 
group by case when `贷款天数`<90 then '3个月以内' when `贷款天数`>=90 and `贷款天数`<=180 then '3个月' else '6个月' end,`贷款利率`
