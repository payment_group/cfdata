----------------1 订单维度统计
---预定
---按开证业务类型统计：存款证明与一金双用
select date_add(current_date,-1) as `预定时间`,
	case when `产品类型` like '存款证明（%' then '存款证明' else `产品类型` end as `产品类型`,`订单状态`,count(distinct `旅行金融订单号`) as `订单数`,sum(applyamount) as `金额`
from ods_innofin.vqq_rpt_deposit_proof_pp
where to_date(`成交日期`)=date_add(current_date,-1)
group by case when `产品类型` like '存款证明（%' then '存款证明' else `产品类型` end,`订单状态`


