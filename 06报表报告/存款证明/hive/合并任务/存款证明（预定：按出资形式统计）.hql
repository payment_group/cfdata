
---按出资形式统计：携程贷款与客人自有资金
select date_add(current_date,-1) as `预定时间`,`贷款出资方`,`订单状态`,
count(distinct `旅行金融订单号`) as `订单数`,sum(case when `贷款出资方`='用户贷款-携程出资' then `携程贷款金额` when `贷款出资方`='用户出资' then `自有资金金额` else 0 end) as `金额`
from ods_innofin.vqq_rpt_deposit_proof_pp
where to_date(`成交日期`)=date_add(current_date,-1)
group by `贷款出资方`,`订单状态`
