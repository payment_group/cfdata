use source_travelmoneydb
select a.orderid as 旅行金融订单号,a.bookingdate,case when a.orderstatus='0' then '待支付' when a.orderstatus='1' then '支付中' when a.orderstatus='2' then '审核中'
       when a.orderstatus='3' then '审核通过' when a.orderstatus='4' then '已成交' when a.orderstatus='5' then '审核失败' when a.orderstatus='7' then '开证中' 
       when a.orderstatus='8' then '已赎回'  when a.orderstatus='9' then '未提交' when a.orderstatus='10' then '审核未通过' else '未知' end 订单状态,
       cast(a.orderid as nvarchar)+'-'+cast(b.OrderApplyerID as nvarchar) as 银行订单号,orderdesc as 产品类型,
       case a.FundingType when 0 then '银行出资' when 1 then '用户贷款-携程出资' when 2 then '用户出资' else '' end as 贷款出资方,
       b.Applyer as 客人姓名,c.quantity as 存款证明开具张数,a.PaidTime as 成交日期,d.refundtime as 退款日期,
       c.StartTime as 开具日期,c.EndTime as 到期日期,c.ServiceDays as 贷款天数,case when a.FundingType=2 then 0 else c.Rates end as 贷款利率,
       case when a.FundingType=0 then a.Amount else 0 end as 银行贷款金额,
       a.CtripPay as 携程贷款金额,a.CustPay as 自有资金金额,b.applyamount,cast(c.ServiceDays*c.Rates*case when FundingType=0 then a.Amount else 0 end/360 as decimal(10,2))as 银行贷款利息,
       cast(c.ServiceDays*c.Rates*a.CtripPay/360 as decimal(10,2))as 携程贷款利息,0.00 as 服务费,c.MaterialFee as 工本费,c.Postage as 银行快递费,
       a.CtripPay+a.CustPay as 调拨金额,c.ServiceDays*c.Rates*case when FundingType=0 then a.Amount else 0 end/360+c.MaterialFee+c.Postage as 银行结算金额,
       c.InvoicePostage as 携程快递费,e.amount as 礼品卡金额,f.incomeamount as 礼品卡利息返还,f.datachange_lasttime as 礼品卡充值时间,case when a.FundingType=2 then c.Rates else 0 end as 存款利率
into #1
from dbo.DepositOrder a 
   join OrderApplyer b on a.orderid=b.orderid
   join OrderItem c on c.orderid=a.orderid
   left join dbo.OrderIncome f on f.orderid=a.orderid and f.isdeleted=0 and f.rechargestatus=1
   left join (select orderid,min(PaidTime) as refundtime 
              from Refund
              where refundstatus<>2
              group by orderid) d on a.orderid=d.orderid
   left join (select orderid,sum(PaidAmount) as amount
              from OrderPaymentDetail
              where PaymentWayID='TMPAY'
              group by orderid) e on e.orderid=a.orderid
where orderstatus<>6
union all
select a.orderid as 旅行金融订单号,a.bookingdate,case when a.orderstatus='0' then '待支付' when a.orderstatus='1' then '支付中' when a.orderstatus='2' then '审核中'
       when a.orderstatus='3' then '审核通过' when a.orderstatus='4' then '已成交' when a.orderstatus='5' then '审核失败' when a.orderstatus='7' then '开证中' 
       when a.orderstatus='8' then '已赎回'  when a.orderstatus='9' then '未提交' when a.orderstatus='10' then '审核未通过' else '未知' end 订单状态,
       cast(a.orderid as nvarchar)+'-'+cast(b.OrderApplyerID as nvarchar) as 银行订单号,orderdesc as 产品类型,
       case a.FundingType when 0 then '银行出资' when 1 then '用户贷款-携程出资' when 2 then '用户出资' else '' end as 贷款出资方,
       b.Applyer as 客人姓名,'' as 存款证明开具张数,a.PaidTime as 成交日期,d.refundtime as 退款日期,
       c.StartTime as 开具日期,c.EndTime as 到期日期,c.ServiceDays as 贷款天数,case when a.FundingType=2 then 0 else c.Rate end as 贷款利率,
       case when a.FundingType=0 then a.Amount else 0 end as 银行贷款金额,
       a.CtripPay as 携程贷款金额,a.CustPay as 自有资金金额,b.applyamount,cast(c.ServiceDays*c.Rate*case when FundingType=0 then a.Amount else 0 end/360 as decimal(10,2))as 银行贷款利息,
       cast(c.ServiceDays*c.Rate*a.CtripPay/360 as decimal(10,2))as 携程贷款利息,cast(a.ctripPay*c.GuarantyRate as decimal(18,2)) as 服务费,0 as 工本费,0 as 银行快递费,
       a.CtripPay+a.CustPay as 调拨金额,c.ServiceDays*c.Rate*case when FundingType=0 then a.Amount else 0 end/360+0 as 银行结算金额,
       c.InvoicePostage as 携程快递费,e.amount as 礼品卡金额,f.incomeamount as 礼品卡利息返还,f.datachange_lasttime as 礼品卡充值时间,case when a.FundingType=2 then c.Rate else 0 end as 存款利率
from dbo.DepositOrder a 
   join OrderApplyer b on a.orderid=b.orderid
   join GuarantyOrderItem c on c.orderid=a.orderid
   left join dbo.OrderIncome f on f.orderid=a.orderid and f.isdeleted=0 and f.rechargestatus=1
   left join (select orderid,min(PaidTime) as refundtime 
              from Refund
              where refundstatus<>2
              group by orderid) d on a.orderid=d.orderid
   left join (select orderid,sum(PaidAmount) as amount
              from OrderPaymentDetail
              where PaymentWayID='TMPAY'
              group by orderid) e on e.orderid=a.orderid
where orderstatus<>6
union all
select a.orderid as 旅行金融订单号,a.bookingdate,case when a.orderstatus='0' then '待支付' when a.orderstatus='1' then '支付中' when a.orderstatus='2' then '审核中'
       when a.orderstatus='3' then '审核通过' when a.orderstatus='4' then '已成交' when a.orderstatus='5' then '审核失败' when a.orderstatus='7' then '开证中' 
       when a.orderstatus='8' then '已赎回'  when a.orderstatus='9' then '未提交' when a.orderstatus='10' then '审核未通过' else '未知' end 订单状态,
       cast(a.orderid as nvarchar)+'-'+cast(b.OrderApplyerID as nvarchar) as 银行订单号,orderdesc as 产品类型,
       case a.FundingType when 0 then '银行出资' when 1 then '用户贷款-携程出资' when 2 then '用户出资' else '' end as 贷款出资方,
       b.Applyer as 客人姓名,b.quantity as 存款证明开具张数,a.PaidTime as 成交日期,d.refundtime as 退款日期,
       c.StartTime as 开具日期,c.EndTime as 到期日期,c.ServiceDays as 贷款天数,case when a.FundingType=2 then 0 else c.Rate end as 贷款利率,
       case when a.FundingType=0 then a.Amount else 0 end as 银行贷款金额,
       a.CtripPay as 携程贷款金额,case when a.orderid in (2147546281,2147550921,2147558461,2147562115,2147567480,2147570699) then a.custpay else b.applyamount end as 自有资金金额,b.applyamount,cast(c.ServiceDays*c.Rate*case when FundingType=0 then a.Amount else 0 end/360 as decimal(10,2))as 银行贷款利息,
       cast(c.ServiceDays*c.Rate*a.CtripPay/360 as decimal(10,2))as 携程贷款利息,c.GuarantyFee as 服务费,b.quantity*10 as 工本费,c.Postage as 银行快递费,
       case when a.orderid in (2147546281,2147550921,2147558461,2147562115,2147567480,2147570699) then a.CtripPay+a.custpay else b.applyamount+a.CtripPay end as 调拨金额,c.ServiceDays*c.Rate*case when FundingType=0 then a.Amount else 0 end/360+b.quantity*10+c.Postage as 银行结算金额,
       c.InvoicePostage as 携程快递费,e.amount as 礼品卡金额,f.incomeamount as 礼品卡利息返还,f.datachange_lasttime as 礼品卡充值时间,case when a.FundingType=2 then c.Rate else 0 end as 存款利率
from dbo.DepositOrder a 
   join OrderApplyer b on a.orderid=b.orderid
   join MultiOrderItem c on c.orderid=a.orderid
   left join dbo.OrderIncome f on f.orderid=a.orderid and f.isdeleted=0 and f.rechargestatus=1
   left join (select orderid,min(PaidTime) as refundtime 
              from Refund
              where refundstatus<>2
              group by orderid) d on a.orderid=d.orderid
   left join (select orderid,sum(PaidAmount) as amount
              from OrderPaymentDetail
              where PaymentWayID='TMPAY'
              group by orderid) e on e.orderid=a.orderid
where orderstatus<>6


----------------1 订单维度统计
---预定
---按开证业务类型统计：存款证明与一金双用
select convert(varchar(10),dateadd(day,-1,getdate()),21) as 预定时间,case when 产品类型 like '存款证明（%' then '存款证明' else 产品类型 end as 产品类型,订单状态,count(distinct 旅行金融订单号) as 订单数,sum(applyamount) as 金额
from #1
where convert(varchar(10),成交日期,21)=convert(varchar(10),dateadd(day,-1,getdate()),21)
group by case when 产品类型 like '存款证明（%' then '存款证明' else 产品类型 end,订单状态
order by 1,2


