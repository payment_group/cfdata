declare @date varchar(8)
set @date=convert(varchar(8),dateadd(dd,-1,getdate()),112)

select id.*
into #IncomeDetail
from IncomeDetail AS id
INNER JOIN IncomeOptLog AS iol ON id.IncomeOptLogID=iol.IncomeOptLogID
WHERE iol.[filename] LIKE '%'+@date+'%' and id.matchresult=1



SELECT distinct id.orderid as [订单号],id.applyer as [客户名],id.cardno as [证件号码],id.mobilephone as [联系电话],id.starttime as [订单申请日]
	  ,id.endtime as [订单到期日],id.servicedays as [存期],convert(varchar(10),isnull(oi.starttime,moi.starttime),21) as [存单起息日]
	  ,convert(varchar(10),isnull(oi.endtime,moi.endtime),21) as [存单到期日],do.amount as [开证金额]
	  ,oa.BankInterest as [利息],do.amount+oa.BankInterest as [本息合计],isnull(oi.quantity,moi.quantity) as [开证数量]
	  ,od.FlowTicketNumber as [快递单号] 
into #1
FROM #IncomeDetail id
left join OrderItem oi on cast(left(id.orderid,charindex('-',id.orderid)-1) as bigint)=oi.orderid
left join DepositOrder do on cast(left(id.orderid,charindex('-',id.orderid)-1) as bigint)=do.orderid
left join OrderApplyer oa on cast(left(id.orderid,charindex('-',id.orderid)-1) as bigint)=oa.orderid
left join OrderDelivery  od on cast(left(id.orderid,charindex('-',id.orderid)-1) as bigint)=od.orderid
LEFT JOIN MultiOrderItem AS moi ON cast(left(id.orderid,charindex('-',id.orderid)-1) as bigint)=moi.OrderID

select convert(varchar(10),dateadd(day,-1,getdate()),21)  as date,case [存期] when '3m' then '3个月' when '6m' then '6个月' when '7' then '3个月内' else [存期] end as [存期]
          ,count(distinct  [订单号]) as [回款订单数],sum([开证金额]) as [回款金额],sum([利息]) as [存款利息]
from #1
group by  [存期]