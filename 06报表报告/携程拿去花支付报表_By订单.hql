use tmp_innofin;
create table vqq_rpt_order_payment as (
	 orderdate string comment '日期',
	 merchantid string comment 'merchantid', 
	 merchantname string comment '事业部', 
	 ord_all int comment '拿去花用户总订单',
	 ord_support  int comment '支持拿去花订单',
	 ord_loanpay  int comment '拿去花支付订单'
);
insert overwrite table vqq_rpt_order_payment
select a.orderdate,
	a.merchantid,
	mct.merchantname,
	sum(a.ord_all),
	sum(b.ord_support),
	sum(a.ord_loanpay)
from
(select uid,orderdate,merchantid,ord_all,ord_loanpay from dw_paypubdb.factnqhuserorder where orderdate >= '${zdt.addDay(-7).format("yyyy-MM-dd")}') a
left join (select uid,dt,bustype,count(1) from dw_paypubdb.FactLoanPayRequest where dt >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' group by uid,dt,bustype)
	b on a.orderdate = b.dt and a.merchantid = b.bustype and lower(a.uid) = lower(b.uid)
inner join dim_paydb.dimmerchant mct on a.merchantid = mct.merchantid
group by a.orderdate,a.merchantid,mct.merchantname
;

-- new
insert overwrite table tmp_innofin.vqq_rpt_order_payment
select dt,a.bustype,mct.merchantname,
	count(case when activenqh = 1 or status&1=1 or formattedsubpaytype = '拿去花' then orderid else null end) as ord_all,
	count(case when supportnqh&128=128 and status&1=1 then orderid else null end) as ord_support,
	count(case when formattedsubpaytype = '拿去花' then orderid else null end) as ord_hqh
from dw_paypubdb.FactLoanPayRequest a
inner join dim_paydb.dimmerchant mct on a.bustype = mct.merchantid
inner join (select distinct bustype from dw_paypubdb.FactLoanPayRequest where dt >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' and burestrict&64=64 and (busubrestrict&1024=1024 or busubrestrict = 0)) en on a.bustype = en.bustype
where dt >= '${zdt.addDay(-7).format("yyyy-MM-dd")}' group by dt,a.bustype,mct.merchantname
;