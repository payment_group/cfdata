use tmp_innofin;
create table if not exists vqq_temp_user_count(
	tag_count string
)partitioned by (
	dt string,
	user_tag string
);

alter table tmp_innofin.vqq_temp_user_count drop partition (dt = '${zdt.format("yyyy-MM-dd")}');
-- 全站用户总量
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '01-全站用户总量')
	select concat('01_',count(distinct uid)) from fin_basic_data.c_corp_oc_cfbdb_uid_signup_info_snap;
-- APP日活跃用户
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '02-APP日活跃用户')
	select concat('02_',count(distinct uid)) from dw_mobdb.factmbpageview where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}';
-- 已实名用户
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '03-已实名用户')
	select concat('03_',count(distinct source_uid)) from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap where auth_status = 1;
-- 实名绑卡用户
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '04-实名绑卡用户')
	select concat('04_',count(distinct a.source_uid)) from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap a 
	inner join dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap b on lower(a.source_uid) = lower(b.uid)
	where auth_status = 1;
-- 实名绑卡已激活用户
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '05-实名绑卡已激活用户')
	select concat('05_',count(distinct a.source_uid)) from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap a 
	inner join dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap b on lower(a.source_uid) = lower(b.uid)
	inner join ods_innofin.user_contract c on lower(a.source_uid) = lower(c.user_id)
	where auth_status = 1 and c.contract_status = 1;
-- 预授信成功用户数
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '06-预授信成功用户')
	select concat('06_',count(distinct a.user_id)) from 
	(select user_id from dw_subject_finance.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1) a
	left join fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap b on a.user_id = b.user_id 
	where b.user_id is null;
-- 预授信成功当前可用
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '07-预授信成功当前可用')
	select concat('07_',count(distinct a.user_id)) from 
	(select user_id from dw_subject_finance.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1 and end_eff_time >= '${zdt.format("yyyy-MM-dd")}') a
	left join dw_subject_finance.a_xj_busi_pay_ious_tbl_credit_activate_snap b on a.user_id = b.user_id 
	where b.user_id is null;
-- 预授信成功当前可用近6个月无主站交易
-- 预授信成功无六位密码
-- C预授信成功无四要素用户数
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '08-预授信成功无四要素')
	select concat('08_',count(distinct a.user_id)) from 
	(select user_id from dw_subject_finance.a_xj_pay_ious_tbl_credit_req_snap where org_channel='CTRIP' and product_no='IOUS' and req_status = 1) a
	left join fin_basic_data.a_xj_busi_pay_ious_tbl_credit_activate_snap b on a.user_id = b.user_id 
	inner join ods_innofin.user_contract d on a.user_id = d.open_id
	left join (
		select distinct a.source_uid from dw_subject_user.c_pay_center_cfbdb_payment_realname_snap a 
		inner join dw_subject_pay.c_pay_center_cfbdb_finance_bind_sec_his_snap b on lower(a.source_uid) = lower(b.uid) 	
		where auth_status = 1
	) e on lower(d.user_id) = lower(e.source_uid)
	where b.user_id is null and e.source_uid is null;
-- 预授信过期用户数

-- 已激活交叉用户
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '09-已激活交叉用户')
	select concat('09_',count(distinct user_id)) from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' and main_channel_code <> 'CTRIP';
-- 已激活非交叉用户
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '10-已激活非交叉用户')
	select concat('10_',count(distinct a.user_id)) from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap a where channel_code='CTRIP' and product_no='IOUS' and main_channel_code = 'CTRIP';

-- 已激活非交叉当前可用
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '11-已激活非交叉当前可用')
	select concat('11_',count(distinct a.user_id)) from (
		select user_id from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' and main_channel_code = 'CTRIP'
	) a 
	inner join (
		select user_id,max(use_status) as use_status from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' group by user_id 
	) b on a.user_id = b.user_id 
	where b.use_status = 1;
-- 已激活非交叉当前不可用
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '12-已激活非交叉当前不可用')
	select concat('12_',count(distinct a.user_id)) from (
		select user_id from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' and main_channel_code = 'CTRIP'
	)a 
	inner join (
		select user_id,max(use_status) as use_status from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap	where channel_code='CTRIP' and product_no='IOUS' group by user_id 
	) b on a.user_id = b.user_id 
	where b.use_status = 0;

-- 已激活当前展期失败用户数
-- 已激活非交叉逾期用户
insert overwrite table tmp_innofin.vqq_temp_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}',user_tag = '13-已激活非交叉逾期用户')
	select concat('13_',count(distinct a.user_id)) from (
		select user_id from dw_subject_finance.q_xj_busi_pay_ious_user_product_info_snap where channel_code='CTRIP' and product_no='IOUS' and main_channel_code = 'CTRIP'
	)a 
	inner join (
	select user_id from dw_subject_finance.a_xj_busi_pay_repay_tbl_schedule_detail_snap
	where org_channel='CTRIP' and product_no='IOUS' and ((status = 3 and datediff(act_repay_date,due_date) > 30) or (status = 0 and datediff('${zdt.format("yyyy-MM-dd")}',due_date) > 30))) b on a.user_id = b.user_id;
-- 已激活近6个月无主站交易用户
-- 

use tmp_innofin;
create table if not exists vqq_rpt_user_count(
	total int comment '全站用户总量',
	active int  comment 'APP日活跃用户',
	realname int  comment '已实名用户',
	realname_card int  comment '实名绑卡用户',
	realname_card_active int  comment '实名绑卡已激活用户',
	credit int  comment '预授信成功用户',
	credit_valid int  comment '预授信成功当前可用',
	credit_not_realname_card int  comment '预授信成功无四要素用户',
	activate_overlap int  comment '已激活交叉用户',
	activate_not_overlap int  comment '已激活非交叉用户',
	activate_not_overlap_valid int  comment '已激活非交叉当前可用',
	activate_not_overlap_invalid int  comment '已激活非交叉当前不可用',
	activate_not_overlap_overdue int  comment '已激活非交叉逾期用户'
)partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_rpt_user_count partition(dt = '${zdt.format("yyyy-MM-dd")}')
select 
	split(ll[0],'_')[1],
	split(ll[1],'_')[1],
	split(ll[2],'_')[1],
	split(ll[3],'_')[1],
	split(ll[4],'_')[1],
	split(ll[5],'_')[1],
	split(ll[6],'_')[1],
	split(ll[7],'_')[1],
	split(ll[8],'_')[1],
	split(ll[9],'_')[1],
	split(ll[10],'_')[1],
	split(ll[11],'_')[1],
	split(ll[12],'_')[1]
from(
select dt,sort_array(collect_list(tag_count)) as ll from 
 tmp_innofin.vqq_temp_user_count where dt = '${zdt.format("yyyy-MM-dd")}' group by dt) a;
