use ods_innofin;
CREATE TABLE tbl_blocked_order (
  id int COMMENT '自增id',
  user_id string  COMMENT '用户id',
  open_id string   COMMENT '携程集团id',
  order_no string   COMMENT '订单号',
  block_reason string   COMMENT '拦截原因id',
  order_time string   COMMENT '订单时间',
  create_time string   COMMENT '创建时间',
  update_time string   COMMENT '更新时间',
  order_busi_type string  COMMENT '订单类型',
  order_amount decimal(15,2) COMMENT '订单金额',
  order_start_time string  COMMENT '出行时间',
  order_desc string  COMMENT '产品名称',
  DataChange_LastTime string  COMMENT 'ctrip mysql 规范强制规定'
) COMMENT '拦截订单表';

 -- datax 
use ods_innofin;
CREATE TABLE if not exists tbl_blocked_order_his (
  id int COMMENT '自增id',
  user_id string  COMMENT '用户id',
  open_id string   COMMENT '携程集团id',
  order_no string   COMMENT '订单号',
  block_reason string   COMMENT '拦截原因id',
  order_time string   COMMENT '订单时间',
  create_time string   COMMENT '创建时间',
  update_time string   COMMENT '更新时间',
  order_busi_type string  COMMENT '订单类型',
  order_amount decimal(15,2) COMMENT '订单金额',
  order_start_time string  COMMENT '出行时间',
  order_desc string  COMMENT '产品名称',
  DataChange_LastTime string  COMMENT 'ctrip mysql 规范强制规定'
) COMMENT '拦截订单表-全部历史数据';

insert into tbl_blocked_order_his
select a.id,a.user_id,a.open_id,a.order_no,a.block_reason,a.order_time,a.create_time,a.update_time,a.order_busi_type,a.order_amount,a.order_start_time,a.order_desc,a.DataChange_LastTime 
from tbl_blocked_order a 
left join tbl_blocked_order_his b on a.id = b.id 
where b.id is null;


