-- 我携小红点实验效果评估
use tmp_innofin;
drop table if exists vqq_ab_180224_cost_wxxhd;
create table vqq_ab_180224_cost_wxxhd as 
select abversion,starttime,uid,d from dw_abtestdb.AbVersionSplitInfo_app where d >= '2018-04-16' and experiment = '180224_cost_wxxhd';

use tmp_innofin;
drop table if exists vqq_ab_180224_cost_wxxhd_mytrip;
create table vqq_ab_180224_cost_wxxhd_mytrip as 
select distinct d,uid from dw_mobdb.factmbpageview
  	where d >= '2018-04-16' and pagecode in ('myctrip_home');



-- 评估指标：
-- 1、两个版本的激活量。 
-- 2、两个版本的授信激活成功率 = 激活成功用户数/发起授信激活请求的用户数
-- 3、激活失败的用户uid
-- 4、两个版本的激活转化率 = 激活成功数/到达我携pagecode = myctrip_home的用户数（限AB）
-- 5、两个版本的入口引流率 = 激活首页用户数/我携用户数（限AB）

use tmp_innofin;
drop table if exists vqq_ab_180224_cost_wxxhd_1;
create table vqq_ab_180224_cost_wxxhd_1(
	dt string,
	abversion string,
	abgroup string,
	uid string,
	myctrip_home int,
	nqh_first int,
	credit_apply int,
	credit_success int,
	active_success int
);

use tmp_innofin;
insert overwrite table vqq_ab_180224_cost_wxxhd_1
select distinct a.d as dt,
	a.abversion,
	a.abgroup,
	a.uid,
	if(e.uid is null,0,1) as myctrip_home,
	nvl(b.ctrip,0) as nqh_first,
	if(c.user_id is null,0,1) as credit_apply,
	if(d.user_id is null,0,1) as credit_success,
	if(f.user_id is null,0,1) as active_success
from (select d,uid, abversion, if(abversion in ('C','D'),'CD',abversion) as abgroup from vqq_ab_180224_cost_wxxhd) a 
inner join ods_innofin.user_contract uu on lower(a.uid) = lower(uu.user_id)
left join tmp_innofin.vqq_ab_180224_cost_wxxhd_mytrip e on lower(a.uid) = lower(e.uid) and a.d = e.d
left join (select uid,d,ctrip,apply from tmp_innofin.apply_process_detail where d>='2018-04-16' and ctrip = 1) b on lower(a.uid) = lower(b.uid) and a.d = b.d
left join (select distinct user_id,to_date(finish_time) as dt from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel = 'CTRIP' and product_no = 'IOUS' and credit_type = 1) c on a.d = c.dt and uu.open_id = c.user_id 
left join (select distinct user_id,to_date(finish_time) as dt from fin_basic_data.a_xj_pay_ious_tbl_credit_req_snap where org_channel = 'CTRIP' and product_no = 'IOUS' and credit_type = 1 and req_status = 1) d on a.d = d.dt and uu.open_id = d.user_id 
left join ods_innofin.nqh_user_active_date f on a.d = f.dt and uu.open_id = f.uid
;


select dt,abgroup,sum(myctrip_home),sum(nqh_first),sum(credit_apply),sum(credit_success),sum(active_success) from tmp_innofin.vqq_ab_180224_cost_wxxhd_1 where abversion in ('B','C','D') group by dt,abgroup;
select dt,abversion,uid from tmp_innofin.vqq_ab_180224_cost_wxxhd_1 where credit_apply = 1 and active_success = 0;
