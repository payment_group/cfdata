use tmp_innofin;
drop table if exists vqq_xwt_FINANCE_4898_1;
create table if not exists vqq_xwt_FINANCE_4898_1(
	dt string,
	uid string,
	
	page_apply int,
	page_cardguide int,
	page_addcard int,
	page_inputguide int,
	page_card int,
	page_confirm int,
	page_text int,
	page_finish int,
	page_password int
);

insert overwrite table tmp_innofin.vqq_xwt_FINANCE_4898_1
select a.*
from
(select
	d,
	uid,
	max(case when pagecode in ('10320607163','10320670431') then 1 else 0 end) as page_apply,
	max(case when pagecode in ('10320643008') then 1 else 0 end) as page_cardguide,
	max(case when pagecode in ('10320643028') then 1 else 0 end) as page_addcard,
	max(case when pagecode in ('10320643010') then 1 else 0 end) as page_inputguide,
	max(case when pagecode in ('10320642998') then 1 else 0 end) as page_card,
	max(case when pagecode in ('10320657064') then 1 else 0 end) as page_confirm,
	max(case when pagecode in ('10320668195') then 1 else 0 end) as page_text,
	max(case when pagecode in ('10320607172','10320607173','10320607171') then 1 else 0 end) as page_finish,
	max(case when pagecode in ('10320668081') then 1 else 0 end) as page_password
from dw_mobdb.factmbpageview 
where d >= '2017-11-01'
group by d,uid) a
where a.page_apply=1;


use tmp_innofin;
drop table if exists vqq_xwt_FINANCE_4898_2;
create table if not exists vqq_xwt_FINANCE_4898_2(
	dt string,
	uid string,
	button_apply int,
	button_contract int,
	button_commit int
);

use tmp_innofin;
insert overwrite table vqq_xwt_FINANCE_4898_2
select dt,uid,
	max(case when bid in('101320','101321') then 1 else 0 end ) as button_apply,
	max(case when bid in('101324') then 1 else 0 end ) as button_contract,
	max(case when bid in('101327') then 1 else 0 end ) as button_commit
from tmp_innofin.vqq_rpt_active_process_click_uv where dt >= '2017-11-01'
group by dt,uid;

use tmp_innofin;
create table vqq_xwt_FINANCE_4898_ab as 
select distinct d as dt,uid,abversion from dw_abtestdb.AbVersionSplitInfo_app where experiment='171018_hod_nqhkt' and d>='2017-11-01';

use tmp_innofin;
drop table if exists vqq_xwt_FINANCE_4898_3;
create table if not exists vqq_xwt_FINANCE_4898_3(
	dt string,
	uid string,
	abversion string,
	
	page_apply int,
	page_cardguide int,
	page_addcard int,
	page_inputguide int,
	page_card int,
	page_confirm int,
	page_text int,
	page_finish int,
	page_password int,
	
	button_apply int,
	button_contract int,
	button_commit int,
	
	sub_process string,
	
	db_success int,
	db_fail int
	
);


use tmp_innofin;
insert overwrite table vqq_xwt_FINANCE_4898_3
select a.dt,a.uid,a.abversion,
	nvl(page_apply,0),
	nvl(page_cardguide,0),
	nvl(page_addcard,0),
	nvl(page_inputguide,0),
	nvl(page_card,0),
	nvl(page_confirm,0),
	nvl(page_text,0),
	nvl(page_finish,0),
	nvl(page_password,0),
	nvl(button_apply,0),
	nvl(button_contract,0),
	nvl(button_commit,0),

	case when (creditProcess = 0 and realName = 'false') then 'noname' 
		when (creditProcess = 1) then 'nocard'
		when creditProcess = 2 then 'rapid'
		when (creditProcess = 0 and realName = 'true') then 'card' else 'na' end,
		
	if(e.uid is null,0,1),
	if(f.uid is null,0,1)
from vqq_xwt_FINANCE_4898_ab a 
inner join vqq_xwt_FINANCE_4898_1 b on lower(a.uid) = lower(b.uid) and a.dt = b.dt
left join vqq_xwt_FINANCE_4898_2 c on lower(a.uid) = lower(c.uid) and a.dt = c.dt
left join (select * from vqq_rpt_active_process_user_subprocess where dt>='2017-11-01') dd on lower(a.uid) = lower(dd.uid) and a.dt = dd.dt
left join (select user_id as uid, to_date(ious_begin_time) as dt from ods_innofin.user_contract where to_date(ious_begin_time)>='2017-11-01' and contract_status=1 and req_src <> 'C_H5_USER' ) e on lower(a.uid) = lower(e.uid) and a.dt = e.dt
left join (select user_id as uid, to_date(create_time) as dt from ods_innofin.user_contract where to_date(create_time)>='2017-11-01' and contract_status in ('0','2','8','9') and req_src <> 'C_H5_USER' and req_src <> 'PRE_CREDIT_TASK') f on lower(a.uid) = lower(f.uid) and a.dt = f.dt;

-- 
use tmp_innofin;
select dt,abversion,
	sum(page_apply),
	sum(button_apply),
	sum(button_contract),
	sum(page_cardguide),
	sum(page_addcard),
	sum(page_inputguide),
	sum(page_card),
	sum(page_confirm),
	sum(button_commit),
	sum(page_text),
	sum(page_finish),
	sum(page_password),
	sum(db_success),
	sum(db_fail)
from vqq_xwt_FINANCE_4898_3 where sub_process = 'card' group by dt,abversion;

use tmp_innofin;
select dt,abversion,
	sum(page_apply),
	sum(button_apply),
	sum(button_contract),
	sum(page_confirm),
	sum(button_commit),
	sum(page_text),
	sum(page_finish),
	sum(page_password),
	sum(db_success),
	sum(db_fail)
from vqq_xwt_FINANCE_4898_3 where sub_process = 'nocard' group by dt,abversion;

use tmp_innofin;
select dt,abversion,
	sum(page_apply),
	sum(button_apply),
	sum(button_contract),
	sum(page_confirm),
	sum(button_commit),
	sum(page_text),
	sum(page_finish),
	sum(db_success),
	sum(db_fail)
from vqq_xwt_FINANCE_4898_3 where sub_process = 'rapid' group by dt,abversion;

use tmp_innofin;
select dt,abversion,
	sum(page_apply),
	sum(button_apply),
	sum(button_contract),
	sum(page_cardguide),
	sum(page_addcard),
	sum(page_card),
	sum(page_inputguide),
	sum(page_password),
	sum(page_confirm),
	sum(page_text),
	sum(button_commit),
	sum(page_finish),
	sum(db_success),
	sum(db_fail)
from vqq_xwt_FINANCE_4898_3 where sub_process = 'noname' group by dt,abversion;

---

use tmp_innofin;
drop table if exists vqq_xwt_FINANCE_4898_4;
create table if not exists vqq_xwt_FINANCE_4898_4(
	dt string,
	uid string,
	button_101320 int,
	button_101321 int,
	button_101324 int,
	button_101327 int
);

use tmp_innofin;
insert overwrite table vqq_xwt_FINANCE_4898_4
select dt,uid,
	max(case when bid in('101320') then 1 else 0 end ) as button_101320,
	max(case when bid in('101321') then 1 else 0 end ) as button_101321,
	max(case when bid in('101324') then 1 else 0 end ) as button_101324,
	max(case when bid in('101327') then 1 else 0 end ) as button_101327
from tmp_innofin.vqq_rpt_active_process_click_uv where dt >= '2017-11-01'
group by dt,uid;


use tmp_innofin;
create table vqq_xwt_FINANCE_4898_5 as 
select a.dt,a.uid,a.abversion,button_101320,button_101321,button_101324,button_101327 from 
vqq_xwt_FINANCE_4898_ab a 
inner join  vqq_xwt_FINANCE_4898_4 on lower(a.uid) = lower(b.uid) and a.dt = b.dt;

select dt,abversion,sum(button_101320),sum(button_101321),sum(button_101324),sum(button_101327)
from tmp_innofin.vqq_xwt_FINANCE_4898_5 group by dt,abversion;

