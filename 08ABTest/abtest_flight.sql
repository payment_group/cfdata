-- 分组数据
use tmp_innofin;
create table if not exists vqq_abtest_flt_group (
	uid string,
	abversion string
) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "'",
   "escapeChar"    = "\\"
)  
STORED AS TEXTFILE;
-- load data
-- flight page data
use tmp_innofin;
create table if not exists vqq_tmp_ab_flight_uv(
	d string,
	uid string
);
insert overwrite table vqq_tmp_ab_flight_uv
select distinct d,uid from dw_mobdb.factmbpageview where pagecode in ('flight_inland_inquire') and d >= '2017-11-15';


use tmp_innofin;
create table if not exists vqq_abtest_detail (
  dt string,
  uid string,
  abversion string,
  experiment_group string,
  first_page int,
  converted int
)partitioned by (
	experiment string
);
-- 机票优惠券
insert overwrite table tmp_innofin.vqq_abtest_detail partition (experiment='FLIGHT_3235_B')
select b.d,a.uid,a.abversion,a.abversion, 
nvl(c.apply,0),if(dd.uid is null,0,1)
from tmp_innofin.vqq_abtest_flt_group a
inner join tmp_innofin.vqq_tmp_ab_flight_uv b on lower(a.uid) = lower(b.uid)
left join (select d,uid,apply from tmp_innofin.apply_process_detail where d >= '2017-11-15') c on lower(a.uid) = lower(c.uid) and b.d = c.d
left join (select uid,to_date(time_apl) as dt from tmp_innofin.nqh_uid_apl) dd on lower(a.uid) = lower(dd.uid) and b.d = dd.dt
;

use tmp_innofin;
create table if not exists vqq_abtest_multi_group (
  dt string,
  experiment_group string,
  user_count int,
  first_page int,
  converted int
)partitioned by (
  experiment string
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_abtest_multi_group partition (experiment='FLIGHT_3235_B')
select dt,experiment_group,count(1),sum(first_page),sum(converted)
from  tmp_innofin.vqq_abtest_detail where (experiment='FLIGHT_3235_B')
group by dt,experiment_group;


select 
  dt as `日期`,
  experiment_group as  `组别`,
  user_count as  `机票查询页`,
  first_page as  `开通首页`,
  converted as  `激活成功`
 from tmp_innofin.vqq_abtest_multi_group where (experiment='FLIGHT_3235_B') order by `日期`,`组别` limit 100
