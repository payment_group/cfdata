-- 
-- use tmp_innofin;
-- create table if not exists vqq_abtest_detail (
--   dt string,
--   uid string,
--   abversion string,
--   experiment_group string,
--   first_page int,
--   converted int
-- )partitioned by (
-- 	experiment string
-- );
--
 -- 机票优惠券
insert overwrite table tmp_innofin.vqq_abtest_detail partition (experiment='FLIGHT_3235_B')
select b.d,a.uid,a.abversion,a.abversion, 
nvl(c.apply,0),if(dd.uid is null,0,1)
from tmp_innofin.vqq_abtest_flt_group a
inner join tmp_innofin.vqq_tmp_ab_flight_uv b on lower(a.uid) = lower(b.uid)
left join (select d,uid,apply from tmp_innofin.apply_process_detail where d >= '2017-11-15') c on lower(a.uid) = lower(c.uid) and b.d = c.d
left join (select uid,to_date(time_apl) as dt from tmp_innofin.nqh_uid_apl) dd on lower(a.uid) = lower(dd.uid) and b.d = dd.dt
;

-- 6)总表
use tmp_innofin;
create table if not exists vqq_tmp_active_process_detail_flt (
	dt string,
	uid string,
	experiment_group string,

	enter_ctrip int,
	enter_realname int,
	enter_wallet int,
	enter_more_func int,
	enter_others_channel int,
	
	page_apply	int,	
	page_cardguide	int,	
	page_card_number	int,	
	page_card	int,	
	page_setpass	int,	
	page_active	int,
	page_finish	int,	
	page_success	int,
	
	button_apply int,
	button_contract int,
	button_active int,

	db_active int,
	db_success int,	

	log_creditProcess int,
	log_existPwd int,
	log_hasMobile int,
	log_preCreditStatus int,
	log_realName int
);

insert overwrite table tmp_innofin.vqq_tmp_active_process_detail_flt
select a.dt,a.uid,a.experiment_group,
		b.enter_ctrip,
		b.enter_realname,
		b.enter_wallet,
		b.enter_more_func,
		b.enter_others_channel,

		b.page_apply	 ,	
		b.page_cardguide	 ,	
		b.page_card_number	 ,	
		b.page_card	 ,	
		b.page_setpass	 ,	
		b.page_active	 ,
		b.page_finish	 ,	
		b.page_success	 ,

		b.button_apply,
		b.button_contract,
		b.button_active,

		b.db_active,
		b.db_success,	

		b.log_creditProcess,
		b.log_existPwd,
		b.log_hasMobile,
		b.log_preCreditStatus,
		b.log_realName  
 from
 (select * from tmp_innofin.vqq_abtest_detail where (experiment='FLIGHT_3235_B')) a 
 inner join (select * from tmp_innofin.vqq_tmp_active_process_detail where dt >= date_add(current_date,-7)) b on lower(a.uid) = lower(b.uid) and a.dt = b.dt;
 


use tmp_innofin;
create table if not exists vqq_tmp_active_process_main_flt (
	dt string,
	experiment_group string,	
	page_apply int,
	button_apply int,
	button_apply_rate decimal(5,4),
	
	button_contract int,
	button_contract_rate decimal(5,4),

	finish_db int,
	success_db int,
	success_rate decimal(5,4)	
);
insert overwrite table tmp_innofin.vqq_tmp_active_process_main_flt
	select dt, experiment_group, sum(page_apply),sum(button_apply),sum(button_apply)/sum(page_apply),sum(button_contract),sum(button_contract)/sum(button_apply),sum(db_active),sum(db_success),sum(db_success)/sum(page_apply)
	from tmp_innofin.vqq_tmp_active_process_detail_flt group by dt, experiment_group;

use tmp_innofin;
create table if not exists vqq_tmp_active_process_card_flt (
	dt string,
	experiment_group string,	
	page_apply int,
	page_cardguide int,
	page_card int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
);
insert overwrite table tmp_innofin.vqq_tmp_active_process_card_flt
select dt, experiment_group, sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_tmp_active_process_detail_flt where log_creditProcess = 0 and log_realName = 1 group by dt,experiment_group;

use tmp_innofin;
create table if not exists vqq_tmp_active_process_noname_flt (
	dt string,
	experiment_group string,	
	page_apply int,
	page_cardguide int,
	page_card int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
);
insert overwrite table tmp_innofin.vqq_tmp_active_process_noname_flt
select  dt, experiment_group, sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_tmp_active_process_detail_flt where log_creditProcess = 0 and log_realName = 0 group by dt,experiment_group;


use tmp_innofin;
create table if not exists vqq_tmp_active_process_nocard_flt (
	dt string,
	experiment_group string,	
	page_apply int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
);
insert overwrite table tmp_innofin.vqq_tmp_active_process_nocard_flt
select dt, experiment_group,sum(page_apply),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_tmp_active_process_detail_flt where log_creditProcess = 1 group by dt, experiment_group;


use tmp_innofin;
create table if not exists vqq_tmp_active_process_rapid_flt (
	dt string,
	experiment_group string,	
	page_apply int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
);
insert overwrite table tmp_innofin.vqq_tmp_active_process_rapid_flt
select dt, experiment_group,sum(page_apply),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_tmp_active_process_detail_flt where log_creditProcess = 2 group by dt, experiment_group;

-- ART
-- main
select dt as `日期`,
	experiment_group as `组别`,
	page_apply as `拿去花主动开通首页UV`,
	button_apply as `免费开通键点击UV`,
	button_apply_rate as `免费开通建点击率%`,
	
	button_contract as `合同确认键点击UV`,
	button_contract_rate as `合同确认键点击率%`,

	success_db as `开通成功数`,
	success_rate as	`来源开通成功率%`
from tmp_innofin.vqq_tmp_active_process_main_flt

-- card 
select dt as `日期`,
	experiment_group as `组别`,
	page_apply as `拿去花主动开通首页UV`,
	page_cardguide as `支付绑卡引导页UV`,
	page_card as `支付绑卡页UV`,
	page_active as `核实信息页UV`,
	button_active as `核实信息页提交btn`,
	need_setpass 	as `需要设置支付密码`,
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	finish_rate as `流程完成率%`,
	success_rate as `流程开通成功率%`
from  tmp_innofin.vqq_tmp_active_process_card_flt


--nocard 
select dt as `日期`,
	experiment_group as `组别`,
	page_apply as `拿去花主动开通首页UV`,
	page_active as `核实信息页UV`,
	
	button_active as `核实信息页提交btn`,
	need_setpass as `需要设置支付密码`,
	
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	finish_rate as `流程完成率%`,
	success_rate as `流程开通成功率%`	
from  tmp_innofin.vqq_tmp_active_process_nocard_flt
-- rapid 
select dt as `日期`,
	experiment_group as `组别`,
	page_apply as `拿去花主动开通首页UV`,
	page_active as `核实信息页UV`,
	
	button_active as `核实信息页提交btn`,
	
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	finish_rate as `流程完成率%`,
	success_rate as `流程开通成功率%`	
from  tmp_innofin.vqq_tmp_active_process_rapid_flt

-- noname
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	page_cardguide as `支付绑卡引导页UV`,
	page_card as `支付绑卡页UV`,
	page_active as `核实信息页UV`,
	button_active as `核实信息页提交btn`,
	need_setpass 	as `需要设置支付密码`,
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	finish_rate as `流程完成率%`,
	success_rate as `流程开通成功率%`
from tmp_innofin.vqq_tmp_active_process_noname_flt


--- 发券，用券情况
-- vqq_abtest_flt_group
-- vqq_tmp_flt_coupon
use tmp_innofin;
select a.uid,a.abversion,b.couponstatus
 from vqq_abtest_flt_group a 
 inner join vqq_tmp_flt_coupon b on lower(a.uid) = lower(b.uid);
 
-- 数据导入公有云
-- 发券用户
use cfbdb;
select abversion,count(orderid),avg(amount) 
	from vqq_tmp_flt_curpon a 
	inner join (select uid,orderid,amount from ctrip_cloud_uid_allorder_info_snap where to_date(orderdate) >= add_months(current_date,-6) and orderstatus = 10) b
		on lower(a.uid) = lower(b.uid)
 group by abversion;

--用券用户
use cfbdb;
select abversion,count(orderid),avg(amount) 
	from vqq_tmp_flt_curpon a 
	inner join (select uid,orderid,amount from ctrip_cloud_uid_allorder_info_snap where to_date(orderdate) >= add_months(current_date,-6) and orderstatus = 10) b
		on lower(a.uid) = lower(b.uid) where a.couponstatus = '已使用'
 group by abversion;
