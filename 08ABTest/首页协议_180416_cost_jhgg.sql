-- 
use tmp_innofin;
drop table if exists vqq_ab_180416_cost_jhgg_uv;
create table vqq_ab_180416_cost_jhgg_uv as 
select distinct d,clientcode,uid from dw_mobdb.factmbpageview where d >= '2018-04-20' and pagecode in ('10320607100','10320607163','10320663287','10320663286','10320670433','10320670431','10650001007');


use tmp_innofin;
drop table if exists vqq_ab_180416_cost_jhgg;
create table vqq_ab_180416_cost_jhgg as 
select distinct a.abversion,b.uid,a.d from dw_abtestdb.AbVersionSplitInfo_app  a
	inner join tmp_innofin.vqq_ab_180416_cost_jhgg_uv b on a.d = b.d and a.clientcode = b.clientcode
	where a.d >= '2018-04-20' and a.experiment = '180416_cost_jhgg';


-- 评估流程完成情况
use tmp_innofin;
drop table if exists vqq_ab_180416_cost_jhgg_1;
create table if not exists vqq_ab_180416_cost_jhgg_1(
	dt string,
	uid string,
	ab string,
	apply int,
	apply_button int,
	db_active int
);
insert overwrite table tmp_innofin.vqq_ab_180416_cost_jhgg_1
select a.d,a.uid,
	if(b.abversion = 'B','B','CD'),
	1,
	if(c.uid is null,0,1),
	if(d.uid is null,0,1)
from (select * from tmp_innofin.apply_process_detail a where d >= '2018-04-20') 
	inner join (select * from tmp_innofin.vqq_ab_180416_cost_jhgg where abversion in ('B','C','D')) b on a.d = b.d and lower(a.uid) = lower(b.uid)
	left join (select * from tmp_innofin.vqq_rpt_active_process_click_uv where dt >= '2018-04-20' and bid in ('101320','101321')) c on a.d = c.dt and lower(a.uid) = lower(c.uid)
	left join (select distinct to_date(request_time) as dt,user_id as uid from ods_innofin.user_activate_req where to_date(request_time) >= '2018-04-20' and req_source <>'PRE_CREDIT_TASK') d
		on a.d = d.dt and lower(a.uid) = lower(d.uid)

select dt,ab,sum(apply),sum(apply_button),sum(apply_button*db_active) from tmp_innofin.vqq_ab_180416_cost_jhgg_1 group by dt,ab