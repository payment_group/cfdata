use tmp_innofin;
-- tmp_innofin.apply_process_detail 

-- 1)来源（不要）
use tmp_innofin;
create table if not exists vqq_rpt_active_process_entrance (
	uid string,
	entrance string
) partitioned by (
	dt string
);
alter table tmp_innofin.vqq_rpt_active_process_entrance drop if exists partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}');
insert into table tmp_innofin.vqq_rpt_active_process_entrance partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
	select uid,'我携' from tmp_innofin.apply_process_detail where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and ctrip = 1;
insert into table tmp_innofin.vqq_rpt_active_process_entrance partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
	select uid,'实名' from tmp_innofin.apply_process_detail where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and realname = 1;
insert into table tmp_innofin.vqq_rpt_active_process_entrance partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
	select uid,'钱包' from tmp_innofin.apply_process_detail where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and wallet = 1;
insert into table tmp_innofin.vqq_rpt_active_process_entrance partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
	select uid,'首页更多' from tmp_innofin.apply_process_detail where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and more_func = 1;
insert into table tmp_innofin.vqq_rpt_active_process_entrance partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
	select uid,'其他' from tmp_innofin.apply_process_detail where d = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and others_channel = 1;
-- 2)页面
use tmp_innofin;
-- tmp_innofin.apply_process_detail
-- 3)按钮
use tmp_innofin;
create table if not exists vqq_rpt_active_process_click_uv(
	bid string,
	uid string
) partitioned by (dt string);

insert overwrite table tmp_innofin.vqq_rpt_active_process_click_uv partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select distinct name as bid,env['UID'] as uid
from dw_mobdb.factmbmetriclog_sdk
where d = date_add(current_date,-1) and h between '0' and '23' and name between '101320' and '101328';


-- 4)后端埋点
use tmp_innofin;
create table if not exists user_action_info_index (
	uid	string,
	action_desc string
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.user_action_info_index partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select user_id as uid,action_desc from
(select user_id,action_desc,row_number() over (partition by user_id order by create_time desc) as no 
from ods_innofin.user_action_info 
where to_date(create_time) = '${zdt.addDay(-1).format("yyyy-MM-dd")}' and action_type = 'INDEX') a
where a.no = 1;

use tmp_innofin;
create table if not exists vqq_rpt_active_process_user_subprocess (
	uid string,
	creditProcess string,
	existPwd string,
	hasMobile string,
	preCreditStatus string,
	realName string	
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_user_subprocess partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select uid,
	get_json_object(action_desc,'$.creditProcess'),
	get_json_object(action_desc,'$.existPwd'),
	get_json_object(action_desc,'$.hasMobile'),
	get_json_object(action_desc,'$.preCreditStatus'),
	get_json_object(action_desc,'$.realName')
from tmp_innofin.user_action_info_index 
where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}';

-- 5)后端开通
-- left join (select uid from tmp_innofin.nqh_uid_apl where to_date(time_apl)='${zdt.addDay(-1).format("yyyy-MM-dd")}' ) d on lower(a.uid) = lower(d.uid)
-- left join (select user_id as uid from ods_innofin.user_contract where to_date(update_time) = '${zdt.addDay(-1).format("yyyy-MM-dd")}') e on lower(a.uid) = lower(e.uid) ;


-- 6)总表
use tmp_innofin;
create table if not exists vqq_rpt_active_process_detail (
	uid string,

	enter_ctrip int,
	enter_realname int,
	enter_wallet int,
	enter_more_func int,
	enter_others_channel int,
	
	page_apply	int,	
	page_cardguide	int,	
	page_card_number	int,	
	page_card	int,	
	page_setpass	int,	
	page_active	int,
	page_finish	int,	
	page_success	int,
	
	button_apply int,
	button_contract int,
	button_active int,

	db_active int,
	db_success int,	

	log_creditProcess int,
	log_existPwd int,
	log_hasMobile int,
	log_preCreditStatus int,
	log_realName int
	
) partitioned by (
	dt string
);

insert overwrite table tmp_innofin.vqq_rpt_active_process_detail partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select a.uid,
	a.ctrip,a.realname,a.wallet,a.more_func,a.others_channel,
	a.apply,a.cardguide,a.card_number,a.card,a.setpass,a.active,a.finish,a.success,
	if(c.uid is null,0,1),if(d.uid is null,0,1),if(h.uid is null,0,1),
	if(f.uid is null,0,1),if(e.uid is null,0,1),
	cast(nvl(g.creditProcess,-1) as int),if(existPwd = 'true',1,0),if(hasMobile = 'true',1,0),if(preCreditStatus = 'true',1,0),if(g.realName  = 'true',1,0)
from (select * from tmp_innofin.apply_process_detail where d='${zdt.addDay(-1).format("yyyy-MM-dd")}') a 
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bid in ('101320','101321')) c on lower(a.uid) = lower(c.uid) 
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bid in ('101324')) d on lower(a.uid) = lower(d.uid)
left join (select distinct uid from tmp_innofin.vqq_rpt_active_process_click_uv where dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and bid in ('101327')) h on lower(a.uid) = lower(h.uid)
left join (select uid from tmp_innofin.nqh_uid_apl where to_date(time_apl)='${zdt.addDay(-1).format("yyyy-MM-dd")}' ) e on lower(a.uid) = lower(e.uid)
left join (select distinct user_id as uid from ods_innofin.user_activate_req where to_date(request_time) = '${zdt.addDay(-1).format("yyyy-MM-dd")}') f on lower(a.uid) = lower(f.uid)
left join (select * from tmp_innofin.vqq_rpt_active_process_user_subprocess where dt = '${zdt.addDay(-1).format("yyyy-MM-dd")}') g on lower(a.uid) = lower(g.uid);


use tmp_innofin;
create table if not exists vqq_rpt_active_process_main (
	page_apply int,
	button_apply int,
	button_apply_rate decimal(5,4),
	
	button_contract int,
	button_contract_rate decimal(5,4),

	finish_db int,
	success_db int,
	success_rate decimal(5,4)	
) partitioned by (
	dt string,entrance string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_main partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}',entrance='我携')
	select sum(page_apply),sum(button_apply),sum(button_apply)/sum(page_apply),sum(button_contract),sum(button_contract)/sum(button_apply),sum(db_active),sum(db_success),sum(db_success)/sum(page_apply)
	from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and enter_ctrip = 1) ;
insert overwrite table tmp_innofin.vqq_rpt_active_process_main  partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}',entrance='实名')
	select sum(page_apply),sum(button_apply),sum(button_apply)/sum(page_apply),sum(button_contract),sum(button_contract)/sum(button_apply),sum(db_active),sum(db_success),sum(db_success)/sum(page_apply)
	from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and enter_realname = 1) ;
insert overwrite table tmp_innofin.vqq_rpt_active_process_main  partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}',entrance='钱包')
	select sum(page_apply),sum(button_apply),sum(button_apply)/sum(page_apply),sum(button_contract),sum(button_contract)/sum(button_apply),sum(db_active),sum(db_success),sum(db_success)/sum(page_apply)
	from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and enter_wallet = 1) ;
insert overwrite table tmp_innofin.vqq_rpt_active_process_main  partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}',entrance='首页更多')
	select sum(page_apply),sum(button_apply),sum(button_apply)/sum(page_apply),sum(button_contract),sum(button_contract)/sum(button_apply),sum(db_active),sum(db_success),sum(db_success)/sum(page_apply)
	from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and enter_more_func = 1) ;
insert overwrite table tmp_innofin.vqq_rpt_active_process_main  partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}',entrance='其他')
	select sum(page_apply),sum(button_apply),sum(button_apply)/sum(page_apply),sum(button_contract),sum(button_contract)/sum(button_apply),sum(db_active),sum(db_success),sum(db_success)/sum(page_apply)
	from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and enter_others_channel = 1) ;

use tmp_innofin;
create table if not exists vqq_rpt_active_process_card (
	page_apply int,
	page_cardguide int,
	page_card int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_card partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and log_creditProcess = 0 and log_realName = 1);

use tmp_innofin;
create table if not exists vqq_rpt_active_process_noname (
	page_apply int,
	page_cardguide int,
	page_card int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_noname partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select sum(page_apply),sum(page_cardguide),sum(page_cardguide*page_card_number*page_card),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and log_creditProcess = 0 and log_realName = 0);


use tmp_innofin;
create table if not exists vqq_rpt_active_process_nocard (
	page_apply int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_nocard partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select sum(page_apply),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and log_creditProcess = 1);


use tmp_innofin;
create table if not exists vqq_rpt_active_process_rapid (
	page_apply int,
	page_active int,
	
	button_active int,
	need_setpass int,
	success_setpass int,
	
	page_finish int,
	db_success int,
	
	finish_rate decimal(5,4),
	success_rate decimal(5,4)	
) partitioned by (
	dt string
);
insert overwrite table tmp_innofin.vqq_rpt_active_process_rapid partition (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}')
select sum(page_apply),sum(page_active),
	sum(button_active),
	sum(case when button_active = 1 and log_existPwd = 0 then 1 else 0 end),
	sum(page_finish)-sum(case when button_active = 1 and log_existPwd = 1 then 1 else 0 end),
	sum(page_finish),sum(db_success),sum(db_active)/sum(page_apply),sum(db_success)/sum(page_apply)
from tmp_innofin.vqq_rpt_active_process_detail where (dt='${zdt.addDay(-1).format("yyyy-MM-dd")}' and log_creditProcess = 2);

-- ART
-- main
select dt as `日期`,
entrance as `进入首页来源`,
	page_apply as `拿去花主动开通首页UV`,
	button_apply as `免费开通键点击UV`,
	100*button_apply_rate as `免费开通建点击率%`,
	
	button_contract as `合同确认键点击UV`,
	100*button_contract_rate as `合同确认键点击率%`,

	success_db as `开通成功数`,
	100*success_rate as	`来源开通成功率%`
from tmp_innofin.vqq_rpt_active_process_main where dt >= date_add(current_date,-7)

-- card 
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	page_cardguide as `支付绑卡引导页UV`,
	page_card as `支付绑卡页UV`,
	page_active as `核实信息页UV`,
	button_active as `核实信息页提交btn`,
	need_setpass 	as `需要设置支付密码`,
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	100*finish_rate as `流程完成率%`,
	100*success_rate as `流程开通成功率%`
from  tmp_innofin.vqq_rpt_active_process_card where dt >= date_add(current_date,-7)


--nocard 
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	page_active as `核实信息页UV`,
	
	button_active as `核实信息页提交btn`,
	need_setpass as `需要设置支付密码`,
	
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	100*finish_rate as `流程完成率%`,
	100*success_rate as `流程开通成功率%`	
from  tmp_innofin.vqq_rpt_active_process_nocard where dt >= date_add(current_date,-7)
-- rapid 
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	page_active as `核实信息页UV`,
	
	button_active as `核实信息页提交btn`,
	
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	100*finish_rate as `流程完成率%`,
	100*success_rate as `流程开通成功率%`	
from  tmp_innofin.vqq_rpt_active_process_rapid where dt >= date_add(current_date,-7)

-- noname
select dt as `日期`,
	page_apply as `拿去花主动开通首页UV`,
	page_cardguide as `支付绑卡引导页UV`,
	page_card as `支付绑卡页UV`,
	page_active as `核实信息页UV`,
	button_active as `核实信息页提交btn`,
	need_setpass 	as `需要设置支付密码`,
	page_finish as `开通完成页UV`,
	db_success as `开通成功数`,
	100*finish_rate as `流程完成率%`,
	100*success_rate as `流程开通成功率%`
from tmp_innofin.vqq_rpt_active_process_noname where dt >= date_add(current_date,-7)