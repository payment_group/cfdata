use tmp_innofin;
create table if not exists vqq_abtest_detail (
  dt string,
  uid string,
  abversion string,
  experiment_group string,
  first_page int,
  converted int
)partitioned by (
	experiment string
);
-- 酒店优惠券
insert overwrite table tmp_innofin.vqq_abtest_detail partition (experiment='171018_hod_nqhkt')
select a.d,a.uid,a.abversion,case when a.abversion in('B') then 'test' when a.abversion in('C','D') then 'control' else 'na' end, 
nvl(b.apply,0),if(c.uid is null,0,1)
from (select distinct d,uid,abversion from dw_abtestdb.AbVersionSplitInfo_app where d >= '2017-11-01' and abversion in ('D','B','C') and experiment = '171018_hod_nqhkt') a
left join tmp_innofin.apply_process_detail b on b.d >= '2017-11-01' and lower(a.uid) = lower(b.uid) and a.d = b.d 
left join (select uid,to_date(time_apl) as d from tmp_innofin.nqh_uid_apl) c on lower(a.uid) = lower(c.uid) and a.d = c.d
;

use tmp_innofin;
create table if not exists vqq_abtest_group (
  dt string,
  user_count int,
  first_page int,
  converted int
)partitioned by (
  experiment string,
  experiment_group string
);

use tmp_innofin;
insert overwrite table tmp_innofin.vqq_abtest_group partition (experiment='171018_hod_nqhkt',experiment_group='test')
select dt,count(1),sum(first_page),sum(converted)
from  tmp_innofin.vqq_abtest_detail where (experiment='171018_hod_nqhkt' and experiment_group='test' )
group by dt,experiment_group;

insert overwrite table tmp_innofin.vqq_abtest_group partition (experiment='171018_hod_nqhkt',experiment_group='control')
select dt,count(1),sum(first_page),sum(converted)
from  tmp_innofin.vqq_abtest_detail where (experiment='171018_hod_nqhkt' and experiment_group='control' )
group by dt,experiment_group;


-- 报表通用化
select a.dt as `日期`,
a.user_count as `分流人数（实验组）`,b.user_count as `分流人数（对照组）`,
a.first_page as `激活首页（实验组）`,b.first_page as `激活首页（对照组）`,
a.converted as `激活成功（实验组）`,b.converted as `激活成功（对照组）` 
from 
(select * from tmp_innofin.vqq_abtest_group where experiment='171018_hod_nqhkt' and experiment_group = 'test') a 
inner join  (select * from tmp_innofin.vqq_abtest_group where experiment='171018_hod_nqhkt' and experiment_group = 'control') b 
on a.dt = b.dt order by `日期` limit 1000

-- 参考指标
use tmp_innofin;
create table if not exists vqq_abtest_t(
	t_index int,
	score double
);
insert into vqq_abtest_t values
(1,6.313751515),
(2,2.91998558),
(3,2.353363435),
(4,2.131846786),
(5,2.015048373),
(6,1.943180281),
(7,1.894578605),
(8,1.859548038),
(9,1.833112933),
(10,1.812461123),
(11,1.795884819),
(12,1.782287556),
(13,1.770933396),
(14,1.761310136),
(15,1.753050356),
(16,1.745883676),
(17,1.739606726),
(18,1.734063607),
(19,1.729132812),
(20,1.724718243),
(21,1.720742903),
(22,1.717144374),
(23,1.713871528),
(24,1.71088208),
(25,1.708140761),
(26,1.70561792),
(27,1.703288446),
(28,1.701130934),
(29,1.699127027),
(30,1.697260887),
(31,1.695518783),
(32,1.693888748),
(33,1.692360309),
(34,1.690924255),
(35,1.689572458),
(36,1.688297714),
(37,1.68709362),
(38,1.68595446),
(39,1.684875122),
(40,1.683851013),
(41,1.682878002),
(42,1.681952357),
(43,1.681070703),
(44,1.680229977),
(45,1.679427393),
(46,1.678660414),
(47,1.677926722),
(48,1.677224196),
(49,1.676550893),
(50,1.675905025),
(51,1.67528495),
(52,1.674689154),
(53,1.674116237),
(54,1.673564906),
(55,1.673033965),
(56,1.672522303),
(57,1.672028888),
(58,1.671552762),
(59,1.671093032),
(60,1.670648865),
(61,1.670219484),
(62,1.669804163),
(63,1.669402222),
(64,1.669013025),
(65,1.668635976),
(66,1.668270514),
(67,1.667916114),
(68,1.667572281),
(69,1.667238549),
(70,1.666914479),
(71,1.666599658),
(72,1.666293696),
(73,1.665996224),
(74,1.665706893),
(75,1.665425373),
(76,1.665151353),
(77,1.664884537),
(78,1.664624645),
(79,1.664371409),
(80,1.664124579),
(81,1.663883913),
(82,1.663649184),
(83,1.663420175),
(84,1.663196679),
(85,1.6629785),
(86,1.662765449),
(87,1.662557349),
(88,1.662354029),
(89,1.662155326),
(90,1.661961084),
(91,1.661771155),
(92,1.661585397),
(93,1.661403674),
(94,1.661225855),
(95,1.661051817),
(96,1.66088144),
(97,1.66071461),
(98,1.660551217),
(99,1.660391156),
(100,1.660234326),
(101,1.66008063),
(102,1.659929976),
(103,1.659782273),
(104,1.659637437),
(105,1.659495383),
(106,1.659356034),
(107,1.659219312),
(108,1.659085144),
(109,1.658953458),
(110,1.658824187),
(111,1.658697265),
(112,1.658572629),
(113,1.658450216),
(114,1.658329969),
(115,1.65821183),
(116,1.658095744),
(117,1.657981659),
(118,1.657869522),
(119,1.657759285),
(120,1.657650899),
(121,1.657544319),
(122,1.657439499),
(123,1.657336397),
(124,1.65723497),
(125,1.657135178),
(126,1.657036982),
(127,1.656940344),
(128,1.656845226),
(129,1.656751594),
(130,1.656659413),
(131,1.656568649),
(132,1.65647927),
(133,1.656391244),
(134,1.656304542),
(135,1.656219133),
(136,1.656134988),
(137,1.65605208),
(138,1.655970382),
(139,1.655889868),
(140,1.655810511),
(141,1.655732287),
(142,1.655655173),
(143,1.655579143),
(144,1.655504177),
(145,1.655430251),
(146,1.655357345),
(147,1.655285437),
(148,1.655214506),
(149,1.655144534),
(150,1.6550755),
(151,1.655007387),
(152,1.654940175),
(153,1.654873847),
(154,1.654808385),
(155,1.654743774),
(156,1.654679996),
(157,1.654617035),
(158,1.654554875),
(159,1.654493503),
(160,1.654432901),
(161,1.654373057),
(162,1.654313957),
(163,1.654255585),
(164,1.654197929),
(165,1.654140976),
(166,1.654084713),
(167,1.654029128),
(168,1.653974208),
(169,1.653919942),
(170,1.653866317),
(171,1.653813324),
(172,1.653760949),
(173,1.653709184),
(174,1.653658017),
(175,1.653607437),
(176,1.653557435),
(177,1.653508002),
(178,1.653459126);

-- 显著性
use tmp_innofin;
create table if not exists vqq_abtest_check (
  value double	
)partitioned by (
  experiment string,
  key string
);

insert overwrite table tmp_innofin.vqq_abtest_check partition (experiment='171018_hod_nqhkt',key='n')
select count(1) 
from  tmp_innofin.vqq_abtest_group where (experiment='171018_hod_nqhkt' and experiment_group='test');

insert overwrite table tmp_innofin.vqq_abtest_check partition (experiment='171018_hod_nqhkt',key='avg_test')
select avg(converted/user_count) 
from  tmp_innofin.vqq_abtest_group where (experiment='171018_hod_nqhkt' and experiment_group='test');

insert overwrite table tmp_innofin.vqq_abtest_check partition (experiment='171018_hod_nqhkt',key='avg_control')
select avg(converted/user_count) 
from  tmp_innofin.vqq_abtest_group where (experiment='171018_hod_nqhkt' and experiment_group='control');

insert overwrite table tmp_innofin.vqq_abtest_check partition (experiment='171018_hod_nqhkt',key='s_test_2')
select sum(pow(a.rate - b.value,2))
from (select experiment,converted/user_count as rate from tmp_innofin.vqq_abtest_group where experiment='171018_hod_nqhkt' and experiment_group='test') a 
inner join (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='avg_test') b on a.experiment = b.experiment;

insert overwrite table tmp_innofin.vqq_abtest_check partition (experiment='171018_hod_nqhkt',key='s_control_2')
select sum(pow(a.rate - b.value,2)) 
from (select experiment,converted/user_count as rate from tmp_innofin.vqq_abtest_group where experiment='171018_hod_nqhkt' and experiment_group='control') a 
inner join (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='avg_control') b on a.experiment = b.experiment;

insert overwrite table tmp_innofin.vqq_abtest_check partition (experiment='171018_hod_nqhkt',key='s')
select sqrt((a.value + b.value)/(c.value*2-2)) 
from (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='s_test_2') a 
inner join (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='s_control_2') b on a.experiment = b.experiment
inner join (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='n') c on a.experiment = c.experiment;

insert overwrite table tmp_innofin.vqq_abtest_check partition (experiment='171018_hod_nqhkt',key='t')
select (a.value - b.value)/(c.value*sqrt(2/d.value))
from (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='avg_test') a 
inner join (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='avg_control') b on a.experiment = b.experiment
inner join (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='s') c on a.experiment = c.experiment
inner join (select experiment,value from tmp_innofin.vqq_abtest_check where experiment='171018_hod_nqhkt' and key='n') d on a.experiment = d.experiment;

-- 报表
use tmp_innofin;
create table if not exists vqq_abtest_checkresult(
	t double,
	score double,
	result string		
)partitioned by (
	experiment string
);
insert overwrite table vqq_abtest_checkresult partition (experiment='171018_hod_nqhkt') 
select abs(a.value),c.score,if(abs(a.value) > score,'显著','不显著') from
(select experiment,value from tmp_innofin.vqq_abtest_check where (key='t')) a
inner join (select experiment,cast(value*2 - 2 as int) as index_t from tmp_innofin.vqq_abtest_check where (key='t')) b on a.experiment = b.experiment
inner join tmp_innofin.vqq_abtest_t c on b.index_t = c.t_index;
-- 报表
select experiment as `试验号`, t as `|t|`,score as `参照值`,result as `显著性` from tmp_innofin.vqq_abtest_checkresult where experiment <> '';
