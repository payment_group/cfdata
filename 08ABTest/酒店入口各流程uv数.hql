use tmp_innofin;
drop table if exists vqq_xwt_htl_ab_1;
create table if not exists vqq_xwt_FINANCE_4898_1(
	dt string,
	uid string,
	
	page_apply int,
	page_cardguide int,
	page_addcard int,
	page_inputguide int,
	page_card int,
	page_confirm int,
	page_text int,
	page_finish int,
	page_password int
);

insert overwrite table tmp_innofin.vqq_xwt_FINANCE_4898_1
select a.*
from
(select
	d,
	uid,
	max(case when pagecode in ('10320607163','10320670431') then 1 else 0 end) as page_apply,
	max(case when pagecode in ('10320643008') then 1 else 0 end) as page_cardguide,
	max(case when pagecode in ('10320643028') then 1 else 0 end) as page_addcard,
	max(case when pagecode in ('10320643010') then 1 else 0 end) as page_inputguide,
	max(case when pagecode in ('10320642998') then 1 else 0 end) as page_card,
	max(case when pagecode in ('10320657064') then 1 else 0 end) as page_confirm,
	max(case when pagecode in ('10320668195') then 1 else 0 end) as page_text,
	max(case when pagecode in ('10320607172','10320607173','10320607171') then 1 else 0 end) as page_finish,
	max(case when pagecode in ('10320668081') then 1 else 0 end) as page_password
from dw_mobdb.factmbpageview 
where d >= '2017-11-28'
group by d,uid) a
where a.page_apply=1;


use tmp_innofin;
drop table if exists vqq_xwt_FINANCE_4898_ab;
create table vqq_xwt_FINANCE_4898_ab as 
select distinct d as dt,uid,abversion from dw_abtestdb.AbVersionSplitInfo_app where experiment='171018_hod_nqhkt' and d>='2017-11-28';

use tmp_innofin;
drop table if exists vqq_xwt_FINANCE_4898_3;
create table if not exists vqq_xwt_FINANCE_4898_3(
	dt string,
	uid string,
	abversion string,
	
	page_apply int,
	sub_process string,
	
	db_success int
);


use tmp_innofin;
insert overwrite table vqq_xwt_FINANCE_4898_3
select a.dt,a.uid,a.abversion,
	nvl(page_apply,0),
	case when (creditProcess = 0 and realName = 'false') then 'noname' 
		when (creditProcess = 1) then 'nocard'
		when creditProcess = 2 then 'rapid'
		when (creditProcess = 0 and realName = 'true') then 'card' else 'na' end,
	if(e.uid is null,0,1) as db_success
from vqq_xwt_FINANCE_4898_ab a 
inner join vqq_xwt_FINANCE_4898_1 b on lower(a.uid) = lower(b.uid) and a.dt = b.dt
left join (select * from vqq_rpt_active_process_user_subprocess where dt>='2017-11-28') dd on lower(a.uid) = lower(dd.uid) and a.dt = dd.dt
left join (select user_id as uid, to_date(ious_begin_time) as dt from ods_innofin.user_contract where to_date(ious_begin_time)>='2017-11-28' and contract_status=1 and req_src <> 'C_H5_USER' ) e on lower(a.uid) = lower(e.uid) and a.dt = e.dt
left join (select user_id as uid, to_date(create_time) as dt from ods_innofin.user_contract where to_date(create_time)>='2017-11-28' and contract_status in ('0','2','8','9') and req_src <> 'C_H5_USER' and req_src <> 'PRE_CREDIT_TASK') f on lower(a.uid) = lower(f.uid) and a.dt = f.dt;

--

select dt,abversion,sub_process,sum(page_apply),sum(db_success) from 
tmp_innofin.vqq_xwt_FINANCE_4898_3 group by dt,abversion,sub_process;
