use tmp_innofin;
drop table if exists tmp_innofin.vqq_temp_cr;
create table vqq_temp_cr as 
select distinct d,uid,pagecode from  dw_mobdb.factmbpageview where d between '2017-10-15' and '2017-10-31' 
and pagecode in('train_order','train6_order','train_rob_order');
tmp_innofin.vqq_temp_cr;

use tmp_innofin;
drop table if exists tmp_innofin.vqq_temp_cr_flag;
create table vqq_temp_cr_flag as 
select d,uid,pagecode,b.uid as active_flag,c.uid as can_flag
from  tmp_innofin.vqq_temp_cr a
left join (select user_id as uid from ods_innofin.user_contract where contract_status = 1) b on lower(a.uid) = lower(b.uid)
left join tmp_innofin.cfbdb_vqq_user_prefix_model c on lower(a.uid) = lower(c.uid);

select d,pagecode,count(uid),count(active_flag),count(can_flag) from tmp_innofin.vqq_temp_cr_flag group by  d,pagecode;


use tmp_innofin;
drop table if exists vqq_temp_cr_uv;
create table vqq_temp_cr_uv(
	d string,
	uid string,
	pagecode string,
	bu string
) partitioned by (dt string);

insert overwrite table tmp_innofin.vqq_temp_cr_uv partition(dt='2017-11-10')
select d,uid,pagecode,'train' from  tmp_innofin.vqq_temp_cr;