use tmp_innofin;
drop table tmp_innofin.issues;
create table tmp_innofin.issues(
	id int 			 comment   '#',
	project string 	 comment   '项目',
	trace string 	 comment   '跟踪',
	status string 	 comment   '状态',
	issue_status string 	 comment   '事件状态',
	priority string 	 comment   '优先级',
	author string 	 comment   '作者',
	assignee string 	 comment   '指派给',
	start_date string 	 comment   '开始日期',
	plan_end_date string 	 comment   '计划完成日期',
	update_time string 	 comment   '更新于',
	parent_id int 	 comment   '父任务',
	finish_percent int 	 comment   '% 完成',
	create_time string 	 comment   '创建于',
	end_date  string comment   '结束日期')
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = "\,",
   "quoteChar"     = "\""
)
STORED AS TEXTFILE
tblproperties("skip.header.line.count"="1");
-- 历史问题
select * from  tmp_innofin.issues where create_time < '${zdt.addDay(-7).format("yyyy-MM-dd")}'; 
-- 上周完成
select * from  tmp_innofin.issues where create_time < '${zdt.addDay(-7).format("yyyy-MM-dd")}' 
	and update_time  between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	and (status in ('已关闭','反馈','已解决','已拒绝') or finish_percent = 100);  
-- 未完成
select * from  tmp_innofin.issues where create_time < '${zdt.addDay(-7).format("yyyy-MM-dd")}' 
	and status not in ('已关闭','反馈','已解决','已拒绝') and finish_percent < 100;  
-- delay
select * from  tmp_innofin.issues where create_time < '${zdt.addDay(-7).format("yyyy-MM-dd")}' 
	and status not in ('已关闭','反馈','已解决','已拒绝') and finish_percent < 100 and trim(plan_end_date) <> '' and plan_end_date <= '${zdt.addDay(-1).format("yyyy-MM-dd")}';  
-- 上周新增问题
select * from  tmp_innofin.issues where create_time between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}'; 

-- 上周完成
select * from  tmp_innofin.issues where create_time between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	and update_time  between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	and (status in ('已关闭','反馈','已解决','已拒绝') or finish_percent = 100);  
-- 未完成
select * from  tmp_innofin.issues where create_time between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}' 
	and status not in ('已关闭','反馈','已解决','已拒绝') and finish_percent < 100;  
-- delay
select * from  tmp_innofin.issues where create_time between '${zdt.addDay(-7).format("yyyy-MM-dd")}' and '${zdt.addDay(-1).format("yyyy-MM-dd")}'
	and status not in ('已关闭','反馈','已解决','已拒绝') and finish_percent < 100 and trim(plan_end_date) <> '' and plan_end_date <= '${zdt.addDay(-1).format("yyyy-MM-dd")}';  
